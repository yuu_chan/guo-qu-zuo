ClassName(CPlayer)   		Pos(-30.0, 0.42, -17.5) Angle(0.0, 90.0, 0.0)   ItemId(0)     Filter(2) 


ClassName(CStage1)   		Pos(0.0, 0.0, 0.0)          ItemId(1)     Filter(2)

ClassName(CSkyMesh)         Pos(0, 0, 0)                ItemId(17)    Filter(1)


ClassName(CBox)  		Pos(1.9, 2.0, -22.5)                ItemId(3)     Filter(2) Weight(0.01)
ClassName(CBox)  		Pos(1.9, 2.0, -22.5)                ItemId(3)     Filter(2) Weight(0.01)
ClassName(CBox)  		Pos(1.9, 2.0, -22.5)                ItemId(3)     Filter(2) Weight(0.01)

ClassName(CBox)  		Pos(-9.649999,4.350000,1.300000)   ItemId(3)     Filter(2) Weight(0.01)
ClassName(CBox)  		Pos(-9.649999,6.250000,1.300000)  ItemId(3)     Filter(2) Weight(0.01)



ClassName(CMushRoom) 		Pos(-14.500001,14.899998,12.500005)                       ItemId(22)    Filter(2)
ClassName(CMushRooms)       Pos(-14.500001,14.899998,12.500005)                       ItemId(14)    Filter(1)

ClassName(CRock)           Pos(-2.400000,7.299996,8.500001)       ItemId(5)    ItemResilience(5) Filter(2)
ClassName(CRock)           Pos(-3.400000,7.299996,8.500001)       ItemId(5)    ItemResilience(5) Filter(2)
ClassName(CBranch)         Pos(-4.400000,7.299996,8.500001)       ItemId(29)    ItemResilience(5) Filter(2)
ClassName(CBranch)         Pos(-5.400000,7.299996,8.500001)       ItemId(29)    ItemResilience(5) Filter(2)

ClassName(CRock)           Pos(-12.400002,0.299997,-23.100008)    ItemId(5)    ItemResilience(5) Filter(2)
ClassName(CRock)           Pos(-12.400002,0.299997,-23.100008)    ItemId(5)    ItemResilience(5) Filter(2)
ClassName(CBranch)           Pos(-12.400002,0.299997,-23.100008)    ItemId(29)    ItemResilience(5) Filter(2)
ClassName(CBranch)           Pos(-12.400002,0.299997,-23.100008)    ItemId(29)    ItemResilience(5) Filter(2)


ClassName(CTreeMultiple) Pos(8.599999,  0,  35.600006)    Angle(0.0, 10.0, 0.0)                ItemId(16)       Filter(1) 
ClassName(CTreeMultiple) Pos(0.900000,  0,  21.600000)    Angle(0.000000,0.000000,0.000000)    ItemId(16)       Filter(1)
ClassName(CTreeMultiple) Pos(7.199999,  0,  23.100002)    Angle(0.000000,37.299999,0.000000)   ItemId(16)       Filter(1)
ClassName(CTreeMultiple) Pos(13.299998, 0,  20.100000)    Angle(0.000000,-33.600002,0.000000)  ItemId(16)       Filter(1)
ClassName(CTreeMultiple) Pos(-5.700002, 0,  19.400002)    Angle(0.000000,87.700020,0.000000)   ItemId(16)       Filter(1)
ClassName(CTreeMultiple) Pos(-11.699999,0,  25.100000)    Angle(0.000000,37.299999,0.000000)   ItemId(16)       Filter(1)
ClassName(CTreeMultiple) Pos(19.699999, 0,  23.900002)    Angle(0.000000,37.299999,0.000000)   ItemId(16)       Filter(1)
ClassName(CTreeMultiple) Pos(-17.600000,0,  19.600002)    Angle(0.000000,63.799995,0.000000)   ItemId(16)       Filter(1)
ClassName(CTreeMultiple) Pos(-21.100002,0,  25.700005)    Angle(0.000000,37.299999,0.000000)   ItemId(16)       Filter(1)
ClassName(CTreeMultiple) Pos(-3.699996, 0,  26.500002)    Angle(0.000000,134.599960,0.000000)  ItemId(16)       Filter(1)
ClassName(CTreeMultiple) Pos(3.000005,  0,  29.900002)    Angle(0.000000,70.499992,0.000000)   ItemId(16)       Filter(1)
ClassName(CTreeMultiple) Pos(14.200009, 0,  27.299999)    Angle(0.000000,37.299999,0.000000)   ItemId(16)       Filter(1)
ClassName(CTreeMultiple) Pos(-1.199995, 0,  35.099998)    Angle(0.000000,78.899979,0.000000)   ItemId(16)       Filter(1)
ClassName(CTreeMultiple) Pos(9.800005,  0,  29.900002)    Angle(0.000000,37.299999,0.000000)   ItemId(16)       Filter(1)
ClassName(CTreeMultiple) Pos(15.200004, 0,  13.200003)    Angle(0.000000,146.200012,0.000000)  ItemId(16)       Filter(1)
ClassName(CTreeMultiple) Pos(21.600002, 0,  17.000006)    Angle(0.000000,50.299988,0.000000)   ItemId(16)       Filter(1)
ClassName(CTreeMultiple) Pos(17.200006, 0,  6.800002)     Angle(0.000000,112.000000,0.000000)  ItemId(16)       Filter(1)
ClassName(CTreeMultiple) Pos(23.400009, 0,  10.099999)    Angle(0.000000,150.699982,0.000000)  ItemId(16)       Filter(1)
ClassName(CTreeMultiple) Pos(15.300010, 0,  0.400000)     Angle(0.000000,95.799995,0.000000)   ItemId(16)       Filter(1)
ClassName(CTreeMultiple) Pos(23.000010, 0,  0.400000)     Angle(0.000000,27.400015,0.000000)   ItemId(16)       Filter(1)
ClassName(CTreeMultiple) Pos(29.700006, 0,  4.800000)     Angle(0.000000,-14.299992,0.000000)  ItemId(16)       Filter(1)
ClassName(CTreeMultiple) Pos(18.900005, 0,  -6.799999)    Angle(0.000000,64.200020,0.000000)   ItemId(16)       Filter(1)
ClassName(CTreeMultiple) Pos(-8.599991, 0,  32.600010)    Angle(0.000000,70.000000,0.000000)   ItemId(16)       Filter(1)
ClassName(CTreeMultiple) Pos(-15.599990,0,  30.500006)    Angle(0.000000,113.100006,0.000000)  ItemId(16)       Filter(1)
ClassName(CTreeMultiple) Pos(4.899998,  0,  35.799999)    Angle(0.000000,-33.600002,0.000000)  ItemId(16)       Filter(1)
ClassName(CTreeMultiple) Pos(26.499996, 0,  23.100000)    Angle(0.000000,-33.600002,0.000000)  ItemId(16)       Filter(1)
ClassName(CTreeMultiple) Pos(27.800001, 0,  14.599999)    Angle(0.000000,-88.399986,0.000000)  ItemId(16)       Filter(1)
ClassName(CTreeMultiple) Pos(21.800007, 0,  30.699995)    Angle(0.000000,-44.299995,0.000000)  ItemId(16)       Filter(1)
ClassName(CTreeMultiple) Pos(16.899998, 0,  34.199989)    Angle(0.000000,-88.399986,0.000000)  ItemId(16)       Filter(1)
ClassName(CTreeMultiple) Pos(10.499997, 0,  38.399979)    Angle(0.000000,-44.499981,0.000000)  ItemId(16)       Filter(1)
ClassName(CTreeMultiple) Pos(-8.100003, 0,  39.499977)    Angle(0.000000,33.700020,0.000000)   ItemId(16)       Filter(1)
ClassName(CTreeMultiple) Pos(-15.200006,0,  36.699986)    Angle(0.000000,-20.999979,0.000000)  ItemId(16)       Filter(1)
ClassName(CTreeMultiple) Pos(-23.199999,0,  34.199982)    Angle(0.000000,79.000015,0.000000)   ItemId(16)       Filter(1)
ClassName(CTreeMultiple) Pos(9.000005,  0,  17.699982)    Angle(0.000000,25.400021,0.000000)   ItemId(16)       Filter(1)
ClassName(CTreeMultiple) Pos(0.100004,  0,  42.199974)    Angle(0.000000,109.100021,0.000000)  ItemId(16)       Filter(1)
ClassName(CTreeMultiple) Pos(27.800014, 0,  -6.000013)    Angle(0.000000,167.200058,0.000000)  ItemId(16)       Filter(1)
ClassName(CTreeMultiple) Pos(30.700008, 0,  -1.800013)    Angle(0.000000,250.800018,0.000000)  ItemId(16)       Filter(1)
ClassName(CTreeMultiple) Pos(15.700012, 0,  -13.100014)   Angle(0.000000,328.299988,0.000000)  ItemId(16)       Filter(1)
ClassName(CTreeMultiple) Pos(24.100008, 0,  -13.100014)   Angle(0.000000,264.300018,0.000000)  ItemId(16)       Filter(1)
ClassName(CTreeMultiple) Pos(19.700010, 0,  -19.300011)   Angle(0.000000,227.199997,0.000000)  ItemId(16)       Filter(1)
ClassName(CTreeMultiple) Pos(32.600014, 0,  11.399988)    Angle(0.000000,378.599915,0.000000)  ItemId(16)       Filter(1)
ClassName(CTreeMultiple) Pos(30.900015, 0,  -12.600009)   Angle(0.000000,482.499847,0.000000)  ItemId(16)       Filter(1)
ClassName(CTreeMultiple) Pos(36.000019, 0,  -6.200007)    Angle(0.000000,535.999939,0.000000)  ItemId(16)       Filter(1)
ClassName(CTreeMultiple) Pos(36.000019, 0,  3.499993)     Angle(0.000000,442.099792,0.000000)  ItemId(16)       Filter(1)



ClassName(CStage1Collision) Pos(0, 0, 0) ItemId(21) Filter(2)