ClassName(CPlayer) Pos(15.149996,0.400000,-15.399998) Angle(0.0, -90.0, 0.0) ItemId(0) Filter(2) Weight(0.01)

ClassName(CStage1) Pos(0, 0, 0)  ItemId(1) Filter(2)

ClassName(CSkyMesh) Pos(0, 0, 0) ItemId(17) Filter(1) 


ClassName(CSeesaw)           Pos(-15.350003,2.300001,-14.500003),    Angle(0.0, 0.0, 0.000000),    Scale(1.000000,1.000000,1.000000) ItemId(35) Filter(2)
ClassName(CSeesawFoundation) Pos(-15.350003,2.300001,-14.500003),      Angle(0.0, 0.0, 0.000000),    Scale(1.000000,1.000000,1.000000) ItemId(35) Filter(1)

ClassName(CLargeRock) Pos(-5.050000,10.000002,5.099998),   Angle(0.000000,0.000000,0.000000),     Scale(1.000000,1.000000,1.000000)  ItemId(36) Filter(2) Weight(1.0)

ClassName(CBridge) Pos(5.549998,5.349999,-1.550000),    Angle(0.000000,0.000000,0.000000),     Scale(1.000000,1.000000,1.000000)  ItemId(20) Filter(2)

ClassName(CBranch) Pos(0.000000,6.800000,0.000000),     Angle(0.000000,0.000000,0.000000),     Scale(1.000000,1.000000,1.000000)  ItemId(29) Filter(2)
ClassName(CBranch) Pos(1.050000,6.800000,0.000000),     Angle(0.000000,0.000000,0.000000),     Scale(1.000000,1.000000,1.000000)  ItemId(29) Filter(2)
ClassName(CBranch) Pos(-1.700000,6.800000,-2.850000),   Angle(0.000000,0.000000,0.000000),     Scale(1.000000,1.000000,1.000000)  ItemId(29) Filter(2)
ClassName(CBranch) Pos(-5.250000,6.800000,0.000000),    Angle(0.000000,0.000000,0.000000),     Scale(1.000000,1.000000,1.000000)  ItemId(29) Filter(2)
ClassName(CBranch) Pos(-0.849999,6.800000,-10.350000),  Angle(0.000000,0.000000,0.000000),     Scale(1.000000,1.000000,1.000000)  ItemId(29) Filter(2)
ClassName(CBranch) Pos(1.350001,6.800000,-10.350000),   Angle(0.000000,0.000000,0.000000),     Scale(1.000000,1.000000,1.000000)  ItemId(29) Filter(2)
ClassName(CBranch) Pos(3.900001,6.800000,-10.350000),   Angle(0.000000,0.000000,0.000000),     Scale(1.000000,1.000000,1.000000)  ItemId(29) Filter(2)


ClassName(CRock)   Pos(0.000000,0.000000,0.000000),     Angle(0.000000,0.000000,0.000000),     Scale(1.000000,1.000000,1.000000)  ItemId(5) Filter(2)
ClassName(CRock)   Pos(0.000000,0.050000,-8.049999),    Angle(0.000000,0.000000,0.000000),     Scale(1.000000,1.000000,1.000000)  ItemId(5) Filter(2)
ClassName(CRock)   Pos(2.800000,0.050000,-8.049999),    Angle(0.000000,0.000000,0.000000),     Scale(1.000000,1.000000,1.000000)  ItemId(5) Filter(2)
ClassName(CRock)   Pos(1.150000,0.050000,-4.700000),    Angle(0.000000,0.000000,0.000000),     Scale(1.000000,1.000000,1.000000)  ItemId(5) Filter(2)
ClassName(CRock)   Pos(7.399999,0.050000,-4.700000),    Angle(0.000000,0.000000,0.000000),     Scale(1.000000,1.000000,1.000000)  ItemId(5) Filter(2)
ClassName(CRock)   Pos(7.399999,0.050000,-4.700000),    Angle(0.000000,0.000000,0.000000),     Scale(1.000000,1.000000,1.000000)  ItemId(5) Filter(2)
ClassName(CRock)   Pos(7.399999,0.050000,-7.349998),    Angle(0.000000,0.000000,0.000000),     Scale(1.000000,1.000000,1.000000)  ItemId(5) Filter(2)
ClassName(CRock)   Pos(9.300001,0.050000,-0.549996),    Angle(0.000000,0.000000,0.000000),     Scale(1.000000,1.000000,1.000000)  ItemId(5) Filter(2)
ClassName(CRock)   Pos(11.350002,0.050000,-0.549996),   Angle(0.000000,0.000000,0.000000),     Scale(1.000000,1.000000,1.000000)  ItemId(5) Filter(2)
ClassName(CRock)   Pos(1.500002,5.899999,-1.749995),    Angle(0.000000,0.000000,0.000000),     Scale(1.000000,1.000000,1.000000)  ItemId(5) Filter(2)
ClassName(CRock)   Pos(1.500002,5.899999,-1.749995),    Angle(0.000000,0.000000,0.000000),     Scale(1.000000,1.000000,1.000000)  ItemId(5) Filter(2)


ClassName(CAppleTree)  Pos(16.150002,9.700005,7.650002) Angle(0, -90.0, 0)                  ItemId(23)  Filter(1)
ClassName(CApple)  Pos(16.300001,10.500002,4.900001) Angle(0.000000,0.000000,0.000000)     ItemId(6)  Filter(1)
ClassName(CApple)  Pos(17.699997,10.500002,4.900001) Angle(0.000000,0.000000,-99.750015)   ItemId(6)  Filter(1)
ClassName(CApple)  Pos(16.949999,10.500002,4.300001) Angle(105.150002,0.000000,-34.650005) ItemId(6)  Filter(1)


ClassName(CBox)     Pos(-14.500003,6.600000,-23.399996),    ItemId(3) Filter(2) Weight(0.02)
ClassName(CBox)     Pos(-16.500000,6.600000,-23.399996),    ItemId(3) Filter(2) Weight(0.02)
ClassName(CBox)     Pos(-18.500002,6.600000,-23.399996),    ItemId(3) Filter(2) Weight(0.02)
ClassName(CBox)     Pos(-17.550003,8.550002,-23.399996),    ItemId(3) Filter(2) Weight(0.02)
ClassName(CBox)     Pos(-15.600004,8.550002,-23.399996),    ItemId(3) Filter(2) Weight(0.02)
ClassName(CBox)     Pos(-16.500004,10.500003,-23.399996),   ItemId(3) Filter(2) Weight(0.02)



ClassName(CTreeMultiple) Pos(0.000000,2.800001,45.950001),Angle(0.000000,25.550001,0.000000),Scale(1.000000,1.000000,1.000000)      ItemId(16) Filter(1)
ClassName(CTreeMultiple) Pos(-14.350000,2.900001,45.950001),Angle(0.000000,49.950008,0.000000),Scale(1.000000,1.000000,1.000000)    ItemId(16) Filter(1)
ClassName(CTreeMultiple) Pos(-42.000000,3.800001,45.950001),Angle(0.000000,7.800006,0.000000),Scale(1.000000,1.000000,1.000000)     ItemId(16) Filter(1)
ClassName(CTreeMultiple) Pos(-28.849991,3.500002,39.400002),Angle(0.000000,21.700016,0.000000),Scale(1.000000,1.000000,1.000000)    ItemId(16) Filter(1)
ClassName(CTreeMultiple) Pos(11.000012,3.200001,48.549992),Angle(0.000000,-10.249982,0.000000),Scale(1.000000,1.000000,1.000000)    ItemId(16) Filter(1)
ClassName(CTreeMultiple) Pos(28.100014,2.700001,-45.700008),Angle(0.000000,-50.049980,0.000000),Scale(1.000000,1.000000,1.000000)   ItemId(16) Filter(1)
ClassName(CTreeMultiple) Pos(16.300013,1.850001,-42.250011),Angle(0.000000,-50.049980,0.000000),Scale(1.000000,1.000000,1.000000)   ItemId(16) Filter(1)
ClassName(CTreeMultiple) Pos(-21.749989,2.250001,-61.700005),Angle(0.000000,-90.499977,0.000000),Scale(1.000000,1.000000,1.000000)  ItemId(16) Filter(1)
ClassName(CTreeMultiple) Pos(37.450008,2.450001,-66.300026),Angle(0.000000,-147.349976,0.000000),Scale(1.000000,1.000000,1.000000)  ItemId(16) Filter(1)
ClassName(CTreeMultiple) Pos(-38.849987,2.900001,-60.550014),Angle(0.000000,-114.199982,0.000000),Scale(1.000000,1.000000,1.000000) ItemId(16) Filter(1)

ClassName(CTreeMultiple) Pos(36.250015,2.200002,-55.300011),Angle(0.000000,-147.949997,0.000000),Scale(1.000000,1.000000,1.000000)   ItemId(16) Filter(1)
ClassName(CTreeMultiple) Pos(69.800026,2.750001,20.899996),Angle(0.000000,-70.999969,0.000000),Scale(1.000000,1.000000,1.000000)     ItemId(16) Filter(1)
ClassName(CTreeMultiple) Pos(65.200035,2.900001,8.449996),Angle(0.000000,-206.699966,0.000000),Scale(1.000000,1.000000,1.000000)     ItemId(16) Filter(1)
ClassName(CTreeMultiple) Pos(69.800049,2.050001,-8.100004),Angle(0.000000,-326.999908,0.000000),Scale(1.000000,1.000000,1.000000)    ItemId(16) Filter(1)
ClassName(CTreeMultiple) Pos(58.800034,2.450001,-24.250006),Angle(0.000000,-186.549911,0.000000),Scale(1.000000,1.000000,1.000000)   ItemId(16) Filter(1)
ClassName(CTreeMultiple) Pos(70.400024,7.200002,-31.200005),Angle(0.000000,-251.049911,0.000000),Scale(1.000000,1.000000,1.000000)   ItemId(16) Filter(1)
ClassName(CTreeMultiple) Pos(58.800034,3.250000,-40.650009),Angle(0.000000,-295.299927,0.000000),Scale(1.000000,1.000000,1.000000)   ItemId(16) Filter(1)


ClassName(CStage1Collision) Pos(0, 0, 0) ItemId(21) Filter(2)

ClassName(CRiver) Pos(0, 0, 0) ItemId(7) Filter(1)