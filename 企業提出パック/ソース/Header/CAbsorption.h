/*--------------------------------------------------

class説明

--------------------------------------------------*/

#ifndef _CLASS_NAME_ABSORPTION_H_
#define _CLASS_NAME_ABSORPTION_H_


/*=============

Class宣言

=============*/
class CAbsorption : public CTextureBase
{
public:
	
	//----------------
	//	コンストラクタ
	//----------------
	CAbsorption();

	//--------------
	//	デストラクタ
	//--------------
	~CAbsorption();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update();

	//	描画
	void Draw(LPDIRECT3DDEVICE9 _lpD3DDevice);
	
	//	解放
	void Release();


private:


	//	メッシュ
	shared_ptr<CMeshObject> m_Mesh;
};

#endif  _CLASS_NAME_ABSORPTION_H_