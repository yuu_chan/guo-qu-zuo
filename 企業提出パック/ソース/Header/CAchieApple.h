/*------------------------------------------------------
	りんご実績解除クラス

------------------------------------------------------*/

#ifndef _CALSS_NAME_ACHIEVEMENT_APPLE_H_
#define _CALSS_NAME_ACHIEVEMENT_APPLE_H_


/*=============

Class宣言

=============*/
class CAchieApple : public CAchievementBase
{
public:

	//----------------
	//  コンストラクタ
	//----------------
	CAchieApple();

	//--------------
	//  デストラクタ
	//--------------
	~CAchieApple();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);


private:

	//	テクスチャの横幅サイズ
	static const float TEXTURE_WIDTH_SIZE;

	//	テクスチャの縦幅サイズ
	static const float TEXTURE_HEIGHT_SIZE;

};


#endif  _CALSS_NAME_ACHIEVEMENT_APPLE_H_