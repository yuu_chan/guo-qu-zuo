/*------------------------------------------------------

	実績解除基底クラス

	リストIDはCObjectBaseを使用

------------------------------------------------------*/

#ifndef _CALSS_NAME_ACHIEVEMENT_BASE_H_
#define _CALSS_NAME_ACHIEVEMENT_BASE_H_


/*==================

Class宣言

==================*/
class CAchievementBase
{
public:

	//----------------
	//  コンストラクタ
	//----------------
	CAchievementBase();

	//--------------
	//  デストラクタ
	//--------------
	~CAchievementBase();


	//	初期化
	virtual void Init(LPDIRECT3DDEVICE9 _lpD3DDevice) = 0;

	//	更新
	virtual void Update();

	//	描画
	virtual void Draw(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	開放
	virtual void Release();


	//----------
	//	セッター
	//----------

	//	解除用変数をセットする
	void SetEnableAchie(bool _Enable) { m_EnableAchievement = _Enable; }

	//	シェーダーマネージャのポインタをセット
	void SetShader(CShaderManager* _pShader) { m_pShader = _pShader; }

	//	ゲームワールドのポインタをセット
	void SetGameWorld(CGameWorld* _pGameWorld) { m_pGame = _pGameWorld; }

	//	IDをセット
	void SetId(int _Id) { m_Id = _Id; }

	//	ItemIDをセットする
	void SetItemId(int _ItemId) { m_ItemId = _ItemId; }


	//----------
	//	ゲッター
	//----------

	//	実績用変数を返す
	bool GetEnableAchie() { return m_EnableAchievement; }
	
	//	シェーダーマネージャのポインタをセット
	CShaderManager* SetShader() { return m_pShader; }

	//	ゲームワールドのポインタをセット
	CGameWorld* SetGameWorld() { return m_pGame; }

	//	IDを返す
	int GetId() { return m_Id; }

	//	ItemIDを返す
	int GetItemId() { return m_ItemId; }


protected:


	//	テクスチャ
	CTexture* m_pTexture;

	//	実績解除用変数
	bool	m_EnableAchievement;

	//	横幅
	float m_TextureWidthSize;

	//	縦幅
	float m_TextureHeightSize;

	//	オブジェクト破棄フラグ
	bool m_EnableDestruction;

	//	ステップ
	int m_Step;

	//	カウンタ
	int m_Cnt;

	//	初期位置
	CVector3 m_vInitPos;

	//	座標
	CVector3 m_vPos;

	//	拡大
	CVector3 m_vScale;

	//	ワールド行列
	CMatrix m_mWorld;

	//	LISTID
	int m_Id;

	//	指定するオブジェクトの名前
	int m_ItemId;


	//--------------------------
	//	別オブジェクトのポインタ
	//--------------------------

	//	シェーダーマネージャーのポインタ
	CShaderManager* m_pShader;

	//	ゲームワールドのポインタ
	CGameWorld* m_pGame;

};


#endif  _CALSS_NAME_ACHIEVEMENT_BASE_H_