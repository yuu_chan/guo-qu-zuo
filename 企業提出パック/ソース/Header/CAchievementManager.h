/*----------------------------------------------------------------

Class説明

----------------------------------------------------------------*/

#ifndef _CALSS_NAME_ACHIEVEMENT_MANAGER_H_
#define _CALSS_NAME_ACHIEVEMENT_MANAGER_H_


/*=============

Class宣言

=============*/
class CAchievementManager
{
public:

	//----------------
	//  コンストラクタ
	//----------------
	CAchievementManager();

	//--------------
	//  デストラクタ
	//--------------
	~CAchievementManager();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	描画
	void Draw(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	開放
	void Release();

	//	指定されたオブジェクトを返す
	CAchievementBase* GetItObject(int _Id);

	//	指定されたクラスをインスタンス化してリストに格納する
	template <class T>
	inline T* CreateObjectTask()
	{
		T* temAchie = MyNew T();
		PushAchieManager(temAchie);
		return temAchie;
	}

	//	シェーダーのポインターを自身の管理しているマネージャーに渡す
	void SetShaderManager(CShaderManager* _pShader);

	//	ゲームワールドのポインターを渡しておく
	void SetGameWorld(CGameWorld* _pGameWorld);


private:


	//	マネージャー
	list <CAchievementBase*> m_listAchieContainer;

	//	マネージャーリストに指定されたオブジェクトを格納
	void PushAchieManager(CAchievementBase* _Achie);
};


#endif  _CALSS_NAME_ACHIEVEMENT_MANAGER_H_