/*----------------------------------------------------------------

実績解除関係の全クラスの親ポインター

----------------------------------------------------------------*/


#ifndef _CLASS_NAME_ACHIEVEMENT_WORLD_H_
#define _CLASS_NAME_ACHIEVEMENT_WORLD_H_


/*===============

前方宣言

===============*/

//	実績解除マネージャー
class CAchievementManager;


/*===============

Class宣言

===============*/
class CAchievementWorld
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CAchievementWorld();

	//--------------
	//	デストラクタ
	//--------------
	~CAchievementWorld();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	描画
	void Draw(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	開放
	void Release();


	//	クラスをインスタンス化させるクラス
	template <class T>
	inline void CreateObject(LPDIRECT3DDEVICE9 _lpD3DDevice, int _ItemId)
	{
		T* pObj = m_pAchieBaseManager->CreateObjectTask<T>();

		pObj->Init(_lpD3DDevice);
		pObj->SetId(m_Cnt);
		pObj->SetItemId(_ItemId);

		//	参照カウンタを1度加算
		m_Cnt++;
	}

	//	実績マネージャのポインタをゲットする
	shared_ptr<CAchievementManager> GetAchieManager() { return m_pAchieBaseManager; }

	//	追加されたクラスの数を返す
	int GetAdd() { return m_Cnt; }

private:
	//	実績マネージャ変数
	shared_ptr<CAchievementManager> m_pAchieBaseManager;

	//	追加されたクラスの数をカウント
	int m_Cnt;

};

#endif  _CLASS_NAME_ACHIEVEMENT_WORLD_H_