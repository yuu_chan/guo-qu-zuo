/*----------------------------------------------------------------

空中に浮いている花
足場として使用　風によって、浮いている位置をかえたりする

----------------------------------------------------------------*/


#ifndef _CLASS_NAME_AIR_FLOWER_H_
#define _CLASS_NAME_AIR_FLOWER_H_


/*=============

Class宣言

=============*/

class CAirFlower : public CObjectBase
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CAirFlower();

	//--------------
	//	デストラクタ
	//--------------
	~CAirFlower();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	解放
	void Release();


private:

};


#endif  _CLASS_NAME_AIR_FLOWER_H_

