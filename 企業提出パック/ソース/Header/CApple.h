/*--------------------------------------------------

class説明

--------------------------------------------------*/

#ifndef _CLASS_NAME_APPLE_H_
#define _CLASS_NAME_APPLE_H_


/*=============

Class宣言

=============*/
class CApple : public CObjectBase
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CApple();

	//--------------
	//	デストラクタ
	//--------------
	~CApple();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	開放
	void Release();


private:


	shared_ptr<CBulletObj_Sphere> m_spRigidSphere;

};


#endif  _CLASS_NAME_APPLE_H_