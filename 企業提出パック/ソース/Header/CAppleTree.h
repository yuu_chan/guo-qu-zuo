/*----------------------------------------------------------------

Class説明

----------------------------------------------------------------*/

#ifndef _CLASS_NAME_APPLE_TREE_H_
#define _CLASS_NAME_APPLE_TREE_H_


/*=============

Class宣言

=============*/

class CAppleTree : public CObjectBase
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CAppleTree();

	//--------------
	//	デストラクタ
	//--------------
	~CAppleTree();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	解放
	void Release();


private:


	//	メッシュ剛体を登録
	shared_ptr<CBulletObj_Mesh> m_spRigidMesh;


};


#endif  _CLASS_NAME_APPLE_TREE_H_

