/*----------------------------------------------

ぼかし描画を行うシェーダークラス

----------------------------------------------*/

#ifndef _CLASS_NAME_BOKASHI_SHADER_H_
#define _CLASS_NAME_BOKASHI_SHADER_H_


/*=============

Class宣言

=============*/
class CBokashiShader : public CShaderBase
{
public:
	
	//----------------
	//	コンストラクタ
	//----------------
	CBokashiShader();

	//--------------
	//	デストラクタ
	//--------------
	~CBokashiShader();

	
	//	初期化
	void Init();

	//	開放
	void DrawMesh(shared_ptr<CMeshObject> _pMesh, int _Pass){}

	//	通常2D描画
	void Draw2D(CTexture* _pTexture, int _x, int _y, int _w, int h);

	//	3D描画
	void Draw3D(CTexture* _pTexture, float _x, float _y, float _w, float _h, float _tuCnt, float _tvCnt);

	//	X側にぼかす(伸ばす)
	void DrawXBokasi(CTexture* _pTexture, int _x = 0, int _y = 0, int _w = 0, int h = 0);

	//	Y側にぼかす(伸ばす)
	void DrawYBokasi(CTexture* _pTexture, int _x = 0, int _y = 0, int _w = 0, int h = 0);

	//	ぼかしたテクスチャを発生させる
	void GenerateBokasi(CTexture* _pTexture);

	//	開放
	void Release();


	//----------
	//	ゲッター
	//----------

	//	ぼかしたテクスチャーを返す
	CTexture GetTexBokashi(int _Num){ return m_texBokasi[_Num]; }


private:


	//	ぼかしたテクスチャを格納する 
	CTexture m_texBokasi[2];	//	0 : Xぼかし 1 : Yぼかし(最終的に描画される側)


};


#endif  _CLASS_NAME_BOKASHI_SHADER_H_