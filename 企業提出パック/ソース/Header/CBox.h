/*--------------------------------------------------

class説明

--------------------------------------------------*/

#ifndef _CLASS_NAME_WOOD_BOX_H_
#define _CLASS_NAME_WOOD_BOX_H_


/*=============

Class宣言

=============*/
class CBox : public CObjectBase
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CBox();

	//--------------
	//	デストラクタ
	//--------------
	~CBox();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	開放
	void Release();


private:


	//	物理剛体
	shared_ptr<CBulletObj_Box> m_spRigidBox;


};

#endif  _CLASS_NAME_WOOD_BOX_H_