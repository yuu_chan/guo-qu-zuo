/*----------------------------------------------------------------

木の枝

----------------------------------------------------------------*/

#ifndef _CLASS_NAME_BRANCH_H_
#define _CLASS_NAME_BRANCH_H_


/*=============

Class宣言

=============*/

class CBranch : public CObjectBase
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CBranch();

	//--------------
	//	デストラクタ
	//--------------
	~CBranch();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	解放
	void Release();


private:


	shared_ptr<CBulletObj_Capsule> m_spRigidSphere;


};


#endif  _CLASS_NAME_BRANCH_H_

