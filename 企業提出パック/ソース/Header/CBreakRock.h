/*----------------------------------------------------------------

ヒビの入った壁が砕けたときに
生成される壁の破片クラス

----------------------------------------------------------------*/


#ifndef _CLASS_NAME_BREAK_ROCK_H_
#define _CLASS_NAME_BREAK_ROCK_H_


/*=============

Class宣言

=============*/

class CBreakRock : public CObjectBase
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CBreakRock();

	//--------------
	//	デストラクタ
	//--------------
	~CBreakRock();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	解放
	void Release();


private:


	//	立方体剛体で登録する
	shared_ptr<CBulletObj_Box> m_spRigidBox;


};


#endif  _CLASS_NAME_BREAK_ROCK_H_

