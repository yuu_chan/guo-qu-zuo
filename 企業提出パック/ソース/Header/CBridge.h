/*----------------------------------------------------------------

橋
強い衝撃を与えると倒れて足場になる

----------------------------------------------------------------*/


#ifndef _CLASS_NAME_BRIDGE_H_
#define _CLASS_NAME_BRIDGE_H_


/*=============

Class宣言

=============*/

class CBridge : public CObjectBase
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CBridge();

	//--------------
	//	デストラクタ
	//--------------
	~CBridge();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	解放
	void Release();


private:


	//	立方体剛体を登録
	shared_ptr<CBulletObj_Mesh> m_spRigidMesh;

};


#endif  _CLASS_NAME_BRIDGE_H_

