#ifndef _CLASS_NAME_BULLET_PHYSICS_ENGINE_H_
#define _CLASS_NAME_BULLET_PHYSICS_ENGINE_H_

#include "btBulletDynamicsCommon.h"
#include "BulletCollision/CollisionShapes/btShapeHull.h"

class CBulletRigid_Base;

//==========================================================================
// Bullet Physics Engine ワールド用クラス
//==========================================================================
class BulletWorld{
public:
	// アクセサ
	btDynamicsWorld* GetWorld(){return m_dynamicsWorld;}		// ワールド

	BulletWorld()
	{
		m_collisionConfiguration = NULL;
		m_dispatcher = NULL;
		m_broadphase = NULL;
		m_solver = NULL;
		m_dynamicsWorld = NULL;
	}
	~BulletWorld(){
		Release();
	}

	
	// 初期化
	void Init();

	// 解放
	void Release();

	// シミュレーション進行
	int StepSimulation(btScalar timeStep,int MaxSubSteps=1, btScalar fixedTimeStep=btScalar(1.)/btScalar(60.))
	{
		return m_dynamicsWorld->stepSimulation( timeStep ,MaxSubSteps, fixedTimeStep);
	}


	// 衝突検知用コールバック通知用
	virtual bool ContactProcessedCallback(btManifoldPoint& cp, CBulletRigid_Base* body0, CBulletRigid_Base* body1){return true;}

	bool IsInit(){return (m_dynamicsWorld != NULL);}

private:
	btDefaultCollisionConfiguration*	m_collisionConfiguration;
	btCollisionDispatcher*				m_dispatcher;
	btBroadphaseInterface*				m_broadphase;
	btConstraintSolver*					m_solver;
	btDynamicsWorld*					m_dynamicsWorld;

protected:
	// 衝突検知用静的コールバック関数
	static bool s_ContactProcessedCallback(btManifoldPoint& cp, void* body0, void* body1);

private:
	// コピー禁止用
	BulletWorld(const BulletWorld& src){}
	void operator=(const BulletWorld& src){}
};

//==========================================================================
// オブジェクト基本クラス
//==========================================================================
class CBulletObj_Base
{
public:
	// クラス種類定数
	enum{
		// 剛体
		BO_BOX,			// 立方体
		BO_CAPSULE,		// カプセル
		BO_SPHERE,		// 球
		BO_COMPOUND,	// 複合体
		BO_MESH,		// 三角形集合

		// ジョイント
		JO_POINT,		// ボールジョイント
		JO_HINGE,		// ヒンジジョイント
		JO_6DOF,		// 汎用ジョイント
		JO_6DOFSPRING,	// 汎用スプリングジョイント
	};

public:
	CBulletObj_Base():
		m_Type(-1),
		m_World(NULL),
		m_lpUserPointer(NULL)
	{
	}
	virtual ~CBulletObj_Base(){
		m_World = NULL;
	}

	BulletWorld* GetWorld(){return m_World;}

	virtual void Release()=0;

	int	GetType(){return m_Type;}

	// ユーザポインタ
	void SetUserPointer(void* p){m_lpUserPointer = p;}
	void* GetUserPointer(){return m_lpUserPointer;}

protected:
	int				m_Type;
	BulletWorld*	m_World;		// 登録しているワールド
	void*			m_lpUserPointer;

};

//==========================================================================
// 剛体ベースクラス
//==========================================================================
class CBulletRigid_Base : public CBulletObj_Base
{
public:
	btRigidBody*	GetBody(){return m_Body;}

	// キネマティック
    void SetKinematic(bool kinematic)
    {
		if(kinematic) {
			m_Body->setCollisionFlags(m_Body->getCollisionFlags() | btCollisionObject::CF_KINEMATIC_OBJECT);
			m_Body->setActivationState(DISABLE_DEACTIVATION);
			m_Body->setMassProps(0, btVector3(0,0,0)); 
			m_Body->updateInertiaTensor();
		}
		else{
			m_Body->setCollisionFlags(m_Body->getCollisionFlags() & ~btCollisionObject::CF_KINEMATIC_OBJECT);
			m_Body->setActivationState(ACTIVE_TAG);
			m_Body->setMassProps(m_Mass, m_Inertia); 
			m_Body->updateInertiaTensor();
		}
    }

	// 質量
	void SetMass(float mass){
		m_Mass = mass;
		if(m_Body->getCollisionFlags() & btCollisionObject::CF_KINEMATIC_OBJECT) {
			m_Body->setMassProps(0, btVector3(0,0,0));
		}
		else{
			m_Body->setMassProps(m_Mass, m_Inertia); 
		}
		m_Body->updateInertiaTensor();
	}

	// 慣性テンソル
	void SetInertia(btVector3& inertia)
	{
		m_Inertia = inertia;
		if(m_Body->getCollisionFlags() & btCollisionObject::CF_KINEMATIC_OBJECT) {
			m_Body->setMassProps(0, btVector3(0,0,0));
		}
		else{
			m_Body->setMassProps(m_Mass, m_Inertia); 
		}
		m_Body->updateInertiaTensor();
	}
	// 反発力
	void SetRestitution(float r)
	{
		m_Body->setRestitution(r);
	}
	// 摩擦力
	void SetFriction(float f)
	{
		m_Body->setFriction(f);
	}
	// 移動減衰
	void SetLinearDamping(float d)
	{
		m_LinearDamping = d;
		m_Body->setDamping(m_LinearDamping, m_AngularDamping);
	}
	// 回転減衰
	void SetAngularDamping(float d)
	{
		m_AngularDamping = d;
		m_Body->setDamping(m_LinearDamping, m_AngularDamping);
	}
	// 移動力
	void SetLinearVelocity(const D3DXVECTOR3& v)
	{
		m_Body->setLinearVelocity(btVector3(v.x, v.y, v.z));
	}
	// 回転力
	void SetAngularVelocity(const D3DXVECTOR3& v)
	{
		m_Body->setAngularVelocity(btVector3(v.x, v.y, v.z));
	}

	// かかる力をクリアする
	void ClearForces()
	{
		m_Body->clearForces();
	}
	// 回転力を与える
	void ApplyTorque(const D3DXVECTOR3& t)
	{
		m_Body->applyTorque(btVector3(t.x, t.y, t.z));
	}
	// 力を与える(継続的な力。シェット噴射のような挙動)
	void ApplyForce(const D3DXVECTOR3& force)
	{
		m_Body->applyCentralForce(btVector3(force.x, force.y, force.z));
	}
	// 指定位置に力を与える(継続的な力。シェット噴射のような挙動)
	void ApplyForce(const D3DXVECTOR3& force, const D3DXVECTOR3& rel_pos )
	{
		m_Body->applyForce(btVector3(force.x, force.y, force.z), btVector3(rel_pos.x, rel_pos.y, rel_pos.z));
	}
	// 力を与える(瞬間的な力。バットで打った時のような挙動)
	void ApplyImpulse(const D3DXVECTOR3& impulse)
	{
		m_Body->applyCentralImpulse(btVector3(impulse.x, impulse.y, impulse.z));
	}
	// 指定座標に力を与える(瞬間的な力。バットで打った時のような挙動)
	void ApplyImpulse(const D3DXVECTOR3& impulse, const D3DXVECTOR3& rel_pos )
	{
		m_Body->applyImpulse(btVector3(impulse.x, impulse.y, impulse.z), btVector3(rel_pos.x, rel_pos.y, rel_pos.z));
	}
	// スリープしないObjへ設定する
	void SetNonSleep(){
		m_Body->setSleepingThresholds(0,0);
//		m_Body->setActivationState(DISABLE_DEACTIVATION);
	}

	// 行列設定
	void SetMatrix(const D3DXMATRIX& mat){
		btTransform t;
		t.setFromOpenGLMatrix(&mat._11);
		m_Body->getMotionState()->setWorldTransform(t);
		m_Body->setCenterOfMassTransform(t);
	}
	// 行列取得
	void GetMatrix(D3DXMATRIX& mOut){
		// 行列取得
		btTransform t = m_Body->getWorldTransform();
		t.getOpenGLMatrix(&mOut._11);
	}

	// モーションステート作成
	template<class T>
	T* CreateMotionState(){
		if(m_Body->getMotionState())delete m_Body->getMotionState();
		T* ms = new T();
		m_Body->setMotionState(ms);
		return ms;
	}
	void SetMotionState(btMotionState* ms){
		if(m_Body->getMotionState())delete m_Body->getMotionState();
		m_Body->setMotionState(ms);
	}

	// ワールドから剛体を解除する
	bool RemoveFromWorld(){
		if(m_Body->isInWorld()){
			if(m_World->GetWorld()){
				m_World->GetWorld()->removeCollisionObject(m_Body);
			}
			return true;
		}
		return false;
	}
	// ワールドへ剛体を追加する
	bool AddToWorld(short group = 1,short mask = -1){
		// ワールドに剛体追加
		if( !m_Body->isInWorld() ){
			m_World->GetWorld()->addRigidBody(m_Body,group,mask);
			return true;
		}
		return false;
	}

	int GetRegist(){ return m_RegistId; }

	void SetRegist(int _RegistId){ m_RegistId = _RegistId; }

	int GetObjectListId(){ return m_ObjectListId; }

	void SetObjectListId(int _ListId){ m_ObjectListId = _ListId; }

protected:
	btVector3		m_Inertia;			// 慣性テンソル
	btRigidBody*	m_Body;				// 剛体
	float			m_Mass;				// 質量
	float			m_LinearDamping;	// 移動減衰
	float			m_AngularDamping;	// 回転減衰


	int				m_ObjectListId;		//	リストID
	int				m_RegistId;			//	登録ID

protected:
	CBulletRigid_Base() : m_Body(NULL),m_Inertia(0,0,0),m_Mass(0),m_LinearDamping(0),m_AngularDamping(0)
	{
	}

};

//==========================================================================
// 立方体剛体クラス
//==========================================================================
class CBulletObj_Box : public CBulletRigid_Base
{
public:
	btBoxShape*		GetShape(){return m_Shape;}

	CBulletObj_Box() :
		m_Shape(NULL)
	{
		m_Type = BO_BOX;
	}

	~CBulletObj_Box(){
		Release();
	}

	// 剛体を作成
	void Create(BulletWorld* world,D3DXMATRIX& startMat,D3DXVECTOR3& HarfSize,float mass,short group = 1,short mask = -1)
	{
		Release();

		m_World = world;

		// 形状作成
		btVector3 shapeHarfSize(HarfSize.x,HarfSize.y,HarfSize.z);
		m_Shape = new btBoxShape(shapeHarfSize);

		// 行列
		btTransform startTransform;
		startTransform.setFromOpenGLMatrix(&startMat._11);

		// 質量が0以外なら、動的物体。0なら静的物体
		if(mass != 0.0f){
			m_Shape->calculateLocalInertia(mass,m_Inertia);
		}

		btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);
		btRigidBody::btRigidBodyConstructionInfo rbInfo(mass,myMotionState,m_Shape,m_Inertia);
		rbInfo.m_additionalDamping = true;
		m_Body = new btRigidBody(rbInfo);
		m_Body->setUserPointer(this);		// ユーザーポインタに自分のアドレスを記憶させておく

		m_LinearDamping = m_Body->getLinearDamping();
		m_AngularDamping = m_Body->getAngularDamping();


		// ワールドに剛体追加
		m_World->GetWorld()->addRigidBody(m_Body,group,mask);
	}
	void Create(BulletWorld* world,D3DXVECTOR3 &pos,D3DXVECTOR3 &shapeHarfSize,float mass,short group = 1,short mask = -1)
	{
		CMatrix m;
		m.CreateMove(pos.x,pos.y,pos.z);
		Create(world,m,shapeHarfSize,mass,group,mask);
	}

	virtual void Release()
	{
		// 形状削除
		SAFE_DELETE(m_Shape);

		// 剛体削除
		if(m_Body){
			if(m_Body->getMotionState())delete m_Body->getMotionState();
			while(m_Body->getNumConstraintRefs() > 0){
				m_Body->removeConstraintRef(m_Body->getConstraintRef(0));
			}

			if(m_World && m_World->GetWorld()){
				m_World->GetWorld()->removeCollisionObject(m_Body);
			}
			delete m_Body;
			m_Body = NULL;
		}
		m_World = NULL;
	}

protected:
	btBoxShape*		m_Shape;		// 形状

private:
	// コピー禁止用
	CBulletObj_Box(const CBulletObj_Box& src){}
	void operator=(const CBulletObj_Box& src){}
};

//==========================================================================
// カプセル剛体クラス
//==========================================================================
class CBulletObj_Capsule : public CBulletRigid_Base
{
public:
	CBulletObj_Capsule() : m_Shape(NULL)
	{
		m_Type = BO_CAPSULE;
	}
	~CBulletObj_Capsule(){
		Release();
	}

	btCapsuleShape*	getShape(){return m_Shape;}

	void Create(BulletWorld* world,D3DXMATRIX& startMat,float radius,float height,float mass,short group = 1,short mask = -1)
	{
		Release();

		m_World = world;

		// 形状作成
		m_Shape = new btCapsuleShape(radius,height);

		// 行列
		btTransform startTransform;
		startTransform.setFromOpenGLMatrix(&startMat._11);

		// 質量が0以外なら、動的物体。0なら静的物体
		if(mass != 0.0f){
			m_Shape->calculateLocalInertia(mass,m_Inertia);
		}

		btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);
		btRigidBody::btRigidBodyConstructionInfo rbInfo(mass,myMotionState,m_Shape,m_Inertia);
		rbInfo.m_additionalDamping = true;
		m_Body = new btRigidBody(rbInfo);
		m_Body->setUserPointer(this);

		m_LinearDamping = m_Body->getLinearDamping();
		m_AngularDamping = m_Body->getAngularDamping();

		// ワールドに剛体追加
		m_World->GetWorld()->addRigidBody(m_Body,group,mask);
	}


	virtual void Release()
	{
		// 形状削除
		SAFE_DELETE(m_Shape);

		// 剛体削除
		if(m_Body){
			if(m_Body->getMotionState())delete m_Body->getMotionState();
			while(m_Body->getNumConstraintRefs() > 0){
				m_Body->removeConstraintRef(m_Body->getConstraintRef(0));
			}
			if(m_World && m_World->GetWorld()){
				m_World->GetWorld()->removeCollisionObject(m_Body);
			}
			delete m_Body;
			m_Body = NULL;
		}
		m_World = NULL;
	}

protected:
	btCapsuleShape*	m_Shape;		// 形状

private:
	// コピー禁止用
	CBulletObj_Capsule(const CBulletObj_Capsule& src){}
	void operator=(const CBulletObj_Capsule& src){}
};

//==========================================================================
// 球剛体クラス
//==========================================================================
class CBulletObj_Sphere : public CBulletRigid_Base
{
public:
	btSphereShape*		getShape(){return m_Shape;}

	CBulletObj_Sphere() :
		m_Shape(NULL)
	{
		m_Type = BO_SPHERE;
	}

	~CBulletObj_Sphere(){
		Release();
	}

	// BOXの剛体を作成
	void Create(BulletWorld* world,D3DXMATRIX& startMat,float radius,float mass,short group = 1,short mask = -1)
	{
		Release();

		m_World = world;

		// 形状作成
		m_Shape = new btSphereShape(radius);

		// 行列
		btTransform startTransform;
		startTransform.setFromOpenGLMatrix(&startMat._11);

		// 質量が0以外なら、動的物体。0なら静的物体
		if(mass != 0.0f){
			m_Shape->calculateLocalInertia(mass,m_Inertia);
		}

		btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);
		btRigidBody::btRigidBodyConstructionInfo rbInfo(mass,myMotionState,m_Shape,m_Inertia);
		rbInfo.m_additionalDamping = true;
		m_Body = new btRigidBody(rbInfo);
		m_Body->setUserPointer(this);

		m_LinearDamping = m_Body->getLinearDamping();
		m_AngularDamping = m_Body->getAngularDamping();

		// ワールドに剛体追加
		m_World->GetWorld()->addRigidBody(m_Body,group,mask);
	}
	void Create(BulletWorld* world,D3DXVECTOR3& pos,btScalar radius,float mass,short group = 1,short mask = -1)
	{
		CMatrix m;
		m.CreateMove(pos.x,pos.y,pos.z);
		Create(world,m,radius,mass,group,mask);
	}

	virtual void Release()
	{
		// 形状削除
		SAFE_DELETE(m_Shape);

		// 剛体削除
		if(m_Body){
			if(m_Body->getMotionState())delete m_Body->getMotionState();
			while(m_Body->getNumConstraintRefs() > 0){
				m_Body->removeConstraintRef(m_Body->getConstraintRef(0));
			}
			if(m_World && m_World->GetWorld()){
				m_World->GetWorld()->removeCollisionObject(m_Body);
			}
			delete m_Body;
			m_Body = NULL;
		}
		m_World = NULL;
	}

protected:
	btSphereShape*	m_Shape;		// 形状

private:
	// コピー禁止用
	CBulletObj_Sphere(const CBulletObj_Sphere& src){}
	void operator=(const CBulletObj_Sphere& src){}
};

//==========================================================================
// 複合剛体クラス
//==========================================================================
class CBulletObj_Compound : public CBulletRigid_Base
{
public:
	btCompoundShape*				GetShape(){return m_Shape;}
	std::vector<btCollisionShape*>&	GetShapeList(){return m_ShapeList;}

	CBulletObj_Compound() :
		m_Shape(NULL)
	{
		m_Type = BO_COMPOUND;
	}

	~CBulletObj_Compound(){
		Release();
	}

	// 剛体を作成
	void Create(BulletWorld* world, D3DXMATRIX& startMat, btScalar mass,short group = 1,short mask = -1)
	{
		Release();

		m_World = world;

		// 形状作成
		m_Shape = new btCompoundShape();

		// 行列
		btTransform startTransform;
		startTransform.setFromOpenGLMatrix(&startMat._11);

		// 質量が0以外なら、動的物体。0なら静的物体
		if(mass != 0.0f){
			m_Shape->calculateLocalInertia(mass,m_Inertia);
		}

		btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);
		btRigidBody::btRigidBodyConstructionInfo rbInfo(mass,myMotionState,m_Shape,m_Inertia);
		rbInfo.m_additionalDamping = true;
		m_Body = new btRigidBody(rbInfo);
		m_Body->setUserPointer(this);

		m_LinearDamping = m_Body->getLinearDamping();
		m_AngularDamping = m_Body->getAngularDamping();

		// ワールドに剛体追加
		m_World->GetWorld()->addRigidBody(m_Body,group,mask);
	}

	void Create(BulletWorld* world, D3DXVECTOR3& pos, btScalar mass,short group = 1,short mask = -1)
	{
		CMatrix m;
		m.CreateMove(pos.x,pos.y,pos.z);
		Create(world, m, mass, group, mask);
	}

	// BOX形状追加
	void AddBox(D3DXMATRIX& mat, D3DXVECTOR3& halfsize)
	{
		btVector3 btHalfSize(halfsize.x,halfsize.y,halfsize.z);
		btBoxShape *box = new btBoxShape(btHalfSize);
		m_ShapeList.push_back(box);

		btTransform t;
		t.setFromOpenGLMatrix(&mat._11);
		m_Shape->addChildShape(t, box);
	}

	virtual void Release()
	{
		// 形状削除
		SAFE_DELETE(m_Shape);

		// 複合形状削除
		for(UINT i=0;i<m_ShapeList.size();i++){
			delete m_ShapeList[i];
		}
		m_ShapeList.clear();

		// 剛体削除
		if(m_Body){
			if(m_Body->getMotionState())delete m_Body->getMotionState();
			while(m_Body->getNumConstraintRefs() > 0){
				m_Body->removeConstraintRef(m_Body->getConstraintRef(0));
			}

			if(m_World && m_World->GetWorld()){
				m_World->GetWorld()->removeCollisionObject(m_Body);
			}
			delete m_Body;
			m_Body = NULL;
		}
		m_World = NULL;
	}

protected:
	btCompoundShape*				m_Shape;		// 形状
	btRigidBody*					m_Body;			// 剛体
	std::vector<btCollisionShape*>	m_ShapeList;	// 実際の形状

private:
	// コピー禁止用
	CBulletObj_Compound(const CBulletObj_Compound& src){}
	void operator=(const CBulletObj_Compound& src){}
};

//==========================================================================
// 三角形集合(メッシュ)剛体クラス
//==========================================================================
class CBulletObj_Mesh : public CBulletRigid_Base{
public:
	btBvhTriangleMeshShape*	GetShape(){return m_Shape;}

	CBulletObj_Mesh():
		m_indexVertexArrays(NULL),
		m_Shape(NULL),
		m_Verts(NULL),
		m_Idxs(NULL)
	{
		m_Type = BO_MESH;
	}

	~CBulletObj_Mesh(){
		Release();
	}

	virtual void Release()
	{
		SAFE_DELETE_ARRAY(m_Verts);
		SAFE_DELETE_ARRAY(m_Idxs);
		SAFE_DELETE_ARRAY(m_indexVertexArrays);

		// 形状削除
		SAFE_DELETE(m_Shape);

		// 剛体削除
		if(m_Body){
			if(m_Body->getMotionState())delete m_Body->getMotionState();
			while(m_Body->getNumConstraintRefs() > 0){
				m_Body->removeConstraintRef(m_Body->getConstraintRef(0));
			}
			if(m_World && m_World->GetWorld()){
				m_World->GetWorld()->removeCollisionObject(m_Body);
			}
			delete m_Body;
			m_Body = NULL;
		}
	}

	// メッシュから剛体作成
	void CreateFromMesh(BulletWorld* world,LPD3DXMESH lpMesh,D3DXMATRIX& startMat,short group = 1,short mask = -1)
	{
		Release();
		if(lpMesh == NULL)return;
		m_World = world;

		int vertStride = sizeof(btVector3);
		int indexStride = 3*sizeof(int);

		// 頂点数
		int totalVerts = lpMesh->GetNumVertices();

		// 頂点
		DWORD VertexSize =lpMesh->GetNumBytesPerVertex();	// １つの頂点のサイズ
		m_Verts = new btVector3[totalVerts];
		BYTE *p;
		HRESULT hr = lpMesh->LockVertexBuffer(D3DLOCK_READONLY,(LPVOID*)&p);
		D3DXVECTOR3 vBBMin,vBBMax;
		if(SUCCEEDED(hr)){
			// AABB
			// バウンディングボックスを算出
			D3DXComputeBoundingBox(	(D3DXVECTOR3*)p,
									totalVerts,
									VertexSize,
									&vBBMin,
									&vBBMax);

			// 頂点配列作成
			for(int i=0;i<totalVerts;i++){

				btVector3* v = (btVector3*)p;
				m_Verts[i] = *v;

				p += VertexSize;
			}

			// 頂点バッファをアンロック
			lpMesh->UnlockVertexBuffer();
		}
		else{
			Release();
		}

		// インデックス
		int totalTriangles = lpMesh->GetNumFaces();
		m_Idxs = new int[totalTriangles*3];
		WORD* pI;

		hr = lpMesh->LockIndexBuffer(D3DLOCK_READONLY,(void**)&pI);
		if(SUCCEEDED(hr)){
			// インデックス配列作成
			for(int i=0;i<totalTriangles*3;i++){
				m_Idxs[i] = (int)pI[i];
			}

			lpMesh->UnlockIndexBuffer();
		}
		else{
			Release();
		}

		m_indexVertexArrays = new btTriangleIndexVertexArray(
			totalTriangles,
			m_Idxs,
			indexStride,
			totalVerts,
			(btScalar*)m_Verts,
			vertStride);

		btVector3 aabbMin(vBBMin.x,vBBMin.y,vBBMin.z);
		btVector3 aabbMax(vBBMax.x,vBBMax.y,vBBMax.z);
		
		bool useQuantizedAabbCompression = true;
		m_Shape = new btBvhTriangleMeshShape(m_indexVertexArrays,useQuantizedAabbCompression,aabbMin,aabbMax);
		
		// 質量は０固定じゃないといけないらしい。つまり固定物専用
		m_Mass = 0;

		// 行列
		btTransform startTransform;
		startTransform.setFromOpenGLMatrix(&startMat._11);

		btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);
		btRigidBody::btRigidBodyConstructionInfo rbInfo(m_Mass,myMotionState,m_Shape,m_Inertia);
		m_Body = new btRigidBody(rbInfo);
		m_Body->setUserPointer(this);

		m_LinearDamping = m_Body->getLinearDamping();
		m_AngularDamping = m_Body->getAngularDamping();

		// ワールドに剛体追加
		m_World->GetWorld()->addRigidBody(m_Body,group,mask);
	}
	void CreateFromMesh(BulletWorld* world,LPD3DXMESH lpMesh,D3DXVECTOR3& pos,short group = 1,short mask = -1)
	{
		CMatrix m;
		m.CreateMove(pos.x,pos.y,pos.z);
		CreateFromMesh(world,lpMesh,m,group,mask);
	}

protected:
	btVector3*						m_Verts;		// 頂点配列
	int*							m_Idxs;			// インデックス配列
	btTriangleIndexVertexArray*		m_indexVertexArrays;
	btBvhTriangleMeshShape*			m_Shape;

private:
	// コピー禁止用
	CBulletObj_Mesh(const CBulletObj_Mesh& src){}
	void operator=(const CBulletObj_Mesh& src){}
};

//==========================================================================
// ジョイント基本クラス
//==========================================================================
class CBulletJoint_Base : public CBulletObj_Base
{
protected:
	CBulletJoint_Base(){}
};

//==========================================================================
// ボールジョイント
//==========================================================================
class CBulletJoint_Point : public CBulletJoint_Base{
public:
	btPoint2PointConstraint* GetJoint(){return m_Joint;}

	CBulletJoint_Point():
		m_Joint(NULL)
	{
		m_Type = JO_POINT;
	}

	~CBulletJoint_Point()
	{
		Release();
	}

	virtual void Release()
	{
		if(m_Joint){
			if(m_World)m_World->GetWorld()->removeConstraint(m_Joint);
			delete m_Joint;
			m_Joint = NULL;
		}
		m_World = NULL;
	}

	// 物体１：body1	pivotA：ジョイントの相対位置
	// 物体２：body2	pivotB：ジョイントの相対位置
	void Create(BulletWorld* world,btRigidBody& body1,btRigidBody& body2,D3DXVECTOR3& pivotA,D3DXVECTOR3& pivotB)
	{
		Release();

		m_World = world;

		btVector3 vA(pivotA.x,pivotA.y,pivotA.z);
		btVector3 vB(pivotB.x,pivotB.y,pivotB.z);
		m_Joint = new btPoint2PointConstraint( body1, body2, vA, vB);
		m_World->GetWorld()->addConstraint(m_Joint, true);
	}

	void Create(BulletWorld* world,btRigidBody& body1,btVector3& pivotA)
	{
		m_World = world;

		m_Joint = new btPoint2PointConstraint( body1, pivotA);
		m_World->GetWorld()->addConstraint(m_Joint, true);
		m_Joint->setUserConstraintPtr(this);
	}

protected:
	btPoint2PointConstraint*		m_Joint;

private:
	// コピー禁止用
	CBulletJoint_Point(const CBulletJoint_Point& src){}
	void operator=(const CBulletJoint_Point& src){}
};

//==========================================================================
// ヒンジジョイント
//==========================================================================
class CBulletJoint_Hinge : public CBulletJoint_Base{
public:
	btHingeConstraint* GetJoint(){return m_Joint;}

	CBulletJoint_Hinge():
		m_Joint(NULL)
	{
		m_Type = JO_HINGE;
	}

	~CBulletJoint_Hinge()
	{
		Release();
	}

	virtual void Release()
	{
		if(m_Joint){
			if(m_World)m_World->GetWorld()->removeConstraint(m_Joint);
			delete m_Joint;
			m_Joint = NULL;
		}
		m_World = NULL;
	}

	void Create(BulletWorld* world,btRigidBody& body1,btRigidBody& body2,btVector3& pivotA,btVector3& pivotB,btVector3& axisA,btVector3& axisB)
	{
		m_World = world;

		m_Joint = new btHingeConstraint( body1, body2, pivotA, pivotB, axisA, axisB);
		m_World->GetWorld()->addConstraint(m_Joint, true);
		m_Joint->setUserConstraintPtr(this);
	}

	void Create(BulletWorld* world,btRigidBody& body1,btVector3& pivotA,btVector3& axisA)
	{
		m_World = world;

		m_Joint = new btHingeConstraint( body1, pivotA, axisA);
		m_World->GetWorld()->addConstraint(m_Joint, true);
	}

protected:
	btHingeConstraint*		m_Joint;

private:
	// コピー禁止用
	CBulletJoint_Hinge(const CBulletJoint_Hinge& src){}
	void operator=(const CBulletJoint_Hinge& src){}
};

class CBulletJoint_6Dof : public CBulletJoint_Base{
public:
	btGeneric6DofConstraint* GetJoint(){return m_Joint;}

	CBulletJoint_6Dof():
		m_Joint(NULL)
	{
		m_Type = JO_6DOF;
	}

	~CBulletJoint_6Dof()
	{
		Release();
	}

	virtual void Release()
	{
		if(m_Joint){
			if(m_World)m_World->GetWorld()->removeConstraint(m_Joint);
			delete m_Joint;
			m_Joint = NULL;
		}
		m_World = NULL;
	}

	// 物体１：body1	pivotA：ジョイントの相対位置
	// 物体２：body2	pivotB：ジョイントの相対位置
	void Create(BulletWorld* world,btRigidBody& body1,btRigidBody& body2,D3DXMATRIX& mA,D3DXMATRIX& mB)
	{
		Release();

		m_World = world;

		btTransform tA;
		tA.setFromOpenGLMatrix(&mA._11);
		btTransform tB;
		tB.setFromOpenGLMatrix(&mB._11);
		m_Joint = new btGeneric6DofConstraint( body1, body2, tA, tB, true);
		m_World->GetWorld()->addConstraint(m_Joint, true);
		m_Joint->setUserConstraintPtr(this);

	}

	//
	// 制限
	// 移動 下限
	void SetLinearLowerLimit(float limitX,float limitY,float limitZ){
		m_Joint->setLinearLowerLimit( btVector3(limitX,limitY,limitZ) );
	}
	// 移動 上限
	void SetLinearUpperLimit(float limitX,float limitY,float limitZ){
		m_Joint->setLinearUpperLimit( btVector3(limitX,limitY,limitZ) );
	}
	// 回転 下限(度)
	void SetAngularLowerLimit(float anglimitX,float anglimitY,float anglimitZ){
		m_Joint->setAngularLowerLimit( btVector3(ToRadian(anglimitX),ToRadian(anglimitY),ToRadian(anglimitZ)) );
	}
	// 回転 上限(度)
	void SetAngularUpperLimit(float anglimitX,float anglimitY,float anglimitZ){
		m_Joint->setAngularUpperLimit( btVector3(ToRadian(anglimitX),ToRadian(anglimitY),ToRadian(anglimitZ)) );
	}
	// Index版
	void SetLimit(int Index,float limitLo,float limitHi){
		m_Joint->setLimit(Index,limitLo,limitHi);
	}

protected:
	btGeneric6DofConstraint*		m_Joint;

private:
	// コピー禁止用
	CBulletJoint_6Dof(const CBulletJoint_6Dof& src){}
	void operator=(const CBulletJoint_6Dof& src){}
};

//==========================================================================
// 汎用スプリングジョイント
//==========================================================================
class CBulletJoint_6DofSpring : public CBulletJoint_Base{
public:
	btGeneric6DofSpringConstraint* GetJoint(){return m_Joint;}

	CBulletJoint_6DofSpring():
		m_Joint(NULL)
	{
		m_Type = JO_6DOFSPRING;
	}

	~CBulletJoint_6DofSpring()
	{
		Release();
	}

	virtual void Release()
	{
		if(m_Joint){
			if(m_World)m_World->GetWorld()->removeConstraint(m_Joint);
			delete m_Joint;
			m_Joint = NULL;
		}
		m_World = NULL;
	}

	// 物体１：body1	pivotA：ジョイントの相対位置
	// 物体２：body2	pivotB：ジョイントの相対位置
	void Create(BulletWorld* world,btRigidBody& body1,btRigidBody& body2,D3DXMATRIX& mA,D3DXMATRIX& mB)
	{
		Release();

		m_World = world;

		btTransform tA;
		tA.setFromOpenGLMatrix(&mA._11);
		btTransform tB;
		tB.setFromOpenGLMatrix(&mB._11);
		m_Joint = new btGeneric6DofSpringConstraint( body1, body2, tA, tB, true);
		m_World->GetWorld()->addConstraint(m_Joint, true);
		m_Joint->setUserConstraintPtr(this);

		m_Joint->setEquilibriumPoint();
	}

	//
	// バネを有効にする
	// Index:0〜2が平行移動x,y,z 3〜5は回転移動x,y,z
	void EnableSpring(int Index,bool enable)
	{
		m_Joint->enableSpring(Index,enable);
	}
	// バネの柔らかさ
	void SetSpring_Stiffness(int Index,float f){
		m_Joint->setStiffness(Index,f);
	}
	// 減衰率
	void SetDamping(int Index,float f){
		m_Joint->setDamping(Index,f); //減衰率
	}

	//
	// 制限
	// 移動 下限
	void SetLinearLowerLimit(float limitX,float limitY,float limitZ){
		m_Joint->setLinearLowerLimit( btVector3(limitX,limitY,limitZ) );
	}
	// 移動 上限
	void SetLinearUpperLimit(float limitX,float limitY,float limitZ){
		m_Joint->setLinearUpperLimit( btVector3(limitX,limitY,limitZ) );
	}
	// 回転 下限(度)
	void SetAngularLowerLimit(float anglimitX,float anglimitY,float anglimitZ){
		m_Joint->setAngularLowerLimit( btVector3(ToRadian(anglimitX),ToRadian(anglimitY),ToRadian(anglimitZ)) );
	}
	// 回転 上限(度)
	void SetAngularUpperLimit(float anglimitX,float anglimitY,float anglimitZ){
		m_Joint->setAngularUpperLimit( btVector3(ToRadian(anglimitX),ToRadian(anglimitY),ToRadian(anglimitZ)) );
	}
	// Index版
	void SetLimit(int Index,float limitLo,float limitHi){
		m_Joint->setLimit(Index,limitLo,limitHi);
	}

	// これを呼んだときのjointの位置関係がバネの復元基準の位置になる
	void SetEquilibriumPoint(){
		m_Joint->setEquilibriumPoint();
	}

protected:
	btGeneric6DofSpringConstraint*		m_Joint;

private:
	// コピー禁止用
	CBulletJoint_6DofSpring(const CBulletJoint_6DofSpring& src){}
	void operator=(const CBulletJoint_6DofSpring& src){}
};


#endif  _CLASS_NAME_BULLET_PHYSICS_ENGINE_H_
