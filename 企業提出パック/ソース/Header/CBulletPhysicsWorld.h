/*-----------------------------------------------------------

やっつけ判定なので後で変更しておく

-----------------------------------------------------------*/

#ifndef _CLASS_NAME_BULLET_PHYSICS_WORLD_H_
#define _CLASS_NAME_BULLET_PHYSICS_WORLD_H_


#include "Game.h"

/*====================

Class宣言

====================*/

class MyBulletWorld : public BulletWorld 
{

public:

	//----------------
	//	コンストラクタ
	//----------------
	MyBulletWorld(){}

	//--------------
	//	デストラクタ
	//--------------
	~MyBulletWorld(){}



	// 物体同士が衝突したとき、これが呼ばれる
	virtual bool ContactProcessedCallback(btManifoldPoint& cp, CBulletRigid_Base* body0, CBulletRigid_Base* body1)
	{

		//	物理エンジンクラスに登録しておいた派生クラスを基底クラスにキャストしておく
		CObjectBase* task0 = (CObjectBase *)body0->GetUserPointer();
		CObjectBase* task1 = (CObjectBase *)body1->GetUserPointer();


		if (task0 && task1) 
		{

			if (task0->GetItemId() == OBJECT_LIST::ID::PLAYER)
			{
				task0->SetUpdateTag("Hit");
			}
			else if (task1->GetItemId() == OBJECT_LIST::ID::PLAYER)
			{
				task1->SetUpdateTag("Hit");
			}

			if (task0->GetItemId() != OBJECT_LIST::ID::SEESAW)
			{
				task1->SetUpdateTag("");
			}
			else if (task1->GetItemId() != OBJECT_LIST::ID::SEESAW)
			{
				task0->SetUpdateTag("");
			}

			if (task0->GetItemId() == OBJECT_LIST::ID::SEESAW)
			{
				//	タグにシーソーの上におかれていると知らせる
				task1->SetUpdateTag("NowSeesaw");
			}
			else if (task1->GetItemId() == OBJECT_LIST::ID::SEESAW)
			{
				//	タグにシーソーの上におかれていると知らせる
				task0->SetUpdateTag("NowSeesaw");
			}


			if (task1->GetItemId() == OBJECT_LIST::ID::BRIDGE
				&&
				task0->GetItemId() == OBJECT_LIST::ID::ROCK
				||
				task1->GetItemId() == OBJECT_LIST::ID::BRIDGE
				&&
				task0->GetItemId() == OBJECT_LIST::ID::WOOD_BOX
				||
				task1->GetItemId() == OBJECT_LIST::ID::BRIDGE
				&&
				task0->GetItemId() == OBJECT_LIST::ID::BRANCH
				)
			{
				if (GET_FLAG_CALC.CheckFlag(task0->GetItemState(), 0x0f00, STOCK::STATE::DO_SPIT))
				{
					int State = task1->GetState();

					State = GET_FLAG_CALC.SetData(State & 0x0000, 0x0002, LOGICAL::OPERATION_MODE::OR);

					task1->SetState(State);
				}
			}
			else if (
				task0->GetItemId() == OBJECT_LIST::ID::BRIDGE
				&&
				task1->GetItemId() == OBJECT_LIST::ID::ROCK
				||
				task0->GetItemId() == OBJECT_LIST::ID::BRIDGE
				&&
				task1->GetItemId() == OBJECT_LIST::ID::WOOD_BOX
				||
				task0->GetItemId() == OBJECT_LIST::ID::BRIDGE
				&&
				task1->GetItemId() == OBJECT_LIST::ID::BRANCH)
			{
				if (GET_FLAG_CALC.CheckFlag(task1->GetItemState(), 0x0f00, STOCK::STATE::DO_SPIT))
				{
					int State = task0->GetState();

					State = GET_FLAG_CALC.SetData(State & 0x0000, 0x0002, LOGICAL::OPERATION_MODE::OR);

					task0->SetState(State);
				}
			}




			if (task1->GetItemId() != OBJECT_LIST::PLAYER
				&&
				task0->GetItemId() != OBJECT_LIST::PLAYER
				)
			{
				task0->SetEnableColl(true);
			}
			else
			{
				task0->SetEnableColl(false);
			}

			if (task0->GetItemId() != OBJECT_LIST::PLAYER
				&&
				task1->GetItemId() != OBJECT_LIST::PLAYER
				)
			{
				task1->SetEnableColl(true);
			}
			else
			{
				task1->SetEnableColl(false);
			}


			if (task0->GetItemId() == OBJECT_LIST::ID::CRACKS_WALL
				&&
				task1->GetItemId() != OBJECT_LIST::ID::STAGE)
			{
				int State = task0->GetState();

				if (!GET_FLAG_CALC.CheckFlag(State, 0x0002))
				{

					State = GET_FLAG_CALC.SetData((State & CRACK::STATE::NOT), CRACK::STATE::COLLISION, LOGICAL::OPERATION_MODE::OR);

					task0->SetState(State);
				}
			}
			else if (
				task1->GetItemId() == OBJECT_LIST::ID::CRACKS_WALL
				&&
				task0->GetItemId() != OBJECT_LIST::ID::STAGE
				)
			{
				int State = task1->GetState();

				if (!GET_FLAG_CALC.CheckFlag(State, 0x0002))
				{
					State = GET_FLAG_CALC.SetData((State & CRACK::STATE::NOT), CRACK::STATE::COLLISION, LOGICAL::OPERATION_MODE::OR);

					task1->SetState(State);
				}
			}


			if (task0->GetItemId() == OBJECT_LIST::ID::PLAYER)
			{
				task1->SetUpdateTag("PHit");
			}
			else if (task1->GetItemId() == OBJECT_LIST::ID::PLAYER)
			{
				task0->SetUpdateTag("PHit");
			}


			if (task0->GetItemId() == OBJECT_LIST::ID::STONE)
			{
				//task0->SetState();
				if (task1->GetObjectWeight() >= 0.9f
					&&
					GET_FLAG_CALC.CheckFlag(task0->GetState(), 0x0001, 0x0001))
				{
					int State = task0->GetState();

					State = GET_FLAG_CALC.SetData((State & 0x0000), 0x0002, LOGICAL::OPERATION_MODE::OR);

					task0->SetState(State);
				}
			}
			else if (task1->GetItemId() == OBJECT_LIST::ID::STONE)
			{
				if (task0->GetObjectWeight() >= 0.9f
					&&
					GET_FLAG_CALC.CheckFlag(task1->GetState(), 0x0001, 0x0001))
				{
					int State = task1->GetState();

					State = GET_FLAG_CALC.SetData((State & 0x0000), 0x0002, LOGICAL::OPERATION_MODE::OR);

					task1->SetState(State);
				}
			}


			if (task0->GetItemId() == OBJECT_LIST::ID::bSWITCH)
			{
				task0->SetEnableColl(true);
			}
			else if (task1->GetItemId() == OBJECT_LIST::ID::bSWITCH)
			{
				task1->SetEnableColl(true);
			}


			return true;
		}
		
		return false;
	}

};

#endif  _CLASS_NAME_BULLET_PHYSICS_WORLD_H_