/*----------------------------------------------

オブジェクトマネージャークラス
マップ上に生成されるオブジェクトを管理する

----------------------------------------------*/

#ifndef _CLASS_NAME_CAMERA_H_
#define _CLASS_NAME_CAMERA_H_


/*=============

マクロ定義

=============*/

#define GET_CAMERA CCamera::GetInstance()


/*=============

Class宣言

=============*/

class CCamera
{
private:

	//----------------
	//	コンストラクタ
	//----------------
	CCamera();

	//--------------
	//	デストラクタ
	//--------------
	~CCamera();


	//	追尾カメラの設定
	void SetChaseCamera(CVector3& _CPos, CVector3& _CLook, CVector3& _CHead, CMatrix& _World);
	void SetCamera(CMatrix& _mView, CVector3 _CPos, CVector3 _DisPos, float _AngX = 0, float _AngY = 0, float _AngZ = 0);


public:


	static CCamera& GetInstance()
	{
		static CCamera Instance;
		return Instance;
	}


	//	カメラセット
	void CameraUpdate(
		LPDIRECT3DDEVICE9 _lpD3DDevice, 
		CVector3& _BasePos, 
		CMatrix* _World = NULL, 
		float _AngX = 0.0f, 
		float _AngY = 0.0f,
		float _AngZ = 0.0f,
		float _fovy = 45.0f, 
		float _asp = 4.0f / 3.0f, 
		float _zn = 1.0f, 
		float _zf = 1500.0f);

	void SetProj(LPDIRECT3DDEVICE9 _lpD3DDevice, CMatrix& mProj, float _AngX = 0.0f, float _AngY = 0.0f,
		float _fovy = 45.0f, float _asp = 4.0f / 3.0f, float _zn = 1.0f, float _zf = 1500.0f);

	void SetView();


	void D3DLMatrixAnimation(D3DXMATRIX* mNow, D3DXMATRIX* mStart, D3DXMATRIX* mEnd, FLOAT AnimeFrame)
	{
		D3DXQUATERNION qStart, qEnd, qNow;
		//行列をクォータニオンに変換する----------
		D3DXQuaternionRotationMatrix(&qStart, mStart);
		D3DXQuaternionRotationMatrix(&qEnd, mEnd);
		//----------------------------------------

		/*
		第１引数にアニメの途中の状態が入る
		第２引数にアニメの最初の状態
		アニメの最後の状態を第３引数に
		第４引数には、アニメの進行具合（０〜１）

		*/
		D3DXQuaternionSlerp(&qNow, &qStart, &qEnd, AnimeFrame);

		//クォータニオンを行列に変換する（回転のみ）
		D3DXMatrixRotationQuaternion(mNow, &qNow);

		//座標を計算する
		D3DXVECTOR3 StartPos, EndPos, NowPos;
		//mStartの座標を入れる(x,y,z)
		StartPos = D3DXVECTOR3(mStart->_41, mStart->_42, mStart->_43);
		EndPos = D3DXVECTOR3(mEnd->_41, mEnd->_42, mEnd->_43);

		//引数
		/*
		１　アニメの途中の座標が入る
		２　最初の位置
		３　最後の位置
		４　アニメの進行具合(0 〜 1)
		*/
		D3DXVec3Lerp(&NowPos, &StartPos, &EndPos, AnimeFrame);

		mNow->_41 = NowPos.x;
		mNow->_42 = NowPos.y;
		mNow->_43 = NowPos.z;
	}


	short CameraMode;
	
	enum Mode
	{
		TPS = 0x0000,
		FPS = 0x0001,
	};


	//----------
	//	セッター
	//----------

	//	射影行列セット
	void SetProjMatrix(CMatrix _mProj){ m_mProj = _mProj; }

	//	ビュー行列セット
	void SetViewMatrix(CMatrix _mView){ m_mView = _mView; }

	//	カメラの座標をセット
	void SetCameraPosition(CVector3 _mPos){ m_mCamPos = _mPos; }

	//	カメラフラグをセットする
	void SetCamera(bool _Enable){ m_EnableCamera = _Enable; }


	//----------
	//	ゲッター
	//----------

	//	射影行列を返す
	CMatrix GetProjMatrix(){return m_mProj;}

	//	ビュー行列を返す
	CMatrix GetViewMatrix(){return m_mView;}

	//	カメラの座標を返す
	CVector3 GetCameraLocalPosition(){ return m_mCamPos; }

	//	カメラのワールド座標を返す
	CVector3 GetCameraWorldPosition() { return m_mCamera.GetPos(); }

	//	カメラフラグをゲットする
	bool GetCamera(){ return m_EnableCamera; }

	//	カメラの向いている角度を返す
	CVector3 GetAngle() { return m_Angle; }

	//	外積情報を返す
	CVFData& GetVF() { return m_VF; }


private:


	//  射影行列
	CMatrix	m_mProj;

	//	ビュー行列
	CMatrix	m_mView;

	//	カメラのワールド行列
	CMatrix		m_mCamera;

	//　カメラの座標
	CVector3 m_mCamPos;

	//	カメラのアングル
	CVector3 m_Angle;

	//	外積情報
	CVFData		m_VF;

	//	カメラを動かすかどうかを制御
	bool m_EnableCamera;
};


#endif  _CLASS_NAME_CAMERA_H_