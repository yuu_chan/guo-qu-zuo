#ifndef _CLASS_NAME_CLICK_FONT_H_
#define _CLASS_NAME_CLICK_FONT_H_

class CPlayer;

class CClickFont : public CTextureBase
{
public:
	//----------------
	//	コンストラクタ
	//----------------
	CClickFont();

	//--------------
	//	デストラクタ
	//--------------
	~CClickFont();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update();

	//	描画
	void Draw(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	開放
	void Release();


private:
	static const float FONT_WIDTH_SIZE;
	static const float FONT_HEIGHT_SIZE;

	shared_ptr<CMeshObject> m_pSky;

	CVector3 m_vAng;
	CMatrix m_mWorld;

	shared_ptr<CMeshObject> m_pPlayer;

	CMatrix m_PlayerWorld;
};

#endif  _CLASS_NAME_CLICK_FONT_H_