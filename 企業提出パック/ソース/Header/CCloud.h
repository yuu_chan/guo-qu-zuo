/*----------------------------------------------------------------

雲クラス

踏むと消えていき、吸い込むと消える

----------------------------------------------------------------*/

#ifndef _CLASS_NAME_CLOUD_H_
#define _CLASS_NAME_CLOUD_H_


/*=============

Class宣言

=============*/

class CCloud : public CObjectBase
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CCloud();

	//--------------
	//	デストラクタ
	//--------------
	~CCloud();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	描画
	void Draw(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	解放
	void Release();


private:


	shared_ptr<CBulletObj_Sphere> m_spRigidSphere;

	bool m_AbleDelete;
};


#endif  _CLASS_NAME_CLOUD_H_

