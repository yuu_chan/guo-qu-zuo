/*------------------------------------------------------

	当たり判定クラス
	より、細かいオブジェクト同士の判定を纏めたクラス

	参考にとってきているので、変数名など改変中

------------------------------------------------------*/

#ifndef _CLASS_NAME_COLLISION_H_
#define _CLASS_NAME_COLLISION_H_


/*===============

Class宣言

===============*/

class CCollision
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CCollision();


	//--------------
	//	デストラクタ
	//--------------
	~CCollision();


	//-----------------------------------
	// 四角形同士、立方体同士の判定(AABB)
	//-----------------------------------

	static bool BoxToBox2D(
		const D3DXVECTOR2* min1,
		const D3DXVECTOR2* max1,
		const D3DXVECTOR2* min2,
		const D3DXVECTOR2* max2
		)
	{
		if( (max1->x >= min2->x) && (min1->x <= max2->x) &&
			(max1->y >= min2->y) && (min1->y <= max2->y))
		{
			return true;
		}
		return false;
	}

	static bool BoxToBox3D(
		const D3DXVECTOR3* min1,
		const D3DXVECTOR3* max1,
		const D3DXVECTOR3* min2,
		const D3DXVECTOR3* max2
		)
	{
		if( (max1->x >= min2->x) && (min1->x <= max2->x) &&
			(max1->y >= min2->y) && (min1->y <= max2->y) &&
			(max1->z >= min2->z) && (min1->z <= max2->z) )
		{
			return true;
		}
		return false;
	}


	//-------
	// 球判定
	//-------
	// 引数：vOut…球２から球１へのベクトルが入る
	// 戻り値：めり込んでいる距離。つまり、＋値だとＨＩＴ、−値だと当たっていない。
	static float SphereToSphere(
		const D3DXVECTOR3*	vQpos1,
		float				r1,
		const D3DXVECTOR3*	vQpos2,
		float				r2,
		D3DXVECTOR3*		vOut
		)
	{
		*vOut = (*vQpos1) - (*vQpos2);
		float len = CVector3::Length(vOut);

		return r1 + r2 - len;
	}

	static bool SphereToSphere(
		const D3DXVECTOR3*	vQpos1,
		float				r1,
		const D3DXVECTOR3*	vQpos2,
		float				r2
		)
	{
		CVector3 vOut = (*vQpos1) - (*vQpos2);
		float lenPow2 = CVector3::Dot(&vOut,&vOut);

		return lenPow2 < (r1 + r2);
	}


	//---------
	// レイ判定
	//---------
	static BOOL IntersectTri(
		const D3DXVECTOR3*	p1,
		const D3DXVECTOR3*	p2,
		const D3DXVECTOR3*	p3,
		const D3DXVECTOR3*	lpRayPos,
		const D3DXVECTOR3*	lpRayDir,
		float*				pU,
		float*				pV,
		float*				dist
		)
	{
		return D3DXIntersectTri(p1,p2,p3,lpRayPos,lpRayDir,pU,pV,dist);
	}

	static BOOL IntersectTri(
		const D3DXVECTOR3*	p1,
		const D3DXVECTOR3*	p2,
		const D3DXVECTOR3*	p3,
		const D3DXVECTOR3*	lpRayPos,
		const D3DXVECTOR3*	lpRayDir,
		float*				dist)
	{
		return D3DXIntersectTri(p1,p2,p3,lpRayPos,lpRayDir,NULL,NULL,dist);
	}

	// 面情報テーブルとレイとの判定
	static BOOL IntersectRay(
		const CFace*		FaceTbl, 
		int					FaceTblNum, 
		const D3DXVECTOR3*	lpRayPos, 
		const D3DXVECTOR3*	lpRayDir, 
		float*				dist, 
		DWORD*				lpHitFaceIndex);
	
	// 行列指定Ver
	static BOOL IntersectRay(
		const CFace*		FaceTbl, 
		int					FaceTblNum, 
		const D3DXVECTOR3*	lpRayPos, 
		const D3DXVECTOR3*	lpRayDir, 
		float*				dist, 
		DWORD*				lpHitFaceIndex, 
		D3DXMATRIX*			D3DXMATRIX
		);
	
	// AABBとレイとの判定
	static BOOL IntersectRayBB(
		const D3DXVECTOR3*	vmin,
		const D3DXVECTOR3*	vmax,
		const CVector3*		lpRayPos,
		const CVector3*		lpRayDir,
		float*				dist
		);
	
	// 行列指定Ver
	static BOOL IntersectRayBB(
		const D3DXVECTOR3*	vmin,
		const D3DXVECTOR3*	vmax,
		const CVector3		*lpRayPos,
		const CVector3		*lpRayDir,
		float				*dist,
		const CMatrix		*lpMat
		);


	//-------
	// 球と面
	//-------
	static BOOL FaceToSphere(
		CFace*		FaceTbl, 
		int			FaceTblNum, 
		CVector3*	lpQPos, 
		float		Qr, 
		CVector3*	lpvOut,
		int			TargetId
		);

	static BOOL FaceToSphere(
		CFace*			FaceTbl, 
		int				FaceTblNum, 
		CVector3*		lpQPos, 
		float			Qr, 
		CVector3*		lpvOut,
		const CMatrix*	lpMat,
		int				Targetid
		);


};


#endif  _CLASS_NAME_COLLISION_H_