/*----------------------------------------------------------------

ヒビの入った壁
物を一定の力でぶつけると砕けて
CBreakRockに変化する

----------------------------------------------------------------*/


#ifndef _CLASS_NAME_CRACK_WALL_H_
#define _CLASS_NAME_CRACK_WALL_H_


/*=============

namespace宣言

=============*/

/*--------------------------------------------------->

壁が何かと衝突したかどうか
衝突後どうするかのステータスを管理


ステータス一覧-------------------------------

NOT			:	何もなし

COLLISION	:	衝突 CBreakRockがインスタンス化される

END			:	自身を破棄する

<---------------------------------------------------*/

namespace CRACK
{
	enum STATE
	{

		NOT			= 0,

		COLLISION	= 1,

		END			= 2,

	};

}


/*=============

Class宣言

=============*/

class CCrackWall : public CObjectBase
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CCrackWall();

	//--------------
	//	デストラクタ
	//--------------
	~CCrackWall();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	解放
	void Release();


private:


	//	立方体剛体で登録
	shared_ptr<CBulletObj_Box> m_spRigidBox;


};


#endif  _CLASS_NAME_CRACK_WALL_H_

