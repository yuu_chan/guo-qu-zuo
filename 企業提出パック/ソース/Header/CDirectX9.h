#ifndef _CLASS_NAME_DIRECTX9_H_
#define _CLASS_NAME_DIRECTX9_H_

// ラジアン角
#define ToRadian(x) ((x)*0.017453f)
#define ToDegree(x) ((x)*57.3f)

//#define D3DX_PI    ((FLOAT)  3.141592654f)
//#define D3DX_1BYPI ((FLOAT)  0.318309886f)

//#define D3DXToRadian( degree ) ((degree) * (D3DX_PI / 180.0f))
//#define D3DXToDegree( radian ) ((radian) * (180.0f / D3DX_PI))


#include "CTexture.h"

/*-------------------------------------------------------------->
簡易Direct3D操作クラス

シングルトンで生成
<--------------------------------------------------------------*/

#define GET_DIRECT_HELPER CDirectX9Helper::GetInstance()

class CDirectX9Helper
{
public:

	//=============================================
	// 取得系
	//=============================================
	LPDIRECT3D9				GetD3D(){ return m_lpD3D; }				// D3Dオブジェクト取得
	LPDIRECT3DDEVICE9		GetDev(){ return m_lpD3DDev; }			// D3Dデバイス取得
	LPD3DXSPRITE			GetSprite(){ return m_lpSprite; }		// D3DSprite取得
	LPD3DXFONT				GetFont(){ return m_lpFont; }			// D3DFont取得
	D3DPRESENT_PARAMETERS*	GetD3DPP(){ return &m_d3dpp; }			// D3Dパラメータ取得
	D3DLIGHT9				GetLight(UINT LightNo);					// ライト情報取得
	D3DCAPS9*				GetCaps(){ return &m_Caps; }			// デバイス情報取得

	int						GetRezoW(){ return m_d3dpp.BackBufferWidth; }	// X解像度取得(画面の幅)
	int						GetRezoH(){ return m_d3dpp.BackBufferHeight; }	// Y解像度取得(画面の高)

	static const int FVF_TLVERTEX = D3DFVF_XYZ | D3DFVF_DIFFUSE | D3DFVF_TEX1;

	//=============================================
	// Direct3D初期化
	//=============================================
	BOOL Init(
		HWND		_hWnd,
		int			_Width,
		int			_Height,
		D3DFORMAT	_Format,
		BOOL		_FullScreen);

	//	Direct3Dの基本的なレンダーステート等の設定
	void SetState();

	//	基本的にライトをいじらない場合のデフォルト値を入れておく
	void DefaultLight(D3DXVECTOR3 _LightDir = D3DXVECTOR3(0, -1, 0));

	//=============================================
	// Direct3D解放
	//=============================================
	void Release();

	//=============================================
	// レンダーターゲット・Zバッファ関係
	//=============================================

	// クリア
	void Clear(
		bool		_bRenderTarget,
		bool		_bZBuffer,
		bool		_bStencilBuffer,
		D3DCOLOR	_color = 0xFFFFFFFF,
		float		_z = 1.0f,
		DWORD		_stencil = 0)
	{
		DWORD flags = 0;
		if (_bRenderTarget)		flags |= D3DCLEAR_TARGET;
		if (_bZBuffer)			flags |= D3DCLEAR_ZBUFFER;
		if (_bStencilBuffer)	flags |= D3DCLEAR_STENCIL;

		m_lpD3DDev->Clear(0, NULL, flags, _color, _z, _stencil);
	}

	// デフォルトのバックバッファをセット。
	void ResetRenderTarget()
	{
		m_lpD3DDev->SetRenderTarget(0, m_OrgRenderTarget);
	}

	// デフォルトのZバッファをセット。
	void ResetDepthStencil()
	{
		m_lpD3DDev->SetDepthStencilSurface(m_OrgDepthStencil);
	}

	// レンダーターゲット変更
	void SetRenderTarget(DWORD _Idx, LPDIRECT3DTEXTURE9 _texture)
	{
		if (_texture == NULL)
		{
			m_lpD3DDev->SetRenderTarget(_Idx, NULL);
		}
		else
		{
			LPDIRECT3DSURFACE9 rt;
			if (SUCCEEDED(_texture->GetSurfaceLevel(0, &rt)))
			{
				m_lpD3DDev->SetRenderTarget(_Idx, rt);
				rt->Release();
			}
		}
	}

	// Zバッファ変更
	void SetDepthStencil(LPDIRECT3DSURFACE9 _depth_stencil)
	{
		m_lpD3DDev->SetDepthStencilSurface(_depth_stencil);
	}

	// 平行光源作成・設定
	void SetDirectionalLight(int _LightNo, D3DXVECTOR3* _vWay, D3DXCOLOR* _Dif, D3DXCOLOR* _Amb, D3DXCOLOR* _Spe);


	//============================================================
	// レンダーステート設定系
	//============================================================
	// ライティング有効無効
	void LightEnable(BOOL _bl)
	{
		m_lpD3DDev->SetRenderState(D3DRS_LIGHTING, _bl);
	}

	// Zバッファ判定有効無効
	void ZEnable(BOOL _bl)
	{
		m_lpD3DDev->SetRenderState(D3DRS_ZENABLE, _bl);
	}

	// Zバッファ書き込み有効無効
	void ZWriteEnable(BOOL _bl)
	{
		m_lpD3DDev->SetRenderState(D3DRS_ZWRITEENABLE, _bl);
	}

	// カリングモード設定 通常はD3DCULL_CCW カリングしない場合はD3DCULL_NONE
	void CullMode(DWORD _Mode)
	{
		m_lpD3DDev->SetRenderState(D3DRS_CULLMODE, _Mode);
	}

	// アルファブレンド有効
	void AlphaBlendEnable(BOOL _bl)
	{
		m_lpD3DDev->SetRenderState(D3DRS_ALPHABLENDENABLE, _bl);
	}

	// 半透明描画設定 加算合成終了
	void Blend_Alpha()
	{
		m_lpD3DDev->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
		m_lpD3DDev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
		m_lpD3DDev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	}

	// 加算合成描画設定 加算合成する
	void Blend_Add()
	{
		m_lpD3DDev->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
		m_lpD3DDev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
		m_lpD3DDev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
	}

	// レンダリングターゲット個別アルファ設定
	void SeparateAlphaBlendEnable(BOOL _enable)
	{

		m_lpD3DDev->SetRenderState(D3DRS_SEPARATEALPHABLENDENABLE, _enable);

		if (_enable)
		{
			m_lpD3DDev->SetRenderState(D3DRS_BLENDOPALPHA, D3DBLENDOP_ADD);
			m_lpD3DDev->SetRenderState(D3DRS_SRCBLENDALPHA, D3DBLEND_ONE);
			m_lpD3DDev->SetRenderState(D3DRS_DESTBLENDALPHA, D3DBLEND_INVSRCALPHA);
		}
	}


	//========================================================================
	// サンプラ
	//========================================================================
	// テクスチャ補間を線形に
	void SetTextureFilter_Linear(DWORD _SamplerStageIndex)
	{
		m_lpD3DDev->SetSamplerState(_SamplerStageIndex, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
		m_lpD3DDev->SetSamplerState(_SamplerStageIndex, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
		m_lpD3DDev->SetSamplerState(_SamplerStageIndex, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR);
	}

	// テクスチャ補間を無しに
	void SetTextureFilter_Point(DWORD _SamplerStageIndex)
	{
		m_lpD3DDev->SetSamplerState(_SamplerStageIndex, D3DSAMP_MINFILTER, D3DTEXF_POINT);
		m_lpD3DDev->SetSamplerState(_SamplerStageIndex, D3DSAMP_MAGFILTER, D3DTEXF_POINT);
		m_lpD3DDev->SetSamplerState(_SamplerStageIndex, D3DSAMP_MIPFILTER, D3DTEXF_NONE);
	}

	// ミップマップ詳細度設定
	void SetMipmapBias(float _fVal)
	{
		m_lpD3DDev->SetSamplerState(0, D3DSAMP_MIPMAPLODBIAS, *((DWORD*)&_fVal));
	}
	// Wrapモード
	void SetAddressU_Wrap(DWORD _SamplerStageIndex)
	{
		m_lpD3DDev->SetSamplerState(_SamplerStageIndex, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
	}

	void SetAddressV_Wrap(DWORD _SamplerStageIndex){
		m_lpD3DDev->SetSamplerState(_SamplerStageIndex, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);
	}

	// Clampモード
	void SetAddressU_Clamp(DWORD _SamplerStageIndex)
	{
		m_lpD3DDev->SetSamplerState(_SamplerStageIndex, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
	}
	void SetAddressV_Clamp(DWORD _SamplerStageIndex){
		m_lpD3DDev->SetSamplerState(_SamplerStageIndex, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);
	}

	////========================================================================
	//// D3DXSprite
	////========================================================================
	// スプライト描画開始
	BOOL BeginSprite(DWORD Flags = D3DXSPRITE_ALPHABLEND){
		HRESULT hr = m_lpSprite->Begin(Flags);
		if (hr == D3D_OK)return TRUE;

		return FALSE;
	}
	// スプライト描画終了
	void EndSprite(){
		m_lpSprite->End();
	}
	// スプライト描画 ※BeginSprite()〜EndSprite()の間で書くこと
	void DrawSprite(CTexture* tex, D3DCOLOR color, D3DXMATRIX* mat);
	void DrawSprite(CTexture* tex, int x, int y, int w, int h, D3DCOLOR color);	// 座標、幅、高さ指定Version
	void DrawSprite(CTexture* _pTex, RECT _Rect, D3DXMATRIX* _pMat, D3DCOLOR _Color);	// 座標、幅、高さ指定Version
	void DrawFont(const char* text, D3DCOLOR color, D3DXMATRIX* mat);

	//========================================================================
	// XYZRHW形式の2D描画設定用
	//========================================================================
	void Begin2DState()
	{
		LightEnable(FALSE);
		ZEnable(FALSE);
		ZWriteEnable(FALSE);
		SetAddressU_Clamp(0);
		SetAddressV_Clamp(0);
	}

	void End2DState()
	{
		LightEnable(TRUE);
		ZEnable(TRUE);
		ZWriteEnable(TRUE);
		SetAddressU_Wrap(0);
		SetAddressV_Wrap(0);
	}

	//========================================================================
	// 簡易描画用
	//========================================================================
	// 2Dの四角形を描画
	void DrawQuad(
		float _x,
		float _y,
		float _w,
		float _h,
		float _tuCnt = 1,
		float _tvCnt = 1,
		D3DCOLOR _color = 0xFFFFFFFF);

	// 3Dの四角形を描画
	void DrawQuad3D(
		float _LSize,
		float _RSize,
		float _TSize,
		float _BSize,
		float _tuCnt = 1,
		float _tvCnt = 1,
		D3DXCOLOR _color = 0xFFFFFFFF);


	//	読み込み補助関数
	// ファイル名と拡張子の間に、指定文字列を挿入する
	inline string ConvertExtFileName(const string& _FileName, const char* _Ext)
	{
		//	ファイルパスをコピー
		string extName = _FileName;

		int pos = extName.find_last_of(".");

		if (pos == -1)
		{
			return "";
		}

		std::string tmp = ".";
		
		tmp += _Ext;
		
		extName.insert(pos, tmp);
		

		return extName;
	}



private:

	IDirect3D9*					m_lpD3D;			// D3Dオブジェクト
	IDirect3DDevice9*			m_lpD3DDev;			// D3Dデバイス
	D3DPRESENT_PARAMETERS		m_d3dpp;			// 詳細設定
	LPD3DXSPRITE				m_lpSprite;			// D3DXSPRITE
	LPD3DXFONT					m_lpFont;			// D3DXFONT
	D3DXFONT_DESC				m_FontDesc;			// ↑のフォントの情報
	D3DCAPS9					m_Caps;				// デバイスの情報


	public:
	LPDIRECT3DSURFACE9			m_OrgRenderTarget;	// デフォルトのバックバッファ
	LPDIRECT3DSURFACE9			m_OrgDepthStencil;	// デフォルトの深度(Z)バッファ

	// シングルトン用
public:

	CDirectX9Helper() :
		m_lpD3D(nullptr),
		m_lpD3DDev(nullptr),
		m_lpSprite(nullptr),
		m_lpFont(nullptr),
		m_OrgRenderTarget(nullptr),
		m_OrgDepthStencil(nullptr)
	{
	}

	~CDirectX9Helper()
	{
		Release();
	}

	static CDirectX9Helper& GetInstance()
	{
		static CDirectX9Helper Instance;
		return Instance;
	}

};

#endif  _CLASS_NAME_DIRECTX9_H_