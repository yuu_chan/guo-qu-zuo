/*--------------------------------------------------

class説明

--------------------------------------------------*/

#ifndef _CLASS_NAME_EFFECT_MANAGER_H_
#define _CLASS_NAME_EFFECT_MANAGER_H_


/*=============

Class宣言

=============*/
class CEffectManager
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CEffectManager();

	//--------------
	//	デストラクタ
	//--------------
	~CEffectManager();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	描画
	void Draw(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	開放
	void Release();

	//	指定されたオブジェクトを返す
	CTextureBase* GetItEffect(int _Id);

	//----------------------------------------------------->
	//	指定されたクラスをインスタンス化してリストに格納する
	//	その後インスタンス化したオブジェクトを返す
	//<-----------------------------------------------------
	template <class T>
	inline T* CreateEffectTask()
	{
		T* temEffect = MyNew T();
		PushEffectManager(temEffect);
		return temEffect;
	}

	//	別のワールドのポインターを自身の管理しているマネージャーに渡す
	void SetShader(CShaderManager* _pShader);


private:


	//	マネージャー
	list  <CTextureBase *> m_listEffContainer;
	
	//	マネージャーリストに指定されたオブジェクトを格納
	void PushEffectManager(CTextureBase* _Object);

};




#endif  _CLASS_NAME_EFFECT_MANAGER_H_