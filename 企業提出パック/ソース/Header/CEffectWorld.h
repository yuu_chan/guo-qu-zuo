/*----------------------------------------------------------------

エフェクト関係の全クラスの親ポインター

----------------------------------------------------------------*/

#ifndef _CLASS_NAME_EFFECT_WORLD_H_
#define _CLASS_NAME_EFFECT_WORLD_H_


/*===============

前方宣言

===============*/

//	エフェクトマネージャー
class CEffectManager;


/*===============

Class宣言

===============*/
class CEffectWorld
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CEffectWorld();

	//--------------
	//	デストラクタ
	//--------------
	~CEffectWorld();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	描画
	void Draw(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	解放
	void Release();


	//	エフェクトマネージャのポインタをゲットする
	shared_ptr<CEffectManager> GetEffectManager(){ return m_spEffectBaseManager; }


private:


	//	エフェクトマネージャ変数
	shared_ptr<CEffectManager> m_spEffectBaseManager;

};

#endif  _CLASS_NAME_EFFECT_WORLD_H_