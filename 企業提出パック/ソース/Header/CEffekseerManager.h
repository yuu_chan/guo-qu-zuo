///*----------------------------------------------------------------
//
//エフェクト描画時に必要なエフェクシア情報管理クラス
//
//
//用意されている初期化関数が存在しているので
//スマートポインタでなく、生のポインタで扱う
//
//----------------------------------------------------------------*/
//
//
//#ifndef _CLASS_NAME_EFFEKSEER_MANAGER_H_
//#define _CLASS_NAME_EFFEKSEER_MANAGER_H_
//
//
///*=============
//
//Class宣言
//
//=============*/
//
//class CEffekSeerManager
//{
//private:
//
//	//----------------
//	//	コンストラクタ
//	//----------------
//	CEffekSeerManager(){}
//
//	//--------------
//	//	デストラクタ
//	//--------------
//	~CEffekSeerManager(){}
//
//
//
//	//	エフェクト描画管理変数
//	::EffekseerRenderer::Renderer*	m_pRender;
//
//	//	エフェクト
//	::Effekseer::Manager*			m_pManager;
//
//	//	最大表示スプライトの枚数
//	static const int				SPRITE_MAX;
//
//
//public:
//
//
//	//----------------------------------------
//	//	インスタンス化された自身のクラスを返す
//	//----------------------------------------
//	static CEffekSeerManager& GetInstance()
//	{
//		static CEffekSeerManager Instance;
//		return Instance;
//	}
//
//
//	//----------
//	//	ゲッター
//	//----------
//
//	::Effekseer::Manager*			GetEffekseer(){ return m_pManager; }
//
//	::EffekseerRenderer::Renderer*	GetRenderer(){ return m_pRender; }
//
//
//	//----------
//	//	セッター
//	//----------
//
//	void SetManager(::Effekseer::Manager* _pManager){ m_pManager = _pManager; }
//
//	void SetRender(::EffekseerRenderer::Renderer* _pRender){ m_pRender = _pRender; }
//
//
//	//	初期化
//	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);
//
//	//	描画
//	void Draw(LPDIRECT3DDEVICE9 _lpD3DDevice);
//
//	//	解放
//	void Release();
//
//
//};
//
//
//#endif  _CLASS_NAME_EFFEKSEER_MANAGER_H_
//
