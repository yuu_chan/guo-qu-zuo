/*--------------------------------------------------

class説明

--------------------------------------------------*/

#ifndef _CLASS_NAME_EVENT_FRAME_H_
#define _CLASS_NAME_EVENT_FRAME_H_


/*=============

Class宣言

=============*/
class CEventFrame : public CTextureBase
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CEventFrame();

	//--------------
	//	デストラクタ
	//--------------
	~CEventFrame();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update();

	//	描画
	void Draw(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	解放
	void Release();


private:


	//	テクスチャのサイズ
	static const int TEXTURE_SIZE;

	RECT		m_Rect;
};

#endif  _CLASS_NAME_EVENT_FRAME_H_