/*----------------------------------------------------------------

三角形面データ

----------------------------------------------------------------*/


#ifndef _CLASS_NAME_FACE_H_
#define _CLASS_NAME_FACE_H_


/*=============

Class宣言

=============*/

class CFace
{
public:


	//----------------
	//	コンストラクタ
	//----------------
	CFace(){}


	//--------------
	//	デストラクタ
	//--------------
	~CFace(){}


	CVector3 v[3];	// ３つの頂点
	CVector3 vN;	// 面の方向ベクトル
	CVector3 min, max;	// 最小点、最大点


	// 三角形のAABB求める
	static void CreateAABB(const D3DXVECTOR3 *v1, const D3DXVECTOR3 *v2, const D3DXVECTOR3 *v3, D3DXVECTOR3 *outMinP, D3DXVECTOR3 *outMaxP)
	{
	
		*outMinP = *v1;
		*outMaxP = *v1;
		
		// 最小点算出
		if (outMinP->x > v2->x)outMinP->x = v2->x;
		if (outMinP->y > v2->y)outMinP->y = v2->y;
		if (outMinP->z > v2->z)outMinP->z = v2->z;
		if (outMinP->x > v3->x)outMinP->x = v3->x;
		if (outMinP->y > v3->y)outMinP->y = v3->y;
		if (outMinP->z > v3->z)outMinP->z = v3->z;
		
		// 最大点算出
		if (outMaxP->x < v2->x)outMaxP->x = v2->x;
		if (outMaxP->y < v2->y)outMaxP->y = v2->y;
		if (outMaxP->z < v2->z)outMaxP->z = v2->z;
		if (outMaxP->x < v3->x)outMaxP->x = v3->x;
		if (outMaxP->y < v3->y)outMaxP->y = v3->y;
		if (outMaxP->z < v3->z)outMaxP->z = v3->z;
	}


	// ３つの頂点をセット
	void SetVertex(const D3DXVECTOR3* v0, const D3DXVECTOR3* v1, const D3DXVECTOR3* v2)
	{
		v[0].Set(v0);
		v[1].Set(v1);
		v[2].Set(v2);

		// 面の方向を求める
		Calc_vN();
		// AABBデータを作成
		Calc_AABB();
	}

	// v[0]〜v[2]の情報からvNを算出
	void Calc_vN()
	{
		D3DXVECTOR3 v1 = v[1] - v[0];
		D3DXVECTOR3 v2 = v[2] - v[0];
		CVector3::Cross(&vN, &v1, &v2);	// 外積
		vN.Normalize();
	}

	// v[0]〜v[2]のAABBを算出
	void Calc_AABB()
	{
		// 最大点、最小点
		CreateAABB(&v[0], &v[1], &v[2], &min, &max);
	}

};


#endif  _CLASS_NAME_FACE_H_

