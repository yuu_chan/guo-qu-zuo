/*--------------------------------------------------

FadeIn FadeOutの演出を管理

管理する変数がかなり多いのでできるだけ少なくする

--------------------------------------------------*/

#ifndef _CLASS_NAME_FADE_H_
#define _CLASS_NAME_FADE_H_


/*=============

namespace

=============*/
namespace FADE
{
	/*
	
		ステップ(状態)管理
		IN OUT がマクロで定義されているので
		先頭にfadeのfをつける
	
	*/
	enum STEP
	{
		fIN  = 1,
		fOUT = 2,
		END  = 3,
	};

}


/*=============

マクロ定義

=============*/
#define GET_FADE CFade::GetInstance()


/*=============

Class宣言

=============*/
class CFade : public CTextureBase
{
private:

	//----------------
	//	コンストラクタ
	//----------------
	CFade();

	//--------------
	//	デストラクタ
	//--------------
	~CFade();


	//	フェードインフェードアウトを行う
	bool m_EnableFade;	//	true(行う) false(行わない)

	//	フェードインが終了した
	bool m_AbleEnd;

	//	フェードアウトが終了した
	bool m_AbleStart;	//	true(フェードアウト完了) false(未完了)

	//	読み込みが終わった
	bool m_Load;

	//	α値用変数
	int  m_Alpha;

	//	ステップ実行
	int  m_Step;

	//	アルファ値を記憶させておく
	int  m_MemoryAlpha;


public:


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update();

	//	描画
	void Draw(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	解放
	void Release();


	//----------
	//	ゲッター
	//----------

	//	フェードアウトが終わったかどうかを返す
	bool GetAbleStart(){ return m_AbleStart; }

	//	ロード終了したかどうかを返す
	bool GetLoadEnd(){ return m_Load; }

	//	重複をさせないようにフラグ返す
	bool GetAbleEnd(){ return m_AbleEnd; }

	//	ステップを返す
	int  GetStep(){ return m_Step; }


	//----------
	//	セッター
	//----------

	//	フェードインを行うかどうか
	void SetEnableFade(bool _Fade){ m_EnableFade = _Fade; }

	//	ロードが終了したかどうか
	void SetLoadEnd(bool _Load){ m_Load = _Load; }

	//	フェードインフェードアウトを重複させない
	void AbleEnd(bool _End){ m_AbleEnd = _End; }


	//------------------------------
	//	インスタンス化した変数を返す
	//------------------------------
	static CFade& GetInstance()
	{
		static CFade Instance;
		return Instance;
	}
};

#endif  _CLASS_NAME_FADE_H_