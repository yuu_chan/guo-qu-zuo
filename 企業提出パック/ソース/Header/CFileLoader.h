/*----------------------------------------------------------------

	ファイルローダークラス
	
	オブジェクトのインスタンスや座標セットとかを読み込む


	LoadとGimmickLoadの中身が同じで
	変更点がManagerのみなので
	template化しておく

----------------------------------------------------------------*/

#ifndef _CLASS_NAME_FILE_LOADER_H_
#define _CLASS_NAME_FILE_LOADER_H_


/*===============

前方宣言

===============*/

//	オブジェクトベース
class CObjectBase;

//	オブジェクトマネージャ
class CObjectManager;

//	ギミックのマネージャ
class CGimmickManager;

//	エフェクトマネージャ
class CEffectManager;


/*===============

Class宣言

===============*/
class CFileLoader
{
public:
	
	//----------------
	//	コンストラクタ
	//----------------
	CFileLoader();

	//--------------
	//	デストラクタ
	//--------------
	~CFileLoader();


	/*Load-------------------------------------------->

	外部ファイル読み込み関数
	ゲームに使用するオブジェクトをインスタンス化して
	リストに格納する

	_FileName	 :	ファイルパス
	_lpD3DDevice :	デバイス
	_List		 :	格納するリスト

	return		 :	戻り値なし

	<------------------------------------------------*/
	//template <class T>
	void Load(
		string						_FileName,
		LPDIRECT3DDEVICE9			_lpD3DDevice,
		shared_ptr<CObjectManager>	_pObjManager
		);


	/*TextureTxtLoad-------------------------------------------->

	外部ファイル読み込み関数
	テクスチャ関係のテキストを読み込む

	_FileName		:	ファイルパス
	_lpD3DDevice&	:	デバイス
	_List*			:	格納するリスト

	return			:	戻り値なし

	<------------------------------------------------*/
	void TextureTxtLoad(
		string							_FileName,
		LPDIRECT3DDEVICE9				_lpD3DDevice,
		shared_ptr<CEffectManager>		_spObjManager
		);


	/*Load消す予定-------------------------------------------->

	外部ファイル読み込み関数

	_FileName	 :	ファイルパス
	_lpD3DDevice :	デバイス
	_pvecPos	 :	格納するリスト

	return		 :	戻り値なし

	<------------------------------------------------*/
	void PointLoad(
		string _FileName,
		LPDIRECT3DDEVICE9 _lpD3DDevice,
		vector<CVector3>* _pvecPos
		);


	/*GetSplit--------------------------------->

	指定した文字を切り取って
	データとなる文字列を返す
	戻り値として切り取った文字列を返す

	_Str	:	元となる文字列
	_Split	:	切り取りたい文字列(複数化)

	return	:	vector型

	<----------------------------------------*/
	vector<string> GetSplit(
		string _Str,
		string _Split
		);


	/*(template)CreateGameObject---------------------->
	
	ゲームワールドへオブジェクトをPushし、
	初期化処理を行う
	戻り値として、プッシュされたオブジェクトの数を返す
	
	template			:	プッシュしたいClass

	_pObjectManager		:	ゲームのマネージャー
	_lpD3DDevice		:	デバイス
	_SetItemId			:	アイテムのID
	_Filter				:	フィルター
	_RegisterRigidType	:	登録する剛体
	_ObjectWeight		:	アイテムの重さ
	_RegisterTag		:	登録したいタグ
	_SetPosition		:	座標
	_SetScal			:	拡大サイズ
	_Angle				:	角度

	return				:	int型

	<------------------------------------------------*/
	template <class T>
	inline int CreateGameObject(
		shared_ptr<CObjectManager>		_pObjectManager,
		LPDIRECT3DDEVICE9				_lpD3DDevice,
		INT								_SetItemId,
		INT								_ItemResilience,
		UINT							_Filter,
		FLOAT							_ObjectWeight,
		string							_RegisterTag,
		CVector3						_SetPosition,
		CVector3						_SetScal,
		CVector3						_Angle
		)
	{
		//	リストにインスタンス化したオブジェクトをプッシュする
		shared_ptr<T> pObject = _pObjectManager->CreateObjectTask<T>();

		//	マネージャーへの登録IDをセットする
		pObject->SetId(m_AddObjectCnt);

		//　拡大サイズをセット
		pObject->SetScaling(_SetScal);

		//	初期座標をセット
		pObject->SetPosition(_SetPosition);

		//	回転量をセット
		pObject->SetAngle(_Angle);

		//	アイテムIDを登録する
		pObject->SetItemId(_SetItemId);

		//	フィルターを登録する
		pObject->SetFilter(_Filter);

		//	回復量を設定する
		pObject->SetItemResilience(_ItemResilience);

		//	オブジェクトの重量を設定
		pObject->SetObjectWeight(_ObjectWeight);

		//	登録タグ
		pObject->SetRegisterTag(_RegisterTag);

		//	オブジェクトの初期化をする
		pObject->Init(_lpD3DDevice);

		//	カウンターを増やして返す
		return m_AddObjectCnt += 1;
	}

	//template <class T>
	//inline int CreateGameObject(
	//	shared_ptr<CObjectManager>		_pObjectManager,
	//	LPDIRECT3DDEVICE9				_lpD3DDevice,
	//	INT								_SetItemId,
	//	INT								_ItemResilience,
	//	UINT							_Filter,
	//	FLOAT							_ObjectWeight,
	//	string							_RegisterTag,
	//	CVector3						_SetPosition,
	//	CVector3						_SetScal,
	//	CVector3						_Angle
	//	)
	//{
	//	//	リストにインスタンス化したオブジェクトをプッシュする
	//	T* pObject = _pObjectManager->CreateObjectTask<T>();

	//	//	マネージャーへの登録IDをセットする
	//	pObject->SetId(m_AddObjectCnt);

	//	//　拡大サイズをセット
	//	pObject->SetScaling(_SetScal);

	//	//	初期座標をセット
	//	pObject->SetPosition(_SetPosition);

	//	//	回転量をセット
	//	pObject->SetAngle(_Angle);

	//	//	アイテムIDを登録する
	//	pObject->SetItemId(_SetItemId);

	//	//	フィルターを登録する
	//	pObject->SetFilter(_Filter);

	//	//	回復量を設定する
	//	pObject->SetItemResilience(_ItemResilience);

	//	//	オブジェクトの重量を設定
	//	pObject->SetObjectWeight(_ObjectWeight);

	//	//	登録タグ
	//	pObject->SetRegisterTag(_RegisterTag);

	//	//	オブジェクトの初期化をする
	//	pObject->Init(_lpD3DDevice);

	//	//	カウンターを増やして返す
	//	return m_AddObjectCnt += 1;
	//}






	/*(template)CreateGameObject---------------------->

	ゲームワールドへオブジェクトをPushし、
	初期化処理を行う
	戻り値として、プッシュされたオブジェクトの数を返す

	template			:	プッシュしたいClass

	_pObjectManager		:	ゲームのマネージャー
	_lpD3DDevice		:	デバイス
	_SetItemId			:	アイテムのID
	_ItemResilience		:	アイテムの回復量
	_Filter				:	フィルター
	_RegisterRigidType	:	登録する剛体
	_ObjectWeight		:	アイテムの重さ
	_SetPosition		:	座標
	_SetScal			:	拡大サイズ
	_Angle				:	角度

	return				:	int型

	<------------------------------------------------*/
	template <class T>
	inline int CreateGimmick(
		shared_ptr<CGimmickManager>		_pGimmcikManager,
		LPDIRECT3DDEVICE9				_lpD3DDevice,
		INT								_SetItemId,
		INT								_ItemResilience,
		UINT							_Filter,
		FLOAT							_ObjectWeight,
		CVector3						_SetPosition,
		CVector3						_SetScal,
		CVector3						_Angle
		)
	{
		//	リストにインスタンス化したオブジェクトをプッシュする
		T* pObject = _pGimmcikManager->CreateGimmickTask<T>();

		//	マネージャーへの登録IDをセットする
		pObject->SetId(m_AddGimmick);

		//　拡大サイズをセット
		pObject->SetScaling(_SetScal);

		//	初期座標をセット
		pObject->SetPosition(_SetPosition);

		//	回転量をセット
		pObject->SetAngle(_Angle);

		//	アイテムIDを登録する
		pObject->SetItemId(_SetItemId);

		//	フィルターを登録する
		pObject->SetFilter(_Filter);

		//	回復量を設定する
		pObject->SetItemResilience(_ItemResilience);

		//	オブジェクトの重量を設定
		pObject->SetObjectWeight(_ObjectWeight);

		//	オブジェクトの初期化をする
		pObject->Init(_lpD3DDevice);

		//	カウンターを増やして返す
		return m_AddGimmick += 1;
	}


	/*(template)CreateTextureObject---------------------->

	テクスチャワールドへオブジェクトをPushし、
	初期化処理を行う
	戻り値として、プッシュされたオブジェクトの数を返す

	template			:	プッシュしたいClass

	_pObjectManager*	:	ゲームのマネージャー
	_lpD3DDevice		:	デバイス
	_vPos,				:	セットしたい座標
	_vScaleSize,		:	拡大サイズ
	_vAngle,			:	角度
	_vTextureSize		:	テクスチャのサイズ(u, v, w)
	_EffectId			:	登録するエフェクトのID
	_DivisionNum		:	画像の分割数

	return				:	int型

	<---------------------------------------------------*/
	template <class T>
	int CreateTextureObject(
		shared_ptr<CEffectManager> _pObjectManager,
		LPDIRECT3DDEVICE9			_lpD3DDevice,
		CVector3					_vPos,
		CVector3					_vScaleSize,
		CVector3					_vAngle,
		CVector3					_vTextureSize,
		int							_EffectId,
		int							_DivisionNum
		)
	{	
		//	リストにインスタンス化したオブジェクトをプッシュする
		T* pObject = _pObjectManager->CreateEffectTask<T>();

		pObject->SetListId(m_AddTextureCnt);

		pObject->SetPosition(_vPos);

		pObject->SetScale(_vScaleSize);

		pObject->SetAngle(_vAngle);

		pObject->SetTextureSize(_vTextureSize);

		pObject->SetDivisionUNum(_DivisionNum);

		pObject->Init(_lpD3DDevice);
	
		pObject->SetEffectId(_EffectId);

		//	カウンターを増やして返す
		return m_AddTextureCnt += 1;
	}


	//----------
	//	ゲッター
	//----------

	//	オブジェクトの数を返す
	int GetCnt(){ return m_AddObjectCnt; }

	//	テクスチャの数を返す
	int GetAddTextureCnt(){ return m_AddTextureCnt; }

	//	ギミックを追加した数を返す
	int GetAddGimmick(){ return m_AddGimmick; }

private:


	//	ゲームオブジェクトの追加数
	int m_AddObjectCnt;

	//	テクスチャオブジェクトの追加数
	int m_AddTextureCnt;

	//	ギミックを追加した数を記憶
	int m_AddGimmick;

};




#endif  _CLASS_NAME_FILE_LOADER_H_