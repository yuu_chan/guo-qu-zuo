/*----------------------------------------------------------------


Class CBulletWorld

物理ワールドを管理する


===================================================================


Class CGameScene

ゲームシーンを管理


----------------------------------------------------------------*/

#ifndef _CLASS_NAME_GAME_H_
#define _CLASS_NAME_GAME_H_


#include "CSceneBase.h"

/*=============

前方宣言

=============*/

//	ゲームワールド
class CGameWorld;

//	エフェクトワールド
class CEffectWorld;

//	実績ワールド
class CAchievementWorld;

//	シェーダーマネージャー
class CShaderManager;

//	ゴール
class CGoal;

//	スタート
class CStart;


/*=============

マクロ定義

=============*/

#define GET_BULLET_WORLD CBulletWorld::GetInstance()


/*=============

Class宣言

=============*/

class CBulletWorld
{
private:
	CBulletWorld(){}
	~CBulletWorld(){}

public:

	static CBulletWorld& GetInstance()
	{
		static CBulletWorld Instance;

		return Instance;
	}

	MyBulletWorld m_pBulletWorld;
};


class CGameScene : public CSceneBase
{
public:
	
	//----------------
	//	コンストラクタ
	//----------------
	CGameScene();

	//--------------
	//	デストラクタ
	//--------------
	~CGameScene();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	描画
	void Draw(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	解放
	void Release();


	shared_ptr<CGameWorld> GetGameWorld(){ return m_spGameWorld; }


private:


	//	ゲームワールド
	//CGameWorld* m_pGameWorld;
	shared_ptr<CGameWorld> m_spGameWorld;

	//	エフェクトワールド
	shared_ptr<CEffectWorld> m_spEffectWorld;

	//	シェーダーマネージャー
	//CShaderManager* m_pShader;

	//	実績解除
	CAchievementWorld* m_pAchievementWorld;

	//	ゲームゴール
	CGoal* m_pGoal;

	//	ゲームスタート
	CStart* m_pStart;

};


#endif  _CLASS_NAME_GAME_H_