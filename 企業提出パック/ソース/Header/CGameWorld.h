/*----------------------------------------------------------------

ゲーム関係の全クラスの親ポインター

----------------------------------------------------------------*/

#ifndef _CLASS_NAME_GAME_WORLD_H_
#define _CLASS_NAME_GAME_WORLD_H_


/*===============

前方宣言

===============*/

//	オブジェクトマネージャー
class CObjectManager;

//	ギミックマネージャー
class CGimmickManager;


/*===============

Class宣言

===============*/
class CGameWorld
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CGameWorld();

	//--------------
	//	デストラクタ
	//--------------
	~CGameWorld();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	描画
	void Draw(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	解放
	void Release();


	//	オブジェクトマネージャのポインタを返す
	shared_ptr<CObjectManager> GetObjectManager(){ return m_spObjectBaseManager; }

	//	先頭のオブジェクトを返す
	int GetBegin(){ return m_BeginId; }

	//	最後にプッシュされたオブジェクトを返す
	int GetEnd(){ return m_EndId; }

	//	最後にプッシュされたオブジェクトの数をセットする
	void SetEnd(int _EndId) { m_EndId = _EndId; }

	//	プッシュされたオブジェクトが削除された場合、オブジェクトの数を-1しておく
	void DeleteEndId(){ m_EndId -= 1; }


private:


	//	オブジェクトマネージャ
	shared_ptr<CObjectManager> m_spObjectBaseManager;

	//	先頭IDを設定
	int m_BeginId;

	//	後ろのIDを設定
	int m_EndId;


	/*StageInit---------------------------------------------------

	更新、描画、開放は
	マネージャー側で、プッシュされた
	オブジェクトの分で行うので
	呼び出すステージごとにそれ専用のプッシュ関数を使用する

	現在のステージの種類は
	Stage1
	Stage2
	Stage3


	_TxtFileName	:	ステージの情報が入ったテキストファイルパス
	_lpD3DDevice	:	デバイス

	return			:	戻り値なし

	------------------------------------------------------------*/

	void StageInit(string _TxtFileName, LPDIRECT3DDEVICE9 _lpD3DDevice);

};

#endif  _CLASS_NAME_GAME_WORLD_H_