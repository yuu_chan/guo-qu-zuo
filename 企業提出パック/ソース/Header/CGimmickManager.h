/*----------------------------------------------------------------

ギミック関係のリストを扱うクラス
ゲームシーンで使用する
マネージャーの登録先はゲームワールドにする

----------------------------------------------------------------*/

#ifndef _CLASS_NAME_GIMMICK_MANAGER_H_
#define _CLASS_NAME_GIMMICK_MANAGER_H_


/*=============

Class宣言

=============*/

class CGimmickManager
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CGimmickManager();

	//--------------
	//	デストラクタ
	//--------------
	~CGimmickManager();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	描画
	void Draw(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	開放
	void Release();

	//	指定されたオブジェクトを返す
	CObjectBase* GetItObject(int _Id);

	//	指定されたクラスをインスタンス化してリストに格納する
	template <class T>
	inline T* CreateGimmickTask()
	{
		T* temGimmick = MyNew T();

		PushGimmcikManager(temGimmick);

		return temGimmick;
	}


	//	マネージャーのポインタを返す
	list<CObjectBase *> GetGimmickContainer(){ return m_listGimmcikContainer; }

	//	シェーダーのポインターを自身の管理している派生クラスに渡す
	void SetShaderManager(CShaderManager* _pShader);

private:


	//	マネージャー
	list<CObjectBase *> m_listGimmcikContainer;

	//	マネージャーリストに指定されたオブジェクトを格納する
	void PushGimmcikManager(CObjectBase* _Object);


};


#endif  _CLASS_NAME_GIMMICK_MANAGER_H_

