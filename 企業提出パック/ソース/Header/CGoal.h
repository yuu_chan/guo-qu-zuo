/*--------------------------------------------------

class説明

--------------------------------------------------*/

#ifndef _CLASS_NAME_GOAL_H_
#define _CLASS_NAME_GOAL_H_


/*=============

前方宣言

=============*/

//	ゲームワールド
class CGameWorld;

//	エフェクトワールド
class CEffectWorld;


/*=============

Class宣言

=============*/
class CGoal
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CGoal();

	//--------------
	//	デストラクタ
	//--------------
	~CGoal();


	//----------
	//	セッター
	//----------

	//	ゲームワールドのポインターをセットする
	void SetGameWorld(shared_ptr<CGameWorld> _pGameWorld){ m_pGameWorld = _pGameWorld; }

	//	エフェクトワールドのポインターをセットする
	void SetEffectWorld(shared_ptr<CEffectWorld> _pEffectWorld){ m_pEffectWorld = _pEffectWorld; }


	//----------
	//	ゲッター
	//----------

	//	ゲームワールドのポインターを返す
	shared_ptr<CGameWorld> GetGameWorld(){ return m_pGameWorld.lock(); }

	//	エフェクトワールドのポインターを返す
	shared_ptr<CEffectWorld> GetEffectWorld(){ return m_pEffectWorld.lock(); }


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lPD3DDevice);

	//	ゴールイベント
	void GoalEvent(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	開放
	void Release();


private:


	//	ゲームワールド
	//CGameWorld*		m_pGameWorld;
	weak_ptr<CGameWorld> m_pGameWorld;

	//	エフェクトワールド
	//CEffectWorld*	m_pEffectWorld;
	weak_ptr<CEffectWorld> m_pEffectWorld;

	//	ポジション
	CVector3		m_vGoalPos;

	//	上側の淵の拡大量
	CVector3		m_vUpScale;

	//	下側の淵の拡大量
	CVector3		m_vDownScale;

	//	イベントが終わったかどうか
	bool m_EventEnd;


};

#endif  _CLASS_NAME_GOAL_H_