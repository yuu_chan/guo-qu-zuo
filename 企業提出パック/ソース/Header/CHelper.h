/*-----------------------------------------------------------------


クラス名	:	CVFData

視錘台カリングを行うさいに、
カメラからの外積を求めるために必要な変数をまとめておく


===================================================================


クラス名	:	COBB

オブジェクトの四角形同士の判定をするためのステータスを保存してある
クラス


===================================================================


クラス名	:	CSearch

中心座標となるオブジェクトからそれ以外のオブジェクトまでの
オブジェクトのIDを距離を保管しておくためのクラス


===================================================================


クラス名	:	CHelper

判定式などを行ってくれる関数
当たり判定　球判定　距離判定　視錘台カリング
などをしてくれる

いつでも関数を使用できるようにするためにシングルトンで生成


===================================================================


クラス名	:	 CWorldPointer

外部ワールドのポインターのコピーをもっておく
ワールドにポインターを渡さずとも、
このクラスを呼べば、外部ワールドのクラスにアクセスすることが可能

weak_ptrでポインターを作っているため
メインのゲームワールドのポインターが開放されれば、
こいつのポインターも一緒に開放される。
ゲッターはshared_ptrで返される


-----------------------------------------------------------------*/


#ifndef _CLASS_NAME_HELPER_H_
#define _CLASS_NAME_HELPER_H_


/*=======================

マクロ定義

=======================*/

#define GET_HELPER CHelper::GetInstance()

#define GET_WORLD  CWorldPointer::GetInstance()


/*=======================

前方宣言

=======================*/

//	ゲームワールド
class CGameWorld;

//	実績解除クラスのワールド
class CAchievementWorld;

//	エフェクトワールド
class CEffectWorld;

//	オブジェクトマネージャー
class CObjectManager;


/*=======================

Class宣言

=======================*/

class CVFData
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CVFData(){};


	//--------------
	//	デストラクタ
	//--------------
	~CVFData(){};


	//	カメラの座標
	CVector3	m_vCamPos;

	//	カメラから法線
	CVector3	m_vN[4];
};


class COBB
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	COBB(){};

	
	//--------------
	//	デストラクタ
	//--------------
	~COBB(){};


	//----------
	//	ゲッター
	//----------

	// 位置を取得
	CVector3	GetPos();

	// 指定軸番号の方向ベクトルを取得
	CVector3	GetDirect(int _Elem){ return m_vNormal[_Elem]; }

	// 指定軸方向の長さを取得
	FLOAT		GetLen(int _Elem){ return m_Length[_Elem]; }


protected:


	// 位置
	CVector3 m_vPos;


	//------------------------------------------------------
	//	配列のサイズが固定なので、後で固定長配列クラスを使う
	//------------------------------------------------------

	// 方向ベクトル
	CVector3 m_vNormal[3];
	
	// 各軸方向の長さ
	FLOAT m_Length[3];


};


class CSearch
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CSearch();

	//--------------
	//	デストラクタ
	//--------------
	~CSearch();


	//	初期化
	static void Init();


	/*DistanceIsShortId--------------------------------------->

	オブジェクトの距離を測り、中心となる座標から一番近い
	オブジェクトのIDだけを検知して、IDを保持しておく

	_SenterPointId		:	中心となる座標を持つオブジェクト
	_TargetPointId		:	比較する対象のオブジェクト
	_pGameWorld*		:	ゲームワールドのポインタ

	return				:	戻り値無し

	<--------------------------------------------------------*/
	static void DistanceIsShortId(
		int						_SenterPointId,
		int						_TargetPointId,
		shared_ptr<CGameWorld>	_pGameWorld
		);

	static void Distance(
		int						_SenterPointId,
		int						_TargetPointId,
		shared_ptr<CGameWorld>	_pGameWorld
		);


	//----------
	//  ゲッター
	//----------

	//	検索結果のIDを返す
	static int	 GetSearchID(){ return m_Search.first; }

	//	検索結果の距離を返す
	static float GetSearchDistance(){ return m_Search.second; }


private:


	//	オブジェクトのIDと距離を保存しておく変数
	static pair<int, float> m_Search;


};


class CHelper
{
private:

	//----------------
	//	コンストラクタ
	//----------------
	CHelper();

	//--------------
	//	デストラクタ
	//--------------
	~CHelper();


public:


	static CHelper& GetInstance()
	{
		static CHelper Instance;
		return Instance;
	}


	void SetAchievementWorld(CAchievementWorld* _pAchie) { m_pAchievementWorld = _pAchie; }


	/*GameWorldStandPosition-------------------------------------------------------->
		ゲームワールド内オブジェクトの足場更新コリジョン関数						
		
		_CollisionObject_Id_1 ->	足場を更新したいオブジェクトのID				
		_CollisionObject_Id_2 ->	足場を更新する際に基準となるメッシュのID		
		_StandAdjustment	  ->	足場の調整										
		_Flg				  ->	フラグをしようする場合は値を入れておく	
	<------------------------------------------------------------------------------*/
	void GameWorldStandPosition(
		int		_CollisionObject_Id_1,
		int		_CollisionObject_Id_2,
		float	_StandAdjustment,
		bool&	_Flag
		);


	///*--------------------------------------------------------------->
	//	ゲームワールド内のオブジェクト同士の判定
	//	レイを好きな方向に飛ばし、距離とヒット判定を取る
	//	ヒット判定が取れれば、
	//	_Id_1のオブジェクトに向かって
	//	_Id_2のオブジェクトが移動してくる
	//	距離によって、ステータスを変更

	//	_CollisionObject_Id_1 ->	当たり判定を起こしたいオブジェクト
	//	_CollisionObject_Id_2 ->	当たり判定を起こされたオブジェクト
	//	_RayDir				  ->	レイを飛ばす方向
	//<---------------------------------------------------------------*/
	//void GameWorldDoAbsorption(
	//	int			_CollisionObject_Id_1,
	//	int			_CollisionObject_Id_2,
	//	CVector3	_RayDir	= CVector3(0, 0, 1)
	//	);


	/*WallScratch---------------------------------------------------->
		壁ずりを行う関数
		メッシュ同士の当たり判定を行う。相手メッシュの法線を
		計算し、当たり判定がでているかどうか、
		当たり判定がでたら、位置を修正し、直した座標を返す

		_CollisionObject_Id_1	:	当たり判定を起こしたいオブジェクト
		_CollisionObject_Id_2	:	当たり判定を起こされたオブジェクト
		_RayDir					:	レイを飛ばす方向
		_BuckPushing			:	推し戻される量
	<---------------------------------------------------------------*/
	void WallScratch(
		int			_CollisionObject_Id_1,
		int			_CollisionObject_Id_2,
		CVector3	_Ray			= CVector3(0, 0, 1),
		CVector3	_RayDir			= CVector3(0, 0, 1),
		float		_BuckPushing	= 1.0f
		);


	/*GetDistance------------------------------------------------------------------------>
		距離判定で使用するオブジェクト同士の距離を計算してくれる関数
		float型でオブジェクトまでの距離を返してくれる


		_vPosA		:	中心となる座標
		_vPosB		:	比較したい対象の座標

		return		:	戻り値 float型
	<-----------------------------------------------------------------------------------*/
	float GetDistance(
		CVector3 _vSenterPointPos,
		CVector3 _vTargetPointPos
		);


	/*UnderWaterDecision----------------------------------------------->
		雲とか水の中に入ったときにキャラクターの動きを制限する
		上側にレイを発射し、IDを検索して指定IDにヒットすれば
		プレイヤーの落下速度をいじる

		_CollisionObject_Id_1	:	判定を行いたいID
		_CollisionObject_Id_2	:	判定を行われるID(検索IDとしても使用)
		_UnderWaterDis			:	水深を返してもらう　いらなければNULL
		_RayDir					:	レイの発射方向
		_RayPos					:	レイの発射座標

		return					:	戻り値 あり(bool)
	<-----------------------------------------------------------------*/
	bool UnderDecision(
		int			_CollisionObject_Id_1,
		int			_CollisionObject_Id_2,
		float&		_UnderWaterDis,
		CVector3	_RayDir = CVector3(0.0f, 1.0f, 0.0f),
		CVector3	_AddPos = CVector3(0.0f, 0.0f, 0.0f));


	/*RayDistanceCollision----------------------------------------------->
		単純に、レイを発射した先のメッシュまでの距離を返す関数
		基本的には地面と水面に飛ばす

		_CollisionObject_Id_1	:	判定を行いたいID
		_CollisionObject_Id_2	:	判定を行われるID(検索IDとしても使用)
		_RayDir					:	レイの発射方向
		_RayPos					:	レイの発射座標

		return					:	戻り値 あり(float)
	<-------------------------------------------------------------------*/
	float RayDistanceCollision(
		int			_CollisionObject_Id_1,
		int			_CollisionObject_Id_2,
		CVector3	_RayDir = CVector3(0.0f, 1.0f, 0.0f),
		CVector3	_AddPos = CVector3(0.0f, 0.0f, 0.0f)
		);


	/*Sphere------------------------------------------------------------>
		球どうしの判定
		接触すればtrueミスればfalse

		_CollisionId_1			:	当たりたい側
		_CollisionId_2			:	当てたい側
	
		return					:	戻り値 bool型
	<------------------------------------------------------------------*/
	bool Sphere(
		int _CollisionId_1,
		int _CollisionId_2
		);


	/*RangeDecision----------------------------------------------------->

	範囲内判定	入っていればtrue 入っていなければfalseを返す
	指定したオブジェクト同士の距離を測って、
	一定範囲内に入っているかどうかを判定

	_ObjectId_1				:	中心となるオブジェクト
	_ObjectId_2				:	距離を測りたい対象のオブジェクト
	_Range					:	指定したい範囲

	return	:	戻り値 bool型

	<------------------------------------------------------------------*/
	bool RangeDecision(
		int		_ObjectId_1,
		int		_ObjectId_2,
		float	_Range
		);


	/*ViewFrustum------------------------------------------------------->
	オブジェクトが視錘台範囲内に入っているかどうかを調べる
	

	_ListId			:	リストのID
	_pVf*			:	外積などのデータを求めておく


	return			:	戻り値 bool型
	<------------------------------------------------------------------*/
	bool ViewFrustum(
		int			_ListId, 
		CVFData*	_pVf
		);


	/*CreateViewFrustum---------------------------------------->
	視錘台範囲を作るために、外積を求めておく関数

	_pVf*			:	外積の値を保管しておくための変数

	return			:	戻り値無し
	<---------------------------------------------------------*/

	void CreateViewFrustum(
		CVFData* _pOut
		);


	/*RangeDoAbsorption---------------------------------------------->residue
	
	一定範囲内のオブジェクトに対し、吸い込み処理を行う
	中心座標から範囲をある程度、決めその範囲内にあるオブジェクトが動く
	レイは外積を求めるために使用。念のため外部から飛ばす方向を
	替えれるようにしておく

	_ObjectId_1		:	中心となる座標を持っているオブジェクトのID
	_ObjectId_2		:	それ以外のオブジェクトのID
	_Range			:	一定範囲を決れるようにする
	_RayDir			:	レイを飛ばしたい方向

	return			:	戻り値無し

	<---------------------------------------------------------------*/
	void RangeDoAbsorption(
		int			_ObjectId_1,
		int			_ObjectId_2,
		float		_Range,
		CVector3	_RayDir = CVector3(0, 0, 1)
		);
	
	bool RangeDoAbsorption(
		int			_ObjectId_1,
		int			_ObjectId_2
		);


	/*GetPercent---------------------------------------------------->

	残り何%かを取得する関数
	結果をfloatで返す

	_WholeValues			:	全体の値
	_CurrentValues			:	現在の値

	return					:	戻り値 float型
	
	<--------------------------------------------------------------*/
	float GetPercent(
		float _WholeValues,
		float _CurrentValues
		);


	/*WindZone------------------------------------------------------>

	風が吹いている空間に入ったら、押し出されるという情報を与える
	trueが帰ってくると、風の吹いている空間
	falseが帰ってくると風何もなし

	_spObjectContainer		:	ゲームに使用されるオブジェクトが格納
								されているコンテナ


	return					:	戻り値 bool型

	<--------------------------------------------------------------*/
	bool WindZone(
		const shared_ptr<CObjectManager> _spObjectContainer
		);


	/*LenSegOnSeparateAxis----------------------------------------->
	
	分離軸に投影された軸成分から投影線分長を算出

	
	
	<-------------------------------------------------------------*/
	FLOAT LenSegOnSeparateAxis(
		CVector3* Sep, 
		CVector3* e1, 
		CVector3* e2, 
		CVector3* e3 = 0
		);


	/*ColOBBs------------------------------------------------------>

	OBB v.s. OBB


	<-------------------------------------------------------------*/
	bool ColOBBs(COBB &obb1, COBB &obb2);


private:


	//	実績系
	CAchievementWorld* m_pAchievementWorld;

};


class CWorldPointer
{
private:
	
	//----------------
	//	コンストラクタ
	//----------------
	CWorldPointer(){}

	//--------------
	//	デストラクタ
	//--------------
	~CWorldPointer(){}


public:


	static CWorldPointer& GetInstance()
	{
		static CWorldPointer Instance;
		return Instance;
	}


	//----------
	//	ゲッター
	//----------

	//static CGameWorld* GetGameWorld(){ return m_weGameWorld; }

	//static CEffectWorld* GetEffectWorld(){ return m_pEffectWorld; }

	shared_ptr<CGameWorld>	GetGameWorld(){ return m_weGameWorld.lock(); }

	shared_ptr<CEffectWorld>	GetEffectWorld(){ return m_weEffectWorld.lock(); }


	//----------
	//	セッター
	//----------

	//static void SetGameWorld(CGameWorld* _pGameWorld){ m_pGameWorld = _pGameWorld; }
	//
	//static void SetEffectWorld(CEffectWorld* _pEffectWorld){ m_pEffectWorld = _pEffectWorld; }

	void SetGameWorld(shared_ptr<CGameWorld> _pGameWorld){ m_weGameWorld = _pGameWorld; }

	void SetEffectWorld(shared_ptr<CEffectWorld> _pEffectWorld){ m_weEffectWorld = _pEffectWorld; }


	/*Release--------------------------------------->
	
	クラス内のポインターをすべての中身をチェックして
	中身が残っていた場合はデータを破棄する
	trueが替えれば開放成功
	falseの場合は開放失敗


	戻り値		:	bool型


	<----------------------------------------------*/
	bool Release()
	{

		//--------------------------------------------------
		//	各ポインターが開放されているかどうかを確認して
		//	開放されていない場合は開放する
		//
		//	そもそもweak_ptrで変数を確保しているので
		//	開放ミスはありえない
		//--------------------------------------------------
		
		if (m_weGameWorld.lock() != nullptr)
		{
			m_weGameWorld.lock() = nullptr;
		}

		if (m_weEffectWorld.lock() != nullptr)
		{
			m_weEffectWorld.lock() = nullptr;
		}

		return true;
	}


private:


	/*static CGameWorld*   m_pGameWorld;
	static CEffectWorld* m_pEffectWorld;*/

	weak_ptr<CGameWorld>		m_weGameWorld;
	weak_ptr<CEffectWorld>	m_weEffectWorld;
};


#endif  _CLASS_NAME_HELPER_H_