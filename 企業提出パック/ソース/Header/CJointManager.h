///*----------------------------------------------------------------
//
//物理剛体のジョイントを作成して管理するクラス
//
//今は、あらかじめ作成したいジョイントをCreate関数に入れておく
//
//イチゴ(複数)とイチゴの茎を指定
//
//----------------------------------------------------------------*/
//
//#ifndef _CLASS_NAME_AA_H_
//#define _CLASS_NAME_AA_H_
//
//
///*=============
//
//Class宣言
//
//=============*/
//
//class CJointManager
//{
//public:
//
//	//----------------
//	//	コンストラクタ
//	//----------------
//	CJointManager();
//
//	//--------------
//	//	デストラクタ
//	//--------------
//	~CJointManager();
//
//
//	//	初期化
//	void Init();
//
//	//	ジョイントを作成
//	void Create();
//
//	//	解放
//	void Release()
//	{
//		auto ItObjContainer = m_welJoint.begin();
//
//		while (ItObjContainer == m_welJoint.end())
//		{
//			(*ItObjContainer) = nullptr;
//
//			++ItObjContainer;
//		}
//
//		return;
//	}
//
//
//	//	Pushする
//	template <class T>
//	shared_ptr<T>& CreateJoint()
//	{
//		//	シェアドポインタを生成
//		temObject = make_shared<T>();
//
//
//
//		return temObject;
//	}
//
//
//	//	マネージャーを返す(参照カウンタを加算しないので&で送る)
//	list<shared_ptr<CObjectBase>>& GetJointManager(){ return m_welJoint; }
//
//
//private:
//
//
//	//	生成されたJointをリストで管理
//	//list<shared_ptr<CObjectBase>> m_welJoint;
//	list<CObjectBase>
//
//	//	生成されたJointの情報を覚えておく
//	int m_Id;
//
//
//	//	マネージャーに指定されたオブジェクトを格納する
//	void PushObjectManager(const CObjectBase* _spPointe);
//
//
//};
//
//
//#endif  _CLASS_NAME_AA_H_
//
