#ifndef _CLASS_NAME_KABO_H_
#define _CLASS_NAME_KABO_H_

class CKabo : public CObjectBase
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CKabo();

	//--------------
	//	デストラクタ
	//--------------
	~CKabo();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	開放
	void Release();

};

#endif  _CLASS_NAME_KABO_H_
