/*----------------------------------------------------------------

大きい岩

----------------------------------------------------------------*/


#ifndef _CLASS_NAME_LARGE_ROCK_H_
#define _CLASS_NAME_LARGE_ROCK_H_


/*=============

Class宣言

=============*/

class CLargeRock : public CObjectBase
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CLargeRock();

	//--------------
	//	デストラクタ
	//--------------
	~CLargeRock();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	解放
	void Release();


private:


	//	特大サイズの岩
	shared_ptr<CBulletObj_Box>		m_spRigidBox;


};


#endif  _CLASS_NAME_LARGE_ROCK_H_

