/*----------------------------------------------------------------

Class説明

----------------------------------------------------------------*/

#ifndef _CLASS_NAME_LEAF_H_
#define _CLASS_NAME_LEAF_H_


/*=============

Class宣言

=============*/

class CLeaf : public CObjectBase
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CLeaf();

	//--------------
	//	デストラクタ
	//--------------
	~CLeaf();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	解放
	void Release();


private:

};


#endif  _CLASS_NAME_LEAF_H_