/*--------------------------------------------------

テクスチャーの読み込みサポートクラス
読み込みをした画像の情報を取得し、
再度同じ画像を読み込む場合、取得してある画像を渡す

いつでも使える場合にそなえてシングルトンで生成

--------------------------------------------------*/

#ifndef _CLASS_NAME_TEXTURE_MANAGER_H_
#define _CLASS_NAME_TEXTURE_MANAGER_H_


/*=============

マクロ定義

=============*/

#define GET_TEXTURE_LOAD_MANAGER CTextureManager::GetInstance()


/*=============

Class宣言

=============*/

class CTextureManager
{
private:

	//----------------
	//	コンストラクタ
	//----------------
	CTextureManager();

	//--------------
	//	デストラクタ
	//--------------
	~CTextureManager();

	
public:


	//------------------------------------------------------------
	//	クラスを一度だけインスタンス化し、以降は同じアドレスを返す
	//------------------------------------------------------------
	static CTextureManager& GetInstance()
	{
		static CTextureManager Instance;
		return Instance;
	}

	//	読み込み関数　テクスチャー型で返す
	shared_ptr<CTexture> Load(string _FileName, LPDIRECT3DDEVICE9 _lpD3DDevice);

	
	//	画像サイズやアルファ値を設定した読み込み関数
	shared_ptr<CTexture> LoadEx(
		string		_FileName, 
		int			_TEXTURE_WIDTH_SIZE, 
		int			_TEXTURE_HEIGHT_SIZE, 
		bool		_EnableCollor			= false, 
		int			_a						= 255,
		int			_r						= 0,		
		int			_g						= 0,
		int			_b						= 0
		);


	//	指定されたファイル名のデータを返す
	shared_ptr<CTexture> Get(string _filename);


	//	マネージャー自身に入っているすべてのデータをクリアーする
	void AllClear();


private:


	//	XFileを格納するためのデータ
	map<string, shared_ptr<CTexture>> m_mapTexture;


};


#endif  _CLASS_NAME_TEXTURE_MANAGER_H_