/*----------------------------------------------------------------

メッシュの読み込みサポートクラス
読み込みをした画像の情報を取得し、
再度同じ画像を読み込む場合、取得してある画像を渡す

いつでも使える場合にそなえてシングルトンで生成


メッシュの情報をshared_ptrで作る

同じデータがあった場合受け取り先のクラスの
shared_ptrで作られたメッシュの情報にデータのコピーをするため
参照カウンタが増える状態になる

受け取り先のクラスのメッシュ情報は必要がなければ必ずnullptrし
最終的に、このクラスにメッシュの元のポインターがくるように
厳しく管理しておく

----------------------------------------------------------------*/

#ifndef _CLASS_NAME_XFILE_MANAGER_H_
#define _CLASS_NAME_XFILE_MANAGER_H_


/*=============

前方宣言

=============*/
class CMeshObject;


/*=============

前方宣言

=============*/

#define GET_XFILE_LOAD_MANAGER CXFileManager::GetInstance()


/*=============

Class宣言

=============*/
class CXFileManager
{
private:	
	
	//----------------
	//	コンストラクタ
	//----------------
	CXFileManager();
	
	//--------------
	//	デストラクタ
	//--------------
	virtual ~CXFileManager();


public:


	//------------------------------------------------------------
	//	クラスを一度だけインスタンス化し、以降は同じアドレスを返す
	//------------------------------------------------------------
	static CXFileManager& GetInstance()
	{
		static CXFileManager Instance;
		return Instance;
	}


	/*Load-------------------------------------------------->

	メッシュ読み込み関数

	メッシュを読み込んで、
	メッシュのパスとメッシュの情報をmapに保存

	再度同じパスのメッシュが読み込まれた場合は
	あらかじめ保存されているメッシュを返すことによって
	読み込みの無駄な処理を防ぐ


	※メッシュの情報がポインターとして共有されるので
	同じメッシュでも一部のメッシュだけ色を変えたりする場合は
	同じ情報を持っているメッシュすべてに適用されていしまう


	_FileName		:	ファイルパス

	_lpD3DDevice	:	デバイス

	<------------------------------------------------------*/

	shared_ptr<CMeshObject> Load(
		string				_Filename, 
		LPDIRECT3DDEVICE9	_lpD3DDevice
		);

	//	指定されたファイル名のデータを返す
	shared_ptr<CMeshObject> Get(string _Filename);

	//	マネージャー自身に入っているすべてのデータをクリアーする
	void AllClear();


private:


	//	XFileを格納するためのデータ
	map<string, shared_ptr<CMeshObject>> m_mapXFile;

};

#endif  _CLASS_NAME_XFILE_MANAGER_H_