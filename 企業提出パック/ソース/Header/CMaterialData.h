/*----------------------------------------------------------------

マテリアル情報を管理

法線マップなどの情報も所持する

----------------------------------------------------------------*/


#ifndef _CLASS_NAME_MATERIAL_DATA_H_
#define _CLASS_NAME_MATERIAL_DATA_H_


/*=============

Class宣言

=============*/

class CMaterialData
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CMaterialData();

	//--------------
	//	デストラクタ
	//--------------
	~CMaterialData();


	//-----
	// 素材
	//-----
	D3DMATERIAL9		m_Material;


	//-----------
	// テクスチャ
	//-----------

	// テクスチャ参照用
	shared_ptr<CTexture>		m_spMeshTex;

	// 法線テクスチャ参照用
	shared_ptr<CTexture>		m_spNormalTex;

	//	開放
	void Release();

	//	読み込み
	void LoadTexture(const LPDIRECT3DDEVICE9 _lpD3DDevice, const char* _TexName);

};


// ファイル名と拡張子の間に、指定文字列を挿入する
inline std::string ConvertExtFileName(const std::string& FileName, const char* ext)
{
	//	ファイルパスをコピー
	std::string extName = FileName;

	//	.がついている文字数を保存
	int pos = extName.find_last_of(".");

	if (pos == -1)return "";

	std::string tmp = ".";

	tmp += ext;

	extName.insert(pos, tmp);

	return extName;
}

#endif  _CLASS_NAME_MATERIAL_DATA_H_