#ifndef _CLASS_NAME_MATRIX_H_
#define _CLASS_NAME_MATRIX_H_



/*================================================
		行列操作クラス
		行列の合成
		内積や外積計算など
		普段の行列の合成や計算などに
		使えそうなものをまとめておく
		基本的に、ワールド行列として宣言すればいい

		D3DXMATRIXを継承
================================================*/
class CMatrix : public D3DXMATRIX
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CMatrix();

	//------------------------
	//	引数つきコンストラクタ
	//------------------------
	CMatrix(const D3DXMATRIX &m);
	CMatrix(FLOAT _11, FLOAT _12, FLOAT _13, FLOAT _14,
		FLOAT _21, FLOAT _22, FLOAT _23, FLOAT _24,
		FLOAT _31, FLOAT _32, FLOAT _33, FLOAT _34,
		FLOAT _41, FLOAT _42, FLOAT _43, FLOAT _44);

	//--------------
	//	デストラクタ
	//--------------
	~CMatrix();


	//==========================================//
	//			行列の合成を管理				//
	//			行列を作成し、合成する			//
	//==========================================//

	//	移動行列に移動量を加算
	void Move(float _x, float _y, float _z)
	{
		_41 += _x;
		_42 += _y;
		_43 += _z;
	}

	//	移動行列に移動量を加算(CVector3型で管理)
	void Move(const CVector3* _Vec)
	{
		_41 += _Vec->x;
		_42 += _Vec->y;
		_43 += _Vec->z;
	}

	//	移動行列に移動量をローカル座標で加算
	void Move_Local(float _x, float _y, float _z)
	{
		D3DXMATRIX TmpMat;		// 作業用
		D3DXMatrixTranslation(&TmpMat, _x, _y, _z);
		D3DXMatrixMultiply(this, &TmpMat, this);// 合成
	}

	//  移動行列に移動量をローカル座標で加算(CVector3型で管理)
	void Move_Local(const CVector3* _Vec)
	{
		D3DXMATRIX TmpMat;		// 作業用
		D3DXMatrixTranslation(&TmpMat, _Vec->x, _Vec->y, _Vec->z);
		D3DXMatrixMultiply(this, &TmpMat, this);// 合成
	}

	//  X軸回転を回転行列に合成
	void RotateX(float _Ang)
	{
		D3DXMATRIX TmpMat;		// 作業用
		D3DXMatrixRotationX(&TmpMat, ToRadian(_Ang));
		D3DXMatrixMultiply(this, this, &TmpMat);// 合成
	}

	//	Y軸回転を回転行列に合成
	void RotateY(float _Ang)
	{
		D3DXMATRIX TmpMat;		// 作業用
		D3DXMatrixRotationY(&TmpMat, ToRadian(_Ang));
		D3DXMatrixMultiply(this, this, &TmpMat);// 合成
	}

	//	Z軸回転を回転行列に合成
	void RotateZ(float _Ang)
	{
		D3DXMATRIX TmpMat;		// 作業用
		D3DXMatrixRotationZ(&TmpMat, ToRadian(_Ang));
		D3DXMatrixMultiply(this, this, &TmpMat);// 合成
	}

	//	回転行列にX軸回転量をセット
	void SetRotateX(float _Ang)
	{
		D3DXMatrixRotationX(this, ToRadian(_Ang));
		D3DXMatrixMultiply(this, this, this);// 合成
	}

	//	回転行列にY軸回転量をセット	
	void SetRotateY(float _Ang)
	{
		D3DXMatrixRotationY(this, ToRadian(_Ang));
		D3DXMatrixMultiply(this, this, this);// 合成
	}

	//	回転行列にZ軸回転量をセット
	void SetRotateZ(float _Ang)
	{
		D3DXMatrixRotationZ(this, ToRadian(_Ang));
		D3DXMatrixMultiply(this, this, this);// 合成
	}

	//	指定軸回転を合成
	void RotateAxis(CVector3* _vAxis, float _Ang)
	{
		D3DXMATRIX TmpMat;		// 作業用
		D3DXMatrixRotationAxis(&TmpMat, _vAxis, ToRadian(_Ang));
		D3DXMatrixMultiply(this, this, &TmpMat);// 合成
	}

	//  X軸回転を行列にローカル軸で合成
	void RotateX_Local(float _Ang)
	{
		D3DXMATRIX TmpMat;		// 作業用
		D3DXMatrixRotationX(&TmpMat, ToRadian(_Ang));
		D3DXMatrixMultiply(this, &TmpMat, this);// 合成
	}

	//  Y軸回転を行列にローカル軸で合成
	void RotateY_Local(float _Ang)
	{
		D3DXMATRIX TmpMat;		// 作業用
		D3DXMatrixRotationY(&TmpMat, ToRadian(_Ang));
		D3DXMatrixMultiply(this, &TmpMat, this);// 合成
	}

	//	Z軸回転を行列にローカル軸で合成
	void RotateZ_Local(float _Ang)
	{
		D3DXMATRIX TmpMat;		// 作業用
		D3DXMatrixRotationZ(&TmpMat, ToRadian(_Ang));
		D3DXMatrixMultiply(this, &TmpMat, this);// 合成
	}

	//	指定軸回転をローカル軸で合成
	void RotateAxis_Local(CVector3* _vAxis, float _Ang)
	{
		D3DXMATRIX TmpMat;		// 作業用
		D3DXMatrixRotationAxis(&TmpMat, _vAxis, ToRadian(_Ang));
		D3DXMatrixMultiply(this, &TmpMat, this);// 合成
	}

	//	拡大行列に拡大量を合成
	void Scale(float _x, float _y, float _z)
	{
		D3DXMATRIX TmpMat;		// 作業用
		D3DXMatrixScaling(&TmpMat, _x, _y, _z);
		D3DXMatrixMultiply(this, this, &TmpMat);// 合成
	}

	//	拡大行列に拡大量を合成(CVector3型を使用)
	void Scale(const CVector3* _Vec)
	{
		D3DXMATRIX TmpMat;		// 作業用
		D3DXMatrixScaling(&TmpMat, _Vec->x, _Vec->y, _Vec->z);
		D3DXMatrixMultiply(this, this, &TmpMat);// 合成
	}

	//	拡大行列をセットする
	void Set_Scale(const CVector3* _Vec)
	{
		D3DXMatrixScaling(this, _Vec->x, _Vec->y, _Vec->z);
		D3DXMatrixMultiply(this, this, this);// 合成
	}

	//  拡大行列に拡大量をローカル行列で合成
	void Scale_Local(float _x, float _y, float _z)
	{
		D3DXMATRIX TmpMat;		// 作業用
		D3DXMatrixScaling(&TmpMat, _x, _y, _z);
		D3DXMatrixMultiply(this, &TmpMat, this);// 合成
	}

	//	拡大行列に拡大量をローカル行列で合成(CVector3型を使用)
	void Scale_Local(const CVector3* _Vec)
	{
		D3DXMATRIX TmpMat;		// 作業用
		D3DXMatrixScaling(&TmpMat, _Vec->x, _Vec->y, _Vec->z);
		D3DXMatrixMultiply(this, &TmpMat, this);// 合成
	}


	//==========================================//
	//	行列作成や操作を管理					//
	//	行列の内容を直接上書きなどの操作をする	//
	//==========================================//

	//----------------
	//  単位行列を作成
	//----------------
	void CreateIdentity()
	{
		D3DXMatrixIdentity(this);
	}

	void CreateIdentityRotate()
	{
		_11 = 1; _12 = 0; _13 = 0; _14 = 0;
		_21 = 0; _22 = 1; _23 = 0; _24 = 0;
		_31 = 0; _32 = 0; _33 = 1; _34 = 0;
	}

	static void CreateIdentity(D3DXMATRIX* _Matrix)
	{
		D3DXMatrixIdentity(_Matrix);
	}

	//------------
	//  逆行列作成
	//------------
	D3DXMATRIX& CreateInverse()
	{
		D3DXMatrixInverse(this, NULL, this);
		return *this;
	}

	void CreateInverse(D3DXMATRIX *destMat)
	{
		D3DXMatrixInverse(destMat, NULL, this);
	}

	static void CreateInverse(
		D3DXMATRIX*			_mOut,
		float*				_pDeterminant,
		const D3DXMATRIX*	_mIn
		)
	{
		D3DXMatrixInverse(_mOut, _pDeterminant, _mIn);
	}

	//--------------
	//  移動行列作成
	//--------------
	void CreateMove(float _x, float _y, float _z)
	{
		D3DXMatrixTranslation(this, _x, _y, _z);
	}

	void CreateMove(const CVector3* _Vec)
	{
		D3DXMatrixTranslation(this, _Vec->x, _Vec->y, _Vec->z);
	}

	static void CreateMove(D3DXMATRIX* _mOut, float _x, float _y, float _z)
	{
		D3DXMatrixTranslation(_mOut, _x, _y, _z);
	}

	static void CreateMove(D3DXMATRIX* _mOut, const CVector3* _Vec)
	{
		D3DXMatrixTranslation(_mOut, _Vec->x, _Vec->y, _Vec->z);
	}

	//---------------
	//  X回転行列作成
	//---------------
	void CreateRotateX(float _Ang)
	{
		D3DXMatrixRotationX(this, ToRadian(_Ang));
	}

	static void CreateRotateX(D3DXMATRIX *_mOut, float _Ang)
	{
		D3DXMatrixRotationX(_mOut, ToRadian(_Ang));
	}

	//---------------
	//  Y回転行列作成
	//---------------
	void CreateRotateY(float _Ang)
	{
		D3DXMatrixRotationY(this, ToRadian(_Ang));
	}

	static void CreateRotateY(D3DXMATRIX* _mOut, float _Ang)
	{
		D3DXMatrixRotationY(_mOut, ToRadian(_Ang));
	}

	//---------------
	//  Z回転行列作成
	//---------------
	void CreateRotateZ(float _Ang)
	{
		D3DXMatrixRotationZ(this, ToRadian(_Ang));
	}

	static void CreateRotateZ(D3DXMATRIX* _mOut, float _Ang)
	{
		D3DXMatrixRotationZ(_mOut, ToRadian(_Ang));
	}

	// 任意軸回転行列を作成
	void CreateRotateAxis(const CVector3* _vAxis, float _Ang)
	{
		D3DXMatrixRotationAxis(this, _vAxis, ToRadian(_Ang));
	}

	static void CreateRotateAxis(D3DXMATRIX* _mOut, const CVector3* _vAxis, float _Ang)
	{
		D3DXMatrixRotationAxis(_mOut, _vAxis, ToRadian(_Ang));
	}

	//----------------------
	//  スケーリング行列作成
	//----------------------
	void CreateScale(float _x, float _y, float _z)
	{
		D3DXMatrixScaling(this, _x, _y, _z);
	}

	void CreateScale(const CVector3* _Vec)
	{
		D3DXMatrixScaling(this, _Vec->x, _Vec->y, _Vec->z);
	}

	static void CreateScale(D3DXMATRIX* _mOut, float _x, float _y, float _z)
	{
		D3DXMatrixScaling(_mOut, _x, _y, _z);
	}

	static void CreateScale(D3DXMATRIX* _mOut, const CVector3* _Vec)
	{
		D3DXMatrixScaling(_mOut, _Vec->x, _Vec->y, _Vec->z);
	}


	//===================================//
	//  指定方向にZ軸を向けた行列を作成	 //
	//===================================//

	//  LookWayの方向に向けた行列にする。Upは上となる方向を指定。※合成でなく、直接回転部分を上書きします。
	void SetLookAt(const CVector3 *LookWay, const CVector3 *Up);
	//  自分からTargetの位置を見た方向の行列にする。Upは上となる方向を指定。※合成でなく、直接回転部分を上書きします。
	void SetLookAtPos(const CVector3 *TargetPos, const CVector3 *Up);

	//	座標設定
	void SetPos(float _x, float _y, float _z)
	{
		_41 = _x;
		_42 = _y;
		_43 = _z;
	}

	//	座標設定(CVector3型を使用)
	void SetPos(const CVector3* _Vec)
	{
		_41 = _Vec->x;
		_42 = _Vec->y;
		_43 = _Vec->z;
	}


	//========//
	//	変換  //
	//========//
	void RotationQuaternion(D3DXQUATERNION* _Out)
	{
		D3DXQuaternionRotationMatrix(_Out, this);
	}

	static void RotationQuaternion(D3DXQUATERNION* _Out, const D3DXMATRIX* _mIn)
	{		
		D3DXQuaternionRotationMatrix(_Out, _mIn);
	}

	
	//==============//
	//	情報取得	//
	//==============//

	//座標をベクトル型で取得
	CVector3& GetPos()
	{
		//	_41と_42と_43がメモリが連続しているので、まとめて返す
		//	_41(Matrix)をCVector3型の参照体にキャストして返す
		return (CVector3&)_41;
	}

	// X軸をベクトル型で取得
	CVector3& GetXAxis()
	{
		return *(CVector3*)&_11;
	}

	// Y軸をベクトル型で取得
	CVector3& GetYAxis()
	{
		return *(CVector3*)&_21;
	}

	// Z軸をベクトル型で取得
	CVector3& GetZAxis()
	{
		return *(CVector3*)&_31;
	}

	//	行列からXの回転量を取得
	float GetAngleX() 
	{
		float Out;

		/*
			_11, _12, _13, _14
			_21, _22, _23, _24
			_31, _32, _33, _34
			_41, _42, _43, _44

			_
		*/

		Out = asinf(this->_32);

		Out = -ToDegree(Out);

		if (Out < 0) { Out += 360.0f; }

		//if (Out < -360.0f) { Out += 360.0f; }
		//if (Out > 360.0f)  { Out -= 360.0f; }

		return Out;
	}

	//	行列からYの回転量を取得
	float GetAngleY()
	{
		float Out;

		Out = atan2f(-this->_31, this->_33);

		Out = -ToDegree(Out);

		if (Out < 0) { Out += 360.0f; }

		return Out;
	}

	//	行列からZの回転量を取得
	float GetAngleZ()
	{
		float Out;

		Out = atan2f(-this->_12, this->_22);

		Out = -ToDegree(Out);

		if (Out < 0) { Out += 360.0f; }

		return Out;
	}
};

#endif  _CLASS_NAME_MATRIX_H_