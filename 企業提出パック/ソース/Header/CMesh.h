/*--------------------------------------------------


CMyAllocateHierarchy

スキンメッシュを読み込み、設定するクラス


====================================================


CMeshParameter

メッシュのパラメーターをいじれるクラス


====================================================


CBounding

メッシュの
バウンディングスフィアと
バウンディングボックスを管理する


====================================================


CMeshObject

通常メッシュ対応クラス
スキンメッシュにも対応
メッシュデータをまとめている


====================================================


--------------------------------------------------*/

#ifndef _CLASS_NAME_SKINMESH_ANIMATION_H_
#define _CLASS_NAME_SKINMESH_ANIMATION_H_


#include "CMeshAnimationController.h"


/*====================

Class宣言

====================*/
class CMyAllocateHierarchy : public ID3DXAllocateHierarchy
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CMyAllocateHierarchy();

	//--------------
	//	デストラクタ
	//--------------
	~CMyAllocateHierarchy();


	//------------
	//	構造体宣言
	//------------

	struct MYD3DXFRAME : public D3DXFRAME
	{
		DWORD			m_dwId;						//	ボーンのID
		D3DXMATRIX		m_mOffsetMatrix;			//	オフセット行列


		//	初期化
		MYD3DXFRAME() 
		: 
		m_dwId(0xffffffff)			//	ボーンのIDがNULLだった場合に0xffffffffを入れておく
		{
			D3DXMatrixIdentity(&m_mOffsetMatrix);

			Name = "";
			D3DXMatrixIdentity(&TransformationMatrix);

			//LPD3DXMESHCONTAINER     pMeshContainer;
			pMeshContainer = nullptr;
		}
	};


	struct MYD3DXMESHCONTAINER : public D3DXMESHCONTAINER
	{
		DWORD					m_BoneCnt;
		DWORD					m_dwMaxFaceInfl;			//	ボーン最大影響数
		DWORD					m_dwNumBoneCombinations;	//	ボーンコンビネーション数
		ID3DXBuffer*			m_bCombinationTable;		//　ボーン構造体配列へのポインタ
		LPD3DXMESH				m_OriginMesh;				//	コピー用のメッシュ情報
		LPD3DXATTRIBUTERANGE	m_pAttributeTable;			//	
		vector <MYD3DXFRAME *>  m_BoneFrameArray;			//	ボーンの数
		vector<CTexture>		m_Textures;					//	メッシュのテクスチャー


		//	初期化
		MYD3DXMESHCONTAINER() 
		: 
		m_dwMaxFaceInfl(1)
		, 
		m_dwNumBoneCombinations(0)
		, 
		m_bCombinationTable(0)
		, 
		m_pAttributeTable(0)
		, 
		m_OriginMesh(0)
		, 
		m_BoneCnt(0)
		{
		
		}
	};


	//	名前をコピーしてくる
	char* CopyName(const char* _Name);

	//	フレームを作成
	STDMETHOD(CreateFrame)(
		THIS_ LPCSTR _Name, 
		LPD3DXFRAME* _ppNewFrame);

	//	メッシュコンテナーを作成
	STDMETHOD(CreateMeshContainer)(
		THIS_ LPCSTR				_Name, 
		CONST D3DXMESHDATA*			_pMeshData, 
		CONST D3DXMATERIAL*			_pMaterials,
		CONST D3DXEFFECTINSTANCE*	_pEffectInstances, 
		DWORD						_NumMaterials,
		CONST DWORD*				_pAdjacencey, 
		LPD3DXSKININFO				_pSkinInfo, 
		LPD3DXMESHCONTAINER*		_ppNewMeshContainer
		);

	//	メッシュコンテナーを開放
	STDMETHOD(DestroyMeshContainer)(THIS_ LPD3DXMESHCONTAINER _pMeshContainerToFree);

	//	フレームを開放
	STDMETHOD(DestroyFrame)(THIS_ LPD3DXFRAME _pFrameToFree);

	//	デバイスのコピー
	LPDIRECT3DDEVICE9	m_CopyDevice;

	//	ボーンの数を記憶するために用意
	UINT m_iBone, m_cBone;
};


class CMeshState
{
public:

	//----------------
	//  コンストラクタ
	//----------------
	CMeshState() 
	{ 
		//	自己発行色
		m_EmissivePower = 1.0f;  

		//	フォグの距離
		m_vFogRange = D3DXVECTOR2(30, 80); 

		//	UVスクロール値
		m_vUVScroll = CVector3(0.0f, 0.0f, 0.0f);

		//	変更色
		m_vChangeColor = D3DXVECTOR4(0, 0, 0, 0);

		//	現在のピクセル位置
		m_vNowPix = CVector3(0, 0, 0);

		//	αテストで使用するα値
		m_AlphaTest = 1.0f;

		m_Reflection = 0.5f;

		m_EnableInk = false;

		//	ミラー描画をするかどうか
		m_Clip = 0;

		//	屈折するかどうか
		m_Refract = 0;

		//	ライト有効無効のフラグを初期化
		m_LightEnable = 1;
	}

	//--------------
	//  デストラクタ
	//--------------
	~CMeshState() {}


	//	自己発行力を制御
	float m_EmissivePower;

	//	UVスクロール
	CVector3 m_vUVScroll; 

	//	変更したい色
	D3DXVECTOR4 m_vChangeColor;

	//	現在のピクセル位置(Y座標)使用
	CVector3 m_vNowPix;

	//	フォグをかける範囲を指定できるようにする
	//	xが手前でyが一番奥
	D3DXVECTOR2 m_vFogRange;

	//	UV展開されたテクスチャのα値
	float m_AlphaTest;

	//	環境マップをどれだけだすか
	float m_Reflection;

	//	輪郭ありなし
	bool  m_EnableInk;	//	true(輪郭描画) : false(輪郭なし)

	//	ミラーを反射させるかどうか
	int m_Clip;

	//	屈折させるかどうか
	int m_Refract;

	//	ライト処理を入れるかどうか
	int m_LightEnable;	//	0で無効 1で有効

};


class CBounding
{
public:

	//----------------
	//  コンストラクタ
	//----------------
	CBounding();

	//--------------
	//  デストラクタ
	//--------------
	~CBounding();


	//	バウンディングボックスとバウンディングスフィアの値を設定する
	void SetBounding(LPD3DXMESH _lpMesh);


	//------------------------
	//	バウンディングボックス
	//------------------------

	//	最小値を返す
	CVector3 GetBoundingBoxMin(){ return m_vBoundingBoxMin; }

	//	最大値を返す
	CVector3 GetBoundingBoxMax(){ return m_vBoundingBoxMax; }

	//	長さを返す
	CVector3 GetBoundingBoxLen(){ return m_vBoundingBoxLen; }


	//------------------------
	//	バウンディングスフィア
	//------------------------

	//	中心座標を返す
	CVector3	GetBoundingSphereCenter(){ return m_vBoundingSphereCenter; }

	//	半径を返す
	float		GetBoundingSphereRadius(){ return m_BoundingSphereRadius; }

	//	半径を設定
	void		SetBoundingSphereRadiuse(float _Radius){ m_BoundingSphereRadius= _Radius; }


private:


	//------------------------
	//	バウンディングボックス
	//------------------------

	//	バウンディングボックスの最小値
	CVector3 m_vBoundingBoxMin;

	//	バウンディングボックスの最大値
	CVector3 m_vBoundingBoxMax;

	//	最小値から最大値までの長さ
	CVector3 m_vBoundingBoxLen;


	//------------------------
	//	バウンディングスフィア
	//------------------------

	//	バウンディングスフィアの中心点
	CVector3 m_vBoundingSphereCenter;

	//	バウンディングスフィアの半径
	float m_BoundingSphereRadius;
};


class CMeshObject
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CMeshObject();

	//--------------
	//	デストラクタ
	//--------------
	~CMeshObject();


	//----------
	//  ゲッター
	//----------

	//  マテリアル数を返す
	int							GetMaterialCnt(){ return m_MaterialCnt; }	

	//	通常メッシュデータを返す
	float						GetMeshAlpha(){ return m_MeshAlpha; }

	//  メッシュ情報を返す
	LPD3DXMESH					GetMesh(){ return m_lpMesh; }

	//  マテリアルテーブルを返す (ユニークポインタは返すことができないのでかわりに生ポインタを返す)
	//D3DMATERIAL9*				GetMaterials(){ return m_Materials; }
	CMaterialData*				GetMaterials(){ return m_pMaterials.get(); }

	//  隣接性データを返す
	LPD3DXBUFFER				GetAdjacency(){ return m_lpAdjacency; }	

	//  テクスチャ配列を返す
	//CTexture*					GetTextures(){ return m_Textures; }

	//	法線テーブルを返す
	CVector3*					GetWallVec(){ return m_vWallVec.get(); }

	//	メッシュのステータスを返す
	shared_ptr<CMeshState>					GetMeshState() { return m_pMeshState; }

	//	バウンディング情報を返す(コピー禁止)
	shared_ptr<CBounding>					GetBounding(){ return m_pBounding; }

	//	アニメーションコントローラを返す
	CHighLevelAnimeController*	GetAnimeController(){ return m_pHighController; }

	//	メッシュの面テーブルを返す
	vector<CFace> GetFace(){ return m_vecFaceTbl; }


	//----------
	//	セッター
	//----------

	//	マテリアル数をセットする
	void SetMaterialCnt(int _Cnt){ m_MaterialCnt = _Cnt; }

	//  通常メッシュデータをセットする
	void SetMeshAlpha(float _Alpha){ m_MeshAlpha = _Alpha; }

	//	メッシュ情報をセットする
	void SetMesh(LPD3DXMESH _Mesh){ m_lpMesh = _Mesh; }

	//  マテリアルテーブルをセットする
	//void SetMaterials(D3DMATERIAL9* _Material){ m_Materials = _Material; }

	//	隣接性データをセットする
	void SetAdjacency(LPD3DXBUFFER _Adjacency){ m_lpAdjacency = _Adjacency; }

	//  テクスチャ配列をセットする
	//void SetTextures(CTexture* _Texture){ m_Textures = _Texture; }

	//	メッシュのステータスをセット
	void SetMeshState(shared_ptr<CMeshState> _pState) { m_pMeshState = _pState; }

	//  通常メッシュ読み込み
	BOOL LoadMesh(const char *lpFileName, LPDIRECT3DDEVICE9 lpD3DDev);


	/* CreateVertexData--------------------------->

	メッシュの法線情報を作る
	面の法線情報を取得して、保存する
	当たり判定などで使用する


	_lpD3DDevice		:	デバイス

	return				:	戻り値無し

	<------------------------------------------*/
	void CreateVertexData(LPDIRECT3DDEVICE9 _lpD3DDevice);


	/* UpdateVertexData------------------------------->
	
	メッシュの法線情報を更新

	_lpD3DDevice		:	デバイス
	_Id					:	更新するメッシュのID

	return				:	戻り値なし
	
	<------------------------------------------------*/
	void UpdateVertexData(LPDIRECT3DDEVICE9 _lpD3DDevice, int _Id);


	/* CreateFaceData----------------------------------->
	
	面情報を生成する

	returna		:	戻り値なし
	
	<--------------------------------------------------*/
	void CreateFaceData();


	/* CreateSkinFaceData---------------------------->
	
	スキンメッシュ用の面生成クラス

	return		:	戻り値なし
	
	<-----------------------------------------------*/
	void CreateSkinFaceData();


	//  通常メッシュ描画
	void DrawMesh();

	//  解放
	void Release();


	//	法線情報を取得するために必要な構造体
	struct CLONEVERTEX
	{
		CVector3	m_Pos;
		CVector3	m_Normal;
		D3DXVECTOR2 m_Tex;
	};


private:


	//------------------------
	//	メッシュに使用する変数
	//------------------------

	//	メッシュ
	LPD3DXMESH		m_lpMesh;

	//	テクスチャ配列
	//CTexture*		m_Textures;

	//	法線マップ
	shared_ptr<CTexture> m_spNormalTex;

	//	マテリアル配列(固定長配列にするためユニークポインタを使用)
	//D3DMATERIAL9*	m_Materials;
	unique_ptr<CMaterialData[]> m_pMaterials;

	//	メッシュのステータス
	shared_ptr<CMeshState>	m_pMeshState;

	//	マテリアル数
	int				m_MaterialCnt;	

	//	隣接性データ
	LPD3DXBUFFER	m_lpAdjacency;	

	//	ファイル名
	string		m_FileName;		

	//	メッシュのアルファ値を管理
	float m_MeshAlpha;

	//	壁の方向
	unique_ptr<CVector3[]>	m_vWallVec;

	//	Meshクローン
	LPD3DXMESH   m_lpCloneMesh;

	//	メッシュの面テーブル
	vector<CFace> m_vecFaceTbl;

	//	頂点の数
	DWORD		 m_NumVertex;

	//	バウンディング系のステータスを管理する
	shared_ptr<CBounding>	m_pBounding;


	//------------------------------
	//	スキンメッシュに必要なクラス
	//------------------------------
	map<DWORD, D3DXMATRIX>								m_CombMatrixMap;
	CMyAllocateHierarchy								m_Allocater;
	CMyAllocateHierarchy::MYD3DXFRAME*					m_pRootFrame;
	vector<CMyAllocateHierarchy::MYD3DXFRAME*>			m_pFrameArray;
	vector<CMyAllocateHierarchy::MYD3DXMESHCONTAINER*>	m_pContainerArray;
	D3DXBONECOMBINATION*								m_pCombination;


	//--------------------------------
	//	アニメーションコントローラ関連
	//--------------------------------
	ID3DXAnimationController*		m_pController;
	CHighLevelAnimeController*		m_pHighController;
	float							m_AdvanceTime;
	float							m_ShiftTime;
	float							m_LoopTime;
	UINT							m_AnimationKey;


public:


	//----------
	//	ゲッター
	//----------

	float GetAdvanceTime(){ return m_AdvanceTime; }
	
	float GetShiftTime(){ return m_ShiftTime; }
	
	float GetLoopTime(){ return m_LoopTime; }
	
	UINT  GetAnimationKey(){ return m_AnimationKey; }

	CMyAllocateHierarchy::MYD3DXMESHCONTAINER* GetCont(int idx){ return m_pContainerArray[idx]; }

	CMyAllocateHierarchy::MYD3DXFRAME* GetRootFram(){ return m_pRootFrame; }

	shared_ptr<CTexture> GetNormalTex(){ return m_spNormalTex; }


	//----------
	//	セッター
	//----------
	
	void SetAdvanceTime(float _AdvanceTime){ m_AdvanceTime = _AdvanceTime; }
	
	void SetShiftTime(float _ShiftTime){ m_ShiftTime = _ShiftTime; }
	
	void SetLoopTime(float _LoopTime){ m_LoopTime = _LoopTime; }
	
	void SetAnimationKey(UINT _AnimationKey){ m_AnimationKey = _AnimationKey; }


	/*-------------------------------------->
		スキンメッシュ関連関数
		読み込み
		セット
		処理更新
		メッシュゲッター
		フレームID
		ボーン合成
		メッシュ描画
	<--------------------------------------*/

	//	スキンメッシュ読み込み
	void LoadSkinMesh(char* _Fileame, LPDIRECT3DDEVICE9 _lpD3DDevice);
	
	//	スキンメッシュセット
	void SetSkinMesh();
	
	//	スキンメッシュ処理更新
	void UpdateSkinMesh(D3DXMATRIX _mWorld);
	
	//	メッシュコンテナーを返す
	void RecGetMeshContainer(D3DXFRAME* _Frame);

	//	メッシュコンテナーを返す
	void GetMeshContainer();

	//	フレームにIDをつける
	void SetFrameId(CMyAllocateHierarchy::MYD3DXFRAME* _Frame, ID3DXSkinInfo* _SkinInfof);

	//	ボーン合成
	void UpDateCombMatrix(std::map<DWORD, D3DXMATRIX>&_CombMatrixMap, CMyAllocateHierarchy::MYD3DXFRAME* _Frame, D3DXMATRIX& _CharMatrix);

	//	スキンメッシュメッシュ描写
	void DrawSkinMesh(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	ソフトウェアスキニングを行う
	void ExecSoftSkinning();


	/*----------------------------------->
	
	法線マップ用のポインタをインスタンス化
	その後読み込む

	_lpD3DDevice	:	デバイス
	_FileName		:	ファイルパス

	return			:	戻り値なし
	
	<-----------------------------------*/
	void LoadNormalMap(LPDIRECT3DDEVICE9 _lpD3DDevice, const char* _FileName)
	{
		//	法線情報をインスタンス化
		m_spNormalTex = make_shared<CTexture>();

		//	法線テクスチャを読み込み
		m_spNormalTex->LoadTexture(_FileName);
	}

	bool ComputeTangentFrame()
	{
		//頂点データ定義
		D3DVERTEXELEMENT9 decl[] =
		{
			{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 },
			{ 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TANGENT, 0 },
			{ 0, 24, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BINORMAL, 0 },
			{ 0, 36, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0 },
			{ 0, 48, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 },
			D3DDECL_END()
		};

		LPD3DXMESH lpTmpMesh;
		if (FAILED(m_lpMesh->CloneMesh(m_lpMesh->GetOptions(), decl, GET_DIRECT_HELPER.GetDev(), &lpTmpMesh))){
			return false;
		}
		SAFE_RELEASE(m_lpMesh);

		D3DXComputeTangentFrameEx(lpTmpMesh,
			D3DDECLUSAGE_TEXCOORD,
			0,
			D3DDECLUSAGE_TANGENT,
			0,
			D3DDECLUSAGE_BINORMAL,
			0,
			D3DDECLUSAGE_NORMAL,
			0,
			0,
			NULL,
			0.01f,    //ボケ具合。値をおおきくするとぼけなくなる
			0.25f,
			0.01f,
			&m_lpMesh,
			NULL
			);
		SAFE_RELEASE(lpTmpMesh);

		return true;
	}

};


#endif _CLASS_NAME_SKINMESH_ANIMATION_H_