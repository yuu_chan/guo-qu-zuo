/*----------------------------------------------

スキンメッシュアニメーション用のクラス

----------------------------------------------*/

#ifndef _CLASS_NAME_MESH_ANIMETION_CONTROLLER_H_
#define _CLASS_NAME_MESH_ANIMETION_CONTROLLER_H_


/*=============

構造体宣言

=============*/
struct HLAnimationDesc
{
	UINT				m_uiAnimeID;		//	アニメーションID
	ID3DXAnimationSet*	m_pAnimeSet;		//	アニメーションセット
	FLOAT				m_fLoopTime;		//	1ループの時間
	FLOAT				m_fTrackSpeed;		//	トラックスピード調節値
	FLOAT				m_fShiftTime;		//	シフトするのにかかる時間
	FLOAT				m_fCurWeightTime;	//	現在のウェイト時間
};


/*=============

Class宣言

=============*/
interface CHighLevelAnimeController
{

public:
	CHighLevelAnimeController();
	~CHighLevelAnimeController();

private:
	static const int			ANIMETION_KEY_MAX = 15;		//	アニメーションキーの数
	int							m_CurAnimeID;				//	現在のアニメーション
	int							m_PreAnimeID;				//	前のアニメーション
	HLAnimationDesc				m_Anime[ANIMETION_KEY_MAX];	//	アニメーション情報

	//	アニメーションが切り替わったかどうか
	bool m_ChangeDid;		//	true(切り替わった) false(切り替わってない)

	//	アニメーション再生時間
	float		m_AnimationTime;


public:

	//	アニメーションコントローラー
	LPD3DXANIMATIONCONTROLLER	m_pAnimeController;

	//	アニメーションコントローラを設定
	void SetAnimationController(ID3DXAnimationController* _pAniCon){m_pAnimeController = _pAniCon;}

	//	ループ時間を設定
	bool SetLoopTime(UINT _uiAnimeID, FLOAT _Time);

	//	動作時間にかかる時間を設定
	bool SetShiftTime(UINT _uiAnimeID, FLOAT _fInterval);

	//	アニメーションを切り替え
	bool ChangeAnimation(UINT _uiAnimeID);

	//	アニメーションを更新
	bool AdvanceTime(FLOAT _fTime);

	//	アニメーション情報をセットする
	bool AnimestionSet();

	//	アニメーションの現在位置を取得後尾にいればtrueを返させる
	bool AnimationPosition(UINT _uiAnimeID);


	/*CheckAnimationTime------------------------------------->
		アニメーションカウンターが最後まで到達したかどうかを
		チェックして、trueかfalseを返す

		_NowAnimationTime	: アニメーションの現在の時間
		_ANIMATION_END_TIME : アニメーションの最後

		return				: true(到達) false(到達せず)
	<-------------------------------------------------------*/
	bool CheckAnimationTime(
		float		_NowAnimationTime,
		const float _ANIMATION_END_TIME);


	//----------
	//	セッター
	//----------
	void SetAnimationTime(float _Time){ m_AnimationTime = _Time; }


	//----------
	//	ゲッター
	//----------
	float GetAnimationTime(){ return m_AnimationTime; }


};

#endif  _CLASS_NAME_MESH_ANIMETION_CONTROLLER_H_