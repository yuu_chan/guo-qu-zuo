/*--------------------------------------------------

ゴールとなる金色のキノコ

--------------------------------------------------*/

#ifndef _CLASS_NAME_MUSHROOM_H_
#define _CLASS_NAME_MUSHROOM_H_


/*=============

Class宣言

=============*/
class CMushRoom : public CObjectBase
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CMushRoom();

	//--------------
	//	デストラクタ
	//--------------
	~CMushRoom();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	解放
	void Release();

};


#endif  _CLASS_NAME_MUSHROOM_H_