/*----------------------------------------------------------------

メッシュ描画を行う際に使用するクラス

----------------------------------------------------------------*/

#ifndef _CLASS_NAME_SHADER_H_
#define _CLASS_NAME_SHADER_H_


/*=============

Class宣言

=============*/
class CMyShader : public CShaderBase
{
public:

	//----------------
	//	コンストラクタ
	//-----------------
	CMyShader();

	//--------------
	//	デストラクタ
	//--------------
	~CMyShader();


	//	初期化
	void Init();

	//	開放
	void Release();


	/*DrawMesh------------------------------->
	通常メッシュ描画

	_pMesh*			:	メッシュのポインタ
	_Pass			:	指定したいPass

	return			:	戻り値無し

	<-------------------------------------------*/
	void DrawMesh(shared_ptr<CMeshObject> _pMesh, int _Pass);


	/*DrawSkinMesh------------------------------->
	スキンメッシュ描画
	
	_pMesh*			:	メッシュのポインタ
	_Pass			:	指定したいPass

	return			:	戻り値無し
	
	<-------------------------------------------*/
	void DrawSkinMesh(shared_ptr<CMeshObject> _pMesh, int _Pass);


	/*AlphaDrawMesh------------------------------>
	α指定可能なメッシュ描画関数

	_pMesh*			:	メッシュのポインタ
	_alpha			:	指定したいα値

	return			:	戻り値無し

	<-------------------------------------------*/
	void AlphaDrawMesh(shared_ptr<CMeshObject> _pMesh, float _alpha);


	//------------------
	//	パラメータ設定系
	//------------------

	//	ライトの向きを設定
	void SetLightDir(CVector3* _vDir)
	{ 
		m_Effect->SetVector("LightDir", (D3DXVECTOR4 *)_vDir); 
	}

	//	カメラのポジションをセット
	void SetCamPos(CVector3* _vPos)
	{ 
		m_Effect->SetVector("CamPos", (D3DXVECTOR4 *)_vPos); 
	}

	//	シャドウマップ関係のデータをセット
	void SetShadowMapDate(CMatrix* _mLVP, CTexture* _ShadowMap)
	{
		m_Effect->SetMatrix("mLVP", _mLVP);
		m_Effect->SetTexture("ShadowTex", _ShadowMap->GetTex());
	}


	//----------
	//	ゲッター
	//----------

	//	ブルームのテクスチャを返す
	CTexture GetBloom(){ return m_texBloom; }


	//	環境マップ
	CTexture m_SphereMap;

	//	デフォルトの法線マップ
	CTexture m_texDefNormal;

	//	トューンテクスチャ
	CTexture m_ToonTex;

	//	ブルーム
	CTexture m_texBloom;

	//	UVスクロール量
	CVector3 m_vUVScroll;

	//	ポイントライトの発生させる座標
	D3DXVECTOR4 m_vPointPos[100];

	//	0設定でポイントライトなし
	float		m_PointRadius[100];
	
	//	色
	D3DXVECTOR4 m_vPointColor[100];
};

#endif  _CLASS_NAME_SHADER_H_