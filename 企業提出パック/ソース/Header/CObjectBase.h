/*----------------------------------------------------------------


CObjectBase

オブジェクトの基底クラス
スキンメッシュとメッシュ両方とも使用可能

string型の変数が2つ用意されている
意図としては、汎用的な判定文を記述することができるので
何かの処理を行いたい場合は、string型の変数を使用しながら行う
もし、処理が重くなった場合は違う方法を探す


メッシュを管理する変数はshared_ptrで管理
受け取り先がローカル変数の場合はshared_ptrで受けとる
受け取り先がメンバ変数若しくはグローバル変数の場合はweak_ptrに

関数の引数は関数が終わりしだい引数が死ぬのでshared_ptrで扱うが
怖いときは参照体で管理させる


==================================================================


CRigidSupervise

剛体を管理するクラス
剛体の登録、初期化なども行う
剛体のポインターはあくまで保存しているだけなのでweak_ptrで管理する


----------------------------------------------------------------*/


#ifndef _CLASS_NAME_OBJECT_BASE_H_
#define _CLASS_NAME_OBJECT_BASE_H_


/*===============

	namespace

===============*/

/*---------------------------------------------------->

		登録グループ

<----------------------------------------------------*/
namespace OBJECT_LIST
{
	enum ID 
	{
		//	プレイヤー
		PLAYER		 = 0,

		//	ステージ
		STAGE		 = 1,


		BULLET				= 2,	//	弾
		WOOD_BOX			= 3,	//	木箱
		AIR					= 4,	//	空気
		ROCK				= 5,	//	岩
		APPLE				= 6,	//	リンゴ
		RIVER				= 7,	//	川
		RIVER2				= 8,	//	合流地点
		TREE_NUTS			= 9,	//	木の実
		MUSH_ROOM			= 10,	//	キノコ
		POND				= 11,	//	池
		KABO				= 12,	//	かぼちゃ
		BULLET_FLOWER		= 13,	//	弾が咲く花
		MUSH_ROOMS			= 14,	//	キノコの大群
		AIR_FLOWER			= 15,	//	空気が咲く花
		STAGE_TREE			= 16,	//	ステージに生える木
		SKY					= 17,	//	そら
		FISH				= 18,	//	魚
		FRUIT_TREE			= 19,	//	木の実が成る木
		BRIDGE				= 20,	//	橋
		STAGE_1_COLLISION	= 21,	//	当たり判定用(1ステージ目)
		LEAF				= 22,	//	草
		APPLE_TREE			= 23,	//	リンゴの木
		HELP				= 24,	//	ヘルプ
		STRAW_BERRY			= 25,	//	イチゴ
		STRAW_BERRY_IVY		= 26,	//	イチゴの蔦
		SPRING_FLOWER		= 27,	//	バネ華
		CLOUD				= 28,	//	雲
		BRANCH				= 29,	//	木の枝
		bSWITCH				= 30,	//	スイッチ	予約語のswitchとかぶるので先頭にButtonの頭文字をつける
		WIND_ZONE			= 31,	//	風が吹いている空間
		STONE				= 32,	//	高さの変わる岩
		CRACKS_WALL			= 33,	//	ヒビ割れた壁
		BREAK_ROCK			= 34,	//	ヒビ割れた壁から発生した破片
		SEESAW				= 35,	//	シーソー
		LARGE_ROCK			= 36,	//	大きい岩


		//--------
		//	ゴール
		//--------
		STAGE_1_GOAL_MUSHROOM,	//	ステージ1のキノコゴール


		//	最大数
		MAX_NUM,


		//--------
		//	初期化
		//--------
		FREE = -1,
	};
}


/*--------------------------------------------------------->
	ストックされているかどうかを管理
	下位1ビット目から4ビット目まではアイテムの情報
	下位5ビット目から8ビット目まではアイテムがどういう状態
	なのかを管理する
<---------------------------------------------------------*/
namespace STOCK
{
	enum STATE
	{
		//	フラグチェック
		CHECK			= 0x000f,


		//	体の中に何のアイテムが入っているのか
		FREE			= 0,
		ROCK			= OBJECT_LIST::ID::ROCK,
		BULLET			= OBJECT_LIST::ID::BULLET,
		WOOD_BOX		= OBJECT_LIST::ID::WOOD_BOX,
		AIR				= OBJECT_LIST::ID::AIR,
		APPLE			= OBJECT_LIST::ID::APPLE,
		STRAW_BERRY		= OBJECT_LIST::ID::STRAW_BERRY,


		//	本来はこっち
		//	体の中にアイテムが入っているかどうか
		//RESET			= 0x00ff,		//	フラグが立っている部分以外OFFに
		//NO_ABSORPTION	= 0x0010,		//	吸い込んでいない
		//DO_ABSORPTION	= 0x0020,		//	吸い込んだ
		//DO_STOCK		= 0x0040,		//	体内にストックされている
		//DO_SPIT		= 0x0080,		//	吐き出した


		//	ためしにこっちでやってみる
		RESET			= 0x00ff,		//	フラグが立っている部分以外OFFに
		NO_ABSORPTION	= 0x0100,		//	吸い込んでいない
		DO_ABSORPTION	= 0x0200,		//	吸い込んだ
		DO_STOCK		= 0x0400,		//	体内にストックされている
		DO_SPIT			= 0x0800,		//	吐き出した
	};
}


/*------------------------------------------------>
	オブジェクトにフィルターを設定する

	当たり判定フィルターを作っておく
	使用するのは下位2ビット
<------------------------------------------------*/
namespace FILTER
{
	enum NAME : u_int
	{

		COLLISION_OFF			= 0x0001,	//	0x0001	当たり判定を行わない
		COLLISION_ON			= 0x0002,	//	0x0002	当たり判定を行う
		COLLISION_WATER			= 0x0004,	//	0x0004	水中判定
		COLLISION_GROUND		= 0x0008,	//	0x0008	床判定のみ
		COLLISION_ABSORPTION	= 0x0010,	//	0x0010	吸い込み飲み


		MAX,	//	最大値
	};
}


/*------------------------------------------->
	登録したい剛体の番号を設定

	MESH型は固定物
	それ以外は流動物
<-------------------------------------------*/
namespace REGISTER_RIGID
{
	enum TYPE : short
	{
		NOT				= -1,	//	登録なし
		BOX				=  0,	//	立方体
		CAPSULE			=  1,	//	カプセル
		SPHERE			=  2,	//	球体
		COMPUND			=  3,	//	複合
		MESH			=  4,	//	三角集合(メッシュ)
		BOX_MESH		=  5,	//	箱とメッシュ(固形)
		CAPSULE_MESH	=  6,	//	カプセルとメッシュ(固形)
		SPHRE_MESH		=  7,	//	球とメッシュ(固形)
		COMPUND_MESH	=  8,	//	複合とメッシュ(固形)
	};
}


/*=============

	前方宣言

=============*/

//	シェーダーマネージャー
class CShaderManager;


/*=============

	Class宣言

=============*/

class CObjectBase
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CObjectBase();

	//--------------
	//	デストラクタ
	//--------------
	virtual ~CObjectBase();


public:


	//----------------
	//純粋仮想関数宣言
	//----------------

	//	初期化
	virtual void Init(LPDIRECT3DDEVICE9 _lpD3DDevice)   = 0;

	//	更新
	virtual void Update(LPDIRECT3DDEVICE9 _lpD3DDevice) = 0;

	//	開放
	virtual void Release()								= 0;


	//----------
	//	仮想関数
	//----------

	//	シャドウマップ入り
	virtual void Draw(LPDIRECT3DDEVICE9 _lpD3DDevice);


protected:


	//	メッシュを扱う
	shared_ptr<CMeshObject>  m_Mesh;


	//----------------
	//	行列関係を管理
	//----------------

	//  ワールド行列
	CMatrix		  m_mWorld;

	//  座標(x, y, z)
	CVector3	  m_vPos;

	//  加速量(x, y, z)
	CVector3	  m_vVec;

	//  大きさ(x, y, z)
	CVector3	  m_vScalSize;

	//  角度(x, y, z)
	CVector3	  m_vAng;


	//--------------------------------
	//	オブジェクトのステータスを管理
	//--------------------------------

	//  ステータス管理(現在何をしているかを知らせる)
	UINT		  m_State;

	//	 アイテムの状態(吸い込んだ、吸い込んでいないなどの情報を管理)
	UINT		  m_ItemState;

	//	フィルター	当たり判定を行うかどうかなどを管理する
	UINT		  m_Filter;

	//	一番初めに登録されるタグ
	string		  m_RegisterTag;

	//	処理中にタグを変更して判定などに使用したい場合はこっちを使う
	string		  m_UpdateTag;

	//	アイテムを吸い込んだときのカウンター
	float		  m_MaxCnt;

	//	アイテムを食べたときの回復量
	int			  m_ItemResilience;

	//	 ゲームマネージャーへの登録ID
	int			  m_Id;

	//	 アイテムのIDを登録する
	int			  m_ItemId;

	//	 落下速度などに使用
	float 		  m_Accel;

	//	 中心点のずれ
	float		  m_GroundAdjustment;

	//	 吐き出す位置の修正
	float		  m_SpitAdjustment;

	//  オブジェクトの重さ(体重)
	float		  m_ObjectWeight;


	//--------
	//	判定用
	//--------

	//	ゴール判定用
	bool		  m_GoalCheck;			//	true(ゴールした) : false(ゴールしていない)

	//	衝突検知判定
	bool		  m_EnableColl;			//	true(衝突)		 : false(衝突していない)

	//	オブジェクトの破棄フラグ
	bool		  m_EnableDestruction;	//	true(破棄する)	 : false(破棄しない)

	//	物理ワールドのワールド行列を取得するかどうか
	bool		  m_EnableGetWorld;		//	true(取得する)	 : false(取得しない)


	//--------------------------
	//	外部ワールドのポインター
	//--------------------------

	//	シェーダーのポインター
	//CShaderManager* m_pShader;

	//	シェーダーのパス指定
	int m_Pass;


	//------
	//	定数
	//------

	//　落下速度(共通)
	static const float GRAVITY_SPEED;		//	重力移動量
	static const float GRAVITY_SPEED_MAX;	//　重力移動量最大値


	//--------------------------------
	//	吐き出されたときの移動量まとめ
	//--------------------------------

	//	Y座標
	static const float SPIT_MOVE_Y_SPEED;		//	Y座標吐き出された時の移動量
	static const float SPIT_MOVE_Y_SPEED_MAX;	//	Y座標最大量

	//	Z座標
	static const float SPIT_MOVE_Z_SPEED;		//	Z座標吐き出された時の移動量
	static const float SPIT_MOVE_Z_SPEED_MAX;	//	Z座標最大量

	//	最大体重
	static const float MAX_WEIGHT;				//	デフォルト体重として使用


	//	b = 100 でやるとプログラムがstatic const int として認識するので
	//	static const int は許される
	static const int a = 0;
	enum{ b = 100 };


	//------------------------
	//　オブジェクトの共通処理
	//------------------------

	/*ObjectMove------------------------------------------------------------->
	
	共通の移動処理
	吸い込み時の移動や吐き出したときの挙動を管理

	_EnableGroundCheck	: 地面への落下処理を行うかどうか
							true  = 行う
							false = 行わない

	_EnableStep			: 吐き出されたときのステップ処理を行うかどうか
							true  = 行う
							false = 行わない

	return				: 戻り値無し
	
	<-----------------------------------------------------------------------*/
	void ObjectMove(bool _EnableGroundCheck = true, bool _EnableStep = true);


	/*DrawShader------------------------------------------------------------->
	
	共通のシャドウマップ入りシェーダー描画
	Draw関数にて呼び出し
	
	_lpD3DDevice		: デバイス
	
	return				: 戻り値無し
	
	<-----------------------------------------------------------------------*/
	void DrawShader(LPDIRECT3DDEVICE9 _lpD3DDevice);


	/*UpdateBulletWorld---------------------------------->

	物理ワールドのワールド行列を取得するかどうかを指定する

	_Enable			:	取得するかどうか

	return			:	戻り値無し

	<---------------------------------------------------*/
	void UpdateBulletWorld(bool _Enable);


public:


	//----------
	//	ゲッター
	//----------

	//	ワールド行列を返す
	CMatrix			GetWorld(){ return m_mWorld; }

	//	ポジションを返す
	CVector3		GetPosition(){ return m_vPos; }

	//	速度を返す
	CVector3		GetVec(){ return m_vVec; }
	
	//	拡大サイズを返す
	CVector3		GetScal(){ return m_vScalSize; }

	//	回転軸を返す
	CVector3		GetAngle(){ return m_vAng; }

	//	メッシュの情報を返す
	shared_ptr<CMeshObject>	GetMeshObject(){ return m_Mesh; }

	//	ステータスを返す
	UINT			GetState(){ return m_State; }

	//	アイテムに対しての状態を返す
	UINT			GetItemState(){ return m_ItemState; }

	//	フィルターを返す
	UINT			GetFilter() { return m_Filter; }

	//	登録タグを返す
	string			GetRegisterTag(){ return m_RegisterTag; }

	//	更新タグを返す
	string			GetUpdateTag(){ return m_UpdateTag; }

	//	回復量を返す
	int				GetItemResilience(){ return m_ItemResilience; }

	//	オブジェクトマネージャーへの登録IDを返す
	int				GetId(){ return m_Id; }

	//	アイテムIDを返す
	int				GetItemId(){ return m_ItemId; }

	//	落下加速量を返す	
	float			GetAccel(){ return m_Accel; }

	//	中心点のブレを返す
	float			GetGroundAdjustment(){ return m_GroundAdjustment; }

	//	吐き出す位置の修正値を返す
	float			GetSpitAdjustment(){ return m_SpitAdjustment; }

	//	オブジェクトの体重を返す
	float			GetObjectWeight(){ return m_ObjectWeight; }

	//	ゴール判定を返す
	bool			GetGoalCheck(){ return m_GoalCheck; }

	//	オブジェクトの破棄フラグを返す
	bool			GetEnableDestruction(){ return m_EnableDestruction; }

	//	衝突検知の状態を返す
	bool			GetEnableColl(){ return m_EnableColl; }

	//	行列を取得するフラグを返す
	bool			GetEnableBulletWorld(){ return m_EnableGetWorld; }

	//	シェーダーのポインターを返す
	//CShaderManager* GetShader(){ return m_pShader; }

	//	シェーダーの指定したパスを返す
	int				GetPass() { return m_Pass; }

	//	カウント
	float			GetMaxCnt(){ return m_MaxCnt; }


	//----------
	//	セッター
	//----------

	//	ワールド行列をセットする
	void SetWorld(CMatrix _World){ m_mWorld = _World; }

	//	ポジションをセットする
	void SetPosition(CVector3 _SetPos){ m_vPos = _SetPos; }

	//	速度をセットする
	void SetVec(CVector3 _Vec){ m_vVec = _Vec; }

	//	拡大サイズをセットする
	void SetScaling(CVector3 _Scal = CVector3(1, 1, 1)){ m_vScalSize = _Scal; }

	//	回転軸をセットする
	void SetAngle(CVector3 _Angle){ m_vAng = _Angle; }

	//	ステータスをセットする
	void SetState(UINT _State){ m_State = _State; }

	//	アイテムに対しての状態をセットする
	void SetItemState(UINT _State){ m_ItemState = _State; }

	//	フィルターをセットする
	void SetFilter(UINT _Filter) { m_Filter = _Filter; }

	//	タグを登録する
	void SetRegisterTag(string _Tag){ m_RegisterTag = _Tag; }

	//	更新タグを登録する
	void SetUpdateTag(string _Tag){ m_UpdateTag = _Tag; }

	//	回復量をセットする
	void SetItemResilience(int _Resilience){ m_ItemResilience = _Resilience; }

	//	オブジェクトマネージャーへの登録IDをセットする
	void SetId(int _Id){ m_Id = _Id; }

	//	アイテムIDをセットする
	void SetItemId(int _ItemId){ m_ItemId = _ItemId; }

	//	落下加速量をセットする
	void SetAccel(float _Accel){ m_Accel = _Accel; }

	//	中心点のブレをセットする
	void SetGroundAdjustment(float _Adjustment){ m_GroundAdjustment = _Adjustment; }

	//	吐き出す位置の修正値をセットする
	void SetSpitAdjustment(float _Adjustment){ m_SpitAdjustment = _Adjustment; }

	//	オブジェクトの体重をセットする
	void SetObjectWeight(float _Weight){ m_ObjectWeight = _Weight; }

	//	ゴール判定をセットする
	void SetGoalCheck(bool _GoalCheck){ m_GoalCheck = _GoalCheck; }

	//	オブジェクトの破棄フラグをセットする
	void SetEnableDestruction(bool _Enable) { m_EnableDestruction = _Enable; }

	//	衝突検知をセットする
	void SetEnableColl(bool _Enable){ m_EnableColl = _Enable; }

	//	バレットワールドの行列を取得するかどうかのフラグをセットする
	void SetEnableBulletWorld(bool _Enable){ m_EnableGetWorld = _Enable; }

	//	シェーダーをセットする
	//void SetShader(CShaderManager* _pShader){ m_pShader = _pShader; }

	//	指定したいパスをセットする
	void SetPass(int _Pass) { m_Pass = _Pass; }


private:


	int m_Cnt;


};


class CRigidSupervise
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CRigidSupervise(){}


	//--------------
	//	デストラクタ
	//--------------
	~CRigidSupervise(){}


	//	デフォルト値の質量
	static const float MASS_DEFAULT;

	//	最小サイズ値の質量
	static const float MASS_MIN;


	/*CreateRigidSphere----------------------------------------------------->

	球剛体を物理ワールドに登録して、初期化を行う
	球の大きさは、SphereSizeがデフォルト引数なら、メッシュの半径で登録
	サイズは必ず-1よりも大きなサイズになるので、-1以上なら指定されたサイズ
	を登録できるようにする
	指定された場合は、指定された値を剛体のサイズとして登録する


	_RigidSphere*	:	生成したい球剛体のポインターを渡す
	_Mesh*			:	メッシュの情報を渡す
	_vPos			:	初期座標
	_RegistId		:	リストへ登録するId

	//	デフォルト引数
	_SphereSize		:	登録する剛体のサイズ
	_mass			:	摩擦

	return			:	戻り値無し

	<--------------------------------------------------------------------*/
	static void CreateRigidSphere(
		weak_ptr<CBulletObj_Sphere>		_RigidSphere,
		shared_ptr<CMeshObject>			_Mesh,
		CVector3						_vPos,
		int								_RegistId,
		float							_SphereSize = -1,
		float							_mass		= 1
		);


	/*CreateRigidBox----------------------------------------------------->

	立方体剛体を物理ワールドに登録して、初期化を行う
	立方体の大きさは、vScaleSizeがデフォルト引数なら、
	メッシュのサイズで登録
	サイズは必ず-1よりも大きなサイズになるので、-1以上なら指定されたサイズ
	を登録できるようにする
	指定された場合は、指定された値を剛体のサイズとして登録する


	_RigidBox*		:	生成したい球剛体のポインターを渡す
	_Mesh			:	メッシュの情報を渡す
	_vPos			:	初期座標
	_RegistId		:	リストへ登録するId

	//	デフォルト引数
	_vScaleSize		:	登録する剛体のサイズ
	_mass			:	摩擦

	return			:	戻り値無し

	<-------------------------------------------------------------------*/
	static void CreateRigidBox(
		weak_ptr<CBulletObj_Box>	_RigidBox,
		shared_ptr<CMeshObject>		_Mesh,
		CVector3					_vPos,
		int							_RegistId,
		CVector3					_vScaleSize = CVector3(-1, -1, -1),
		float						_mass		= 1
		);


	/*CreateRigidCapsule-------------------------------------------------->

	カプセル剛体を物理ワールドに登録して、初期化を行う

	_Mesh			:	メッシュの情報を渡す
	_vPos			:	座標
	_vScale			:	拡大サイズ
	_RigidCapsule	:	生成したいカプセル剛体のポインターを渡す

	return			:	戻り値無し

	<--------------------------------------------------------------------*/
	static void CreateRigidCapsule(
		weak_ptr<CBulletObj_Capsule>	_spCapsule,
		shared_ptr<CMeshObject>			_spMesh,
		CMatrix							_mMatrix,
		CVector3						_vPos,
		float							_Height,
		int								_RegistId,
		CVector3						_vScaleSize		= CVector3(-1, -1, -1),
		float							_mass			= 1
		);


	/*CreateRigidCompound------------------------------------------------->

	複合剛体を物理ワールドに登録して、初期化を行う

	_Mesh			:	メッシュの情報を渡す
	_vPos			:	座標
	_vScale			:	拡大サイズ
	_RigidCompound	:	生成したい複合剛体のポインターを渡す

	return			:	戻り値無し

	<--------------------------------------------------------------------*/
	static void CreateRigidCompound(
		weak_ptr<CBulletObj_Capsule>	_spCapsule,
		shared_ptr<CMeshObject>			_Mesh,
		CVector3						_vPos,
		CVector3						_Scale,
		CBulletObj_Compound*			_RigidCompound
		);


	/*CreateRigidMesh------------------------------------------------->

	メッシュ剛体を物理ワールドに登録して、初期化を行う


	_RigidMesh*		:	登録したい剛体のタイプ
	_Mesh*			:	メッシュの情報を渡す
	_vPos			:	座標
	_RegistId		:	リストへ登録するId

	//	デフォルト引数
	_vScaleSize		:	拡大サイズ
	_RigidMesh		:	摩擦

	return			:	戻り値無し

	<----------------------------------------------------------------*/
	static void CreateRigidMesh(
		weak_ptr<CBulletObj_Mesh>	_RigidMesh,
		shared_ptr<CMeshObject>		_Mesh,
		CVector3					_vPos,
		int							_RegistId,
		CVector3					_vScaleSize = CVector3(-1.0f, -1.0f, -1.0f),
		float						_mass		= 1
		);


	/*PushRigid------------------------------------------------------->

	登録した剛体をリストに格納する

	template		:	剛体クラス

	_RegistId		:	検索したい番号を登録する
	_Rigid			:	登録したいオブジェクト

	return			:	戻り値無し

	<----------------------------------------------------------------*/
	template <class T>
	static void PushRigid(int _RegistId, weak_ptr<T> _Rigid)
	{
		//	ポインターがインスタンス化されていない場合は警告を出す
		if (_Rigid.lock() == nullptr)
		{
			MessageBox(NULL, "剛体ポインタがインスタンス化されていません", "警告", MB_OK);
			return;
		}


		//	登録IDをセット
		_Rigid.lock()->SetRegist(_RegistId);

		//	物理ListのId
		_Rigid.lock()->SetObjectListId(m_EndId);

		//	渡された剛体クラスのポインタをプッシュする
		m_lweRigidSupervise.push_back(weak_ptr<CBulletRigid_Base>(_Rigid));

		m_EndId += 1;
	}


	//	指定されたオブジェクトを返す
	static shared_ptr<CBulletRigid_Base> GetItObject(int _RegistId)
	{
		//	イテレータを生成
		auto itObjContainer = m_lweRigidSupervise.begin();

		//	リストの後尾までまわす
		while (itObjContainer != m_lweRigidSupervise.end())
		{
			//	指定されたIdが入っていた場合
			if (itObjContainer->lock()->GetRegist() == _RegistId)
			{
				//	指定されたIdの中身を返す
				return itObjContainer->lock();
			}

			//	無ければ次へ
			++itObjContainer;
		}

		return nullptr;
	}


	static shared_ptr<CBulletRigid_Base> GetItList(int _RegistId)
	{
		//	イテレータを生成
		auto itObjContainer = m_lweRigidSupervise.begin();

		//	リストの後尾までまわす
		while (itObjContainer != m_lweRigidSupervise.end())
		{
			//	指定されたIdが入っていた場合
			if (itObjContainer->lock() != nullptr
				&&
				itObjContainer->lock()->GetObjectListId() == _RegistId)
			{
				//	指定されたIdの中身を返す
				return itObjContainer->lock();
			}

			//	無ければ次へ
			++itObjContainer;
		}

		return nullptr;
	}


	static void DeleteItList()
	{
		//	リストのコピーをとる
		//auto CopyPtr = m_lweRigidSupervise;


		//------------------------------------
		//	weak_ptrの中身を一度すべて破棄する
		//------------------------------------
		auto It = m_lweRigidSupervise.begin();

		while (It != m_lweRigidSupervise.end())
		{
			(*It).lock() = nullptr;
			

			It = m_lweRigidSupervise.erase(It);

			//++It;
		}

		m_EndId = 0;


		////------------------------------------------
		////	コピーをとっておいたリストを入れなおして
		////	nullが入っているものがあれば飛ばす
		////	ついでに中のIDも更新
		////------------------------------------------

		//auto CopyIt = CopyPtr.begin();

		//while (CopyIt != CopyPtr.end())
		//{
		//	if ((*CopyIt).lock() != nullptr)
		//	{
		//		m_lweRigidSupervise.push_back((*CopyIt));
		//	}

		//	++CopyIt;
		//}

	}


	//----------
	//	ゲッター
	//----------

	//	IDのコピーを返す
	static int GetStockId(){ return m_StockId; }

	//	後尾のIDを返す
	static int GetEnd(){ return m_EndId; }

	//	リストを返す
	static list<weak_ptr<CBulletRigid_Base>> GetRigidList(){ return m_lweRigidSupervise; }


	//----------
	//	セッター
	//----------

	//	IDのコピーをセット
	static void SetStockId(int _Id){ m_StockId = _Id; }

	//	IDを-1しておく
	static void DeleteId(){ m_StockId -= 1; }


private:


	//	登録された剛体クラスをリストで保存
	static list<weak_ptr<CBulletRigid_Base>> m_lweRigidSupervise;


	//	ストックされているオブジェクトのID
	static int m_StockId;

	//	ストックされているオブジェクトの数
	static int m_EndId;
};


#endif  _CLASS_NAME_OBJECT_BASE_H_