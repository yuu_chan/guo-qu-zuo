/*----------------------------------------------

オブジェクトマネージャークラス
マップ上に生成されるオブジェクトを管理する

----------------------------------------------*/

#ifndef _CLASS_NAME_OBJECT_MANAGER_H_
#define _CLASS_NAME_OBJECT_MANAGER_H_


/*=============

Class宣言

=============*/

class CObjectManager
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CObjectManager();

	//--------------
	//	デストラクタ
	//--------------
	~CObjectManager();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	描画
	void Draw(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	開放
	void Release();

	//	指定されたオブジェクトを返す
	shared_ptr<CObjectBase> GetItObject(int _Id);

	//	指定されたクラスをインスタンス化してリストに格納する
	template <class T>
	inline shared_ptr<T> CreateObjectTask()
	{
		shared_ptr<T> temObject = make_shared<T>(); 
		PushObjectManager(temObject);
		return temObject;
	}

	//	シェーダーのポインターを自身の管理しているマネージャーに渡す
	void SetShaderManager(CShaderManager* _pShader);

	//	マネージャーを返す
	list<shared_ptr<CObjectBase>> GetObjectContainer(){ return m_listObjContainer; }

	//	指定したオブジェクトを破棄する
	void ItObjectKill(int _ObjectId);


private:


	//	マネージャー
	list<shared_ptr<CObjectBase>> m_listObjContainer;
	
	//	マネージャーリストに指定されたオブジェクトを格納
	void PushObjectManager(shared_ptr<CObjectBase> _spObject);
};

#endif  _CLASS_NAME_OBJECT_MANAGER_H_

///*----------------------------------------------
//
//オブジェクトマネージャークラス
//マップ上に生成されるオブジェクトを管理する
//
//----------------------------------------------*/
//
//#ifndef _CLASS_NAME_OBJECT_MANAGER_H_
//#define _CLASS_NAME_OBJECT_MANAGER_H_
//
//
///*=============
//
//Class宣言
//
//=============*/
//
//class CObjectManager
//{
//public:
//
//	//----------------
//	//	コンストラクタ
//	//----------------
//	CObjectManager();
//
//	//--------------
//	//	デストラクタ
//	//--------------
//	~CObjectManager();
//
//
//	//	初期化
//	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);
//
//	//	更新
//	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);
//
//	//	描画
//	void Draw(LPDIRECT3DDEVICE9 _lpD3DDevice);
//
//	//	開放
//	void Release();
//
//	//	指定されたオブジェクトを返す
//	CObjectBase* GetItObject(int _Id);
//
//	//	指定されたクラスをインスタンス化してリストに格納する
//	template <class T>
//	inline T* CreateObjectTask()
//	{
//		T* temObject = new T();
//		PushObjectManager(temObject);
//		return temObject;
//	}
//
//	//	シェーダーのポインターを自身の管理しているマネージャーに渡す
//	void SetShaderManager(CShaderManager* _pShader);
//
//	//	マネージャーを返す
//	list<CObjectBase *> GetObjectContainer(){ return m_listObjContainer; }
//
//	//	指定したオブジェクトを破棄する
//	void ItObjectKill(int _ObjectId);
//
//
//private:
//
//
//	//	マネージャー
//	list<CObjectBase *> m_listObjContainer;
//
//	//	マネージャーリストに指定されたオブジェクトを格納
//	void PushObjectManager(CObjectBase* _Object);
//};
//
//#endif  _CLASS_NAME_OBJECT_MANAGER_H_