/*--------------------------------------------------

ステージセレクトで使用される写真

--------------------------------------------------*/

#ifndef _CLASS_NAME_PICTURE_H_
#define _CLASS_NAME_PICTURE_H_


/*=============

namespace

=============*/

namespace STAGE_SELECT
{
	enum NAME
	{
		STAGE1 = 1,
		STAGE2 = 2,
		STAGE3 = 3,

		MAX,
	};
}


/*=============

Class宣言

=============*/
class CPicture : public CTextureBase
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CPicture();

	//--------------
	//	デストラクタ
	//--------------
	~CPicture();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update();

	//	描画
	void Draw(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	開放
	void Release();


	static enum SELECT_PICTURE
	{
		STAGE_1 = 1,
		STAGE_2 = 2,
	};


};

#endif  _CLASS_NAME_PICTURE_H_