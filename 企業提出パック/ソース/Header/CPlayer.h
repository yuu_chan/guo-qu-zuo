/*----------------------------------------------------------------
		プレイヤークラス
		CObjectBase継承
		スキンメッシュ使用
----------------------------------------------------------------*/

#ifndef _CLASS_NAME_PLAYER_H_
#define _CLASS_NAME_PLAYER_H_


/*========================

Class宣言

========================*/

class CPlayer : public CObjectBase
{
public:
	
	//----------------
	//	コンストラクタ
	//----------------
	CPlayer();

	//--------------
	//	デストラクタ
	//--------------
	~CPlayer();

	
	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);
	
	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);
	
	//	描画
	void Draw(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	解放
	void Release();


	//----------
	//	ゲッター
	//----------

	int GetSpitCnt(){ return m_SpitCnt; }


private:


	/*<-----------------------------------
		プレイヤーステータス管理
	----------------------------------->*/

	//----------------------
	//　プレイヤーの状態管理
	//----------------------
	enum STATE
	{	
		//------------------------------
		//	初期化とデフォルト状態を管理
		//------------------------------
		INIT    = 0x0000,		//	初期化用
		DEFAULT = 0x0001,		//	通常状態
		FORMAT  = 0x0fff,		//	フラグ整理 0x0fff


		//---------------------------------------------------
		//	アクションフラグを管理  
		//	下位2ビット	-> 歩行管理
		//	下位3ビット -> 吸い込んでいる又は吸い込んでいない
		//	下位4ビット -> ジャンプ管理
		//	下位5ビット -> 浮力が働いている又は働いていない
		//	下位6ビット -> 空気が放出されている(消す)
		//---------------------------------------------------
		ACTION_WALKING			= 0x0002,	//	歩行中				   0010 
		ACTION_ABSORPTION_OFF	= 0xfffb,	//	吸い込みアクションなし 1011
		ACTION_ABSORPTION_ON	= 0x0004,	//　吸い込みアクション中   0100
		ACTION_JUMP_OFF			= 0xfff7,	//	ジャンプしていない	   0111
		ACTION_JUMP_ON			= 0x0008,	//　ジャンプした		   1000
		ACTION_SPRING_OFF		= 0xffef,	//	バウンディングモード   1101
		ACTION_SPRING_ON		= 0x0010,	//	バウンディングモード   0010

	};

	//---------------------------
	//　アニメーションキーを管理 
	//---------------------------
	enum ANIMATION_KEY
	{
		//	指定なし
		//RESET			= -1,

		//	デフォルトポーズ
		DEFAULT_POSE	= 0,

		//	立つ
		STAND			= 1,

		//	歩く
		WALK			= 2,

		//	吸い込む
		ABSORPTION		= 3,

		//	飲み込む
		SWALLOW			= 4,

		//	吐き出す
		SPIT			= 5,

		//	ためておく
		STOCK			= 6,

		//	クリア
		CLEAR			= 7,

		//	クリアモーション固定
		CLEAR_CONST		= 8,

		//	食べる
		EAT				= 10,
	};

	//----------------------
	//	アニメーションの速度
	//----------------------
	enum ANIMATION_TIME
	{

	};


	//	アニメーション再生時間
	float				m_AnimationTime;

	//	重力
	float				m_Grav;

	//	内積用のアングルY
	float				m_NormalAngY;
	
	//	ストックされている時間
	int					m_StockCnt;

	//	水中生存カウンター
	int					m_WaterInCnt;

	//	アニメーションが最後までいったかどうか
	bool				m_AnimationPeriod;	//	true(最後に到達) false(まだ)

	//	内積を求めるようのフラグ
	bool				m_NormalCalc;		//	true(求める) false(求めない)

	//	ジャンプ状態管理
	bool				m_jumpFlg;			//	true(ジャンプする) false(ジャンプしない)

	//	吐き出し可能
	bool				m_EnableSpit;		//	true(吐き出すことが可能) false(吐き出し不可)

	//	物理ワールド
	shared_ptr<CBulletObj_Sphere> m_spRigidSphere;

	//	水中の重み
	float				m_WaterWeight;

	//	水に流される
	float				m_WaterFlow;

	//	吐き出しを行っているアニメのカウンター
	int					m_SpitCnt;


	//------------------------------
	//	プレイヤーのみが使用する関数
	//------------------------------

	//	全行列更新
	void UpdateMatrix();
	
	//	床に立たせておく
	void StandPosition();
	
	//	飲み込みアクション
	void SwallowAction();
	
	//	吐き出しアクション
	void SpitAction();

	//	ステージ２用処理
	void Stage2Update();


	/*---------------------------------------------------------->
		プレイヤーの吐き出し処理
		自身のステータスと対象のオブジェクトの
		ステータスを変更させる

		Object_Id_1		:	プレイヤーのID
		Object_Id_2		:	プレイヤー以外のID

		return		:	戻り値なし
	<----------------------------------------------------------*/

	void Spit(
		int _Object_Id_1,
		int _Object_Id_2
		);


	/*---------------------------------------------------------->
		壁ずりによる座標修正関数

		_RayDir		:	レイを発射する位置

		return		:	戻り値なし
	<----------------------------------------------------------*/

	void WallScratchUpdatePosition(CVector3 _vRayDir);


	/*---------------------------------------------------------->
		吸い込みを行う関数

		_RayDir		:	レイを発射する位置

		return		:	戻り値なし
	<----------------------------------------------------------*/
	
	void AbsorptionAction(CVector3 _vRayDir);


	/*---------------------------------------------------------->
		(通常)移動アクション
	
		_vToVec		:	移動する方向
	
		return		:	戻り値なし
	<----------------------------------------------------------*/

	void MovePosition(CVector3 _vToVec);


	/*Jump----------------------------------------------------->
		
		ジャンプ関数
		スペースを押し込んだ時間によって、ジャンプの段階を変える

		return : 戻り値なし

	<---------------------------------------------------------*/
	void ChageJump();


	/*CheckActionKey------------------------------------------------->

		キーチェック用関数
		falseが返されればアクションが起こる条件ではない
		trueが返されればアクションが起こる条件になる


		return : bool型

	<---------------------------------------------------------*/
	bool CheckActionKey();


	//--------------
	//	後で整理する
	//--------------
	float m_ChageJump;

	bool m_AbleChage;

	static const float JUMP_CHAGE;

	static const float JUMP_GAUGE_MAX;

};



class CStuffedMeter
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CStuffedMeter(){}

	//------------
	//デストラクタ
	//------------
	~CStuffedMeter(){}


	//	現在の満腹度の値
	static int m_StuffedMeter;

	//	満腹度の最大値
	static const int STUFFED_METER_MAX;

	//	満腹度の最小値
	static const int STUFFED_METER_MIN;


	//	満腹度更新
	static void UpdateStuffedMeter();


private:


	//	回復量
	static int m_Recovery;

	//	満腹度が下がっていく量
	static int m_DownMeter;


};


#endif  _CLASS_NAME_PLAYER_H_