/*----------------------------------------------------------------

リザルト画面

----------------------------------------------------------------*/


#ifndef _CLASS_NAME_AA_H_
#define _CLASS_NAME_AA_H_


/*=============

Class宣言

=============*/

class CResultScene : public CSceneBase
{


public:

	//----------------
	//	コンストラクタ
	//----------------
	CResultScene();

	//--------------
	//	デストラクタ
	//--------------
	~CResultScene();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新											
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	描画
	void Draw(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	開放
	void Release();


private:



};


#endif  _CLASS_NAME_AA_H_

