/*----------------------------------------------------------------

Class説明

----------------------------------------------------------------*/

#ifndef _CLASS_NAME_RIVER_H_
#define _CLASS_NAME_RIVER_H_


/*=============

Class宣言

=============*/

class CRiver : public CObjectBase
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CRiver();

	//--------------
	//	デストラクタ
	//--------------
	~CRiver();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	描画
	void Draw(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	解放
	void Release();


private:

};


#endif  _CLASS_NAME_RIVER_H_

