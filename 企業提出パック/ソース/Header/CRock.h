/*--------------------------------------------------

岩クラス

重さが1.0だった場合は大きな岩を読み込む

--------------------------------------------------*/

#ifndef _CLASS_NAME_ROCK_BASE_H_
#define _CLASS_NAME_ROCK_BASE_H_


/*=============

Class宣言

=============*/
class CRock : public CObjectBase
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CRock();

	//--------------
	//	デストラクタ
	//--------------
	~CRock();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	解放
	void Release();


private:


	//------------------------------------------
	//	岩のサイズによって使用する剛体を変更する
	//------------------------------------------

	//	通常の岩
	shared_ptr<CBulletObj_Sphere>	m_spRigidSphere;

	//	特大サイズの岩
	shared_ptr<CBulletObj_Box>		m_spRigidBox;

	//	使用する岩がどっちかを管理しておく
	int m_UseType;


	//--------------------------------------------
	//
	//	岩のサイズによって使用する剛体を替えるので
	//	あらかじめタグを作っておく
	//	名前でわかるが、一応意味を書いておく
	//
	//	SAMLL	:	最小サイズの岩を扱う
	//
	//	LARGE	:	最大サイズの岩を扱う
	//
	//--------------------------------------------
	enum SIZE
	{
		SMALL = 0,

		LARGE = 1,
	};

};

#endif  _CLASS_NAME_ROCK_BASE_H_