/*------------------------------------------------------

	シーン管理用クラスのベース

------------------------------------------------------*/

#ifndef _CLASS_NAME_SCENE_BASE_H_
#define _CLASS_NAME_SCENE_BASE_H_



/*===============

namespace

===============*/

/*------------------------------>
	シーンを管理するID

	全シーンのIDを保管
<------------------------------*/
namespace SCENE
{

	/*
	
		各シーンの状態を管理
	
	*/
	enum NAME
	{
		//	何もなし
		NOT = -1,


		//----------
		//	タイトル
		//----------
		TITLE_INIT			= 0,					//	初期化用
		NOW_TITLE			= 1,					//	現在タイトルシーン
		TITLE_RELEASE		= 2,					//	現在のタイトルシーンを開放


		//------------------
		//	ステージセレクト
		//------------------
		STAGE_SELECT_INIT	= 3,					//	初期化用
		NOW_STAGE_SELECT	= 4,					//	現在ステージセレクトシーン
		STAGE_SELECT_RELEASE= 5,					//	現在のステージセレクトシーンを開放


		//--------
		//	ゲーム
		//--------
		GAME_INIT			= 6,					//	初期化用
		NOW_GAME			= 7,					//	現在ゲームシーン
		GAME_RELEASE		= 8,					//	現在のゲームシーンを開放


		MAX,										//	最大値
	};


	/*
	
		シーンの実行状態
	
	*/
	enum STEP
	{
		INIT		= 0,
		UPDATE		= 1,
		RELEASE		= 2,
	};
}


/*=============

Class宣言

=============*/

class CSceneBase
{
public:
	//----------------
	//	コンストラクタ
	//----------------
	CSceneBase();

	//--------------
	//	デストラクタ
	//--------------
	~CSceneBase();


	//----------------
	//純粋仮想関数宣言
	//----------------

	//	初期化
	virtual void Init(LPDIRECT3DDEVICE9 _lpD3DDevice)	 = 0;

	//	更新											
	virtual void Update(LPDIRECT3DDEVICE9 _lpD3DDevice)  = 0;

	//	描画
	virtual void Draw(LPDIRECT3DDEVICE9 _lpD3DDevice)	 = 0;

	//	開放
	virtual void Release() = 0;


	/*-------------------------------------------------->
		次のシーンへ移動

		EnableNextScene->次のシーンへ行くかどうかの判定
			キーが押されたらNowSceneに
			NextSceneの情報を入れる
			初期化を行ってNowシーンにいく
	<--------------------------------------------------*/

	static int EnableNextScene(int _NowScene, bool& _Back ,bool _Enable = false);

};

#endif  _CALSS_NAME_SCENE_BASE_H_