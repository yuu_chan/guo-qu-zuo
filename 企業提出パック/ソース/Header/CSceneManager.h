/*----------------------------------------------

シーンを管理する
更新描画処理は基本的にvectorで管理
shared_ptrをもたせることによって簡単に
削除できるようにしておく

----------------------------------------------*/


#ifndef _CLASS_NAME_SCENE_MANAGER_H_
#define _CLASS_NAME_SCENE_MANAGER_H_


#include "CSceneBase.h"

/*=============

前方宣言

=============*/

//	ゲームシーン
class CGameScene;

//	タイトルシーン
class CTitleScene;

//	ステージセレクトシーン
class CStageSelectScene;

//	リザルトシーン
class CResultScene;


/*=============

Class宣言

=============*/
class CSceneManager
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CSceneManager();

	//--------------
	//	デストラクタ
	//--------------
	~CSceneManager();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新と描画をまとめたもの
	void Proc(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	開放
	void Release();


	//----------
	//	セッター
	//----------

	//	現在のシーンをセットする
	void SetNowScene(int _NowScene){ m_NowScene = _NowScene; }


	//----------
	//	ゲッター
	//----------

	//	現在のシーンを返す
	int GetNowScene(){ return m_NowScene; }


	//	今現在どのステージを選んでいるか
	static int m_NowSelectStage;


private:


	/*SceneProcess------------------------------->
	
	すべてのシーンの初期化 更新描画 開放を管理
	
	_lpD3DDevice	:	デバイス

	return			:	戻り値なし
	
	<-------------------------------------------*/
	void SceneProcess(LPDIRECT3DDEVICE9 _lpD3DDevice);


	/*CreateScene------------------------------------->

	新しいシーンを生成する関数
	インスタンス化した後vectorにプッシュ
	vectorは、shared_ptrで管理しているため
	関数が終了しても参照カウンタが1残るようにする

	どのクラスがきてもいいようにテンプレートにしておく

	template<class T>		:	生成するシーンクラス

	戻り値					:	なし

	<------------------------------------------------*/
	template <class T>
	void CreateScene()
	{
		shared_ptr<T> Scene = make_shared<T>();
		m_vspScene.push_back(Scene);
	}


	/*--------------------------------->
	
	各シーンのプロセス
	
	Init	:	シーンを初期化

	Update	:	更新と描画を行う

	Release	:	シーンを開放
	
	<---------------------------------*/

	void SceneInit();

	void SceneUpdate();

	void SceneRelease();


	/*--------------------------------------->
	
	生成されたシーンを格納している
	vectorの中身を開放
	つまり、この関数が呼ばれればシーンが消える
	
	<---------------------------------------*/
	void KillScene();


	//	シーン管理用変数
	vector<shared_ptr<CSceneBase>> m_vspScene;

	//	現在のシーン
	int m_NowScene;

	//	初期化  更新  開放のどれか
	int m_SceneStep;

	//	前のシーンに戻るかどうか
	bool m_AbleBack;	//	true(戻る)	:	false(戻らない)

};


#endif  _CLASS_NAME_SCENE_MANAGER_H_
