/*----------------------------------------------------------------

タイトル専用シェーダ

----------------------------------------------------------------*/


#ifndef _CLASS_NAME_TITLE_SHADER_H_
#define _CLASS_NAME_SCENE_SHADER_H_


//	ランダム管理
#include "Rand.h"


/*=============

Class宣言

=============*/

class CSceneShader : public CShaderBase
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CSceneShader();

	//--------------
	//	デストラクタ
	//--------------
	~CSceneShader();


	//	初期化
	void Init();

	//	開放
	void Release();

	//	描画関数(念のため純粋仮想関数化)
	void DrawMesh(shared_ptr<CMeshObject> _pMesh, int _Pass){};

	//	ハイトマップと法線マップをを更新
	void UpdateMap();

	//	デバイス
	void OnLostDevice();
	void OnResetDevice();


	/*DrawTitle----------------------------->
	タイトル描画

	_pTexture	:	テクスチャのポインタ
	_Pass		:	指定したいPass
	_mWorld		:	ワールド行列

	return		:	戻り値無し

	<--------------------------------------*/
	void Draw(
		shared_ptr<CTexture>	_spTexture, 
		int&					_Pass,
		CMatrix&				_mMatrix
		);


	//----------
	//	セッター
	//----------

	//	波を作る位置を設定する
	void SetCreateWavePos(const CVector3 _vPos){ m_vCreateWavePos = _vPos; }


private:


	//	Heightマップを作る [0](高さ) [1](速度)
	CTexture m_HeightTex[2];

	//	法線マップを作る
	CTexture m_NormalTex;

	//	RT
	LPDIRECT3DSURFACE9 m_lpRenderTarget;

	//	深度
	LPDIRECT3DSURFACE9 m_lpDepthStencil;

	//	テクスチャー更新ようタイマー
	int m_Time, m_TextureTime;

	//	波生成のランダム変数
	CRand Rand;

	//	波紋を作る位置
	CVector3 m_vCreateWavePos;

	//	波紋を作り始めるかどうかを管理
	bool m_EnableCreateWave;	//	true(作ることが可能) false(作ることが不可能)

};


#endif  _CLASS_NAME_TITLE_SHADER_H_

