/*----------------------------------------------------------------

シーソー
重いものがある方に傾く

----------------------------------------------------------------*/


#ifndef _CLASS_NAME_SEESAW_H_
#define _CLASS_NAME_SEESAW_H_


/*=============

Class宣言

=============*/

class CSeesaw : public CObjectBase
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CSeesaw();

	//--------------
	//	デストラクタ
	//--------------
	~CSeesaw();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	解放
	void Release();


private:


	//------------------------------------------------------
	//	右か左どっちにあるかを変数に格納
	//
	//	Obj   This		 Objのほうが-だった場合はLEFTと判定
	//		  This	Obj  Objのほうが+だった場合はRIGHTと判定
	//------------------------------------------------------
	enum DIRECTION
	{
		CENTER = 0,
		LEFT = 1,
		RIGHT = 2,
	};


	//	立方体剛体で登録する
	shared_ptr<CBulletObj_Box> m_spRigidBox;

	//	傾く方向を管理
	int m_Direction;

	//	距離によって傾く角度を変更しておく
	float m_SlopeDegree;


	/* UpdateSeesaw-------------------------------->

	シーソーの傾き更新処理

	_Direction		:	傾いている方向

	return			:	戻り値無し

	<---------------------------------------------*/

	void UpdateSeesaw(const int _Direction);


};


#endif  _CLASS_NAME_SEESAW_H_

