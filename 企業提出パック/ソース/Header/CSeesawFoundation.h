/*----------------------------------------------------------------

シーソーの土台部分

----------------------------------------------------------------*/

#ifndef _CLASS_NAME_SEESAWFOUNDATION_H_
#define _CLASS_NAME_SEESAWFOUNDATION_H_


class CSeesawFoundation : public CObjectBase
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CSeesawFoundation();

	//--------------
	//	デストラクタ
	//--------------
	~CSeesawFoundation();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	解放
	void Release();

};


#endif  _CLASS_NAME_SEESAWFOUNDATION_H_