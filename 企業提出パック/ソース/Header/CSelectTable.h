/*--------------------------------------------------

class説明

--------------------------------------------------*/

#ifndef _CLASS_NAME_SELECT_TABLE_H_
#define _CLASS_NAME_SELECT_TABLE_H_


/*=============

Class宣言

=============*/
class CSelectTable : public CTextureBase
{
public:

	//----------------
	//  コンストラクタ
	//----------------
	CSelectTable();

	//--------------
	//  デストラクタ
	//--------------
	~CSelectTable();
	
	
	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update();

	//	描画
	void Draw(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	開放
	void Release();


private:


	CMeshObject* m_pMesh;
};


#endif  _CLASS_NAME_SELECT_TABLE_H_