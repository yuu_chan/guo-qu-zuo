/*-------------------------------------------------------------

シェーダに必要な基本的なデータや処理を扱う


-------------------------------------------------------------*/

#ifndef _CLASS_NAME_SHADER_BASE_H_
#define _CLASS_NAME_SHADER_BASE_H_


#include "CMesh.h"


/*===============

namespace

===============*/

//	シェーダーの描画モード
namespace DRAW_MODE
{

	//	シェーダーのID
	enum SHADER_ID
	{
		//	特に使っていない
		VERTEX		= 0,

		//	シャドウマップ
		SHADOW_MAP	= 1,

		//	マイシェーダー
		MY			= 2,

		//	ぼかしシェーダー
		BOKASHI		= 3,

		//	水面
		WATER		= 4,

		//	IDの数	
		MAX
	};

}


/*=============

Class宣言

=============*/

class CShaderBase
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CShaderBase();

	//--------------
	//	デストラクタ
	//--------------
	virtual ~CShaderBase();


	//	初期化
	virtual void Init() = 0;

	//	解放
	virtual void Release();

	//	描画
	virtual void DrawMesh(shared_ptr<CMeshObject> _pMesh, int _Pass) = 0;


	//----------------------
	//	パラメータ設定
	//	シェーダに行列を設定
	//----------------------

	//	ワールド
	virtual void SetConstTransformWorld(const CMatrix* _Mat)
	{
		//	NULLチェック
		if (m_Effect == nullptr)return;

		//	エフェクトセット
		m_Effect->SetMatrix("mW", _Mat);

		m_Effect->SetFloat("Y", _Mat->_42);
	}

	virtual void SetConstTransformWorld2(const CMatrix* _Mat)
	{
		//	NULLチェック
		if (m_Effect == nullptr)return;

		//	エフェクトセット
		m_Effect->SetMatrix("mW2", _Mat);

		m_Effect->SetFloat("Y", _Mat->_42);
	}

	//	ビュー
	virtual void SetConstTransformView(const CMatrix* _Mat)
	{
		//	NULLチェック
		if (m_Effect == nullptr)return;

		//	エフェクトセット
		m_Effect->SetMatrix("mV", _Mat);
	}

	//	射影
	virtual void SetConstTransformProj(const CMatrix* _Mat)
	{
		//	NULLチェック
		if (m_Effect == nullptr)return;

		//	エフェクトセット
		m_Effect->SetMatrix("mP", _Mat);
	}

	//	ワールド
	virtual void SetTransformWorld(CMatrix* _Mat)
	{
		//	NULLチェック
		if (m_Effect == nullptr)return;

		//	エフェクトセット
		m_Effect->SetMatrix("mW", _Mat);
	}

	//	ビュー
	virtual void SetTransformView(CMatrix* _Mat)
	{
		//	NULLチェック
		if (m_Effect == nullptr)return;

		//	エフェクトセット
		m_Effect->SetMatrix("mV", _Mat);
	}

	//	射影
	virtual void SetTransformProj(CMatrix* _Mat)
	{
		//	NULLチェック
		if (m_Effect == nullptr)return;

		//	エフェクトセット
		m_Effect->SetMatrix("mP", _Mat);
	}

	//	ライト情報
	virtual void SetLightDir(CVector3* _vDir)
	{
		//	NULLチェック
		if (m_Effect == nullptr)return;

		//	エフェクトセット
		m_Effect->SetValue("LightDir", _vDir, sizeof(CVector3));
	}

	//	カメラ位置設定
	virtual void SetCamPos(CVector3* _vPos)
	{
		//	NULLチェック
		if (m_Effect == nullptr)return;

		//	エフェクトセット
		m_Effect->SetValue("CamPos", _vPos, sizeof(CVector3));
	}

	//	UVスクロール値を設定
	virtual void SetScroll(float _Scroll)
	{
		//	NULLチェック
		if (m_Effect == nullptr)return;

		//	スクロール値をセットする
		m_Effect->SetFloat("Scroll", _Scroll);
	}

	//	ミラーのクリップを設定
	virtual void SetClip(const int _Clip = 0)
	{
		if (m_Effect == nullptr) return;

		//	ミラーのクリップをセット 0で無効 1で有効
		m_Effect->SetInt("bClip", _Clip);
	}

	//	ミラーのテクスチャを設定
	virtual void SetMirrorTex(const shared_ptr<CTexture> _spTex)
	{
		if (m_Effect == nullptr) return;

		m_Effect->SetTexture("MirrorTex", _spTex->GetTex());
	}

	//	背景画像(通常画面)をサンプラーに設定させる
	virtual void SetBackTex(const shared_ptr<CTexture> _spTex)
	{
		if (m_Effect == nullptr) return;

		m_Effect->SetTexture("BackTex", _spTex->GetTex());
	}

	//	屈折表現をするかどうかのフラグ
	virtual void SetRefract(int _Refract)
	{
		if (m_Effect == nullptr) return;

		m_Effect->SetInt("bRefract", _Refract);
	}


	//----------
	//	セッター
	//----------

	//	エフェクトをセットする
	void SetEffect(ID3DXEffect* _Effect){ m_Effect = _Effect; }

	//	マネージャーへの登録IDをセットする
	void SetId(int _Id){ m_ManagerId = _Id; }

	//	シェーダ描画モードへの登録IDをセットする
	void SetModeId(int _Id){ m_DrawModeId = _Id; }

	//	先頭IDをセットする
	void SetFirstId(int _Id){ m_FirstId = _Id; }

	//	後尾IDをセットする
	void SetLastId(int _Id){ m_LastId = _Id; }


	//----------
	//	ゲッター
	//----------

	//	エフェクトを返す
	ID3DXEffect* GetEffect(){ return m_Effect; }

	//  マネージャへの登録IDを返す
	int GetId(){ return m_ManagerId; }

	//	シェーダ描画モードへの登録IDを返す
	int GetModeId(){ return m_DrawModeId; }

	//	先頭IDを返す
	int GetFirstId(){ return m_FirstId; }

	//	後尾IDを返す
	int GetLastId(){ return m_LastId; }


	/*DefaultSetState-------------------------------->

	シェーダー側に渡す基本的な情報を
	あらかじめ、関数化しておく

	_CamPos		:		カメラのポジションをセット
	_mProj		:		射影行列をセット
	_mView		:		ビュー行列をセット

	
	return		:	戻り値なし

	<-----------------------------------------------*/
	void DefaultSetState(
		CVector3 _CamPos  = GET_CAMERA.GetCameraWorldPosition(), //	カメラのワールド座標をセット
		CMatrix  _mProj	  = GET_CAMERA.GetProjMatrix(),			 //	射影行列をセット
		CMatrix  _mView	  = GET_CAMERA.GetViewMatrix()			 //	ビュー行列をセット
		);


protected:


	//	エフェクト
	ID3DXEffect* m_Effect;

	//	テクスチャ
	CTexture	 m_TexDot;

	//	マネージャーへの登録ID
	int m_ManagerId;

	//	シェーダ描画モードへの登録ID
	int m_DrawModeId;

	//　先頭ID
	int m_FirstId;

	//	後尾ID
	int m_LastId;


};

#endif  _CLASS_NAME_SHADER_BASE_H_