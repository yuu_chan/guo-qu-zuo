/*----------------------------------------------

シェーダークラスを管理するクラス
必要な更新なども行う

----------------------------------------------*/

#ifndef _CLASS_NAME_SHADER_MANAGER_H_
#define _CLASS_NAME_SHADER_MANAGER_H_


/*======================

マクロ宣言

======================*/

#define GET_SHADER CShaderManager::GetInstance()


/*=============

namespcae

=============*/

/*------------------------------------->

シェーダー関係のデータをenumでもっておく

<-------------------------------------*/
namespace DRAW_SHADER
{

	/*

	描画したいモード


	SHADOW	:	影描画を呼び出す

	BOKASHI	:	ぼかしを呼びだす

	MY		:	メッシュ描画を呼び出す

	WATER	:	水面波紋を呼び出す

	*/
	enum NAME
	{
		SHADOW	= 0,
		BOKASHI = 1,
		MY		= 2,
		WATER	= 3,
		MAX,
	};


	/*

	指定したいパスの名前


	DEFAULT		:	デフォルト描画

	SPHERE		:	環境マップあり

	NORMAL		:	法線マップあり

	*/
	enum PASS
	{
		DEFAULT = 0,
		SPHERE  = 4,
		NORMAL  = 5,
	};



	/*
		使用したいRT(念のため作っておく)

		COLOR		:	通常使用している色を摘出するRT
		BLOOM		:	ブルームで使用しているRT(MRT)
	*/


	enum USE_RT
	{
		COLOR		= 0,
		BLOOM		= 1,
	};
}


/*=============

前方宣言

=============*/

//	ゲームワールド
class GameWorld;

//	エフェクトワールド
class CEffectWorld;

//	ステージセレクトワールド
class CStageSelectWorld;


/*=============

Class宣言

=============*/

class CShaderManager
{
private:

	//----------------
	//	コンストラクタ
	//----------------
	CShaderManager();

	//--------------
	//	デストラクタ
	//--------------
	~CShaderManager();


public:


	static CShaderManager& GetInstance()
	{
		static CShaderManager Instance;
		return Instance;
	}


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	解放
	void Release();


	//-----------------------------------------
	//	シェーダー描画に必要な手順をまとめておく
	//-----------------------------------------

	//	シェーダー描画開始
	void DrawShaderBegin();

	void DrawShaderBegin(CVector3 _vPos);

	//	ブルームシェーダー描画終了
	void DrawBloomShaderEnd();

	//	ミラーの描画に必要な開始処理
	void DrawMirrorBegin();

	//	ミラーの描画に必要な終了処理
	void DrawMirrorEnd();

	//	背景画像描画開始
	void DrawBackBegin();

	//	背景画像描画終了
	void DrawBackEnd();


	//----------
	//	ゲッター
	//----------

	//	シャドウマップシェーダのポインターを返す
	shared_ptr<CShadowMapShader>	GetShadowMapShader(){ return m_spShadowMapShader; }

	//	ぼかしシェーダのポインターを返す
	shared_ptr<CBokashiShader>		GetBokashiShader(){ return m_spBokashiShader; }

	//	マイシェーダのポインターを返す
	shared_ptr<CMyShader>			GetMyShader(){ return m_spMyShader; }

	//	水シェーダのポインターを返す
	shared_ptr<CWaterShader>		GetWaterShader() { return m_spWaterShader; }

	//	シーンシェーダのポインターを返す
	shared_ptr<CSceneShader>		GetSceneShader(){ return m_spSceneShader; }


	//----------
	//	セッター
	//----------

	//	ゲームワールドをコピーしてくる
	void SetGameWorld(shared_ptr<CGameWorld> _World){ m_spGameWorld = _World; }


private:


	//	ゲームワールドのポインターのコピー
	weak_ptr<CGameWorld>			m_spGameWorld;

	//	ステージセレクトのポインター
	shared_ptr<CStageSelectWorld>	m_spSelectWorld;

	//	シャドウマップを扱う
	shared_ptr<CShadowMapShader>	m_spShadowMapShader;

	//	ぼかしを扱う
	shared_ptr<CBokashiShader>		m_spBokashiShader;

	//	マイシェーダー
	shared_ptr<CMyShader>			m_spMyShader;

	//	シーン用のシェーダ
	shared_ptr<CSceneShader>		m_spSceneShader;

	//	水
	shared_ptr<CWaterShader>		m_spWaterShader;

	//	ミラー用テクスチャ
	shared_ptr<CTexture>			m_spWaterReflection;

	//	ミラー用RT
	LPDIRECT3DSURFACE9 m_MirrorRT;
	
	//	背景描画用画像
	shared_ptr<CTexture>			m_spBack;

	//	背景描画用RT
	LPDIRECT3DSURFACE9				m_BackRT;


	//------------------------------------------------------
	//	インスタンス化と初期化を行ってくれる関数を作っておく
	//------------------------------------------------------
	template <class T>
	inline shared_ptr<T> CreateShaderObject()
	{
		shared_ptr<T> CreateObject = make_shared<T>();
		CreateObject->Init();
		return CreateObject;
	}


	/*CreateShadowMap------------------------------------------->
	シャドウマップに必要な深度情報を生成する関数
	一度だけ通す

	_vLightPos		:	ライトの座標
	_CameraHead		:	カメラの向いている方向

	return			;	戻り値なし
	
	<----------------------------------------------------------*/
	void CreateShadowMap(CVector3 _vLightPos, CVector3 _CameraHead);


	/*--------------------------------------------------->

	ライトブルーム処理

	RT0には通常の描画を行わせる
	RT1に光らせたい部分を抽出しておき
	RT0とRT1を加算合成し、自己発効している
	オブジェクトに輝きを行わせる

	<---------------------------------------------------*/

	//------------------------------------------
	//	ブルーム用にレンダーターゲットを描画する
	//------------------------------------------
	void SetBloom();


	//--------------------------
	//	ブルーム描画を描画させる
	//--------------------------
	void DrawBloom();


	//----------------
	//	デバック表示用
	//----------------
	void ShaderDebugSprite();

	//	シャドウマップを作る範囲の横幅
	float m_CreateShadowWidthRange;

	//	シャドウマップを作る範囲の縦幅
	float m_CreateShadowHeightRange;

};

#endif  _CLASS_NAME_SHADER_MANAGER_H_