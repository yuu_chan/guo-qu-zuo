/*----------------------------------------------

シャドウマップを描画するためのクラス

----------------------------------------------*/

#ifndef _CLASS_NAME_SHADOW_MAP_SHADER_H_
#define _CLASS_NAME_SHADOW_MAP_SHADER_H_


/*=============

Class宣言

=============*/
class CShadowMapShader : public CShaderBase
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CShadowMapShader();

	//--------------
	//	デストラクタ
	//--------------
	~CShadowMapShader();


	//	初期化
	void Init();

	//	描画
	void DrawMesh(shared_ptr<CMeshObject> _pMesh, int _Pass);

	//	スキンメッシュ描画
	void DrawSkinMesh(shared_ptr<CMeshObject> _pMesh);

	//	開放
	void Release(); 

	//	シャドウマップ描画開始
	void Begin(
		CVector3*		_vLightPos, 
		CVector3		_vCameraHead	= CVector3(0, 1, 0),
		const float		_ProjWidthSize  = 0.0f,
		const float		_ProjHeightSize = 0.0f
		);

	//	シャドウマップ描画終了
	void End();


	//----------
	//	ゲッター
	//----------

	//	深度マップを返す
	CTexture GetTexture(){ return m_TexShadow; }

	//	ライト用行列を返す
	CMatrix  GetMatrix(){ return m_LVP; }


private:


	//--------------------------------------
	//	シャドウマップに必要な変数を追加する
	//--------------------------------------

	//	深度マップ
	CTexture			m_TexShadow;	

	//	シャドウ用深度バッファ
	CSurface			m_ZBuffer;

	//	ライト用ビュー行列 * 射影行列
	CMatrix				m_LVP;

	//	現在のRTとZバッファ記憶用
	LPDIRECT3DSURFACE9	m_pNowTarget;
	LPDIRECT3DSURFACE9	m_pNowZBuffer;
};

#endif  _CLASS_NAME_SHADOW_MAP_SHADER_H_