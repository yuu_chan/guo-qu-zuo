/*--------------------------------------------------

class説明

--------------------------------------------------*/

#ifndef _CLASS_NAME_EFFECT_SHINE_H_
#define _CLASS_NAME_EFFECT_SHINE_H_


/*=============

Class宣言

=============*/
class CShine : public CTextureBase
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CShine();

	//--------------
	//	デストラクタ
	//--------------
	~CShine();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update();

	//	描画
	void Draw(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	解放
	void Release();

};

#endif  _CLASS_NAME_EFFECT_SHINE_H_