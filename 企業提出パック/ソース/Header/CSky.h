/*--------------------------------------------------

class説明

--------------------------------------------------*/

#ifndef _CLASS_NAME_SKY_H_
#define _CLASS_NAME_SKY_H_


/*=============

Class宣言

=============*/
class CSkyMesh : public CObjectBase
{
public:
	
	//----------------
	//	コンストラクタ
	//----------------
	CSkyMesh();

	//--------------
	//	デストラクタ
	//--------------
	~CSkyMesh();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	開放
	void Release();

};


#endif  _CLASS_NAME_SKY_H_