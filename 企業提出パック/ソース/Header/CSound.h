#ifndef _CLASS_SOUND_H_
#define _CLASS_SOUND_H_

#include "CSoundBase.h"


/*------------------------->
	namespace
<-------------------------*/

//	サウンド
namespace SE
{
	enum NAME
	{
		JUMP		= 1,
		WALK		= 2,
		NOMIKOMU	= 3,
		ITEM_FALL	= 4,
		CLICK		= 5,

	};
}

//	BGM
namespace BGM
{
	enum NAME
	{
		GAME = -1,
		TITLE_AND_SELECT = -2,
	};
}


/*---------------------------------------------->
	サウンド再生クラス

	後でmapに変更

	シングルトンで生成
<----------------------------------------------*/

#define SOUNDPLAYER CSound::GetInstance()

class CSound : CWaveSoundRead
{
private:
	//----------------
	//	コンストラクタ
	//----------------
	CSound(){}

	//--------------
	//	デストラクタ
	//--------------
	~CSound(){}

public:

	//	生成した変数を返す
	static CSound& GetInstance()
	{
		static CSound Instance;
		return Instance;
	}

	//	初期化
	void Init();

	/*-------------------------------------------------->
		特定のサウンドを鳴らす関数

		_SoundNo		:	鳴らすサウンドを決める
		_AblePlay		:	音を再生するかどうか
	<--------------------------------------------------*/
	void PlayTheSound(
		int  _SoundNo,
		bool _AblePlay
		);

	//	開放
	void Release();

	//	読み込み
	void Load(int _Id, char* _FileName);

	//	HWNDハンドルをセット
	void SetHwnd(HWND _BaseHwnd){ m_CopyHwnd = _BaseHwnd; }


private:
	HWND m_CopyHwnd;
	long PVol;
	long PPan;

	//	サウンドのボリューム
	int m_SoundVol;

	//	再生するかどうかを管理 
	bool m_AblePlay;	 //	true(再生) false(再生しない)

};

#endif  _CLASS_SOUND_H_