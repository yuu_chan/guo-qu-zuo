#ifndef _CLASS_SOUND_BASE_H_
#define _CLASS_SOUND_BASE_H_

//-----------------------------------------------------------------------------
// File: WavRead.h
//
// Desc: Support for loading and playing Wave files using DirectSound sound
//       buffers.
//
// Copyright (c) 1999 Microsoft Corp. All rights reserved.
//-----------------------------------------------------------------------------


#include <mmsystem.h>
#include <mmreg.h>
#include<dsound.h>
#pragma comment(lib, "dsound.lib")

//-----------------------------------------------------------------------------
// Name: class CWaveSoundRead
// Desc: A class to read in sound data from a Wave file
//-----------------------------------------------------------------------------
class CWaveSoundRead
{
public:
	WAVEFORMATEX* m_pwfx;        // Pointer to WAVEFORMATEX structure
	HMMIO         m_hmmioIn;     // MM I/O handle for the WAVE
	MMCKINFO      m_ckIn;        // Multimedia RIFF chunk
	MMCKINFO      m_ckInRiff;    // Use in opening a WAVE file


	static const int		SOUND_NUM_MAX = 50;
	LPDIRECTSOUND8			m_lpDSound;
	LPDIRECTSOUNDBUFFER		m_lpSPrimary;
	DWORD					m_Hz;
	LPDIRECTSOUNDBUFFER8	m_lpSSecond[SOUND_NUM_MAX];


public:
	CWaveSoundRead();
	~CWaveSoundRead();

	HRESULT Open(CHAR* strFilename);
	HRESULT Reset();
	HRESULT Read(UINT nSizeToRead, BYTE* pbData, UINT* pnSizeRead);
	HRESULT Close();
	void LoadWave(LPDIRECTSOUNDBUFFER8& _pDSData, char* _FileName);

};


#endif _CLASS_SOUND_BASE_H_

