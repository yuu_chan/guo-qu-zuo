/*----------------------------------------------------------------

バネ華用クラス
プレイヤーが吸い込み対象としてバネをすいこんだときだけ
物理ワールドから行列をもらう
プレイヤーが乗った場合と、設置された場合は行列を直接いじる

----------------------------------------------------------------*/

#ifndef _CLASS_NAME_SPRING_FLOWER_H_
#define _CLASS_NAME_SPRING_FLOWER_H_


/*=============

Class宣言

=============*/

class CSpringFlower : public CObjectBase
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CSpringFlower();

	//--------------
	//	デストラクタ
	//--------------
	~CSpringFlower();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	解放
	void Release();


private:


	//	立方体剛体
	shared_ptr<CBulletObj_Box> m_spRigidBox;

	//	上下移動
	bool m_AbleNext;		//	true(上) false(下)


};


#endif  _CLASS_NAME_SPRING_FLOWER_H_

