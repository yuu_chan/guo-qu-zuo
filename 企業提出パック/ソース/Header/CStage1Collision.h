/*--------------------------------------------------

class説明

--------------------------------------------------*/

#ifndef _CLASS_NAME_STAGE_1_COLLISION_H_
#define _CLASS_NAME_STAGE_1_COLLISION_H_


/*=============

Class宣言

=============*/
class CStage1Collision : public CObjectBase
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CStage1Collision();

	//--------------
	//	デストラクタ
	//--------------
	~CStage1Collision();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	開放
	void Release();


private:


	//	立方体剛体
	shared_ptr<CBulletObj_Box> m_spRigidBox;

	//	メッシュ剛体
	shared_ptr<CBulletObj_Mesh> m_spRigidMesh;

};

#endif  _CLASS_NAME_STAGE_1_COLLISION_H_