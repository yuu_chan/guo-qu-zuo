#ifndef _CLASS_NAME_STAGE_FONT_H_
#define _CLASS_NAME_STAGE_FONT_H_

class CStageFont : public CTextureBase
{
public:
	//----------------
	//	コンストラクタ
	//----------------
	CStageFont();

	//--------------
	//	デストラクタ
	//--------------
	~CStageFont();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update();

	//	描画
	void Draw(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	開放
	void Release(LPDIRECT3DDEVICE9 _lpD3DDevice);


private:
	static const float FONT_WIDTH_SIZE;
	static const float FONT_HEIGHT_SIZE;


	CVector3 m_NowPos;

};

#endif  _CLASS_NAME_STAGE_FONT_H_