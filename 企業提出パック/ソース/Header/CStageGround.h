/*----------------------------------------------------------------

ステージの床
物理ワールドの衝突検知をさけるために分ける

----------------------------------------------------------------*/

#ifndef _CLASS_NAME_STAGE_GROUND_H_
#define _CLASS_NAME_STAGE_GROUND_H_


/*=============

Class宣言

=============*/

class CStageGround : public CObjectBase
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CStageGround();

	//--------------
	//	デストラクタ
	//--------------
	~CStageGround();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	解放
	void Release();


private:


	//	立方体剛体
	shared_ptr<CBulletObj_Box> m_spRigidBox;

	//	メッシュ剛体
	shared_ptr<CBulletObj_Mesh> m_spRigidMesh;


};


#endif  _CLASS_NAME_STAGE_GROUND_H_

