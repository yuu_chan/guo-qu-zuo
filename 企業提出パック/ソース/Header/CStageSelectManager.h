/*----------------------------------------------

セレクト関係のクラスをまとめる

----------------------------------------------*/

#ifndef _CLASS_NAME_STAGE_SELECT_MANAGER_H_
#define _CLASS_NAME_STAGE_SELECT_MANAGER_H_


/*=============

Class宣言

=============*/
class CStageSelectManager
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CStageSelectManager();

	//--------------
	//	デストラクタ
	//--------------
	~CStageSelectManager();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	描画
	void Draw(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	開放
	void Release();

	//	指定されたオブジェクトを返す
	CTextureBase* GetItSelect(int _Id);


	//----------------------------------------------------->
	//	指定されたクラスをインスタンス化してリストに格納する
	//	その後インスタンス化したオブジェクトを返す
	//<-----------------------------------------------------
	template <class T>
	inline T* CreateSelectTask()
	{
		T* temSelect = MyNew T();
		PushSelectManager(temSelect);
		return temSelect;
	}


	//	別のワールドのポインターを自身の管理しているマネージャーに渡す
	inline void SetShader(CShaderManager* _pShader)
	{
		if (_pShader == nullptr)
		{
			MessageBox(NULL, "シェーダーワールドがセットされていません。このままゲームを終了します", "警告", MB_OK);
			APP.m_bEndFlag = true;
			return;
		}


		//	リストの数を変数に代入
		auto itTexContainer = m_listSelectContainer.begin();

		//	リストの後尾に達していなければ
		while (itTexContainer != m_listSelectContainer.end())
		{
			//	指定したオブジェクトにシェーダーワールドをセット
			(*itTexContainer)->SetBokashiShader(_pShader);

			//	次のリストへ
			++itTexContainer;
		}
	}


private:


	//	マネージャー
	list<CTextureBase *> m_listSelectContainer;

	//	マネージャーリストに指定されたオブジェクトを格納
	void PushSelectManager(CTextureBase* _Object);

};


#endif  _CLASS_NAME_STAGE_SELECT_MANAGER_H_