/*----------------------------------------------

ステージセレクトシーン


================================================


CSelectStateBase

各ステージの座標や必要な情報を管理する


================================================


CStageSelectScene

ステージセレクトのシーンを管理する


----------------------------------------------*/

#ifndef _CLASS_NAME_STAGE_SELECT_SCENE_H_
#define _CLASS_NAME_STAGE_SELECT_SCENE_H_

#include "CSceneBase.h"


/*=============

前方宣言

=============*/

//	ステージセレクトワールド
class CStageSelectWorld;

//	シェーダーマネージャ
class CShaderManager;


/*=============

Class宣言

=============*/

class CSelectStateBase
{
public:

	//----------------
	//	コンストラクタ
	//----------------

	//	座標を指定してワールド行列に座標を合成
	CSelectStateBase(CVector3 _vPos)
	{ 
		m_vPos = _vPos; 

		m_mWorld.CreateMove(&_vPos);
	}

	//	座標と各IDを設定
	CSelectStateBase(CVector3 _vPos, int _Id, int _MapId)
	{ 
		m_vPos = _vPos; 

		m_Id = _Id;

		m_MapId = _MapId;

		m_mWorld.CreateMove(&_vPos);
	}

	CSelectStateBase(CVector3 _vPos, int _Id, int _MapId, float _AngleY)
	{
		m_vPos = _vPos;

		m_Id = _Id;

		m_MapId = _MapId;

		//m_mWorld.CreateMove(&_vPos);

		m_mWorld.CreateRotateY(_AngleY);
		m_mWorld.Move(&_vPos);
	}

	//--------------
	//	デストラクタ
	//--------------
	~CSelectStateBase(){}


	//----------
	//  ゲッター
	//----------

	//	リスト登録のIDを返す
	int		GetListId(){ return m_Id; }

	//	名前つきIDを返す
	int		GetMapId(){ return m_MapId; }

	//	行列を返す
	CMatrix GetWorld(){ return m_mWorld; }


	//----------
	//  セッター
	//----------

	//	リストのIDを登録
	void SetListId(int _ListId){ m_Id = _ListId; }

	//	名前つきIDを登録
	void SetMapId(int _MapId){ m_MapId = _MapId; }


	//--------------------------------------------------------
	//	ステージセレクトの選択面とアイテムを名前つきにしておく
	//--------------------------------------------------------
	static enum SelectionSelect
	{
		//	ステージ1
		STAGE_1			= 1,
		STAGE_1_ITEM	= 10,

		//	ステージ2
		STAGE_2			= 2,
		STAGE_2_ITEM	= 20,

		//	ステージ3
		STAGE_3			= 3,
		STAGE_3_ITEM	= 30,

		MAX
	};


private:


	//	設置する座標
	CVector3	m_vPos;

	//	ワールド行列
	CMatrix		m_mWorld;


	//	リスト登録へのID
	int			m_Id;

	//	名前つきID
	int			m_MapId;


};


class CStageSelectScene : public CSceneBase
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CStageSelectScene();

	//--------------
	//	デストラクタ
	//--------------
	~CStageSelectScene();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	描画
	void Draw(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	開放
	void Release();


	static bool m_AbleNext;

	int m_Now;


private:


	//	ステージセレクトワールド
	shared_ptr<CStageSelectWorld> m_spSelectWorld;

	//	各マップとマップに応じたアイテムの座標
	vector<shared_ptr<CSelectStateBase>> m_spMapState;


	//--------------------------------------
	//	クオータニオン用に行列を用意しておく
	//--------------------------------------

	//	現在の座標を示す行列
	CMatrix m_NowMatrix;

	//	スタート位置を示す行列
	CMatrix m_StartMatrix;

	//	終了位置を示す行列
	CMatrix m_EndMatrix;

	//	アニメーションフレーム
	float	m_Animation;

	//	アニメーションを行うか行わないかを管理
	bool m_EnableAnimation;		//	false(アニメーションを行わない)	:	true(アニメーションを行う)


};


#endif  _CLASS_NAME_STAGE_SELECT_SCENE_H_