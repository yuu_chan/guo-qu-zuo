/*----------------------------------------------------------------

ゲームセレクト関係の全クラスの親ポインター

----------------------------------------------------------------*/

#ifndef _CLASS_NAME_STAGE_SELECT_WORLD_H_
#define _CLASS_NAME_STAGE_SELECT_WORLD_H_


/*===============

前方宣言

===============*/
class CStageSelectManager;


/*===============

Class宣言

===============*/
class CStageSelectWorld
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CStageSelectWorld();

	//--------------
	//	デストラクタ
	//--------------
	~CStageSelectWorld();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	描画
	void Draw(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	開放
	void Release();


	//	エフェクトマネージャのポインタをゲットする
	shared_ptr<CStageSelectManager> GetSelectManager(){ return m_SelectManager; }


private:


	//	タイトルマネージャー変数
	shared_ptr<CStageSelectManager> m_SelectManager;


};

#endif  _CLASS_NAME_STAGE_SELECT_WORLD_H_