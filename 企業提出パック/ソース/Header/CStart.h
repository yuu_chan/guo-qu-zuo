/*--------------------------------------------------

ゲームスタート時の演出を管理するクラス

--------------------------------------------------*/

#ifndef _CLASS_NAME_START_H_
#define _CLASS_NAME_START_H_


/*=============

前方宣言

=============*/

//	ゲームワールド
class CGameWorld;

//	エフェクトワールド
class CEffectWorld;


/*=============

Class宣言

=============*/
class CStart
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CStart();

	//--------------
	//	デストラクタ
	//--------------
	~CStart();


	//----------
	//	セッター
	//----------

	//	ゲームワールドをセットする
	void SetGameWorld(shared_ptr<CGameWorld> _GameWorld){ m_pGameWorld = _GameWorld; }

	//	エフェクトワールドをセットする
	void SetEffectWorld(shared_ptr<CEffectWorld> _EffectWorld){ m_pEffectWorld = _EffectWorld; }

	//	イベントの状態をセットする
	void SetEvent(bool _Enable){ m_NowEvent = _Enable; }

	//----------
	//	ゲッター
	//----------

	//	ゲームワールドのポインターを返す
	shared_ptr<CGameWorld>   GetGameWorld(){ return m_pGameWorld.lock(); }

	//	エフェクトワールドのポインターを返す
	shared_ptr<CEffectWorld> GetEffectWorld(){ return m_pEffectWorld.lock(); }

	//	イベントの状態を返す
	bool		  GetEvent(){ return m_NowEvent; }


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	スタート演出
	void StartEvent(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	開放
	void Release();


private:


	//	ゲームワールドのポインター
	//CGameWorld*		m_pGameWorld;
	weak_ptr<CGameWorld> m_pGameWorld;

	//	エフェクトワールドのポインター
	//CEffectWorld*	m_pEffectWorld;
	weak_ptr<CEffectWorld> m_pEffectWorld;

	//	スタートポジション
	CVector3		m_vStartPos;

	//	上側の淵の拡大量
	CVector3		m_vUpScale;

	//	下側の淵の拡大量
	CVector3		m_vDownScale;

	//	スタートイベントが終わったかどうかを知らせる
	bool m_NowEvent;	//	true: イベント中 false: イベント終了

	//	カメラ移動のアングル
	CVector3 m_vMoveAngle;

	//	ステップ
	int m_Step;

	//	スタート終了位置
	CVector3 m_vEndPos;
};

#endif  _CLASS_NAME_START_H_