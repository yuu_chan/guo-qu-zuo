/*----------------------------------------------------------------

胃袋画像クラス

----------------------------------------------------------------*/

#ifndef _CLASS_NAME_STOMACH_H_
#define _CLASS_NAME_STOMACH_H_


/*=============

Class宣言

=============*/

class CStomach : public CTextureBase
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CStomach();

	//--------------
	//	デストラクタ
	//--------------
	~CStomach();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update();

	//	描画
	void Draw(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	開放
	void Release();


private:

};


#endif  _CLASS_NAME_STOMACH_H_