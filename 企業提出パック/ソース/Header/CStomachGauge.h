/*----------------------------------------------------------------

満腹ゲージ
じわじわとゲージが減っていって、色が変化していく

----------------------------------------------------------------*/


#ifndef _CLASS_NAME_STOMACH_GAUGE_H_
#define _CLASS_NAME_STOMACH_GAUGE_H_


/*=============

Class宣言

=============*/

class CStomachGauge : public CTextureBase
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CStomachGauge();

	//--------------
	//	デストラクタ
	//--------------
	~CStomachGauge();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update();

	//	描画
	void Draw(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	開放
	void Release();


private:


	//	満腹ゲージの色を管理
	D3DXCOLOR m_Color;

	//	満腹度(減る量)を管理
	int		 m_Meter;

	//	満腹度ゲージが全体の何%残っているかを管理
	float	 m_Residue;

};


#endif  _CLASS_NAME_STOMACH_GAUGE_H_

