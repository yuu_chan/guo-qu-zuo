/*----------------------------------------------------------------

段差が変更される岩

----------------------------------------------------------------*/


#ifndef _CLASS_NAME_STONE_H_
#define _CLASS_NAME_STONE_H_


/*=============

Class宣言

=============*/

class CStone : public CObjectBase
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CStone();

	//--------------
	//	デストラクタ
	//--------------
	~CStone();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	解放
	void Release();


private:


	shared_ptr<CBulletObj_Box> m_spRigidBox;

};


#endif  _CLASS_NAME_STONE_H_

