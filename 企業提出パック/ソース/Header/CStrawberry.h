/*----------------------------------------------------------------

イチゴのクラス

イチゴの茎の後にインスタンス化すると
茎にくっつくようになっている

----------------------------------------------------------------*/

#ifndef _CLASS_NAME_STRAWBERRY_H_
#define _CLASS_NAME_STRAWBERRY_H_


/*=============

Class宣言

=============*/

class CStrawberry : public CObjectBase
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CStrawberry();

	//--------------
	//	デストラクタ
	//--------------
	~CStrawberry();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	解放
	void Release();


private:


	//	球剛体
	shared_ptr<CBulletObj_Sphere> m_spRigidSphere;

	//	ボールジョイント
	shared_ptr<CBulletJoint_Point> m_spJoint;


	//	ジョイントを設定する関数
	void CreateJoint();

};


#endif  _CLASS_NAME_STRAWBERRY_H_