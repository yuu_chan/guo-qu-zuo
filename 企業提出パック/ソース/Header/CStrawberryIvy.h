/*----------------------------------------------------------------

イチゴの蔦クラス
イチゴが取れれば自動的に消える

----------------------------------------------------------------*/

#ifndef _CLASS_NAME_STRAWBERRY_INV_H_
#define _CLASS_NAME_STRAWBERRY_INV_H_


/*=============

Class宣言

=============*/

class CStrawberryIvy : public CObjectBase
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CStrawberryIvy();

	//--------------
	//	デストラクタ
	//--------------
	~CStrawberryIvy();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	解放
	void Release();


private:


	shared_ptr<CBulletObj_Mesh> m_spMesh;


	//	イチゴを格納するリストを用意
	

};


#endif  _CLASS_NAME_STRAWBERRY_INV_H_

