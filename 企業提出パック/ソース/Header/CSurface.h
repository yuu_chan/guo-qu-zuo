#ifndef _CLASS_NAME_SURFACE_H_
#define _CLASS_NAME_SURFACE_H_

/*------------------------------------------------>
	サーフェイスクラス
<------------------------------------------------*/

class CSurface
{
public:


	//------
	//	取得
	//------

	//	サーフェイス取得
	LPDIRECT3DSURFACE9 GetSur(){ return m_lpSur; }

	//	情報取得
	D3DSURFACE_DESC* GetDesc(){ return &m_Desc; }


	//------
	//	作成
	//------
	
	//	Zバッファ作成
	BOOL CreateDepthStencil(
		UINT				_Width, 
		UINT				_Height, 
		D3DFORMAT			_Format, 
		D3DMULTISAMPLE_TYPE _MultiSample, 
		DWORD				_MultisampleQuality, 
		BOOL				_Discard,
		LPDIRECT3DDEVICE9  _lpD3DDevice);

	//	短縮した場合
	BOOL CreateDepthStencil(
		UINT				_Width, 
		UINT				_Height, 
		D3DFORMAT			_Format,
		LPDIRECT3DDEVICE9	_lpD3DDevice);

	//------------------------------
	//	深度バッファとしてセットする
	//------------------------------
	void SetDepthStencil(LPDIRECT3DDEVICE9  _lpD3DDevice);

	//------
	//	解放
	//------
	void Release();


	//----------------
	//	コンストラクタ
	//----------------
	CSurface() 
		: m_lpSur(0)
	{
	}

	//--------------
	//	デストラクタ
	//--------------
	~CSurface()
	{
		Release();
	}

private:
	LPDIRECT3DSURFACE9		m_lpSur;
	D3DSURFACE_DESC			m_Desc;
/*
	CSurface(const CSurface& src){}
	void operator = (const CSurface& src){}*/
};

#endif  _CLASS_NAME_SURFACE_H_