/*----------------------------------------------------------------

スイッチのギミック
作動させたいギミックのアイテムIDと一致させておく

----------------------------------------------------------------*/


#ifndef _CLASS_NAME_SWICH_H_
#define _CLASS_NAME_SWICH_H_


/*=============

Class宣言

=============*/

class CSwitch : public CObjectBase
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CSwitch();

	//--------------
	//	デストラクタ
	//--------------
	~CSwitch();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	解放
	void Release();


private:


	//	ボタンが押されたか押されていないか
	bool m_EnableSwitch;	//	true(押されて次の処理が可能) :  false(押されていない)

	//	立方体剛体
	shared_ptr<CBulletObj_Box> m_spRigidBox;

	unique_ptr<CVector3>	m_unGimmickPos;


	//	初期座標を覚えておく
	CVector3 m_vRememberPos;


};


#endif  _CLASS_NAME_SWICH_H_

