/*----------------------------------------------------------------

システム関係を管理


==================================================================


CFps

FPSを管理


==================================================================


CKeyCode

キーの状態を取得するクラス


==================================================================


CFlagCalc

フラグを計算するクラス


==================================================================


CMousePointer

マウスポインターを管理するクラス


==================================================================


CFont

デバッグフォント用のクラス


----------------------------------------------------------------*/

#ifndef _CLASS_NAME_SYSTEM_H_
#define _CLASS_NAME_SYSTEM_H_


/*===============

namespace

===============*/

/*--------------------------->

論理演算のパターンを管理
ORならOR計算
ANDならAND計算
(SetDataに使用)

<---------------------------*/

namespace LOGICAL
{
	enum OPERATION_MODE
	{
		OR = 0,
		AND = 1,
	};
}


/*===============

マクロ定義

===============*/

#define GET_KEY CKeyCode::GetInstance()

#define GET_FLAG_CALC CFlagCalc::GetInstance()

#define GET_MOUSE CMousePointer::GetInstance()

#define GET_FONT CFont::GetInstance()


/*===============

Class宣言

===============*/

class CFps
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CFps(){}

	//--------------
	//	デストラクタ
	//--------------
	~CFps(){}


	//	現在のFPSを返す
	DWORD FpsProc()
	{
		static DWORD fps, Nowfps, BaseTime, SaveTime;
		SaveTime = timeGetTime();
		fps++;
		if ((SaveTime - BaseTime) >= 1000){
			BaseTime = timeGetTime();
			Nowfps = fps;
			fps = 0;
		}

		return Nowfps;
	}
};


class CKeyCode
{
private:

	//----------------
	//	コンストラクタ
	//----------------
	CKeyCode();

	//--------------
	//	デストラクタ
	//--------------
	~CKeyCode();


public:


	static CKeyCode& GetInstance()
	{
		static CKeyCode Instance;
		return Instance;
	}


private:

	//-----------------------------
	//　キーコードの名前のみなので
	//  m_はなし
	//-----------------------------
	struct Code_t
	{
		short Up;
		short Dwon;
		short Left;
		short Right;
		short A;
		short D;
		short S;
		short W;
		short E;
		short Space;
		short Escape;
		short MouseLeft;
		short MouseRight;
		short Return;
	};

	//　キーの状態を調べる
	static short GetKeyState(short _KeyCode, short _KeyState);


public:


	//	キーの情報
	static Code_t m_Code;

	//------------
	//　列挙方宣言
	//------------
	enum KEY_INFORMATION
	{
		FREE,	//　離されている状態
		PULL,	//　離された瞬間の状態
		PUSH,	//　押された瞬間の状態
		HOLD	//　押され続けている状態
	};

	//----------
	//　関数宣言
	//----------
	//　キー状態の取得
	static void UpdateKeyState();
};


class CFlagCalc
{
private:

	//----------------
	//	コンストラクタ
	//----------------
	CFlagCalc(){}

	//--------------
	//	デストラクタ
	//--------------
	~CFlagCalc(){}


public:


	static CFlagCalc& GetInstance()
	{
		static CFlagCalc Instance;
		return Instance;
	}


	/*CheckFlag--------------------------------------------->

	指定されたステータスが指定されたフラグかどうかをチェック
	true(フラグ一致)かfalse(フラグ不一致)で返す

	_State	:	チェックしたいステータス

	_Flag	:	チェックしたいフラグ

	return	:	戻り値bool型

	<------------------------------------------------------*/
	inline bool CheckFlag(UINT _State, UINT _Flag)
	{
		return ((_State & _Flag) == _Flag) ? true : false;
	}


	/*CheckFlag--------------------------------------------->

	指定されたステータスが指定されたフラグかどうかをチェック
	true(フラグ一致)かfalse(フラグ不一致)で返す

	_State		:	チェックしたいステータス

	_Flag		:	計算したいフラグ

	_CheckFlag	:	その値になったかどうかを判定 

	return		:	戻り値bool型

	<------------------------------------------------------*/
	inline bool CheckFlag(UINT _State, UINT _Flag, UINT _CheckFlag)
	{
		return ((_State & _Flag) == _CheckFlag) ? true : false;
	}


	/*SetData------------------------------------------------->
	
	指定したステータスをセットする
	セットされた値が戻り値としてかえる

	_State		:	セットする先

	_SetFlag	:	セットしたいステータス

	_Type		:	AND計算かOR計算かを指定

	return		:	戻り値UINT型

	<--------------------------------------------------------*/
	inline UINT SetData(UINT _State, UINT _SetFlag, short _Type)
	{
		//	ORが送られたら
		if (_Type == LOGICAL::OPERATION_MODE::OR)
			//  OR計算で送り返す
			return _State |= _SetFlag;
		//	ANDが送られたら
		else if (_Type == LOGICAL::OPERATION_MODE::AND)
			//  AND計算で送り返す
			return _State &= _SetFlag;
		else
			//	それ以外なら0を返す
			return 0;
	}


	//	フラウｇをセット
	inline UINT SetFlag(UINT _State, UINT _InitFlag, UINT _SetFlag)
	{
		return _State = ((_State & _InitFlag) | _SetFlag);
	}
};


class CMousePointer
{
private:

	//----------------
	//	コンストラクタ
	//----------------
	CMousePointer();

	//--------------
	//	デストラクタ
	//--------------
	~CMousePointer();


public:
	
	
	static CMousePointer& GetInstance()
	{
		static CMousePointer Instance;
		return Instance;
	}


private:
	
	
	POINT		 m_BasePointer;					//　マウスポインターの座標制限
	float		 m_MouseSpeed;					//　マウスの速さ
	float		 m_MouseSensor;					//　マウスの情報
	const float  DEFAULT_MOUSE_SPEED = 4.0f;	//　センシ最大量
	HWND		 m_CopyHwnd;					//　Hwndのコピー
	D3DXVECTOR3	 m_Ang;							//　マウスの座標


public:


	//　マウスポインター初期化
	void InitMousePointer();

	//　マウスポインターをセット
	void SetMousePointer();


	//----------
	//	ゲッター
	//----------

	//座標のゲッター
	inline D3DXVECTOR3 GetAngle(){ return m_Ang; }

	//----------
	//	セッター
	//----------
	
	//	Hwndのセッター
	inline void SetHwnd(HWND _Hwnd){ m_CopyHwnd = _Hwnd; }

	//	座標のセッター
	inline void SetAngle(D3DXVECTOR3 _Angle){ m_Ang = _Angle; }

};


class CFont
{
private:
	
	//----------------
	//	コンストラクタ
	//----------------
	CFont();

	//--------------
	//	デストラクタ
	//--------------
	~CFont();


public:


	//	重複インスタンスを防ぐ
	static CFont& GetInstance()
	{
		static CFont Instance;
		return Instance;
	}


private:


	LPD3DXFONT			m_lpFont;

	static const short	FONT_MAX_SIZE = 255;

	char				m_Buffer[FONT_MAX_SIZE];


public:


	struct FontPos_t
	{
		int m_Left;
		int m_Top;
		int m_Right;
		int m_Bottom;
	};

	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	描画
	void Draw(FontPos_t _Pos, const char* _Format,float _data, char _Locale...);

	//	解放
	void Release();
};


//------------------
//　テンプレート関数
//------------------
template<class T>
inline void Delete(T*& lpAdd)
{
	if (lpAdd != NULL){
		delete lpAdd;
		lpAdd = NULL;
	}
}

template <class T>
inline void DeleteArray(T*& lpAdd)
{
	if (lpAdd != NULL)
	{
		delete[] lpAdd;
		lpAdd = NULL;
	}
}

template<class T>
inline void SafeRelease(T*& lpAdd)
{
	if (lpAdd)
	{
		lpAdd->Release();
		lpAdd = NULL;
	}
}

//安全なポインタの解放
#define SAFE_FREE(p)		{if(p){free(p);p=NULL;}}			// mallocしたものを安全にfreeするマクロ
#define SAFE_RELEASE(p)		{if(p){p->Release();p=NULL;}}		// COMオブジェクト系を安全にReleaseするマクロ
#define SAFE_DELETE(p)		{if(p){delete p;p=NULL;}}			// newされた変数を安全に解放するマクロ
#define SAFE_DELETE_ARRAY(p){if(p){delete[] p;p=NULL;}}			// newされた配列を安全に解放するマクロ


#endif  _CLASS_NAME_SYSTEM_H_