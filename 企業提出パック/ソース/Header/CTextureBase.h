/*----------------------------------------------------------------

テクスチャ規定クラス

----------------------------------------------------------------*/

#ifndef _CLASS_NAME_TEXTURE_BASE_H_
#define _CLASS_NAME_TEXTURE_BASE_H_


/*===============

namespace

===============*/

/*-------------------------------------------->
		エフェクトのリストID
<--------------------------------------------*/
namespace TEX_LIST
{
	enum ID
	{
		//	指定なし
		NO				 = -1,

		//	フレーム上側
		EVENT_FRAME_UP	 = 1,

		//	フレーム下側
		EVENT_FRAME_DOWN = 2, 

		//	リング
		RING			 = 3,

		//	輝き
		SHINE			 = 4,

		//	胃袋
		STOMACH			= 5,

		//	胃袋のゲージ
		STOMACH_GAUGE	= 6,

		//	最大数
		MAX,
	};
}


/*=============

前方宣言

=============*/

//	エフェクトワールド
class CEffectWorld;

//	ゲームワールド
class CGameWorld;

//	シェーダーマネージャー
class CShaderManager;


/*=============

Class宣言

=============*/

class CTextureBase
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CTextureBase();

	//--------------
	//	デストラクタ
	//--------------
	virtual ~CTextureBase();


	//--------------
	//	純粋仮想関数
	//--------------

	//	初期化
	virtual void Init(LPDIRECT3DDEVICE9 _lpD3DDevice)		= 0;

	//	更新
	virtual void Update()									= 0;

	//	描画
	virtual void Draw(LPDIRECT3DDEVICE9 _lpD3DDevice)		= 0;

	//	開放
	virtual void Release()									= 0;


	//----------
	//	ゲッター
	//----------
	
	//	ポジションを返す
	CVector3		GetPosition(){ return m_vPos; }

	//	Listへの登録IDを返す
	int				GetListId(){ return m_ListId; }

	//	エフェクトの登録IDを返す
	int				GetEffectId(){ return m_EffectId; }

	//	ぼかしシェーダの情報を返す
	CShaderManager* GetBokashiShader(){ return m_pShader; }

	//	テクスチャへのポインタを返す
	shared_ptr<CTexture>	GetTexture(){ return m_pTexture; }

	//	メッシュを返す
	shared_ptr<CMeshObject>	GetMesh(){ return m_Mesh; }

	//	先頭IDを返す
	int				GetFirstId(){ return m_FirstId; }

	//	後尾IDを返す
	int				GetLastId(){ return m_LastId; }

	//	ワールド行列を返す
	CMatrix			GetWorld(){ return m_mWorld; }


	//----------
	//	セッター
	//----------

	//	ポジションをセットする
	void SetPosition(CVector3 _Pos){ m_vPos = _Pos; }

	//	Listへの登録IDをセットする
	void SetListId(int _SetId){ m_ListId = _SetId; }

	//	エフェクトIDをセット
	void SetEffectId(int _EffectId){ m_EffectId = _EffectId; }

	//	エフェクトワールドをセットする
	void SetEffectWorld(CEffectWorld* _World){ m_pEffectWorld = _World; }

	//	ゲームワールドをセットする
	void SetGameWorld(CGameWorld* _World){ m_pGameWorld = _World; }

	//	ぼかしシェーダーのポインターをセットする
	void SetBokashiShader(CShaderManager* _Bokashi){ m_pShader = _Bokashi; }

	//	テクスチャへのポインタをセットする
	//void SetTexture(CTexture* _Texture){ m_pTexture = _Texture; }

	//	メッシュをセットする
	//void SetMesh(CMeshObject* _Mesh){ m_Mesh = _Mesh; }

	//	ワールド行列をセット
	void SetWorld(CMatrix _World){ m_mWorld = _World; }

	//	先頭IDをセットする
	void SetFirstId(int _FirstId){ m_FirstId = _FirstId; }

	//	後尾IDをセットする
	void SetLastId(int _LastId){ m_LastId = _LastId; }

	//	拡大量をセットする
	void SetScale(CVector3 _Scale){ m_vScaleSize = _Scale; }

	//	角度
	void SetAngle(CVector3 _vAngle){ m_vAngle = _vAngle; }

	//	テクスチャのサイズをセット
	void SetTextureSize(CVector3 _vTextureSize){ m_vTextureSize = _vTextureSize; }

	//	テクスチャの分割数をセットする
	void SetDivisionUNum(int _Num){ m_DivisionNumU = _Num; }

	//
	void SetDivisionVNum(int _Num){ m_DivisionNumV = _Num; }


protected:


	//	テクスチャを扱う変数
	shared_ptr<CTexture>	m_pTexture;

	//	ブレンドテクスチャを扱う変数
	shared_ptr<CTexture>	m_pBlendTexture;

	//	設置したい座標
	CVector3				m_vPos;
	
	//	拡大行列
	CVector3				m_vScaleSize;

	//	角度
	CVector3				m_vAngle;

	//	合成行列
	CMatrix					m_mWorld;

	//	メッシュを扱う
	shared_ptr<CMeshObject>	m_Mesh;

	//	テクスチャのサイズを指定
	CVector3				m_vTextureSize;

	//	Listへの登録ID
	int						m_ListId;

	//	エフェクトの登録ID
	int						m_EffectId;

	//	先頭へのID
	int						m_FirstId;

	//	後尾へのID
	int						m_LastId;

	//	テクスチャの分割数
	int						m_DivisionNumU;

	int						m_DivisionNumV;


	//--------------------------
	//	外部ワールドのポインター
	//--------------------------

	//	エフェクトワールド
	CEffectWorld*		m_pEffectWorld;

	//	ゲームワールドのポインタ
	CGameWorld*			m_pGameWorld;

	//	シェーダーワールドのポインタ
	CShaderManager*		m_pShader;


	struct VERTEX
	{
		D3DXVECTOR3 Pos;
		D3DCOLOR    Color;
		D3DXVECTOR2 Tex;
	};

	VERTEX m_v[4];
};

#endif  _CLASS_NAME_TEXTURE_BASE_H_
