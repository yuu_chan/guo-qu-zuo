#ifndef _CLASS_NAME_TITLE_FONT_H_
#define _CLASS_NAME_TITLE_FONT_H_

class CTitleFont : public CTextureBase
{
public:
	//----------------
	//	コンストラクタ
	//----------------
	CTitleFont();

	//--------------
	//	デストラクタ
	//--------------
	~CTitleFont();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update();

	//	描画
	void Draw(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	開放
	void Release();


private:
	static const float FONT_WIDTH_SIZE;
	static const float FONT_HEIGHT_SIZE;
};

#endif  _CLASS_NAME_TITLE_FONT_H_