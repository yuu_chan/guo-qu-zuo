/*----------------------------------------------

タイトルで使用するクラスをまとめておく

----------------------------------------------*/

#ifndef _CLASS_NAME_TITLE_MANAGER_H_
#define _CLASS_NAME_TITLE_MANAGER_H_


class CTitleManager
{
public:
	
	//----------------
	//	コンストラクタ
	//----------------
	CTitleManager();

	//--------------
	//	デストラクタ
	//--------------
	~CTitleManager();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	描画
	void Draw(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	開放
	void Release();

	//	指定されたオブジェクトを返す
	CTextureBase* GetItTitle(int _Id);


	//----------------------------------------------------->
	//	指定されたクラスをインスタンス化してリストに格納する
	//	その後インスタンス化したオブジェクトを返す
	//<-----------------------------------------------------
	template <class T>
	inline T* CreateTitleTask()
	{
		T* temTitle = MyNew T();
		PushTitleManager(temTitle);
		return temTitle;
	}


private:


	//	マネージャー
	list<CTextureBase *> m_listTitleContainer;

	//	マネージャーリストに指定されたオブジェクトを格納
	void PushTitleManager(CTextureBase* _Object);

};


#endif  _CLASS_NAME_TITLE_MANAGER_H_