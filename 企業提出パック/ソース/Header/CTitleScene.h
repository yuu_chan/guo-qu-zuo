/*----------------------------------------------

タイトルシーンを管理

----------------------------------------------*/

#ifndef _CLASS_NAME_TITLE_SCENE_H_
#define _CLASS_NAME_TITLE_SCENE_H_


#include "CSceneBase.h"


/*=============

前方宣言

=============*/
class CTitleWorld;


/*=============

Class宣言

=============*/
class CTitleScene : public CSceneBase
{
public:
	//----------------
	//	コンストラクタ
	//----------------
	CTitleScene();

	//--------------
	//	デストラクタ
	//--------------
	~CTitleScene();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	描画
	void Draw(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	開放
	void Release();


private:


	//	タイトルワールド
	shared_ptr<CTitleWorld> m_pTitleWorld;

	//	アングル
	float m_Ang;

};

#endif  _CLASS_NAME_TITLE_SCENE_H_