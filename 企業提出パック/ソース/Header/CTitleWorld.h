/*----------------------------------------------------------------

タイトル関係の全クラスの親ポインター

----------------------------------------------------------------*/

#ifndef _CLASS_NAME_TITLE_WORLD_H_
#define _CLASS_NAME_TITLE_WORLD_H_


/*===============

前方宣言

===============*/

class CTitleManager;


/*===============

Class宣言

===============*/
class CTitleWorld
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CTitleWorld();

	//--------------
	//	デストラクタ
	//--------------
	~CTitleWorld();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	描画
	void Draw(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	開放
	void Release();


	//	エフェクトマネージャのポインタをゲットする
	shared_ptr<CTitleManager> GetTitleManager(){ return m_TitleManager; }


private:


	//	タイトルマネージャー変数
	shared_ptr<CTitleManager> m_TitleManager;
};

#endif  _CLASS_NAME_TITEL_WORLD_H_