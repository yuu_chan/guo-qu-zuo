/*--------------------------------------------------

class説明

--------------------------------------------------*/

#ifndef _CLASS_NAME_TREE_MULTIPLE_H_
#define _CLASS_NAME_TREE_MULTIPLE_H_


/*=============

Class宣言

=============*/
class CTreeMultiple : public CObjectBase
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CTreeMultiple();

	//--------------
	//	デストラクタ
	//--------------
	~CTreeMultiple();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	解放
	void Release();
};

#endif  _CLASS_NAME_TREE_MULTIPLE_H_