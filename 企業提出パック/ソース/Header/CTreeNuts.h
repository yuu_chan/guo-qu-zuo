/*--------------------------------------------------

class説明

--------------------------------------------------*/

#ifndef _CLASS_NAME_FISH_BAIT_H_
#define _CLASS_NAME_FISH_BAIT_H_


/*=============

Class宣言

=============*/
class CTreeNuts : public CObjectBase
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CTreeNuts();

	//--------------
	//	デストラクタ
	//--------------
	~CTreeNuts();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);
	
	//	解放
	void Release();


private:


	//-----------------------------------------------
	//	変数
	//	Able   : できる  true(できる) false(できない)
	//	Enbale : 有効	 true(有効)   false(無効)
	//-----------------------------------------------
	
	//	木の実の設置されている座標を記憶するための変数
	CVector3	m_vMemoryPos;

	//	落下処理を行うかどうか	:	true(落下する)	false(落下しない)
	bool		m_EnableFall;


	//------
	//	定数
	//------

	//	拡大行列の最大値
	static const float SCALE_MAX_SIZE;

	//	拡大行列の拡大する量
	static const float SCALE_AMOUNT;
};

#endif  _CLASS_NAME_FISH_BAIT_H_