#ifndef _CLASS_NAME_VECTOR3_H_
#define _CLASS_NAME_VECTOR3_H_


/*===============================
		3次元ベクトルクラス
		
		D3DXVECTOR3継承
===============================*/
class CVector3 : public D3DXVECTOR3
{
public:
	
	//----------------
	//	コンストラクタ
	//----------------
	CVector3();

	//------------------------
	//	引数つきコンストラクタ
	//------------------------
	CVector3(float _x, float _y, float _z);
	CVector3(const D3DXVECTOR3& _In);

	//--------------
	//	デストラクタ
	//--------------
	~CVector3();


	void Set(float _x, float _y, float _z)
	{
		x = _x;
		y = _y;
		z = _z;
	}

	void Set(const D3DXVECTOR3* _Vec)
	{
		x = _Vec->x;
		y = _Vec->y;
		z = _Vec->z;
	}

	CVector3& operator=(const D3DXVECTOR3& _Vec)
	{
		x = _Vec.x;
		y = _Vec.y;
		z = _Vec.z;
		return *this;
	}


	//----------
	//  各演算子
	//----------
	CVector3 operator + () const{ return D3DXVECTOR3::operator+(); }
	CVector3 operator - () const{ return D3DXVECTOR3::operator-(); }

	CVector3 operator + (CONST D3DXVECTOR3& _Vec) const{ return D3DXVECTOR3::operator+(_Vec); }
	CVector3 operator - (CONST D3DXVECTOR3& _Vec) const{ return D3DXVECTOR3::operator-(_Vec); }
	CVector3 operator * (FLOAT _Vec) const{ return D3DXVECTOR3::operator*(_Vec); }
	CVector3 operator / (FLOAT _Vec) const{ return D3DXVECTOR3::operator/(_Vec); }
	

	//--------------
	//	ベクトル加算
	//--------------

	// 静的関数
	static void Add(
		D3DXVECTOR3* _vOut, 
		const D3DXVECTOR3* _Vec1, 
		const D3DXVECTOR3* _Vec2)
	{	
		D3DXVec3Add(_vOut, _Vec1, _Vec2);
	}

	void Add(float _x, float _y, float _z)
	{
		Add(this, this, &D3DXVECTOR3(_x, _y, _z));
	}

	void Add(const CVector3* _Vec)
	{
		Add(this, this, _Vec);
	}


	//--------------
	//	ベクトル減算
	//--------------
	
	// 静的関数
	static void Sub(
		D3DXVECTOR3* _vOut, 
		const D3DXVECTOR3* _Vec1, 
		const D3DXVECTOR3* _Vec2)
	{	
		D3DXVec3Subtract(_vOut, _Vec1, _Vec2);
	}

	void Sub(float _x, float _y, float _z)
	{
		Sub(this, this, &D3DXVECTOR3(_x, _y, _z));
	}

	void Sub(const D3DXVECTOR3* _Vec)
	{
		Sub(this, this, _Vec);
	}


	//------
	//  内積
	//------
	
	// 静的関数
	static float Dot(const D3DXVECTOR3* _Vec1, const D3DXVECTOR3* _Vec2)
	{			
		return D3DXVec3Dot(_Vec1, _Vec2);
	}
	static float Dot2(const D3DXVECTOR3* _Vec1, const D3DXVECTOR3* _Vec2)
	{
		float retdot = D3DXVec3Dot(_Vec1, _Vec2);
		
		if (retdot < -1.0f)
		{
			retdot = -1.0f;
		}
		if (retdot > 1.0f)
		{
			retdot = 1.0f;
		}

		return retdot;
	}

	float Dot(const D3DXVECTOR3* _Vec)
	{
		return Dot(this, _Vec);
	}

	float Dot2(const D3DXVECTOR3* _Vec)
	{
		return Dot2(this, _Vec);
	}


	//------
	//  外積
	//------

	// 静的関数
	static void Cross(
		D3DXVECTOR3* _vOut, 
		const D3DXVECTOR3* _Vec1, 
		const D3DXVECTOR3* _Vec2)
	{
		D3DXVec3Cross(_vOut, _Vec1, _Vec2);
	}


	//-------------------
	//  外積 自分 x vBack
	//-------------------
	void CrossBack(D3DXVECTOR3* _vOut, const D3DXVECTOR3* _vBack)
	{
		Cross(_vOut, this, _vBack);
	}
	
	
	//-------------------
	//  外積 vBack x 自分
	//-------------------
	void CrossFront(D3DXVECTOR3* _vOut, const D3DXVECTOR3* _vFront)
	{
		Cross(_vOut, _vFront, this);
	}


	//------------
	//  スカラー倍
	//------------

	// 静的関数
	static void Scale(
		D3DXVECTOR3* _vOut, 
		const D3DXVECTOR3* _vSrc, 
		float _Scale)
	{		
		D3DXVec3Scale(_vOut, _vSrc, _Scale);
	}

	void Scale(float _Scale)
	{
		Scale(this, this, _Scale);
	}


	//------
	//  長さ
	//------

	// 静的関数
	static float Length(const D3DXVECTOR3* _vSrc)
	{						
		return D3DXVec3Length(_vSrc);
	}
	
	float Length()
	{
		return Length(this);
	}
	
	
	//--------
	//  正規化
	//--------

	// 静的関数
	static void Normalize(D3DXVECTOR3* _vOut, const D3DXVECTOR3* _vSrc)
	{			
		D3DXVec3Normalize(_vOut, _vSrc);
	}

	void Normalize()
	{
		Normalize(this, this);
	}

	void Normalize(D3DXVECTOR3* _vOut) const
	{
		Normalize(_vOut, this);
	}


	//------------------------
	//  行列で変換(回転・座標)
	//------------------------

	// 静的関数
	static void Transform(
		D3DXVECTOR3*		_vOut, 
		const D3DXVECTOR3*	_vSrc, 
		const D3DXMATRIX*	_Matrix)
	{	
		D3DXVec3TransformCoord(_vOut, _vSrc, _Matrix);
	}

	void Transform(const D3DXMATRIX* _Matrix)
	{
		Transform(this, this, _Matrix);
	}

	void Transform(D3DXVECTOR3* _vOut, const D3DXMATRIX* _Matrix)
	{
		Transform(_vOut, this, _Matrix);
	}


	//----------------------------
	//  行列で変換(回転のみの変換)
	//----------------------------
	
	// 静的関数
	static void TransformNormal(
		D3DXVECTOR3*		_vOut, 
		const D3DXVECTOR3*	_vSrc, 
		const D3DXMATRIX*	_Matrix)
	{
		D3DXVec3TransformNormal(_vOut, _vSrc, _Matrix);
	}

	void TransformNormal(const D3DXMATRIX* _Matrix)
	{
		TransformNormal(this, this, _Matrix);
	}

	void TransformNormal(
		D3DXVECTOR3*		_vOut, 
		const D3DXMATRIX*	_Matrix)
	{
		TransformNormal(_vOut, this, _Matrix);
	}

	
	//------------------------------------------------
	//  指定方向に指定角度だけ回転。(正反対でも曲がる)
	//------------------------------------------------
	void Homing(const CVector3* vTargetDir, float MaxAng);


};

#endif  _CLASS_NAME_VECTOR3_H_