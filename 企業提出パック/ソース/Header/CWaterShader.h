/*----------------------------------------------

水辺の波紋を生成するクラス

----------------------------------------------*/

#ifndef _CLASS_NAME_WATER_SHADER_H_
#define _CLASS_NAME_WATER_SHADER_H_


/*=============

Class宣言

=============*/
class CWaterShader : public CShaderBase
{
public:

	//----------------
	//  コンストラクタ
	//----------------
	CWaterShader();

	//--------------
	//  デストラクタ
	//--------------
	~CWaterShader();


	//	初期化
	void Init();

	//	解放
	void Release();

	//	描画
	void DrawMesh(shared_ptr<CMeshObject> _pMesh, int _Pass);

	//	ハイトマップと法線マップをを更新
	void UpdateMap();

	void TexDraw();

	//	移動
	void Update();

	//	デバイス
	void OnLostDevice();
	void OnResetDevice();


	//----------
	//	セッター
	//----------

	//	Fromをセット
	void SetFrom(CVector3 _vFrom) { m_vFrom = _vFrom; }
	
	//	LookAtをセット
	void SetLookAt(CVector3 _vLookAt) { m_vLookAt = _vLookAt; }

	//	LightWVPosをセット
	void SetLightWVPos(D3DXVECTOR4 _Pos4) { m_vLightWVPos = D3DXVECTOR4(_Pos4.x, _Pos4.y, _Pos4.z, 1.0f); }

	//	Colorをセット
	void SetLightColor(D3DXVECTOR4 _Color) { m_vLightColor = D3DXVECTOR4(_Color.x, _Color.y, _Color.z, 1.0f); }

	//	Timeをセット
	void SetTime(int _Time) { m_Time = _Time; }

	//	TextureTimeをセット
	void SetTextureTime(int _Time) { m_TextureTime = _Time; }

	//	Modeをセット
	void SetMode(int _Mode) { m_Mode = _Mode; }

	//	UVスクロール量をセット
	void SetUVScroll(CVector3 _vScroll) { m_vUVScroll = _vScroll; }


	//----------
	//	ゲッター
	//----------
	
	//	Fromを返す
	CVector3 GetFrom(){ return m_vFrom; }

	//	LookAtを返す
	CVector3 GetLookAt() { return m_vLookAt; }

	//	LightWVPosを返す
	D3DXVECTOR4 GetLightWVPos() { return m_vLightWVPos; }

	//	Colorを返す
	D3DXVECTOR4 GetLightColor() { return m_vLightColor; }

	//	Timeを返す
	int GetTime() { return m_Time; }

	//	TextureTimeを返す
	int GetTextureTime() { return m_TextureTime; }

	//	Modeを返す
	int GetMode() { return m_Mode; }

	//	Heightマップを返す
	CTexture GetHeightTex(int _Id) { return m_HeightTex[_Id]; }

	//	法線マップを返す
	CTexture GetNormalTex() { return m_NormalTex; }

	//	Zバッファを返す
	LPDIRECT3DSURFACE9 GetDepthStencil() { return m_lpDepthStencil; }

	//	UVスクロール量を返す
	CVector3 GetUVScroll() { return m_vUVScroll; }


private:


	//	Heightマップを作る [0](高さ) [1](速度)
	CTexture m_HeightTex[2];

	//	法線マップを作る
	CTexture m_NormalTex;

	//	RT
	LPDIRECT3DSURFACE9 m_lpRenderTarget;

	//	深度
	LPDIRECT3DSURFACE9 m_lpDepthStencil;

	//float m_Yaw, m_VYaw;

	CVector3 m_vFrom, m_vLookAt;

	D3DXVECTOR4 m_vLightWVPos, m_vLightColor;

	int m_Mode, m_Time, m_TextureTime;

	//	スクロール量
	CVector3 m_vUVScroll;
};


#endif  _CLASS_NAME_WATER_SHADER_H_
