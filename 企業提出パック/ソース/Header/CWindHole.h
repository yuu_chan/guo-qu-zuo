/*----------------------------------------------------------------

風がでてくる穴
風はでてくるパターンとでてこないパターンで分ける

----------------------------------------------------------------*/

#ifndef _CLASS_NAME_WIND_HOLE_H_
#define _CLASS_NAME_WIND_HOLE_H_


/*=============

Class宣言

=============*/

class CWindHole : public CObjectBase
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CWindHole();

	//--------------
	//	デストラクタ
	//--------------
	~CWindHole();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	解放
	void Release();


private:


	shared_ptr<CBulletObj_Box> m_spRigidBox;

};


#endif  _CLASS_NAME_WIND_HOLE_H_
