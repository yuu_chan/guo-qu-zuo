/*----------------------------------------------------------------

風が吹いている空間を決める
このクラスの範囲内では、軽いものは押し流される

----------------------------------------------------------------*/

#ifndef _CLASS_NAME_WIND_ZONE_H_
#define _CLASS_NAME_WIND_ZONE_H_


/*=============

Class宣言

=============*/

class CWindZone : public CObjectBase
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CWindZone();

	//--------------
	//	デストラクタ
	//--------------
	~CWindZone();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	解放
	void Release();


private:




};


#endif  _CLASS_NAME_WIND_ZONE_H_

