/*----------------------------------------------------------------

Window画面その他の情報管理クラス
必要なのでシングルトンで生成

----------------------------------------------------------------*/

#ifndef _CLASS_NAME_WINDOW_H_
#define _CLASS_NAME_WINDOW_H_


/*===============

マクロ定義

===============*/
#define APP CWindow::GetInstance()


/*===============

前方宣言

===============*/
class CWindow
{
private:

	//----------------
	//	コンストラクタ
	//----------------
	CWindow();		

	//--------------
	//	デストラクタ
	//--------------
	~CWindow();
	

public:


	static CWindow& GetInstance()
	{
		static CWindow Instance;	
		return Instance;
	}

	// Hwndを確保してくる
	HWND GetHwnd(){ return m_hWnd; }


	//=================================================
	// ウィンドウ関係
	//=================================================
	char		m_DefDir[256];		// ゲームのトップディレクトリ
	HINSTANCE	m_hInst;			// アプリケーションのインスタンスハンドル
	HWND		m_hWnd;				// ウィンドウハンドル
	int			m_Width;			// ウィンドウクライアント解像度
	int			m_Height;			// ウィンドウクライアント解像度
	bool		m_CloseFlag;		// ウィンドウクローズフラグ false:何もなし true:×ボタンが押された
	bool		m_bEndFlag;			// 終了フラグ trueを入れるとメインループから抜ける
	DWORD		m_FrameCnt;			// 現在のフレーム値
	BOOL		m_FullScreen;		// フルスクリーンフラグ


	//--------------
	//	現在のシーン
	//--------------
	int m_NowScene;

	//--------------------------
	//	ウィンドウの大きさ(固定)
	//--------------------------
	static const int WINDOW_WIDTH_SIZE;
	static const int WINDOW_HEIGHT_SIZE;


	// ウィンドウ初期化
	BOOL InitWindow(HINSTANCE hInstance, int w, int h, BOOL bFullScreen);	// ウィンドウ作成、Direct3D初期化など
	
	// メインループ
	int Loop();								

	// ウィンドウ関数
	static LRESULT CALLBACK callWindowProc(HWND _hWnd, UINT _Message, WPARAM _wParam, LPARAM _lParam);
	LRESULT CALLBACK WindowProc(HWND _hWnd, UINT _Message, WPARAM _wParam, LPARAM _lParam);
	
	// クライアントサイズの設定
	void SetClientSize(HWND _hWnd, int _Width, int _Height);

};


#endif  _CLASS_NAME_WINDOW_H_