/*----------------------------------------------------------------

全ワールドのポインターを管理
コピーや受け渡しを可能にする

又、シーンクラスでわけられたものも管理できるようにする

クラスにワールドマネージャーの変数を用意しておき、
本体からコピーしておくだけで、全ワールドにアクセスすることができる

----------------------------------------------------------------*/

#ifndef _CLASS_NAME_AA_H_
#define _CLASS_NAME_AA_H_


/*=============

Class宣言

=============*/

class CWorldManager
{
public:

	//----------------
	//	コンストラクタ
	//----------------
	CWorldManager();

	//--------------
	//	デストラクタ
	//--------------
	~CWorldManager();


	//	初期化
	void Init(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	更新
	void Update(LPDIRECT3DDEVICE9 _lpD3DDevice);

	//	描画
	void Draw(LPDIRECT3DDEVICE9 _lpDd3DDevice);

	//	解放
	void Release();


private:



	//list<

};


#endif  _CLASS_NAME_AA_H_

