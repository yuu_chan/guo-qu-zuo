/*==============================================================>
	ゲーム関係のインクルードファイルを纏めておいて、
	簡単に呼び出せるようにしておく。

<==============================================================*/

#ifndef _GAME_HEADER_
#define _GAME_HEADER_


//------------------
//	シェーダーを扱う
//------------------
#include "Shader.h"


//------------
//	基底クラス
//------------
#include "CMesh.h"
#include "CObjectBase.h"

#include "CAchievementBase.h"


//-----------------------
//	Xファイルマネージャー
//-----------------------
#include "CLoadXFileManager.h"


//--------------
//　オブジェクト
//--------------
#include "CPlayer.h"
#include "CBox.h"
#include "CMushRoom.h"
#include "CMushRooms.h"
#include "CRock.h"
#include "CSky.h"
#include "CStage1.h"
#include "CStageGround.h"
#include "CStage1Collision.h"
#include "CApple.h"
#include "CTreeMultiple.h"
#include "CTreeNuts.h"
#include "CKabo.h"
#include "CLeaf.h"
#include "CStrawberry.h"
#include "CStrawberryIvy.h"
#include "CAppleTree.h"
#include "CSpringFlower.h"
#include "CCloud.h"
#include "CBranch.h"
#include "CStone.h"
#include "CAirFlower.h"
#include "CSwitch.h"
#include "CWindZone.h"
#include "CWindHole.h"
#include "CRiver.h"
#include "CStone.h"
#include "CCrackWall.h"
#include "CBreakRock.h"
#include "CSeesaw.h"
#include "CBridge.h"
#include "CSeesawFoundation.h"
#include "CLargeRock.h"


//--------------------------
//	ユーザーインターフェース
//--------------------------
#include "CAchieApple.h"
#include "CHelp.h"


//----------------------
//	描画や更新を行うもの
//----------------------
#include "CObjectManager.h"
#include "CGameWorld.h"
#include "CGimmickManager.h"


#include "CAchievementManager.h"
#include "CAchievementWorld.h"

#endif  _GAME_HEADER_