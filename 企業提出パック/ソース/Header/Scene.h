#ifndef _CLASS_NAME_SCENE_H_
#define _CLASS_NAME_SCENE_H_

#include "CSceneManager.h"
#include "CSceneBase.h"

//--------------
//	ゲームシーン
//--------------
#include "Game.h"
#include "CGameScene.h"


//----------------
//	タイトルシーン
//----------------
#include "Font.h"
#include "CTitleScene.h"


//------------------------
//	ステージセレクトシーン
//------------------------
#include "CStageSelectScene.h"



//----------------
//	リザルトシーン
//----------------
#include "CResultScene.h"


#endif  _CLASS_NAME_SCENE_H_