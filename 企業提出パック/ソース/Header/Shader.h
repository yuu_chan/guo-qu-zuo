#ifndef _SHADER_H_
#define _SHADER_H_


#include "CShaderBase.h"

#include "CMyShader.h"
#include "CBokashiShader.h"
#include "CShadowMapShader.h"
#include "CWaterShader.h"
#include "CSceneShader.h"

#include "CShaderManager.h"


#endif  _SHADER_H_