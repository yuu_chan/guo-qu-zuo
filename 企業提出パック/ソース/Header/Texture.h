#ifndef _TEXTURE_H_
#define _TEXTURE_H_


//------------
//	基底クラス
//------------
#include "CMesh.h"
#include "CTextureBase.h"


//--------------------------
//	テクスチャーマネージャー
//--------------------------
#include "CLoadTextureManager.h"


//--------------
//	オブジェクト
//--------------
#include "CEventFrame.h"
#include "CShine.h"
#include "CFade.h"
#include "CAbsorption.h"

#include "CStomach.h"
#include "CStomachGauge.h"

//----------
//	描画更新
//----------
#include "CEffectManager.h"
#include "CEffectWorld.h"


#endif  _TEXTURE_H_