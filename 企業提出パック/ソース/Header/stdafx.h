//	サポートシステムだけインクルードする

/*-------------------------------------------------->
	サポートされているライブラリ
<--------------------------------------------------*/

#pragma warning (disable : 4996)
#pragma warning (disable : 4819)
#pragma warning (disable : 4005)
#pragma warning (disable : 4024)
#pragma warning (disable : 4204)
#pragma warning (disable : 4224)
#pragma warning (disable : 4316)

#include <windows.h>
#include <iostream>
#include <stdio.h>
#include <string>
#include <vector>
#include <map>
#include <time.h>
#include <map>
#include <string>
#include <stack>
#include <list>
#include <iterator>
#include <queue>
#include <algorithm>
#include <memory>
#include <fstream>
#include <sstream>
#include <process.h>
#include <math.h>
#include <commctrl.h>
#include <ctime>
#include <random>
#include <functional>
#include <minmax.h>
#include <algorithm>
#include <thread>
#include <process.h>
#include <math.h>
#include <commctrl.h>
#include <ctime>
#include <atomic>
#include <cassert>
#pragma comment(lib,"Microsoft DirectX SDK/Lib/x86/d3d9.lib")
#pragma comment(lib,"Microsoft DirectX SDK/Lib/x86/d3dx9.lib")
#pragma comment(lib,"Microsoft DirectX SDK/Lib/x86/dxguid.lib")
#pragma comment(lib,"Microsoft DirectX SDK/Lib/x86/dinput8.lib")
#pragma comment(lib,"Microsoft DirectX SDK/Lib/x86/dsound.lib")
#pragma comment(lib,"Microsoft DirectX SDK/Lib/x86/dxerr.lib")

#include "d3d9.h"
#include "d3dx9.h"
#include "dsound.h"
#include "dxerr.h"


#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dsound.lib")
#pragma comment(lib, "dxerr.lib")
#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "winmm.lib")
#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")


// Bullet Physics Engine
#ifdef _DEBUG
#pragma comment(lib,"BulletPhysicsEngine/lib/Debug/BulletCollision_debug.lib")
#pragma comment(lib,"BulletPhysicsEngine/lib/Debug/BulletDynamics_debug.lib")
#pragma comment(lib,"BulletPhysicsEngine/lib/Debug/LinearMath_debug.lib")
#else
#pragma comment(lib,"BulletPhysicsEngine/lib/Release/BulletCollision.lib")
#pragma comment(lib,"BulletPhysicsEngine/lib/Release/BulletDynamics.lib")
#pragma comment(lib,"BulletPhysicsEngine/lib/Release/LinearMath.lib")
#endif



/*------------------------------->

メモリリークの位置を知らせる

<-------------------------------*/
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>

#define MyNew new(_NORMAL_BLOCK, __FILE__, __LINE__)


/*------------------------------------->
	STL名前省略
<-------------------------------------*/
using namespace std;


/*------------------------------------>

開放関数

<-----------------------------------*/




/*----------------------------------------->
	独自ライブラリ
	DirectX9サポート関係のヘッダーは
	常に呼び出せるようにしておく
<-----------------------------------------*/

//  Windowサポート
#include "CWindow.h"

//	DirectX9のサポート
#include "CDirectX9.h"

//	サウンドサポート
#include "CSound.h"

//	テクスチャーサポート
#include "CTexture.h"

//	テクスチャーマネージャー
#include "CLoadTextureManager.h"

//	システムサポート
#include "CSystem.h"

//	VECTOR3型サポート
#include "CVector3.h"

//	行列サポート
#include "CMatrix.h"

//	面情報管理
#include "CFace.h"

//	ヘルパークラス
#include "CHelper.h"

//	当たり判定クラス
#include "CCollision.h"

//	スレッドサポートクラス
#include "CThread.h"

//	マテリアル情報
#include "CMaterialData.h"

//	カメラサポート
#include "CCamera.h"

//	サーフェイスサポート
#include "CSurface.h"

//	バレット
#include "CBulletPhysicsEngine.h"
#include "CBulletPhysicsWorld.h"

//	エフェクシア
//#include "Effekseer.h"
//#include "EffekseerRendererDX9.h"