//==============================================================
// グローバル変数
//==============================================================

// 変換行列
float4x4 mW;		// ワールド行列
float4x4 mV;		// ビュー行列
float4x4 mP;		// 射影行列


//	ライトをいれるかどうか
int bLight = 0; // 0で無効 1で有効


//----
//	UV
//----

//	必要ないのであとで消しておく
float  Scroll = 0.05f;

//	UVテクスチャのスクロール量を管理
float3 ScrollUV = 0.0f;


//--------
//	ミラー
//--------

// クリップ用
int bClip = 0;	//	クリップのフラグ 0で無効 1で有効

//	屈折表現をするかどうか
int bRefract = 0;	//	0で無効 1で有効

//	クリップする位置
float3 ClipPos = float3(0, 0, 0);

//	クリップの方向 水面はy方向
float3 ClipDir = float3(0, 1, 0);

// SphereTex用サンプラ
texture MirrorTex;
sampler MirrorSmp = sampler_state {
	Texture		= <MirrorTex>;
	MipFilter	= LINEAR;
	MinFilter	= LINEAR;
	MagFilter	= LINEAR;
	AddressU	= Clamp;
	AddressV	= Clamp;
};

//	屈折に必要な背景画像
texture BackTex;
sampler BackSmp = sampler_state
{
	Texture		= <BackTex>;
	MipFilter	= LINEAR;
	MinFilter	= LINEAR;
	MagFilter	= LINEAR;
	AddressU	= Clamp;
	AddressV	= Clamp;
};


//----------------
//	ポイントライト
//----------------

//	ポイントライトの座標
float3 PointPos[100];

//	ポイントライトの半径
float  PointRadius[100];

//	ポイントライトの色
float4 PointColor[100];


//--------
//	フォグ
//--------

//	フォグをかける範囲の一番手前
float FogNear = 30;

//	フォグをかける範囲の一番奥
float FogFar = 80;


//-----------------
// シャドウマップ用
//-----------------

// ライトのビュー行列＊射影行列
float4x4 mLVP;

// マッハバンド(縞々模様)防止用
float Margin = 0.004;

// シャドウマップ(深度マップ)
texture ShadowTex;

//	シャドウのサンプラ
sampler ShadowSmp = sampler_state
{
	Texture = <ShadowTex>;
	MipFilter = NONE;
	MinFilter = POINT;
	MagFilter = POINT;
	AddressU = Clamp;
	AddressV = Clamp;
};


//--------------------
//	スフィア環境マップ
//--------------------

float Reflection = 0.8f; //0.5f

texture SphereTex;

//	環境マップのサンプラー
sampler SphereSmp = sampler_state
{
	Texture = <SphereTex>;
	MipFilter = LINEAR;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	AddressU = Clamp;
	AddressV = Clamp;
};


//--------------------
//	輪郭シェーディング
//--------------------

//	輪郭サイズ
float  InkSize = 0.05f;

//	輪郭色
float4 InkColor = float4(0, 0, 0, 1);


//----------------------
//	トューンレンダリング
//----------------------

texture ToonTex;

//	トューンサンプラー
sampler ToonSmp = sampler_state
{
	Texture = <ToonTex>;
	MipFilter = LINEAR;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	AddressU = Clamp;
	AddressV = Clamp;
};


//------------
//	法線マップ
//------------

//	視差マップ用係数
float ParallaxVal = 0.04f;

texture NormalTex;

//	法線マップのサンプラー
sampler NormalSmp = sampler_state
{
	Texture = <NormalTex>;
	MipFilter = LINEAR;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	AddressU = Wrap;
	AddressV = Wrap;
};


//---------------
// マテリアル関係
//---------------

// 基本色
float4	Diffuse = float4(1, 1, 1, 1);

// 環境光の強さ
float	Ambient = 0.3;

// 反射色
float4	Specular = float4(1, 1, 1, 1);

// 自己照明
float4	Emissive;

//	自己発行色の強さを管理
float	EmissivePow = 1.0f;

// 反射色の力
float	Power = 50;

//	αテストで使用するα値
float   AlphaTest = 1.0f;


// マテリアルのテクスチャ
texture MeshTex;

// MeshTex用サンプラ
sampler MeshSmp = sampler_state
{
	Texture = <MeshTex>;
	MipFilter = LINEAR;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	AddressU = Wrap;
	AddressV = Wrap;
};


//---------
// ライト用
//---------

// ライトの方向
float3 LightDir;

//	法線ようライト
float3 NormalLightDir;

//---------
// カメラ用
//---------

// 視点の位置
float3 CamPos;


//----------------------------------
//	一部の色を変更する場合に使用する
//----------------------------------

//	あらかじめ決められているプレイヤーのYのサイズ
float Height = 0.0f;

//	替えたい色を指定
float4 ChangeColor = 0.0f;

//	現在ピクセル位置 (0に近いほど下　1に近いほど上)
float NowPix = 0.0f;	//	0 ~ 1に指定

//	プレイヤーの一番下のピクセル
float Y = 0;


//=====================================================
// 頂点シェーダからピクセルシェーダに渡すデータ用構造体
//=====================================================

struct VS_OUT
{
	float4 Pos		: POSITION;		// 2D座標
	float2 MeshUV	: TEXCOORD0;	// テクスチャUV

	float3 wPos		: TEXCOORD1;	// 3D座標
	float3 wN		: TEXCOORD2;	// 法線

	float3 wViewPos : TEXCOORD3;	// ビュー行列に変換した座標

	float3 wBiN		: TEXCOORD4;
	float3 wTan		: TEXCOORD5;

	float4 Pos2D	: TEXCOORD6;		// 2D座標
};


//--------------
//	輪郭用構造体
//--------------
struct INKVS_OUT
{
	float4 Pos : POSITION;
};


//================
//	頂点シェーダー
//================

//	・頂点が１つ送られてくるので、それを処理しピクセルシェーダに渡す。
VS_OUT VC(
	float4 pos		: POSITION,			// 頂点座標
	float3 normal	: NORMAL,			// 法線
	float3 binormal : BINORMAL,
	float3 tangent	: TANGENT,
	float2 mesh_uv	: TEXCOORD0			// テクスチャUV
	)
{
	// ピクセルデータに渡すデータ用構造体
	VS_OUT Out = (VS_OUT)0;

	// 頂点座標
	Out.Pos = mul(pos, mW);

	// 3D空間での座標になる
	Out.wPos = Out.Pos.xyz;

	// 3Dカメラ座標系での座標になる
	Out.Pos = mul(Out.Pos, mV);

	//	ビュー行列に変換された座標を別の変数に保存しておく
	Out.wViewPos = Out.Pos.xyz;

	// 2D空間での座標になる
	Out.Pos = mul(Out.Pos, mP);

	// テクスチャ座標
	Out.MeshUV = mesh_uv;

	//	UVスクロール
	Out.MeshUV.x = mesh_uv.x - ScrollUV.x;
	Out.MeshUV.y = mesh_uv.y - ScrollUV.y;

	//	2D座標をTEXCOORDのセマンティクスに渡す
	Out.Pos2D = Out.Pos;

	// 法線をワールドに変換する
	float3 w_normal = normalize(mul(normal, (float3x3)mW));

	Out.wN = w_normal;

	Out.wBiN = normalize(mul(binormal, (float3x3)mW));

	Out.wTan = normalize(mul(tangent, (float3x3)mW));


	//	ピクセルシェーダーにデータを渡す
	return Out;
}


//--------------------
//	輪郭用頂点シェーダ
//--------------------
INKVS_OUT InkVC(
	float4 pos		: POSITION,
	float3 normal	: NORMAL)
{
	INKVS_OUT Out;

	//	座標を法線方向にずらす
	float3 newPos = pos.xyz + normal * InkSize;

	//	2D変換
	Out.Pos = mul(float4(newPos, 1), mW);
	Out.Pos = mul(Out.Pos, mV);
	Out.Pos = mul(Out.Pos, mP);

	return Out;
}



/*
//VS_OUT VC(
//	float4 pos		: POSITION,			// 頂点座標
//	float3 normal	: NORMAL,			// 法線
//	float2 mesh_uv	: TEXCOORD0			// テクスチャUV
//	)
//{
//	// ピクセルデータに渡すデータ用構造体
//	VS_OUT Out = (VS_OUT)0;
//
//	// 頂点座標
//	Out.Pos = mul(pos, mW);
//
//	// 3D空間での座標になる
//	Out.wPos = Out.Pos.xyz;
//
//	// 3Dカメラ座標系での座標になる
//	Out.Pos = mul(Out.Pos, mV);
//
//	//	ビュー行列に変換された座標を別の変数に保存しておく
//	Out.wViewPos = Out.Pos.xyz;
//
//	// 2D空間での座標になる
//	Out.Pos = mul(Out.Pos, mP);
//
//	// テクスチャ座標
//	//Out.MeshUV = mesh_uv - Scroll;
//	Out.MeshUV.x = mesh_uv.x;
//	Out.MeshUV.y = mesh_uv.y - Scroll;
//
//
//	// 法線
//	float3 w_normal = normalize(mul(normal, (float3x3)mW));	// ワールド系に変換
//	Out.wN = w_normal;
//
//	return Out;
//}
*/


/*
//==============================================================
// 頂点シェーダ
//	・頂点が１つ送られてくるので、それを処理しピクセルシェーダに渡す。
//==============================================================
VS_OUT VCANIME(
float4 pos		: POSITION,			// 頂点座標
float3 normal	: NORMAL,			// 法線
float3 binormal : BINORMAL,
float3 tangent	: TANGENT,
float2 mesh_uv	: TEXCOORD0			// テクスチャUV
)
{
// ピクセルデータに渡すデータ用構造体
VS_OUT Out = (VS_OUT)0;

// 頂点座標
Out.Pos = mul(pos, mW);

// 3D空間での座標になる
Out.wPos = Out.Pos.xyz;

// 3Dカメラ座標系での座標になる
Out.Pos = mul(Out.Pos, mV);

//	ビュー行列に変換された座標を別の変数に保存しておく
Out.wViewPos = Out.Pos.xyz;

// 2D空間での座標になる
Out.Pos = mul(Out.Pos, mP);

// テクスチャ座標
Out.MeshUV = mesh_uv;

//	UVスクロール
Out.MeshUV.x = mesh_uv.x - ScrollUV.x;
Out.MeshUV.y = mesh_uv.y - ScrollUV.y;


// 法線をワールドに変換する
float3 w_normal = normalize(mul(normal, (float3x3)mW));

Out.wN = w_normal;
Out.wBiN = normalize(mul(binormal, (float3x3)mW));
Out.wTan = normalize(mul(tangent, (float3x3)mW));


//	ピクセルシェーダーにデータを渡す
return Out;
}
*/


//====================
//	ピクセルシェーダー
//====================

//	出力用の構造体
struct PS_OUT
{
	//	色
	float4 Color : COLOR0;

	//	自己発行色
	float4 Bloom : COLOR1;
};


/*
環境マップ入り
*/
PS_OUT SpherePS(VS_OUT In)
{
	//	ミラーのクリップ
	if (bClip)
	{
		float3 v = normalize(In.wPos - ClipPos);
			float dt = dot(v, ClipDir);
		clip(dt);
	}


	//=========================
	// ライティング(ランバート)
	//=========================

	// 法線を正規化
	float3 w_normal = normalize(In.wN);

	// 光の強さ
	float LPow = max(0, dot(-LightDir, w_normal));


	//============
	//	法線マップ
	//============

	float4 normal = tex2D(NormalSmp, In.MeshUV);


	//===================
	// シャドウマップ判定
	//===================

	// ライト側の2D座標
	float4 LiPos = mul(float4(In.wPos, 1), mLVP);
	LiPos.xyz = LiPos.xyz / LiPos.w;

	//	影を薄める (影が濃くなる間の範囲)
	float2 range = float2(15, 25);

	//	0 ~ 1に切りつめる 遠い影は1にして近い影は0にする
	float ShadowPow = saturate((range.y - In.wViewPos.z) / (range.y - range.x));


	//--------------------------------------------
	// シャドウマップ内判定(xyは-1〜1以内 zは0〜1)
	//--------------------------------------------
	if (abs(LiPos.x) <= 1 && abs(LiPos.y) <= 1 && LiPos.z <= 1){

		// 2D座標系からUV座標系に変換する
		float2 uv = LiPos.xy * 0.5 + 0.5;
			uv.y = 1 - uv.y;

		float z = LiPos.z - Margin;

		// 影になってるかどうか判定
		//float S_Pow = tex2D(ShadowSmp, uv).r < z ? 0 : 1;
		//　シャドウマップにいろいろ追加してみる

		int ShadowPixelSize = 1024;

		//簡易ソフト化バージョン(ガタガタが多少改善される)
		float ShadowTexelSize = 1.0 / ShadowPixelSize; //　画像のサイズ(テクセル)。　VC側で計算して持っていく

		float t0, t1, t2, t3;

		t0 = tex2D(ShadowSmp, uv).r < z ? 0 : 1;
		t1 = tex2D(ShadowSmp, uv + float2(ShadowTexelSize, 0)).r < z ? 0 : 1; // 1つ右
		t2 = tex2D(ShadowSmp, uv + float2(0, ShadowTexelSize)).r < z ? 0 : 1; // 1つ下
		t3 = tex2D(ShadowSmp, uv + ShadowTexelSize).r < z ? 0 : 1; // 1つ右下

		//　判定結果を線形補間する
		float2 f = frac(uv * ShadowPixelSize); // ピクセル座標系に変換し、小数部のみを取得
			float S_Pow = lerp( //補間された影の強さをy方向で補間
			lerp(t0, t1, f.x),	//上の2ピクセルをx方向で補間
			lerp(t2, t3, f.x),	//下の2ピクセルをx方向で補間
			f.y);

		//	補間
		S_Pow = lerp(1, S_Pow, ShadowPow);

		LPow = min(LPow, S_Pow);	// 暗い方を使用する
	}



	//================================================
	// スペキュラ
	//================================================
	float3 vCam = normalize(CamPos - In.wPos);		// 目(カメラ)への方向
	float3 vRef = LightDir + 2.0f * dot(w_normal, -LightDir) * w_normal;
	float SpePow = pow(max(0, dot(vCam, vRef)), Power);
	float4 Spe = Specular * SpePow;

	//================================================
	// モデル色算出
	//================================================
	// モデルの色
	float4 Col = Diffuse * tex2D(MeshSmp, In.MeshUV);
	// ライトの当たりの強さ+環境光
	Col.rgb *= min(1, LPow + Ambient);

	//	αテスト
	//clip(tex2D(MeshSmp, In.MeshUV).a - 0.01f);

	//	αテスト
	clip(Col.a - AlphaTest);


	//============
	//	環境マップ
	//============

	//	環境色
	float3 vSphRef = reflect(-vCam, w_normal);
	float2 SphUV = vSphRef.xy;
	SphUV = SphUV * 0.5f + 0.5;
	SphUV.y = 1 - SphUV.y;
	float4 SphCol = tex2D(SphereSmp, SphUV);

	//	素材 + 背景
	float4 TCol = Col * (1 - Reflection) + SphCol * Reflection;


	//================================================
	// 色を出力
	//================================================
	PS_OUT Out;
	Out.Color = TCol + Spe;
	Out.Bloom = Spe + (Emissive * EmissivePow) * tex2D(MeshSmp, In.MeshUV);
	Out.Bloom.a = 1;


	//----------------
	//	フォグをかける
	//----------------
	float2 FogRange = float2(FogNear, FogFar);

	float FogPow = saturate((FogRange.y - In.wViewPos.z) / (FogRange.y - FogRange.x));

	Out.Color.rgb = lerp(float3(0.7, 0.8, 1), Out.Color.rgb, FogPow);


	return Out;
}


/*

法線マップあり

*/
PS_OUT NormalPS(VS_OUT In)
{
	//	ミラーのクリップ
	if (bClip)
	{
		float3 v = normalize(In.wPos - ClipPos);
			float dt = dot(v, ClipDir);
		clip(dt);
	}


	// 目(カメラ)への方向
	float3 vCam = normalize(CamPos - In.wPos);

	float4 normal = tex2D(NormalSmp, In.MeshUV);

	// 法線を正規化
	float3 w_normal = normal.xyz * 2.0f - 1.0f;


	//============
	//	法線マップ
	//============

	//	変換行列を作成
	float3x3 mTBN;
	mTBN[0] = normalize(In.wTan);
	mTBN[1] = normalize(In.wBiN);
	mTBN[2] = normalize(In.wN);

	//	転置行列(逆行列)
	float3x3 mInvTBN = transpose(mTBN);

	//	視差マップ
	float3 vCamLocal = mul(vCam, mInvTBN);
	//	aに高さが入っている
	In.MeshUV = In.MeshUV + ParallaxVal * tex2D(NormalSmp, In.MeshUV).a * vCamLocal;

	w_normal = normalize(mul(w_normal, (float3x3)mTBN));


	//================================================
	// スペキュラ
	//================================================

	float3 vRef = NormalLightDir + 2.0f * dot(w_normal, -NormalLightDir) * w_normal;

	float  SpePow = pow(max(0, dot(vCam, vRef)), Power);

	float4 Spe = Specular * SpePow;


	//====================
	// 光の強さ(ランバート
	//===================

	float LPow = max(0, dot(-NormalLightDir, w_normal));

	//LPow = max(0, dot(-LightDir, w_normal));

	//	影はライト固定のため、あらかじめ使用されているライトの方向から計算
	float ShadowLPow = max(0, dot(-LightDir, w_normal));


	//===================
	// シャドウマップ判定
	//===================

	// ライト側の2D座標
	float4 LiPos = mul(float4(In.wPos, 1), mLVP);
	LiPos.xyz = LiPos.xyz / LiPos.w;

	//	影を薄める (影が濃くなる間の範囲)
	float2 range = float2(15, 25);

	//	0 ~ 1に切りつめる 遠い影は1にして近い影は0にする
	float ShadowPow = saturate((range.y - In.wViewPos.z) / (range.y - range.x));


	//--------------------------------------------
	// シャドウマップ内判定(xyは-1〜1以内 zは0〜1)
	//--------------------------------------------
	if (abs(LiPos.x) <= 1 && abs(LiPos.y) <= 1 && LiPos.z <= 1)
	{

		// 2D座標系からUV座標系に変換する
		float2 uv = LiPos.xy * 0.5 + 0.5;
			uv.y = 1 - uv.y;

		float z = LiPos.z - Margin;

		// 影になってるかどうか判定
		//float S_Pow = tex2D(ShadowSmp, uv).r < z ? 0 : 1;
		//　シャドウマップにいろいろ追加してみる

		int ShadowPixelSize = 1024;

		//簡易ソフト化バージョン(ガタガタが多少改善される)
		float ShadowTexelSize = 1.0 / ShadowPixelSize; //　画像のサイズ(テクセル)。　VC側で計算して持っていく

		float t0, t1, t2, t3;

		t0 = tex2D(ShadowSmp, uv).r < z ? 0 : 1;
		t1 = tex2D(ShadowSmp, uv + float2(ShadowTexelSize, 0)).r < z ? 0 : 1; // 1つ右
		t2 = tex2D(ShadowSmp, uv + float2(0, ShadowTexelSize)).r < z ? 0 : 1; // 1つ下
		t3 = tex2D(ShadowSmp, uv + ShadowTexelSize).r < z ? 0 : 1; // 1つ右下

		//　判定結果を線形補間する
		float2 f = frac(uv * ShadowPixelSize); // ピクセル座標系に変換し、小数部のみを取得
			float S_Pow = lerp( //補間された影の強さをy方向で補間
			lerp(t0, t1, f.x),	//上の2ピクセルをx方向で補間
			lerp(t2, t3, f.x),	//下の2ピクセルをx方向で補間
			f.y);

		//	補間
		S_Pow = lerp(1, S_Pow, ShadowPow);

		ShadowLPow = min(ShadowLPow, S_Pow);	// 暗い方を使用する

	}


	//================================================
	// モデル色算出
	//================================================
	// モデルの色
	float4 Col = Diffuse * tex2D(MeshSmp, In.MeshUV);

	// ライトの当たりの強さ+環境光
	LPow = min(1, ShadowLPow + Ambient);
	Col.rgb *= min(1, LPow + Ambient);

	//	αテスト
	clip(Col.a - AlphaTest);


	//==========================
	//	反射した画像を貼り付ける
	//==========================

	if (bRefract)
	{
		//	屈折
		float3 vRefract = refract(-vCam, w_normal, 0.5f);	//	屈折率 0.0f(最大) 〜1.0f(屈折なし)

		float3 vRefract2D = mul(vRefract, (float3x3)mV).xyz;

		In.Pos2D.xy /= In.Pos2D.w;

		float2 Pos2D = In.Pos2D.xy + vRefract2D.xy * 0.05f;

		float2 uv = Pos2D.xy * 0.5f + 0.5f;

		uv.y = 1 - uv.y;

		//	背景の色 (2回目で描画した背景のテクスチャ)
		float4 BackCol = tex2D(BackSmp, uv);

		//	ミラーの色 (1回目で描画した反転したテクスチャ)
		float4 BackRefCol = tex2D(MirrorSmp, uv);

		//	水の色
		float3 WaterCol = tex2D(MeshSmp, In.MeshUV).rgb * Diffuse.rgb;

		//	一度色を持っておく
		Col.rgb = BackCol.rgb * (1 - Diffuse.a) + WaterCol.rgb * Diffuse.a;


		//--------------
		//	フレネル反射
		//--------------

		float dt = 1.0f - max(0, dot(vCam, w_normal));

		dt = dt * 0.8f + 0.1f;

		Col.rgb = Col.rgb * (1 - dt) + BackRefCol.rgb * WaterCol.rgb * dt;


		//	ライティング適用
		Col.rgb *= min(1, LPow + (1.0f - Diffuse.a) * 0.5f);
		Col.a = 1.0f;




		/*
		//In.Pos2D.xy /= In.Pos2D.w;

		//float2 uv = In.Pos2D.xy * 0.5 + 0.5;

		//uv.y = 1 - uv.y;

		////	背景の色(BackColor)
		//float4 WaterRefColor = tex2D(MirrorSmp, uv);

		//	Col.rgb = WaterRefColor.rgb;
		*/
	}


	//================================================
	// 色を出力
	//================================================
	PS_OUT Out;
	Out.Color = Col + Spe;
	Out.Bloom = Spe + (Emissive * EmissivePow) * tex2D(MeshSmp, In.MeshUV);
	Out.Bloom.a = 1;


	//----------------
	//	フォグをかける
	//----------------
	float2 FogRange = float2(FogNear, FogFar);

	float FogPow = saturate((FogRange.y - In.wViewPos.z) / (FogRange.y - FogRange.x));

	Out.Color.rgb = lerp(float3(0.7, 0.8, 1), Out.Color.rgb, FogPow);




	return Out;
}


PS_OUT PS(VS_OUT In)
{
	//	ミラーのクリップ
	if (bClip)
	{
		float3 v = normalize(In.wPos - ClipPos);
			float dt = dot(v, ClipDir);
		clip(dt);
	}


	//=========================
	// ライティング(ランバート)
	//=========================

	// 法線を正規化
	float3 w_normal = normalize(In.wN);

	float LPow = 1;

	//	ライト処理が入った場合は計算して算出
	if (bLight)
	{
		// 光の強さ
		LPow = max(0, dot(-LightDir, w_normal));
	}


	//======================
	//	トューンレンダリング
	//======================
	float4 ToonCol = tex2D(ToonSmp, float2(LPow, 0));


	// 目(カメラ)への方向
	float3 vCam = normalize(CamPos - In.wPos);


	//===================
	// シャドウマップ判定
	//===================

	// ライト側の2D座標
	float4 LiPos = mul(float4(In.wPos, 1), mLVP);
	LiPos.xyz = LiPos.xyz / LiPos.w;

	//	影を薄める (影が濃くなる間の範囲)
	float2 range = float2(15, 25);

	//	0 ~ 1に切りつめる 遠い影は1にして近い影は0にする
	float ShadowPow = saturate((range.y - In.wViewPos.z) / (range.y - range.x));


	//--------------------------------------------
	// シャドウマップ内判定(xyは-1〜1以内 zは0〜1)
	//--------------------------------------------
	if (abs(LiPos.x) <= 1 && abs(LiPos.y) <= 1 && LiPos.z <= 1)
	{

		// 2D座標系からUV座標系に変換する
		float2 uv = LiPos.xy * 0.5 + 0.5;
			uv.y = 1 - uv.y;

		float z = LiPos.z - Margin;

		// 影になってるかどうか判定
		//float S_Pow = tex2D(ShadowSmp, uv).r < z ? 0 : 1;
		//　シャドウマップにいろいろ追加してみる

		int ShadowPixelSize = 1024;

		//簡易ソフト化バージョン(ガタガタが多少改善される)
		float ShadowTexelSize = 1.0 / ShadowPixelSize; //　画像のサイズ(テクセル)。　VC側で計算して持っていく

		float t0, t1, t2, t3;

		t0 = tex2D(ShadowSmp, uv).r < z ? 0 : 1;
		t1 = tex2D(ShadowSmp, uv + float2(ShadowTexelSize, 0)).r < z ? 0 : 1; // 1つ右
		t2 = tex2D(ShadowSmp, uv + float2(0, ShadowTexelSize)).r < z ? 0 : 1; // 1つ下
		t3 = tex2D(ShadowSmp, uv + ShadowTexelSize).r < z ? 0 : 1; // 1つ右下

		//　判定結果を線形補間する
		float2 f = frac(uv * ShadowPixelSize); // ピクセル座標系に変換し、小数部のみを取得
			float S_Pow = lerp( //補間された影の強さをy方向で補間
			lerp(t0, t1, f.x),	//上の2ピクセルをx方向で補間
			lerp(t2, t3, f.x),	//下の2ピクセルをx方向で補間
			f.y);

		//	補間
		S_Pow = lerp(1, S_Pow, ShadowPow);

		LPow = min(LPow, S_Pow);	// 暗い方を使用する
	}


	//================================================
	// スペキュラ
	//================================================

	float3 vRef = LightDir + 2.0f * dot(w_normal, -LightDir) * w_normal;

	float  SpePow = pow(max(0, dot(vCam, vRef)), Power);

	float4 Spe = Specular * SpePow;


	//================================================
	// モデル色算出
	//================================================
	// モデルの色
	float4 Col = Diffuse * tex2D(MeshSmp, In.MeshUV);

	// ライトの当たりの強さ+環境光
	Col.rgb *= min(1, LPow + Ambient);
	//Col.rgb *= ToonCol.rgb;

	//	αテスト
	clip(Col.a - AlphaTest);


	//================================================
	// 色を出力
	//================================================
	PS_OUT Out;
	Out.Color = Col + Spe;
	Out.Bloom = Spe + (Emissive * EmissivePow) * tex2D(MeshSmp, In.MeshUV);
	Out.Bloom.a = 1;


	//----------------
	//	フォグをかける
	//----------------
	float2 FogRange = float2(FogNear, FogFar);

	float FogPow = saturate((FogRange.y - In.wViewPos.z) / (FogRange.y - FogRange.x));

	Out.Color.rgb = lerp(float3(0.7, 0.8, 1), Out.Color.rgb, FogPow);




	return Out;
}


//	一部分の色を変更
/*
float4 PS1(VS_OUT In) : COLOR0
{


//	プレイヤーの座標を取得してくる

float WY = Y;//mW._24;

//	プレイヤーのピクセル高さを取得する
float L = In.wPos.y - WY;

//	高さが 0 ~ 1になる
float Ratio = saturate(L / Height);



// 法線を正規化
float3 w_normal = normalize(In.wN);

//================================================
// ライティング(ランバート)
//================================================
float LPow = max(0, dot(-LightDir, w_normal));	// 光の強さ



//================================================
// シャドウマップ判定
//================================================
// ライト側の2D座標
float4 LiPos = mul(float4(In.wPos, 1), mLVP);
LiPos.xyz = LiPos.xyz / LiPos.w;
// シャドウマップ内判定(xyは-1〜1以内 zは0〜1)
if (abs(LiPos.x) <= 1 && abs(LiPos.y) <= 1 && LiPos.z <= 1){
// 2D座標系からUV座標系に変換する
float2 uv = LiPos.xy*0.5 + 0.5;
uv.y = 1 - uv.y;

float z = LiPos.z - Margin;

// 影になってるかどうか判定
//float S_Pow = tex2D(ShadowSmp, uv).r < z ? 0 : 1;
//　シャドウマップにいろいろ追加してみる

int ShadowPixelSize = 1024;

//簡易ソフト化バージョン(ガタガタが多少改善される)
float ShadowTexelSize = 1.0 / ShadowPixelSize; //　画像のサイズ(テクセル)。　VC側で計算して持っていく

float t0, t1, t2, t3;

t0 = tex2D(ShadowSmp, uv).r < z ? 0 : 1;
t1 = tex2D(ShadowSmp, uv + float2(ShadowTexelSize, 0)).r < z ? 0 : 1; // 1つ右
t2 = tex2D(ShadowSmp, uv + float2(0, ShadowTexelSize)).r < z ? 0 : 1; // 1つ下
t3 = tex2D(ShadowSmp, uv + ShadowTexelSize).r < z ? 0 : 1; // 1つ右下

//　判定結果を線形補間する
float2 f = frac(uv * ShadowPixelSize); // ピクセル座標系に変換し、小数部のみを取得
float S_Pow = lerp( //補間された影の強さをy方向で補間
lerp(t0, t1, f.x),	//上の2ピクセルをx方向で補間
lerp(t2, t3, f.x),	//下の2ピクセルをx方向で補間
f.y);


LPow = min(LPow, S_Pow);	// 暗い方を使用する
}

//================================================
// スペキュラ
//================================================
float3 vCam = normalize(CamPos - In.wPos);		// 目(カメラ)への方向
float3 vRef = LightDir + 2.0f * dot(w_normal, -LightDir) * w_normal;
float SpePow = pow(max(0, dot(vCam, vRef)), Power);
float4 Spe = Specular * SpePow;


//================================================
// モデル色算出
//================================================
// モデルの色
float4 Col = Diffuse * tex2D(MeshSmp, In.MeshUV);


//	プレイヤーの一番下のピクセル位置よりも現在のピクセル位置のほうが小さければ
//	その部分までが色を変更される
if (Ratio < NowPix)
{
Col.rgb *= ChangeColor;
}

// ライトの当たりの強さ+環境光
Col.rgb *= min(1, LPow + Ambient);

//	αテスト
//clip(tex2D(MeshSmp, In.MeshUV).a - 0.01f);

//	αテスト
//clip(Col.a - 0.5f);


return Col + Spe;
}
*/

//	ポイントライト
/*
//----------------
//	ポイントライト
//----------------

//
//PS_OUT PointLight(VS_OUT In)
//{
//
//	// 法線を正規化
//	float3 w_normal = normalize(In.wN);
//
//		//================================================
//		// ライティング(ランバート)
//		//================================================
//		float LPow = max(0, dot(-LightDir, w_normal));	// 光の強さ
//
//
//
//	//================================================
//	// シャドウマップ判定
//	//================================================
//	// ライト側の2D座標
//	float4 LiPos = mul(float4(In.wPos, 1), mLVP);
//		LiPos.xyz = LiPos.xyz / LiPos.w;
//	// シャドウマップ内判定(xyは-1〜1以内 zは0〜1)
//	if (abs(LiPos.x) <= 1 && abs(LiPos.y) <= 1 && LiPos.z <= 1){
//		// 2D座標系からUV座標系に変換する
//		float2 uv = LiPos.xy*0.5 + 0.5;
//			uv.y = 1 - uv.y;
//
//		float z = LiPos.z - Margin;
//
//		// 影になってるかどうか判定
//		//float S_Pow = tex2D(ShadowSmp, uv).r < z ? 0 : 1;
//		//　シャドウマップにいろいろ追加してみる
//
//		int ShadowPixelSize = 1024;
//
//		//簡易ソフト化バージョン(ガタガタが多少改善される)
//		float ShadowTexelSize = 1.0 / ShadowPixelSize; //　画像のサイズ(テクセル)。　VC側で計算して持っていく
//
//		float t0, t1, t2, t3;
//
//		t0 = tex2D(ShadowSmp, uv).r < z ? 0 : 1;
//		t1 = tex2D(ShadowSmp, uv + float2(ShadowTexelSize, 0)).r < z ? 0 : 1; // 1つ右
//		t2 = tex2D(ShadowSmp, uv + float2(0, ShadowTexelSize)).r < z ? 0 : 1; // 1つ下
//		t3 = tex2D(ShadowSmp, uv + ShadowTexelSize).r < z ? 0 : 1; // 1つ右下
//
//		//　判定結果を線形補間する
//		float2 f = frac(uv * ShadowPixelSize); // ピクセル座標系に変換し、小数部のみを取得
//			float S_Pow = lerp( //補間された影の強さをy方向で補間
//			lerp(t0, t1, f.x),	//上の2ピクセルをx方向で補間
//			lerp(t2, t3, f.x),	//下の2ピクセルをx方向で補間
//			f.y);
//
//
//		LPow = min(LPow, S_Pow);	// 暗い方を使用する
//	}
//
//	//================================================
//	// スペキュラ
//	//================================================
//	float3 vCam = normalize(CamPos - In.wPos);		// 目(カメラ)への方向
//		float3 vRef = LightDir + 2.0f * dot(w_normal, -LightDir) * w_normal;
//		float SpePow = pow(max(0, dot(vCam, vRef)), Power);
//	float4 Spe = Specular * SpePow;
//
//		//================================================
//		// モデル色算出
//		//================================================
//		// モデルの色
//		float4 Col = Diffuse * tex2D(MeshSmp, In.MeshUV);
//		// ライトの当たりの強さ+環境光
//		Col.rgb *= min(1, LPow + Ambient);
//
//	//	αテスト
//	//clip(tex2D(MeshSmp, In.MeshUV).a - 0.01f);
//
//	//	αテスト
//	clip(Col.a - 0.5f);
//
//	//================================================
//	// 色を出力
//	//================================================
//	PS_OUT Out;
//	Out.Color = Col + Spe;
//
//	//---------------------------
//	//	ポイントライト
//
//	for (int i = 0; i < 100; i++)
//	{
//		//	ポイントライトまでの距離
//		float Dis = distance(In.wPos, PointPos[i]);
//
//		//	半径が0以上なら 1よりも小さいほうを返す
//		float PointPow = PointRadius[i] == 0 ? 0 : 1 - min(Dis / PointRadius[i], 1);
//
//		PointPow = PointPow * PointPow;	//	光の強度
//
//		Out.Color.rgb += (PointColor[i].rgb * PointPow);
//	}
//
//	Out.Bloom = Spe + (Emissive * EmissivePow) * tex2D(MeshSmp, In.MeshUV);
//	Out.Bloom.a = 1;
//
//
//
//
//	return Out;
//}
*/


//----------------------
//	輪郭ピクセルシェーダ
//----------------------
float4 InkPS(INKVS_OUT In) : COLOR0
{
	return InkColor;
}


//==============================================================
// テクニック
//==============================================================
technique Tech
{
	pass P0
	{
		VertexShader = compile vs_3_0 VC();
		PixelShader = compile ps_3_0 PS();
	}
	//	UVスクロールあり
	pass P1
	{
		VertexShader = compile vs_3_0 VC();
		PixelShader = compile ps_3_0 PS();
	}
	pass P2
	{
		VertexShader = compile vs_3_0 VC();
		PixelShader = compile ps_3_0 PS();
	}
	pass P3
	{
		VertexShader = compile vs_3_0 VC();
		PixelShader = compile ps_3_0 PS();
	}
	pass P4
	{
		VertexShader = compile vs_3_0 VC();
		PixelShader = compile ps_3_0 SpherePS();
	}
	pass P5
	{
		VertexShader = compile vs_3_0 VC();
		PixelShader = compile ps_3_0 NormalPS();
	}
	/*pass P2
	{
	VertexShader = compile vs_3_0 VC();
	PixelShader = compile ps_3_0 PointLight();
	}*/
}


//------------------------------
//	輪郭シェーダー用のテクニック
//------------------------------
technique InkTech
{
	pass P0
	{
		VertexShader = compile vs_3_0 InkVC();
		PixelShader = compile ps_3_0 InkPS();
		CullMode = CW;
	}
};