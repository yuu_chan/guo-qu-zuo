//==============================================================
// グローバル変数
float4x4 mW;		// モデルのワールド行列
float4x4 mLV;		// ライト用のビュー行列
float4x4 mLP;		// ライト用の射影行列

//==============================================================
// シェーダ（シャドウマップの描画）

Texture MeshTex;

// MeshTex用サンプラ
sampler MeshSmp = sampler_state 
{	
	Texture = <MeshTex>;
	MipFilter = LINEAR;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	AddressU = Wrap;
	AddressV = Wrap;
};


struct VS_OUT
{
	float4 Pos		: POSITION;		// 2D座標(射影座標系)
	float2 Depth	: TEXCOORD0;	// 深度値(zとw)
	float2 MeshUV	: TEXCOORD1;
};



// 頂点シェーダ
VS_OUT VS (
	float4 pos : POSITION,
	float3 normal : NORMAL,
	float2 mesh_uv : TEXCOORD0
){
	VS_OUT Out;

	// 頂点座標をライトに対して射影変換(2D化)
	Out.Pos = mul(pos, mW);
	Out.Pos = mul(Out.Pos, mLV);
	Out.Pos = mul(Out.Pos, mLP);

	// 手前の物体の影が欠けるのを防止する工夫
	Out.Pos.z = max(0, Out.Pos.z);
	
	// 深度
	Out.Depth.xy = Out.Pos.zw;	// PS()側でzをwで割るためzwを持っていく(そうしないと正確なzにならない)
	
	//	メッシュUVをもらってくる
	Out.MeshUV   = mesh_uv;

	return Out;
}

// ピクセルシェーダ
float4 PS(VS_OUT In) : COLOR 
{

	//	αテスト
	clip(tex2D(MeshSmp, In.MeshUV).a - 0.01);

	// 正確な深度値を求める
	return In.Depth.x / In.Depth.y; // RGBA全てに出力されますが、今回のテクスチャはR32Fなので、赤のみに出力される

};

//==============================================================
// テクニック

technique Tech 
{
	pass P0 
	{
		AlphaBlendEnable = false;		// アルファブレンドをOFFにする

		VertexShader = compile vs_3_0 VS();
		PixelShader  = compile ps_3_0 PS();
	}
};