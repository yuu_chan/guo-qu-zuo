#include "CMesh.h"
#include "Shader.h"
#include "Game.h"
#include "CGameScene.h"
#include "Texture.h"


CAbsorption::CAbsorption()
{
}

CAbsorption::~CAbsorption()
{
	m_Mesh->Release();
	m_Mesh = nullptr;
}

void CAbsorption::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	メッシュ読み込み
	//m_Mesh = GET_XFILE_LOAD_MANAGER.Load("Data/XFile/Effect/Absorption.x", _lpD3DDevice);
	m_Mesh = make_shared<CMeshObject>();
	m_Mesh->LoadMesh("Data/XFile/Effect/Absorption.x", _lpD3DDevice);
}

void CAbsorption::Update()
{
	const float MOVE = 5.0f;
	const float V_SCROLL = 0.005f;

	//	回転軸更新
	m_vAngle.z += MOVE;

	//	UVアニメーションのスピードを設定
	m_Mesh->GetMeshState()->m_vUVScroll.y -= V_SCROLL;

	//	プレイヤーのワールド行列を取得
	m_mWorld = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(0)->GetWorld();

	//	回転軸をセット
	m_mWorld.RotateZ_Local(m_vAngle.z);

	m_Mesh->GetMeshState()->m_AlphaTest = 0.1f;
}

void CAbsorption::Draw(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	ライティング開始
	_lpD3DDevice->SetRenderState(D3DRS_LIGHTING, TRUE);

	//	プレイヤーのステータスの状態を取得
	auto State = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(0)->GetUpdateTag();

	//	ステータスが任意の状態なら描画
	if (State == "Abso")
	{

		//	ワールド行列をマイシェーダーに渡す
		GET_SHADER.GetMyShader()->SetConstTransformWorld(&m_mWorld);


		//	オブジェクト自身の描画を行う
		GET_SHADER.GetMyShader()->DrawMesh(m_Mesh, 3);

	}

	//	ライティング終了
	_lpD3DDevice->SetRenderState(D3DRS_LIGHTING, FALSE);
}

void CAbsorption::Release()
{

}