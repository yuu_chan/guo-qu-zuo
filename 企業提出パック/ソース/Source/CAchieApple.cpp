#include "Game.h"
#include "CAchieApple.h"


//------------------------>
//
//	定数の値を決めておく
//
//<------------------------

//	テクスチャの横幅
const float CAchieApple::TEXTURE_WIDTH_SIZE = 256;

//	テクスチャの縦幅
const float CAchieApple::TEXTURE_HEIGHT_SIZE = 128;


CAchieApple::CAchieApple()
{

}


CAchieApple::~CAchieApple()
{

}

//	初期化
void CAchieApple::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	テクスチャのサイズを決める
	m_TextureWidthSize = TEXTURE_WIDTH_SIZE;

	m_TextureHeightSize = TEXTURE_HEIGHT_SIZE;

	//	テクスチャを読み込む
	m_pTexture->LoadTextureEx(
		"Data/Graphics/ActualResults/AppleResult.png",
		static_cast<int>(m_TextureWidthSize),
		static_cast<int>(m_TextureHeightSize),
		1,
		0,
		D3DFMT_UNKNOWN,
		D3DPOOL_MANAGED,
		D3DX_FILTER_NONE,
		D3DX_DEFAULT,
		D3DCOLOR_ARGB(255, 255, 255, 255),
		NULL
		);


	//	テクスチャが読み込めていない場合は、エラー警告
	if (m_pTexture->GetTex() == nullptr)
	{
		MessageBox(NULL, "テクスチャーが正しく読み込めていません CAchieApple", "警告", MB_OK);
	}


	m_vPos = m_vInitPos = CVector3(APP.m_Width - m_TextureWidthSize, 0.0f - m_TextureHeightSize, 0.0f);

	m_mWorld.CreateMove(&m_vPos);

}