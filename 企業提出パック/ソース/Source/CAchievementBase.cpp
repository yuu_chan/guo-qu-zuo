#include "Texture.h"
#include "CAchievementBase.h"


CAchievementBase::CAchievementBase()
	:
	m_pTexture(nullptr)
{
	m_pTexture = MyNew CTexture();

	m_Step = 0;

	m_ItemId = 0;

	m_Id = 0;
}


CAchievementBase::~CAchievementBase()
{
	SafeRelease(m_pTexture);
	Delete(m_pTexture);
}


void CAchievementBase::Update()
{

	//	もし、アイテムが回収されたら
	if (m_EnableAchievement == true)
	{
		enum STEP
		{
			INIT,
			DOWN,
			STOP,
			UP,
			END,
		};


		//	
		if (m_Step == STEP::INIT)
		{
			m_Step = STEP::DOWN;
			m_Cnt = 0;
		}

		//	
		if (m_Step == STEP::DOWN)
		{
			m_vPos.y += 1.0f;

			if (m_vPos.y > 0.0f)
			{
				m_vPos.y = 0.0f;
				m_Step = STEP::STOP;
			}
		}

		//
		if (m_Step == STEP::STOP)
		{
			m_Cnt++;

			if (m_Cnt > 100)
			{
				m_Step = STEP::UP;
			}
		}

		//
		if (m_Step == STEP::UP)
		{
			m_vPos.y -= 1.0f;
			if (m_vPos.y < m_vInitPos.y)
			{
				m_vPos.y = m_vInitPos.y;
				m_EnableDestruction = true;
			}
		}
	}

	m_mWorld.CreateMove(&m_vPos);
}


void CAchievementBase::Draw(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	GET_DIRECT_HELPER.BeginSprite();

	GET_DIRECT_HELPER.DrawSprite(m_pTexture, D3DCOLOR_ARGB(255, 255, 255, 255), &m_mWorld);

	GET_DIRECT_HELPER.EndSprite();
}


void CAchievementBase::Release()
{
	SafeRelease(m_pTexture);
	Delete(m_pTexture);
}