#include "Game.h"
#include "CAchievementManager.h"


CAchievementManager::CAchievementManager()
{

}


CAchievementManager::~CAchievementManager()
{
	Release();
}


void CAchievementManager::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
}


void CAchievementManager::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	イテレータを生成最初の位置を指す
	auto itObjContainer = m_listAchieContainer.begin();

	while (itObjContainer != m_listAchieContainer.end())
	{
		(*itObjContainer)->Update();
		++itObjContainer;
	}
}


void CAchievementManager::Draw(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	イテレータのほうが早い
	auto itObjContainer = m_listAchieContainer.begin();

	//	イテレータ
	//vector <CObjectBase *>::iterator itObjContainer;
	//	イテレータを生成最初の位置を指す
	//itObjContainer = m_listObjContainer.begin();

	while (itObjContainer != m_listAchieContainer.end())
	{
		(*itObjContainer)->Draw(_lpD3DDevice);
		
		++itObjContainer;
	}
}


void CAchievementManager::Release()
{
	//	イテレータを生成
	auto itObjContainer = m_listAchieContainer.begin();

	while (itObjContainer != m_listAchieContainer.end())
	{
		//	イテレータの中にあるvectorの中身を消す
		delete (*itObjContainer);

		//	消したアドレスにNULLを入れておく
		*itObjContainer = NULL;

		//	消して、位置をつめる
		itObjContainer = m_listAchieContainer.erase(itObjContainer);
	}
}


void CAchievementManager::PushAchieManager(CAchievementBase* _Achie)
{
	if (_Achie != NULL)
	{
		m_listAchieContainer.push_back(_Achie);
	}
}


CAchievementBase* CAchievementManager::GetItObject(int _Id)
{
	//	イテレータを生成
	auto itObjContainer = m_listAchieContainer.begin();

	//	イテレータ
	//vector <CObjectBase *>::iterator itObjContainer;
	//	イテレータを生成最初の位置を指す
	//itObjContainer = m_listObjContainer.begin();

	//	リストの後尾までまわす
	while (itObjContainer != m_listAchieContainer.end())
	{
		//	指定されたIdが入っていた場合
		if ((*itObjContainer)->GetId() == _Id)
		{
			//	指定されたIdの中身を返す
			return (*itObjContainer);
		}

		//	無ければ次へ
		++itObjContainer;
	}

	//	何も無ければNULLを返す
	return nullptr;
}


void CAchievementManager::SetShaderManager(CShaderManager* _pShader)
{
	//	nullチェック
	if (_pShader == nullptr)
	{
		MessageBox(NULL, "シェーダーのポインターがセットされていません。このままゲームを終了します", "警告", MB_OK);
		APP.m_bEndFlag = true;
		return;
	}

	//	リストの数を変数に代入
	auto itObjContainer = m_listAchieContainer.begin();

	//	リストの後尾に達していなければ
	while (itObjContainer != m_listAchieContainer.end())
	{

		//	指定したオブジェクトにシャドウマップシェーダのポインタをセット
		(*itObjContainer)->SetShader(_pShader);

		//	次のリストへ
		++itObjContainer;
	}
}


void CAchievementManager::SetGameWorld(CGameWorld* _pGameWorld)
{
	//	nullチェック
	if (_pGameWorld == nullptr)
	{
		MessageBox(NULL, "ゲームワールドへのポインターがセットされていません。このままゲームを終了します", "警告", MB_OK);
		APP.m_bEndFlag = true;
		return;
	}

	//	リストの数を変数に代入
	auto itObjContainer = m_listAchieContainer.begin();

	//	リストの後尾に達していなければ
	while (itObjContainer != m_listAchieContainer.end())
	{

		//	指定したオブジェクトにシャドウマップシェーダのポインタをセット
		(*itObjContainer)->SetGameWorld(_pGameWorld);

		//	次のリストへ
		++itObjContainer;
	}
}