#include "Game.h"
#include "CAchievementWorld.h"


CAchievementWorld::CAchievementWorld()
{

}


CAchievementWorld::~CAchievementWorld()
{
}


void CAchievementWorld::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//--------------------------------------
	//	エフェクトマネージャをインスタンス化
	//--------------------------------------

	m_pAchieBaseManager = make_shared<CAchievementManager>();

	//--------
	//	初期化
	//--------

	m_pAchieBaseManager->Init(_lpD3DDevice);

}


void CAchievementWorld::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	m_pAchieBaseManager->Update(_lpD3DDevice);
}


void CAchievementWorld::Draw(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	m_pAchieBaseManager->Draw(_lpD3DDevice);
}


void CAchievementWorld::Release()
{
}
