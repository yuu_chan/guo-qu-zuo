#include "Game.h"

CAirFlower::CAirFlower()
{

}


CAirFlower::~CAirFlower()
{

}


void CAirFlower::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	m_Mesh = GET_XFILE_LOAD_MANAGER.Load("Data/XFile/Object/AirFlower.x", _lpD3DDevice);

	m_State = 0;
}


void CAirFlower::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//----------------------------------
	//	指定した座標にオブジェクトを配置
	//----------------------------------

	m_mWorld.CreateRotateY(m_vAng.y);
	m_mWorld.Move(&m_vPos);


	//-------------------------------------
	//	常にクルクルと回転するようにしておく
	//-------------------------------------

	m_vAng.y += 1.0f;

	if (m_vAng.y >= 360.0f)
	{
		m_vAng.y -= 360.0f;
	}


	//----------------
	//	上下に移動する
	//----------------

	//const float MOVE_MAX = 0.5f;

	////	上
	//if (GET_FLAG_CALC.CheckFlag(m_State, 0x000f, 0x0000))
	//{
	//	m_vVec.y += 0.1f;

	//	m_vPos.y += m_vVec.y;

	//	if (m_vVec.y > MOVE_MAX)
	//	{
	//		m_vVec.y = MOVE_MAX;

	//		m_State = GET_FLAG_CALC.SetData(m_State, 0x0001, LOGICAL::OPERATION_MODE::OR);
	//	}
	//}
	////	下
	//else if (GET_FLAG_CALC.CheckFlag(m_State, 0x000f, 0x0001))
	//{
	//	m_vVec.y -= 0.1f;

	//	m_vPos.y -= m_vVec.y;

	//	if (m_vVec.y < -MOVE_MAX)
	//	{
	//		m_vVec.y = -MOVE_MAX;

	//		m_State = GET_FLAG_CALC.SetData(m_State, 0x0000, LOGICAL::OPERATION_MODE::AND);
	//	}
	//}
}


void CAirFlower::Release()
{

}

