#include "Game.h"
#include "Scene.h"


CApple::CApple()
{

}


CApple::~CApple()
{
	m_spRigidSphere->Release();
}


void CApple::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	メッシュ読み込み
	m_Mesh = GET_XFILE_LOAD_MANAGER.Load("Data/XFile/Object/GorldApple.x", _lpD3DDevice);

	m_spRigidSphere = make_shared<CBulletObj_Sphere>();

	CRigidSupervise::CreateRigidSphere(m_spRigidSphere, m_Mesh, m_vPos, m_Id, m_Mesh->GetBounding()->GetBoundingSphereRadius() / 2 + 0.02f, 1.0f);

	//	このタスクをUserPointerに記憶
	m_spRigidSphere->SetUserPointer(this);

	//	質量0
	m_spRigidSphere->SetMass(0.0f);

	m_Pass = DRAW_SHADER::PASS::SPHERE;
}


void CApple::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	m_mWorld.CreateRotateY(m_vAng.y);
	m_mWorld.RotateX(m_vAng.x);
	m_mWorld.RotateZ(m_vAng.z);
	m_mWorld.Move(&m_vPos);

	//	物理ワールドから行列を取得
	m_spRigidSphere->SetMatrix(m_mWorld);

	//	座標を取得
	m_vPos = m_mWorld.GetPos();

}


void CApple::Release()
{

}