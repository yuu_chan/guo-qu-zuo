#include "Game.h"
#include "CGameScene.h"


CAppleTree::CAppleTree()
{
	
}


CAppleTree::~CAppleTree()
{
	m_spRigidMesh->Release();
	m_spRigidMesh = nullptr;
}


void CAppleTree::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	XファイルマネージャにXファイルを格納その後読み込みよう変数にコピー
	m_Mesh = GET_XFILE_LOAD_MANAGER.Load("Data/XFile/Object/AppleTree.x", _lpD3DDevice);

	//	テクスチャのα値をすこし上げておく
	m_Mesh->GetMeshState()->m_AlphaTest = 0.8f;


	//------------------------
	//	メッシュ剛体を登録する
	//------------------------

	m_spRigidMesh = make_shared<CBulletObj_Mesh>();

	CRigidSupervise::CreateRigidMesh(m_spRigidMesh, m_Mesh, m_vPos, m_Id);

	m_spRigidMesh->SetUserPointer(this);

	m_spRigidMesh->SetMass(0.0f);
}


void CAppleTree::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	行列の更新
	m_mWorld.CreateRotateY(m_vAng.y);
	m_mWorld.RotateX(m_vAng.x);
	m_mWorld.RotateZ(m_vAng.z);
	m_mWorld.Move(&m_vPos);
	m_vPos = m_mWorld.GetPos();

	//	行列を物理ワールドへ送る
	m_spRigidMesh->SetMatrix(m_mWorld);

}


void CAppleTree::Release()
{

}

