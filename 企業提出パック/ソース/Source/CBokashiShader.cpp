#include "Shader.h"


CBokashiShader::CBokashiShader()
{

}


CBokashiShader::~CBokashiShader()
{

}


void CBokashiShader::Init()
{
	LPD3DXBUFFER	pError;		//	エラーメッセージ用
	HRESULT			hr;			//	エラー判定


	//--------------------------------------
	//	読み込みとエラーチェックを同時に行う
	//--------------------------------------

	//	読み込みをして、読み込めたらD3D_OKを返す
	hr = D3DXCreateEffectFromFile(
		GET_DIRECT_HELPER.GetDev(),			//	デバイス
		"Shader/BokashiShader.fx",		//	エフェクトファイル
		NULL,
		NULL,
		0,
		NULL,
		&m_Effect,						//	読み込まれたシェーダを格納する
		&pError							//	エラー内容
		);


	//	ファイルが入っているかどうかを判定
	if (hr != D3D_OK)
	{
		//	入っていない場合はメッセージを出す
		MessageBox(NULL, (LPCSTR)pError->GetBufferPointer(),"Bokasiシェーダのエラーです", MB_ICONERROR | MB_OK);

		//	エラーメッセージをリリース
		pError->Release();

		//	何もせずに返す
		return;
	}

	//	RTを生成
	for (int i = 0; i < 2; i++)
	{
		m_texBokasi[i].CreateRenderTarget(APP.m_Width / 2, APP.m_Height / 2, D3DFMT_A8R8G8B8);
	}

}


void CBokashiShader::Release()
{

}


void CBokashiShader::Draw2D(CTexture *_SrcTex, int _x, int _y, int _w, int _h)
{

	//	テクセルサイズ
	D3DXVECTOR2 ts;

	ts.x = 1.0f / _SrcTex->GetInfo()->Width;
	ts.y = 1.0f / _SrcTex->GetInfo()->Height;
	m_Effect->SetValue("TexelSize", &ts, 8);

	m_Effect->SetTexture("InputTex", _SrcTex->GetTex());

	//テクニック選択
	m_Effect->SetTechnique("NormalTech");

	//テクニック開始
	m_Effect->Begin(0, 0);
	
	//パス開始
	m_Effect->BeginPass(0);

	//	描画
	GET_DIRECT_HELPER.DrawQuad((float)_x, (float)_y, (float)_w, (float)_h, 1.0f, 1.0f, D3DCOLOR_ARGB(255, 255, 255, 255));

	//	パス終了
	m_Effect->EndPass();

	//	テクニック終了
	m_Effect->End();
}


void CBokashiShader::Draw3D(CTexture* _pTexture, float _x, float _y, float _w, float _h, float _tuCnt, float _tvCnt)
{

	//	テクセルサイズ
	D3DXVECTOR2 ts;
	const float TEX_SIZE = 1.0f;

	ts.x = TEX_SIZE / _pTexture->GetInfo()->Width;
	ts.y = TEX_SIZE / _pTexture->GetInfo()->Height;

	m_Effect->SetValue("TexelSize", &ts, 8);

	m_Effect->SetTexture("InputTex", _pTexture->GetTex());

	//テクニック選択
	m_Effect->SetTechnique("NormalTech");

	//テクニック開始
	m_Effect->Begin(0, 0);

	//パス開始
	m_Effect->BeginPass(0);

	//	描画
	GET_DIRECT_HELPER.DrawQuad3D(_x, _y, _w, _h, _tuCnt, _tvCnt, D3DCOLOR_ARGB(255, 255, 255, 255));

	//	パス終了
	m_Effect->EndPass();

	//	テクニック終了
	m_Effect->End();
}


void CBokashiShader::DrawXBokasi(CTexture *_SrcTex, int _x, int _y, int _w, int _h)
{
	//	現在のレンダータゲットを取得するための変数
	LPDIRECT3DSURFACE9 nowRT;

	//	現在のレンダーターゲットを取得する (0番目)
	GET_DIRECT_HELPER.GetDev()->GetRenderTarget(0, &nowRT);

	//	レンダーターゲットのテクスチャーの情報を取得するための変数
	D3DSURFACE_DESC desc;

	//	テクスチャーの情報を取得
	nowRT->GetDesc(&desc);

	//	現在のレンダーターゲットがいらなくなったので廃棄
	nowRT->Release();

	//	サイズ指定が無い場合はテクスチャのデフォルトサイズで描画
	if (_w == 0)_w = desc.Width;
	if (_h == 0)_h = desc.Height;


	D3DXVECTOR2 ts;
	ts.x = 1.0f / _SrcTex->GetInfo()->Width;
	ts.y = 1.0f / _SrcTex->GetInfo()->Height;


	//テクスチャサイズを渡す
	m_Effect->SetValue("TexelSize", &ts, sizeof(FLOAT)* 2);
	//テクスチャ設定
	m_Effect->SetTexture("InputTex", _SrcTex->GetTex());
	//テクニックの選択
	m_Effect->SetTechnique("XBokasiTech");
	//テクニック開始
	m_Effect->Begin(0, 0);

	//パス開始
	m_Effect->BeginPass(0);


	GET_DIRECT_HELPER.DrawQuad((float)_x, (float)_y, (float)_w, (float)_h);


	//パス終了
	m_Effect->EndPass();

	//テクニック終了
	m_Effect->End();
}


void CBokashiShader::DrawYBokasi(CTexture *_SrcTex, int _x, int _y, int _w, int _h)
{
	LPDIRECT3DSURFACE9 nowRT;
	GET_DIRECT_HELPER.GetDev()->GetRenderTarget(0, &nowRT);
	D3DSURFACE_DESC desc;
	nowRT->GetDesc(&desc);
	nowRT->Release();
	if (_w == 0)_w = desc.Width;
	if (_h == 0)_h = desc.Height;


	D3DXVECTOR2 ts;
	ts.x = 1.0f / _SrcTex->GetInfo()->Width;
	ts.y = 1.0f / _SrcTex->GetInfo()->Height;


	//テクスチャサイズを渡す
	m_Effect->SetValue("TexelSize", &ts, sizeof(FLOAT)* 2);

	//テクスチャ設定
	m_Effect->SetTexture("InputTex", _SrcTex->GetTex());

	//テクニックの選択
	m_Effect->SetTechnique("YBokasiTech");

	//テクニック開始
	m_Effect->Begin(0, 0);

	//パス開始
	m_Effect->BeginPass(0);

	//●ここでDirectXの描画を書くと、このシェーダの描画となる

	GET_DIRECT_HELPER.DrawQuad((float)_x, (float)_y, (float)_w, (float)_h);

	//パス終了
	m_Effect->EndPass();

	//テクニック終了
	m_Effect->End();
}


void CBokashiShader::GenerateBokasi(CTexture* _pTexture)
{
	LPDIRECT3DSURFACE9 nowRT;
	GET_DIRECT_HELPER.GetDev()->GetRenderTarget(0, &nowRT);

	m_texBokasi[0].SetRenderTarget();
	DrawXBokasi(_pTexture);
	m_texBokasi[1].SetRenderTarget();
	DrawYBokasi(&m_texBokasi[0]);
	
	for (int i = 0; i < 5; i++)
	{
		m_texBokasi[0].SetRenderTarget();
		DrawXBokasi(&m_texBokasi[1]);
		m_texBokasi[1].SetRenderTarget();
		DrawYBokasi(&m_texBokasi[0]);
	}



	GET_DIRECT_HELPER.GetDev()->SetRenderTarget(0, nowRT);
	nowRT->Release();

}

//	アルファ値を入れる場合
//void CBokashiShader::GenerateBokasi(CTexture* _pTexture)
//{
//	LPDIRECT3DSURFACE9 nowRT;
//	GET_DIRECT_HELPER.GetDev()->GetRenderTarget(0, &nowRT);
//
//	m_texBokasi[0].SetRenderTarget();
//	//GET_DIRECT_HELPER.Clear(true, false, false, D3DCOLOR_ARGB(0, 0, 0, 0));
//
//	DrawXBokasi(_pTexture);
//	m_texBokasi[1].SetRenderTarget();
//	//GET_DIRECT_HELPER.Clear(true, false, false, D3DCOLOR_ARGB(0, 0, 0, 0));
//
//
//	DrawYBokasi(&m_texBokasi[0]);
//
//	for (int i = 0; i < 5; i++){
//		m_texBokasi[0].SetRenderTarget();
//		//GET_DIRECT_HELPER.Clear(true, false, false, D3DCOLOR_ARGB(0, 0, 0, 0));
//		DrawXBokasi(&m_texBokasi[1]);
//
//		m_texBokasi[1].SetRenderTarget();
//		//GET_DIRECT_HELPER.Clear(true, false, false, D3DCOLOR_ARGB(0, 0, 0, 0));
//		DrawYBokasi(&m_texBokasi[0]);
//	}
//
//
//
//	GET_DIRECT_HELPER.GetDev()->SetRenderTarget(0, nowRT);
//	nowRT->Release();
//
//}
