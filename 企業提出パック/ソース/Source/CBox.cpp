#include "Shader.h"
#include "Game.h"
#include "Scene.h"


CBox::CBox()
{
}


CBox::~CBox()
{
	//	開放処理を行う
	m_spRigidBox->Release();

	m_spRigidBox = nullptr;
}


void CBox::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{

	//	XファイルマネージャにXファイルを格納その後読み込みよう変数にコピー
	m_Mesh = GET_XFILE_LOAD_MANAGER.Load("Data/XFile/Object/WoodBox.x", _lpD3DDevice);


	//------------------
	//	立方体剛体を登録
	//------------------

	m_spRigidBox = make_shared<CBulletObj_Box>();

	CRigidSupervise::CreateRigidBox(m_spRigidBox, m_Mesh, m_vPos, m_Id);

	m_spRigidBox->SetUserPointer(this);

}


void CBox::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	共通処理
	ObjectMove();


	if (m_UpdateTag == "PHit")
	{
		m_vPos = m_mWorld.GetPos();
		m_spRigidBox->SetMatrix(m_mWorld);
	}

	if (m_EnableGetWorld == true)
	{
		//	物理ワールドから行列を取得
		m_spRigidBox->GetMatrix(m_mWorld);

		//	座標を取得してくる
		m_vPos = m_mWorld.GetPos();
	}
	else if (m_EnableGetWorld == false)
	{
		m_mWorld.CreateScale(&m_vScalSize);

		m_mWorld.Move(&m_vPos);
	}


	//	タグを初期化しておく
	m_UpdateTag = "";

	m_Mesh->GetMeshState()->m_EnableInk = true;
}


void CBox::Release()
{
}