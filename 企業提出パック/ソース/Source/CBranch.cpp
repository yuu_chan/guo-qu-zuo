#include "Game.h"


CBranch::CBranch()
{

}


CBranch::~CBranch()
{
	m_spRigidSphere->Release();
	m_spRigidSphere = nullptr;
}


void CBranch::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	メッシュ読み込み
	m_Mesh = GET_XFILE_LOAD_MANAGER.Load("Data/XFile/Object/Branch.x", _lpD3DDevice);


	//------------------
	//	球剛体を登録する
	//------------------

	m_spRigidSphere = make_shared<CBulletObj_Capsule>();

	const float Height = 0.2f;

	CRigidSupervise::CreateRigidCapsule(m_spRigidSphere, m_Mesh, m_mWorld,m_vPos, Height, m_Id);

	m_spRigidSphere->SetUserPointer(this);

	m_spRigidSphere->GetMatrix(m_mWorld);

	m_spRigidSphere->getShape()->setMargin(0.001f);

	m_Mesh->GetMeshState()->m_EnableInk = true;
}


void CBranch::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	更新
	ObjectMove();

	m_spRigidSphere->GetMatrix(m_mWorld);

	m_vPos = m_mWorld.GetPos();
}


void CBranch::Release()
{

}

