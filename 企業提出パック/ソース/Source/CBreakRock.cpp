#include "Game.h"


CBreakRock::CBreakRock()
{

}


CBreakRock ::~CBreakRock()
{

}


void CBreakRock::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	メッシュ読み込み
	m_Mesh = GET_XFILE_LOAD_MANAGER.Load("Data/XFile/Object/BreakRock.x", _lpD3DDevice);


	//	物理ワールドに登録
	m_spRigidBox = make_shared<CBulletObj_Box>();

	m_vScalSize = CVector3(m_Mesh->GetBounding()->GetBoundingBoxMax().x, m_Mesh->GetBounding()->GetBoundingBoxMax().y * 0.1f, m_Mesh->GetBounding()->GetBoundingBoxMax().z);

	CRigidSupervise::CreateRigidBox(m_spRigidBox, m_Mesh, m_vPos, m_Id, m_vScalSize, 0.5f);

	m_spRigidBox->SetMass(0.5f);

	m_spRigidBox->SetUserPointer(this);
}


void CBreakRock::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	m_spRigidBox->GetMatrix(m_mWorld);

	m_vPos = m_mWorld.GetPos();
}


void CBreakRock::Release()
{

}

