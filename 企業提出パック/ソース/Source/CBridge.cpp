#include "Game.h"


CBridge::CBridge()
{

}


CBridge::~CBridge()
{
	m_spRigidMesh->Release();
}


void CBridge::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	メッシュ読み込み
	m_Mesh = GET_XFILE_LOAD_MANAGER.Load("Data/XFile/Object/Bridge.x", _lpD3DDevice);


	//------------------
	//	立方体剛体を登録
	//------------------

	m_spRigidMesh = make_shared<CBulletObj_Mesh>();

	CRigidSupervise::CreateRigidMesh(m_spRigidMesh, m_Mesh, m_vPos, m_Id);

	m_spRigidMesh->SetUserPointer(this);

	//	質量0として設定
	m_spRigidMesh->SetMass(0.0f);

	m_spRigidMesh->SetMatrix(m_mWorld);


	m_Mesh->GetBounding()->SetBoundingSphereRadiuse(100.0f);

	m_Mesh->GetMeshState()->m_EnableInk = true;
}


void CBridge::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{

	if (GET_FLAG_CALC.CheckFlag(m_State, 0x0002))
	{
		/* CheckCnt----------------------------------------
		
		ラムダ判定関数
		引数のカウンターが一定以上超えていれば
		自身のステータスを次の処理へ移行するようにする

		キャプチャ		:	無し

		_Cnt			:	判定しておきたいカウター

		return			:	戻り値無し
		
		-------------------------------------------------*/

		function<void(int)> CheckCnt = 
		[&]
		(int _Cnt)
		{

			//	溜めゲージが一定以上なら次のステータスへ
			if (_Cnt >= 50)
			{
				m_State = GET_FLAG_CALC.SetData((m_State & 0x0000), 0x0004, LOGICAL::OPERATION_MODE::OR);
			}

		};


		for (auto &This : CRigidSupervise::GetRigidList())
		{
			//------------------------------------------
			//	ダウンキャストしてプレイヤーの中身を見る
			//	プレイヤーの吐き出しカウンターを見て、
			//	一定以上の場合次のステップへ移行する
			//------------------------------------------

			//	基底クラスをまずとってくる
			CObjectBase* ThisObj = (CObjectBase *)This.lock()->GetUserPointer();

			//	プレイヤー型の変数にキャスト
			CPlayer* Player = dynamic_cast<CPlayer *>(ThisObj);

			//	成功すれば中身を覗く
			if (Player)
			{
				int Cnt = Player->GetSpitCnt();
				CheckCnt(Cnt);
			}
		}

	}
	else if (GET_FLAG_CALC.CheckFlag(m_State, 0x0004))
	{
		m_vVec.z += 0.03f;

		m_vAng.z -= m_vVec.z;

		if (m_vAng.z < -90.0f)
		{
			m_vAng.z = -90.0f;
		}


		//	メッシュの法線情報を取得する
		//m_Mesh->UpdateVertexData(_lpD3DDevice, m_Id);

	}


	//----------------
	//	行列を更新する
	//----------------

	m_mWorld.CreateRotateZ(m_vAng.z);
	m_mWorld.RotateY(m_vAng.y);
	m_mWorld.Move(&m_vPos);

	m_vPos = m_mWorld.GetPos();

	m_spRigidMesh->SetMatrix(m_mWorld);
}


void CBridge::Release()
{

}

