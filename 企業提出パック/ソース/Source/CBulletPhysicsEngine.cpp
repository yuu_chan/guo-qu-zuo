

#include "CBulletPhysicsEngine.h"

extern ContactProcessedCallback gContactProcessedCallback;

void BulletWorld::Init()
{
	// bullet初期化
	m_collisionConfiguration = new btDefaultCollisionConfiguration();		// デフォルトのコリジョンコンフィグ作成
	m_dispatcher = new btCollisionDispatcher(m_collisionConfiguration);	// デフォルトのコリジョンディスパッチャ作成

	// ブロードフェーズ衝突判定
	m_broadphase = new btDbvtBroadphase();
	/*
	btVector3 worldAabbMin(-10000,-10000,-10000);
	btVector3 worldAabbMax(10000,10000,10000);
	m_broadphase = new btAxisSweep3 (worldAabbMin, worldAabbMax);
	*/

	// デフォルトの拘束計算（数値解析）ソルバを作成する
	m_solver = new btSequentialImpulseConstraintSolver;

	// 物理ワールドの作成
	m_dynamicsWorld = new btDiscreteDynamicsWorld(m_dispatcher, m_broadphase, m_solver,m_collisionConfiguration);

	// 重力の設定
	m_dynamicsWorld->setGravity(btVector3(0,-9.8f,0));

	// 衝突検知用コールバック関数設定
	gContactProcessedCallback = BulletWorld::s_ContactProcessedCallback;
}

bool BulletWorld::s_ContactProcessedCallback(btManifoldPoint& cp, void* body0, void* body1)
{
	btRigidBody* pBody0 = (btRigidBody*)body0;
	btRigidBody* pBody1 = (btRigidBody*)body1;

	CBulletRigid_Base* pRigidBase0 = (CBulletRigid_Base*)pBody0->getUserPointer();
	CBulletRigid_Base* pRigidBase1 = (CBulletRigid_Base*)pBody1->getUserPointer();

	// Bullet Worldのアドレスを取得
	BulletWorld* world = NULL;
	if(pRigidBase0)world = pRigidBase0->GetWorld();
	else if(pRigidBase1)world = pRigidBase1->GetWorld();

	// 仮想関数呼び出しで通知する
	if(world){
		bool Check;
		//return
			
		Check =	pRigidBase0->GetWorld()->ContactProcessedCallback(cp, pRigidBase0, pRigidBase1);

		if (Check == true)
		{
			return true;
		}
		else if (Check == false)
		{
			return false;
		}
	}

	return true;
}

// 解放
void BulletWorld::Release()
{
	if(m_dynamicsWorld){
		while(m_dynamicsWorld->getNumConstraints() > 0){
			btTypedConstraint* obj = m_dynamicsWorld->getConstraint(0);
			CBulletObj_Base* p = (CBulletObj_Base*)obj->getUserConstraintPtr();
			if(p){
				p->Release();
			}
		}
		// 剛体
		for (int i=m_dynamicsWorld->getNumCollisionObjects()-1; i>=0 ;i--){
			btCollisionObject* obj = m_dynamicsWorld->getCollisionObjectArray()[i];
			btRigidBody* body = btRigidBody::upcast(obj);
			if(body){
				CBulletObj_Base* p = (CBulletObj_Base*)body->getUserPointer();
				if(p){
					p->Release();
				}
			}
		}
	}

	SAFE_DELETE(m_dynamicsWorld);			// ワールド削除
	SAFE_DELETE(m_solver);
	SAFE_DELETE(m_broadphase);
	SAFE_DELETE(m_dispatcher);
	SAFE_DELETE(m_collisionConfiguration);
}
