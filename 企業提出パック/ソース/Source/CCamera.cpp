#include "CDirectX9.h"
#include "CSystem.h"
#include "CCamera.h"


CCamera::CCamera()
:
m_EnableCamera(false)
{
}


CCamera::~CCamera()
{
}


void CCamera::SetChaseCamera(CVector3& _CPos, CVector3& _CLook, CVector3& _CHead, CMatrix& _World)
{
	//	追尾カメラの設定
	D3DXVec3TransformCoord(&_CPos,  &CVector3(0.0f, 1.5f, -3.0f), &_World);
	D3DXVec3TransformCoord(&_CLook, &CVector3(0.0f, 0.0f, 0.0f), &_World);
	D3DXVec3TransformCoord(&_CHead, &CVector3(0.0f, 1.0f, 0.0f), &_World);
	_CHead = _CHead - _CLook;
}


void CCamera::SetCamera(CMatrix& _mView, CVector3 _CPos, CVector3 _DisPos, float _AngX, float _AngY, float _AngZ)
{
	CMatrix  mPos, mDis, mRot;
	float		m_CamDis = 5;

	D3DXMatrixTranslation(&mPos, _CPos.x, _CPos.y, _CPos.z);
	D3DXMatrixTranslation(&mDis, _DisPos.x, _DisPos.y, _DisPos.z);
	D3DXMatrixRotationYawPitchRoll(&mRot, D3DXToRadian(_AngY), D3DXToRadian(_AngX), D3DXToRadian(_AngZ));

	//	カメラのワールド行列にそれぞれの行列を合成
	m_mCamera = mDis * mRot * mPos;

	//	カメラのワールド行列から逆行列を作って、ビュー行列に格納
	D3DXMatrixInverse(&_mView, NULL, &m_mCamera);
}


void CCamera::CameraUpdate(
	LPDIRECT3DDEVICE9 _lpD3DDevice, 
	CVector3& _BasePos, 
	CMatrix* _World, 
	float _AngX, 
	float _AngY,
	float _AngZ,
	float _fovy, 
	float _asp, 
	float _zn, 
	float _zf)
{
	//------------------
	//	作業用変数を生成
	//------------------

	//	カメラの見ている位置を設定
	CVector3 CamLook = CVector3(_BasePos.x, _BasePos.y, _BasePos.z);

	//	カメラのポジションを設定
	m_mCamPos = CVector3(0.0f, 1.5f, -(6.0f + _AngZ));


	//----------------------
	//	カメラのモードを変更
	//----------------------

	////	右クリックが押されているかどうか
	//if (CKeyCode::m_Code.MouseRight == CKeyCode::HOLD)
	//{
	//	//	カメラの位置を変更する
	//	SetCamera(m_mView, CamLook, CVector3(0, 1, -0.5), _AngX, _AngY);
	//	//	FPSモードに変更する
	//	CameraMode = GET_FLAG_CALC.SetData(CameraMode, Mode::FPS, LOGICAL::OPERATION_MODE::OR);
	//}
	////	それ以外はすべてデフォルト
	//else
	//{
		//	 座標セット
		SetCamera(m_mView, CamLook, m_mCamPos, _AngX, _AngY);
		//	デフォルトはTPS視点
		CameraMode &= Mode::TPS;
	//}

	m_Angle = CVector3(_AngX, _AngY, _AngZ);

	
	//----------------
	//　投影行列の設定
	//----------------
	D3DXMatrixPerspectiveFovLH(&m_mProj, D3DXToRadian(_fovy), _asp, _zn, _zf);


	//------------
	//　行列セット
	//------------

	//	ビュー行列をセット
	_lpD3DDevice->SetTransform(D3DTS_VIEW, &m_mView);

	//	射影行列をセット
	_lpD3DDevice->SetTransform(D3DTS_PROJECTION, &m_mProj);


	//	視錘台を作る
	GET_HELPER.CreateViewFrustum(&m_VF);


	//	カメラをセット
	D3DXMatrixPerspectiveFovLH(&m_mProj, D3DXToRadian(_fovy), _asp, _zn, _zf);
	_lpD3DDevice->SetTransform(D3DTS_PROJECTION, &m_mProj);
}


void CCamera::SetProj(LPDIRECT3DDEVICE9 _lpD3DDevice, CMatrix& _mProj, float _AngX, float _AngY,
	float _fovy, float _asp, float _zn, float _zf)
{
	D3DXMatrixPerspectiveFovLH(&_mProj, D3DXToRadian(_fovy), _asp, _zn, _zf);
	_lpD3DDevice->SetTransform(D3DTS_PROJECTION, &_mProj);
}


void CCamera::SetView()
{

}
