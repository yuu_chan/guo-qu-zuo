#include "Texture.h"
#include "Game.h"
#include "CClickFont.h"


//------------------
//	定数の値を決める
//------------------

//	フォントの横幅
const float CClickFont::FONT_WIDTH_SIZE = 180;

//	フォントの縦幅
const float CClickFont::FONT_HEIGHT_SIZE = 120;


CClickFont::CClickFont()
	:
	m_pPlayer(nullptr)
	,
	m_pSky(nullptr)
{
}

CClickFont::~CClickFont()
{
	////SafeRelease(m_pTexture);
	//m_pTexture->Release();
	//m_pTexture = nullptr;

	////SafeRelease(m_Mesh);
	////Delete(m_Mesh);
	//SAFE_RELEASE(m_Mesh);
	//SAFE_RELEASE(m_pSky);

	////m_pPlayer = nullptr;

}


//	初期化
void CClickFont::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	////m_pTexture = GET_TEXTURE_LOAD_MANAGER.LoadEx(
	////	"Data/Graphics/Font/PleaseClick.png",
	////	(int)FONT_WIDTH_SIZE,
	////	(int)FONT_HEIGHT_SIZE,
	////	true,
	////	255,
	////	255,
	////	255,
	////	255
	////	);


	////m_pPlayer = make_shared<CMeshObject>();
	//////m_pPlayer = GET_XFILE_LOAD_MANAGER.Load("Data/XFile/Object/PlayerTitle.x", _lpD3DDevice);

	////m_pPlayer->LoadSkinMesh("Data/XFile/Character/Player.x", _lpD3DDevice);
	////m_pPlayer->SetSkinMesh();

	////m_pPlayer->SetAdvanceTime(0.3f);
	////m_pPlayer->SetLoopTime(60.0f);
	////m_pPlayer->SetAnimationKey(7);


	////m_Mesh = new CMeshObject();
	//m_Mesh = make_shared <CMeshObject>();
	//m_Mesh = GET_XFILE_LOAD_MANAGER.Load("Data/XFile/Object/Title.x", _lpD3DDevice);


	////m_pSky = new CMeshObject();
	//m_pSky = make_shared<CMeshObject>();
	//m_pSky = GET_XFILE_LOAD_MANAGER.Load("Data/XFile/Stage/Sky.x", _lpD3DDevice);
}


//	更新
void CClickFont::Update()
{
	////D3DXMatrixTranslation(&m_mWorld, 1.0f, 2.0f, -8.0f);

	////	スカイスフィアを回転
	//m_vAng.y += 0.008f;
	//if (m_vAng.y > 360)
	//	m_vAng.y = 0;

	////	回転量を設定
	//m_mWorld.CreateRotateY(m_vAng.y);

	////m_pPlayer->UpdateSkinMesh(m_PlayerWorld);
}


//	描画
void CClickFont::Draw(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//CMatrix mWorld, mRotX, mTrans;


	//D3DXMatrixTranslation(&mTrans, 0.0f, 0.0f, 8.0f);
	//D3DXMatrixRotationX(&mRotX, D3DXToRadian(0.0f));

	//mWorld = mRotX * mTrans;

	//_lpD3DDevice->SetTransform(D3DTS_WORLD, &mWorld);
	//_lpD3DDevice->SetRenderState(D3DRS_LIGHTING, TRUE);

	//m_Mesh->DrawMesh();

	//_lpD3DDevice->SetRenderState(D3DRS_LIGHTING, FALSE);


	//_lpD3DDevice->SetTexture(0, NULL);

	//_lpD3DDevice->SetRenderState(D3DRS_LIGHTING, FALSE);
	//_lpD3DDevice->SetTransform(D3DTS_WORLD, &m_mWorld);

	//m_pSky->DrawMesh();

	//_lpD3DDevice->SetTexture(0, NULL);
	//_lpD3DDevice->SetRenderState(D3DRS_LIGHTING, FALSE);

	////	ライティング開始
	//_lpD3DDevice->SetRenderState(D3DRS_LIGHTING, TRUE);

	////	テクスチャはなし
	//_lpD3DDevice->SetTexture(0, NULL);

	//D3DXMatrixTranslation(&mWorld, 0.0f, 0.8f, -2.0f);

	//CMatrix mRotY;
	//D3DXMatrixRotationY(&mRotY, D3DXToRadian(180));

	////m_PlayerWorld = mRotY * mWorld;

	////_lpD3DDevice->SetTransform(D3DTS_WORLD, &m_PlayerWorld);

	////m_pPlayer->DrawSkinMesh(_lpD3DDevice);

	////	ライティング終了
	//_lpD3DDevice->SetRenderState(D3DRS_LIGHTING, FALSE);
}


//	開放
void CClickFont::Release()
{

}
