#include "Game.h"


CCloud::CCloud()
{

}


CCloud::~CCloud()
{
	m_spRigidSphere->Release();
}


void CCloud::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	メッシュ読み込み
	//m_Mesh = GET_XFILE_LOAD_MANAGER.Load("Data/XFile/Object/Cloud.x", _lpD3DDevice);

	m_Mesh->LoadSkinMesh("Data/XFile/Object/Cloud.x", _lpD3DDevice);

	m_Mesh->SetSkinMesh();

	m_Mesh->SetAnimationKey(0);

	m_Mesh->SetAdvanceTime(0.001f);

	m_Mesh->SetShiftTime(1.0f);

	m_Mesh->SetLoopTime(60.0f);

	m_spRigidSphere = make_shared<CBulletObj_Sphere>();

	CRigidSupervise::CreateRigidSphere(m_spRigidSphere, m_Mesh, m_vPos, m_Id, m_Mesh->GetBounding()->GetBoundingSphereRadius(), 0.0f);

	//	タスクをUserPointerに記憶
	m_spRigidSphere->SetUserPointer(this);


	m_spRigidSphere->GetMatrix(m_mWorld);

	m_vPos = m_mWorld.GetPos();

	m_Pass = 2;
}


void CCloud::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	更新
	ObjectMove();

	m_Mesh->UpdateSkinMesh(m_mWorld);

	static float a = 0;
	
	m_AbleDelete = GET_HELPER.UnderDecision(OBJECT_LIST::ID::PLAYER, m_Id, a, CVector3(0, -1, 0));

	if (m_AbleDelete == true)
	{
		CMyAllocateHierarchy::MYD3DXMESHCONTAINER* mc = m_Mesh->GetCont(0);
		
		
		//----------------------------------------------------------------------
		//	使用しているマテリアルが1つだけなので、0番目をアクセスするようにする
		//----------------------------------------------------------------------

		mc->pMaterials[0].MatD3D.Diffuse.a -= 0.03f;

		if (mc->pMaterials[0].MatD3D.Diffuse.a < 0.0f)
		{
			mc->pMaterials[0].MatD3D.Diffuse.a = 0.0f;
			m_Filter = 1;
		}

	}

	////	物理ワールドから行列を取得
	//m_spRigidSphere->GetMatrix(m_mWorld);

	////	座標を取得
	//m_vPos = m_mWorld.GetPos();




}


void CCloud::Draw(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	ライティング開始
	_lpD3DDevice->SetRenderState(D3DRS_LIGHTING, TRUE);


	//	マイシェーダー側にワールド行列を送る
	GET_SHADER.GetMyShader()->SetConstTransformWorld(&m_mWorld);

	//	マイシェーダー描画を行う(シャドウマップ入り)
	GET_SHADER.GetMyShader()->DrawSkinMesh(m_Mesh, m_Pass);


	//	ライティング終了
	_lpD3DDevice->SetRenderState(D3DRS_LIGHTING, FALSE);
}


void CCloud::Release()
{

}

