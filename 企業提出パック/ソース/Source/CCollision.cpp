#include "CCollision.h"


CCollision::CCollision()
{

}


CCollision::~CCollision()
{

}


BOOL CCollision::IntersectRay(
	const CFace*		FaceTbl, 
	int					FaceTblNum, 
	const D3DXVECTOR3*	lpRayPos,
	const D3DXVECTOR3*	lpRayDir,
	float*				dist, 
	DWORD*				lpHitFaceIndex
	)
{
	float d = 0;

	//	*dist = FLT_MAX;

	BOOL bl;
	
	BOOL RetBl = FALSE;
	
	int i;
	
	int FndCnt = 0;
	
	for(i=0;i<FaceTblNum;i++)
	{
		// 0,1,2と判定
		bl = D3DXIntersectTri(	&FaceTbl[i].v[0],
								&FaceTbl[i].v[1],
								&FaceTbl[i].v[2],
								lpRayPos,
								lpRayDir,
								NULL,
								NULL,
								&d);
		if(bl)
		{
			if(FndCnt == 0)
			{
				*dist = d;
				if(lpHitFaceIndex){
					*lpHitFaceIndex = i;
				}
			}
			else
			{
				if(d < *dist)
				{
					*dist = d;
					if(lpHitFaceIndex)
					{
						*lpHitFaceIndex = i;
					}
				}
			}

			RetBl = TRUE;
			FndCnt++;
		}
	}

	return RetBl;
}


BOOL CCollision::IntersectRay(
	const CFace*		FaceTbl, 
	int					FaceTblNum, 
	const D3DXVECTOR3*	lpRayPos, 
	const D3DXVECTOR3*	lpRayDir, 
	float*				dist, 
	DWORD*				lpHitFaceIndex, 
	D3DXMATRIX*			D3DXMATRIX
	)
{
	CVector3 vWay,vPos;
	CMatrix InvMat;
	CMatrix::CreateInverse(&InvMat,NULL,D3DXMATRIX);
	// レイ情報を指定した行列の逆行列で変換し、ローカル座標系へ変換する。
	D3DXVec3TransformNormal(&vWay,lpRayDir,&InvMat);	// lpRayDirをInvMatで変換(座標は無効。つまり回転だけ)
	D3DXVec3TransformCoord(&vPos,lpRayPos,&InvMat);	// lpRayPosをInvMatで変換(座標も有効)

	return IntersectRay(FaceTbl,FaceTblNum,&vPos,&vWay,dist,lpHitFaceIndex);

}


BOOL CCollision::IntersectRayBB(
	const D3DXVECTOR3*	vmin,
	const D3DXVECTOR3*	vmax,
	const CVector3*		lpRayPos,
	const CVector3*		lpRayDir,
	float*				dist)
{
	float d = 0;
//	*dist = FLT_MAX;
	BOOL bl;
	BOOL RetBl = FALSE;
	DWORD i;
	int FndCnt = 0;

	CVector3 vec[4];

	for (i = 0; i < 6; i++)
	{
		if(i == 0)
		{
			// 上
			vec[0].x = vmax->x;	vec[0].y = vmax->y;	vec[0].z = vmax->z;
			vec[1].x = vmin->x;	vec[1].y = vmax->y;	vec[1].z = vmax->z;
			vec[2].x = vmax->x;	vec[2].y = vmax->y;	vec[2].z = vmin->z;
			vec[3].x = vmin->x;	vec[3].y = vmax->y;	vec[3].z = vmin->z;
		}
		else if(i == 1)
		{
			// 下
			vec[0].x = vmin->x;	vec[0].y = vmin->y;	vec[0].z = vmin->z;
			vec[1].x = vmin->x;	vec[1].y = vmin->y;	vec[1].z = vmax->z;
			vec[2].x = vmax->x;	vec[2].y = vmin->y;	vec[2].z = vmin->z;
			vec[3].x = vmax->x;	vec[3].y = vmin->y;	vec[3].z = vmax->z;
		}
		else if(i == 2)
		{
			// 左
			vec[0].x = vmin->x;	vec[0].y = vmin->y;	vec[0].z = vmin->z;
			vec[1].x = vmin->x;	vec[1].y = vmin->y;	vec[1].z = vmax->z;
			vec[2].x = vmin->x;	vec[2].y = vmax->y;	vec[2].z = vmin->z;
			vec[3].x = vmin->x;	vec[3].y = vmax->y;	vec[3].z = vmax->z;
		}
		else if(i == 3)
		{
			// 右
			vec[0].x = vmax->x;	vec[0].y = vmin->y;	vec[0].z = vmin->z;
			vec[1].x = vmax->x;	vec[1].y = vmin->y;	vec[1].z = vmax->z;
			vec[2].x = vmax->x;	vec[2].y = vmax->y;	vec[2].z = vmin->z;
			vec[3].x = vmax->x;	vec[3].y = vmax->y;	vec[3].z = vmax->z;
		}
		else if(i == 4)
		{
			// 前
			vec[0].x = vmin->x;	vec[0].y = vmin->y;	vec[0].z = vmin->z;
			vec[1].x = vmax->x;	vec[1].y = vmin->y;	vec[1].z = vmin->z;
			vec[2].x = vmin->x;	vec[2].y = vmax->y;	vec[2].z = vmin->z;
			vec[3].x = vmax->x;	vec[3].y = vmax->y;	vec[3].z = vmin->z;
		}
		else if(i == 5)
		{
			// 後
			vec[0].x = vmin->x;	vec[0].y = vmin->y;	vec[0].z = vmax->z;
			vec[1].x = vmax->x;	vec[1].y = vmin->y;	vec[1].z = vmax->z;
			vec[2].x = vmin->x;	vec[2].y = vmax->y;	vec[2].z = vmax->z;
			vec[3].x = vmax->x;	vec[3].y = vmax->y;	vec[3].z = vmax->z;
		}

		bl = D3DXIntersectTri(	&vec[0],&vec[1],&vec[2],lpRayPos,lpRayDir,NULL,NULL,&d);
		if(bl)
		{
			if(FndCnt == 0){
				*dist = d;
			}
			else{
				if(d < *dist){
					*dist = d;
				}
			}
			RetBl = TRUE;
			FndCnt++;
		}
		bl = D3DXIntersectTri(	&vec[1],&vec[2],&vec[3],lpRayPos,lpRayDir,NULL,NULL,&d);
		if(bl){
			if(FndCnt == 0){
				*dist = d;
			}
			else{
				if(d < *dist){
					*dist = d;
				}
			}
			RetBl = TRUE;
			FndCnt++;
		}
	}


	return RetBl;
}


BOOL CCollision::IntersectRayBB(
	const D3DXVECTOR3*	vmin,
	const D3DXVECTOR3*	vmax,
	const CVector3*		lpRayPos,
	const CVector3*		lpRayDir,
	float*				dist,
	const CMatrix*		lpMat)
{
	CVector3 vWay,vPos;
	CMatrix InvMat;
	D3DXMatrixInverse(&InvMat,NULL,lpMat);
	// レイ情報を指定した行列の逆行列で変換し、ローカル座標系へ変換する。
	D3DXVec3TransformNormal(&vWay,lpRayDir,&InvMat);	// lpRayDirをInvMatで変換(座標は無効。つまり回転だけ)
	D3DXVec3TransformCoord(&vPos,lpRayPos,&InvMat);	// lpRayPosをInvMatで変換(座標も有効)

	return IntersectRayBB(vmin,vmax,&vPos,&vWay,dist);
}


BOOL CCollision::FaceToSphere(
	CFace*			FaceTbl, 
	int				FaceTblNum, 
	CVector3*		lpQPos, 
	float			Qr, 
	CVector3*		lpvOut, 
	const CMatrix*	lpMat,
	int				Targetid
	)
{
	CVector3 vPos;
	CMatrix InvMat;
	CMatrix::CreateInverse(&InvMat,NULL,lpMat);
	// レイ情報を指定した行列の逆行列で変換し、ローカル座標系へ変換する。
	CVector3::Transform(&vPos,lpQPos,&InvMat);	// lpRayPosをInvMatで変換(座標も有効)

	BOOL ret = FaceToSphere(FaceTbl, FaceTblNum, &vPos, Qr, lpvOut, Targetid);
	if(ret){
		// 壁の方向を変換
		CVector3::TransformNormal(lpvOut,lpvOut,lpMat);
		lpQPos->x += lpvOut->x;
		lpQPos->y += lpvOut->y;
		lpQPos->z += lpvOut->z;
	}
	return ret;
}


BOOL CCollision::FaceToSphere(
	CFace*		FaceTbl,		//	相手の面テーブルを渡す
	int			FaceTblNum,		//	vector配列のサイズを渡す
	CVector3*	lpQPos,			//	自分の座標
	float		Qr,				//	半径を渡す
	CVector3*	lpvOut,			//	最終的にもらえる座標
	int			TargetId
	)
{
	//	当たり判定フィルターが通っているかどうかを確認
	UINT Filter = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(TargetId)->GetFilter();

	//	フィルターをチェックして、当たり判定用のフラグが立っていなければ
	if (!GET_FLAG_CALC.CheckFlag(Filter, FILTER::NAME::COLLISION_ON))
	{
		//	なにもせずに返す
		return false;
	}


	std::queue<CFace*> faceQ;

	BOOL bRet = FALSE;
	int i;
	CVector3 ret;
	CVector3 v1,v2;
	float dot,dot2;
	lpvOut->x = 0;
	lpvOut->y = 0;
	lpvOut->z = 0;


	for (i = 0; i < FaceTblNum; i++)
	{

		// �@無限平面と球との判定
		v1 = *lpQPos - FaceTbl[i].v[0];

		dot2 = CVector3::Dot(&v1,&FaceTbl[i].vN);
//		if(dot2 > -Qr && dot2 < Qr){ // 半径より近いなら、衝突している

		// 半径より近いなら、衝突している
		if(dot2 >= 0 && dot2 < Qr)
		{
			// �A三角形と点との内外判定
			v1 = FaceTbl[i].v[1]-FaceTbl[i].v[0];

			v2 = *lpQPos-FaceTbl[i].v[0];

			CVector3::Cross(&ret,&v1,&v2);

			dot = CVector3::Dot(&ret,&FaceTbl[i].vN);

			if(dot < 0.0f)
			{
				// BB判定で当たってるなら、線分判定行き
				if( (FaceTbl[i].max.x >= -Qr+lpQPos->x) && (FaceTbl[i].min.x <= Qr+lpQPos->x)
				&&	(FaceTbl[i].max.y >= -Qr+lpQPos->y) && (FaceTbl[i].min.y <= Qr+lpQPos->y)
				&&	(FaceTbl[i].max.z >= -Qr+lpQPos->z) && (FaceTbl[i].min.z <= Qr+lpQPos->z) )
				{
					// キューへ登録
					faceQ.push(&FaceTbl[i]);
				}
				
				continue;// 外側決定
			}


			v1 = FaceTbl[i].v[2]-FaceTbl[i].v[1];

			v2 = *lpQPos-FaceTbl[i].v[1];

			CVector3::Cross(&ret,&v1,&v2);

			dot = CVector3::Dot(&ret,&FaceTbl[i].vN);

			if(dot < 0.0f)
			{
				// BB判定で当たってるなら、線分判定行き
				if( (FaceTbl[i].max.x >= -Qr+lpQPos->x) && (FaceTbl[i].min.x <= Qr+lpQPos->x)
				&&	(FaceTbl[i].max.y >= -Qr+lpQPos->y) && (FaceTbl[i].min.y <= Qr+lpQPos->y)
				&&	(FaceTbl[i].max.z >= -Qr+lpQPos->z) && (FaceTbl[i].min.z <= Qr+lpQPos->z) )
				{
					// キューへ登録
					faceQ.push(&FaceTbl[i]);
				}

				continue;// 外側決定
			}


			v1 = FaceTbl[i].v[0]-FaceTbl[i].v[2];

			v2 = *lpQPos-FaceTbl[i].v[2];

			CVector3::Cross(&ret,&v1,&v2);

			dot = CVector3::Dot(&ret,&FaceTbl[i].vN);

			if(dot < 0.0f)
			{
				// BB判定で当たってるなら、線分判定行き
				if( (FaceTbl[i].max.x >= -Qr+lpQPos->x) && (FaceTbl[i].min.x <= Qr+lpQPos->x)
				&&	(FaceTbl[i].max.y >= -Qr+lpQPos->y) && (FaceTbl[i].min.y <= Qr+lpQPos->y)
				&&	(FaceTbl[i].max.z >= -Qr+lpQPos->z) && (FaceTbl[i].min.z <= Qr+lpQPos->z) )
				{
					// キューへ登録
					faceQ.push(&FaceTbl[i]);
				}

				continue;// 外側決定
			}

			*lpvOut += FaceTbl[i].vN * (Qr - dot2);

			*lpQPos += FaceTbl[i].vN * (Qr - dot2);	// 球の座標も変更

			bRet = TRUE;

		}
	}
	
	// �B球と線分との判定
	CFace*		lpFace;
	CVector3	v,vQ,vPos;
	float		f;
	DWORD		j;

	while( !faceQ.empty() )
	{
		lpFace = faceQ.front();

		// 辺と
		float t;
		v1 = lpFace->v[1] - lpFace->v[0];
		vQ = *lpQPos - lpFace->v[0];
		t = CVector3::Dot(&vQ,&v1) / CVector3::Dot(&v1,&v1);
		vPos = lpFace->v[0] + v1*t;
		v1 = *lpQPos - vPos;
		if(t >= 0 && t <= 1)
		{
			if(CVector3::Dot(&v1,&v1) <= Qr*Qr)
			{
				f = Qr - v1.Length();
				v1.Normalize();
				*lpvOut += v1 * f;
				*lpQPos += v1 * f;	// 球の座標も変更
			
				bRet = TRUE;
			}
		}

		v1 = lpFace->v[2] - lpFace->v[1];
		vQ = *lpQPos - lpFace->v[1];
		t = CVector3::Dot(&vQ,&v1) / CVector3::Dot(&v1,&v1);
		vPos = lpFace->v[1] + v1*t;
		v1 = *lpQPos - vPos;

		if(t >= 0 && t <= 1)
		{
			if(CVector3::Dot(&v1,&v1) <= Qr*Qr)
			{
				f = Qr - v1.Length();
				v1.Normalize();
				*lpvOut += v1 * f;
				*lpQPos += v1 * f;	// 球の座標も変更

				bRet = TRUE;
			}
		}


		v1 = lpFace->v[0] - lpFace->v[2];
		vQ = *lpQPos - lpFace->v[2];
		t = CVector3::Dot(&vQ,&v1) / CVector3::Dot(&v1,&v1);
		vPos = lpFace->v[2] + v1*t;
		v1 = *lpQPos - vPos;
		if(t >= 0 && t <= 1)
		{
			if(CVector3::Dot(&v1,&v1) <= Qr*Qr)
			{
				f = Qr - v1.Length();
				v1.Normalize();
				*lpvOut += v1 * f;
				*lpQPos += v1 * f;	// 球の座標も変更
				bRet = TRUE;
			}
		}


		// 点との判定
		for(j=0;j<3;j++)
		{
			v1 = (*lpQPos) - lpFace->v[j];
			if(CVector3::Dot(&v1,&v1) <= Qr*Qr)
			{
				f = Qr - v1.Length();
				v1.Normalize();
				*lpvOut += v1 * f;
				*lpQPos += v1 * f;	// 球の座標も変更
				bRet = TRUE;
			}
		}


		faceQ.pop();
	}


	return bRet;
}
