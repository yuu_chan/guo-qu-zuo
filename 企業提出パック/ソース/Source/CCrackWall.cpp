#include "Game.h"


CCrackWall::CCrackWall()
	:
	m_spRigidBox(nullptr)
{

}


CCrackWall::~CCrackWall()
{
	m_spRigidBox = nullptr;
}


void CCrackWall::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	メッシュ読み込み
	m_Mesh = GET_XFILE_LOAD_MANAGER.Load("Data/XFile/Object/CrackWall.x", _lpD3DDevice);


	//------------------------------
	//	物理ワールドに剛体を登録する
	//------------------------------

	m_spRigidBox = make_shared<CBulletObj_Box>();

	CRigidSupervise::CreateRigidBox(m_spRigidBox, m_Mesh, m_vPos, m_Id);

	m_spRigidBox->SetUserPointer(this);

	//	質量は0(固定物)にしておく
	m_spRigidBox->SetMass(0.0f);
}


void CCrackWall::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{

	//----------------
	//	行列を更新する
	//----------------

	m_mWorld.CreateScale(&m_vScalSize);

	m_mWorld.RotateY(m_vAng.y);

	m_mWorld.Move(&m_vPos);

	m_spRigidBox->SetMatrix(m_mWorld);

	//	座標だけ記憶させる
	m_vPos = m_mWorld.GetPos();


	//------------------------------
	//	指定されたステータスがきたら
	//	各自の処理を行う
	//------------------------------

	const int CREATE_NUM = 10;

	//	力強く衝突された場合指定した数分CBreakRockをインスタンス化する
	if (GET_FLAG_CALC.CheckFlag(m_State, CRACK::STATE::COLLISION))
	{

		/*CreateObject------------------------
		
		ラムダ初期化関数


		キャプチャ		:	全参照キャプチャ

		_pBreakRock		:	生成したいクラス

		戻り値			:	無し

		-------------------------------------*/
		function<void(shared_ptr<CBreakRock> )> CreateObject = 
		[&]
		(shared_ptr<CBreakRock> _pBreakRock)
		{

			//	きちんとインスタンス化されているかをチェック
			//	されていなければメッセージを出す
			if (_pBreakRock == nullptr)
			{
				MessageBox(NULL, "オブジェクトがインスタンス化されていません", "警告", MB_OK);
				return;
			}


			//------------------------------------------------
			//	拡大座標や回転軸はランダムでもいいかもしれない
			//------------------------------------------------


			//	マネージャーへの登録IDをセットする
			_pBreakRock->SetId(GET_WORLD.GetGameWorld()->GetEnd());

			//	拡大座標をセット
			_pBreakRock->SetScaling(CVector3(1.0f, 1.0f, 1.0f));

			//	初期座標をセット
			_pBreakRock->SetPosition(m_vPos);

			//	回転量をセット
			_pBreakRock->SetAngle(CVector3(0.0f, 0.0f, 0.0f));

			//	アイテムIDを登録する
			_pBreakRock->SetItemId(OBJECT_LIST::ID::BREAK_ROCK);

			//	フィルターを登録する
			_pBreakRock->SetFilter(FILTER::NAME::COLLISION_ON);

			//	オブジェクトの重量を設定
			_pBreakRock->SetObjectWeight(0.3f);

			//	シェーダーのポインターをコピーさせておく
			//_pBreakRock->SetShader(m_pShader);

			//	初期ステータスを変更
			_pBreakRock->SetState(1);

			//	登録タグ
			//_pBreakRock->SetRegisterTag("");

			//	オブジェクトを初期化する
			_pBreakRock->Init(_lpD3DDevice);


			//	後尾のIDをカウントアップさせる(Begin Endがあるのでそっちに変える)
			GET_WORLD.GetGameWorld()->SetEnd(GET_WORLD.GetGameWorld()->GetEnd() + 1);

		};


		for (int i = 0; i < CREATE_NUM; i++)
		{
			//	CBreakRockをインスタンス化
			shared_ptr<CBreakRock> pBreakRock = GET_WORLD.GetGameWorld()->GetObjectManager()->CreateObjectTask<CBreakRock>();

			//	初期化処理を行う
			CreateObject(pBreakRock);
		}


		//----------------------------------------------
		//	初期化処理が終了すれば次のステータスへ移行し
		//	自身を破棄する
		//----------------------------------------------

		m_State = GET_FLAG_CALC.SetData((m_State & 0x0000), CRACK::STATE::END, LOGICAL::OPERATION_MODE::OR);

	}
	else if (GET_FLAG_CALC.CheckFlag(m_State, CRACK::STATE::END))
	{
		/*
		GET_WORLD.GetGameWorld()->GetObjectManager()->ItObjectKill(m_Id);

		GET_WORLD.GetGameWorld()->DeleteEndId();
		*/

		//	オブジェクトを破棄する命令をだす
		//m_EnableDestruction = true;


		//	いったん場外へ
		const float Y = -1000.0f;

		m_vPos.y = Y;

	}




}


void CCrackWall::Release()
{
}