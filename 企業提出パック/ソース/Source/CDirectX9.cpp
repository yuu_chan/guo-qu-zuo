#include "CDirectX9.h"

//================================================================================
//
// Direct3Dを初期化する。
//	引数：_hWnd･･･Direct3Dで使用するウィンドウ
//	　　　_Width･･･幅
//　　　　_Height･･･高さ
//　　　　_Format･･･色モード
//　　　　_FullScreen･･･フルスクリーンフラグ
//　戻り値：成功･･･TRUE
//　　　　　失敗･･･FALSE
//	跡できれいにする
//================================================================================
BOOL CDirectX9Helper::Init(
	HWND		_hWnd,
	int			_Width,
	int			_Height,
	D3DFORMAT	_Format,
	BOOL		_FullScreen)
{
	//	メッセージ取得用変数
	int Message;

	//	メッセージを返させる
	Message = MessageBox(APP.GetHwnd(), "フルスクリーンにしますか？", "", MB_YESNO);

	//	YESが帰ってきたら
	if (Message == IDYES)
	{
		//	フルスクリーン
		_FullScreen = 1;
	}
	//	NOが帰ってきたら
	else if (Message == IDNO)
	{
		//	デフォルトの大きさ
		_FullScreen = 0;
	}

	//=======================================================
	// D3Dオブジェクト作成
	//=======================================================
	if (NULL == (m_lpD3D = Direct3DCreate9(D3D_SDK_VERSION)))
		return FALSE;

	//=======================================================
	// 現在の画面モードを取得
	//=======================================================
	D3DDISPLAYMODE d3ddm;
	if (FAILED(m_lpD3D->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &d3ddm))) return FALSE;

	//=======================================================
	// 設定
	//=======================================================
	ZeroMemory(&m_d3dpp, sizeof(m_d3dpp));
	m_d3dpp.BackBufferCount = 2;
	if (_FullScreen)
	{
		m_d3dpp.Windowed = FALSE;
		m_d3dpp.BackBufferWidth = _Width;
		m_d3dpp.BackBufferHeight = _Height;
		m_d3dpp.BackBufferFormat = _Format;
	}
	else
	{
		m_d3dpp.Windowed = TRUE;
		m_d3dpp.BackBufferWidth = 0;
		m_d3dpp.BackBufferHeight = 0;
		m_d3dpp.BackBufferFormat = d3ddm.Format;
	}
	m_d3dpp.MultiSampleType = D3DMULTISAMPLE_NONE;

	// スワップ エフェクト
	m_d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	//m_d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;	// モニタの垂直同期を待たない
	m_d3dpp.hDeviceWindow = _hWnd;
	
	// Z バッファの自動作成
	m_d3dpp.EnableAutoDepthStencil = TRUE;
	m_d3dpp.AutoDepthStencilFormat = D3DFMT_D24S8;	// 24bitZバッファ + 8bitステンシルバッファ

	//デバイスの作成 - T&L HAL
	if (FAILED(m_lpD3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, _hWnd, D3DCREATE_HARDWARE_VERTEXPROCESSING | D3DCREATE_MULTITHREADED | D3DCREATE_FPU_PRESERVE, &m_d3dpp, &m_lpD3DDev)))
	{
		//失敗したのでHALで試行
		if (FAILED(m_lpD3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, _hWnd, D3DCREATE_SOFTWARE_VERTEXPROCESSING | D3DCREATE_MULTITHREADED | D3DCREATE_FPU_PRESERVE, &m_d3dpp, &m_lpD3DDev)))
		{
			//失敗したのでREFで試行
			if (FAILED(m_lpD3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_REF, _hWnd, D3DCREATE_SOFTWARE_VERTEXPROCESSING | D3DCREATE_MULTITHREADED | D3DCREATE_FPU_PRESERVE, &m_d3dpp, &m_lpD3DDev)))
			{
				//結局失敗
				MessageBox(_hWnd, "Direct3Dの初期化に失敗", "Direct3D初期化", MB_OK | MB_ICONSTOP);
				//終了する
				return FALSE;
			}
		}
	}

	//=======================================================
	// Caps取得
	//=======================================================
	m_lpD3DDev->GetDeviceCaps(&m_Caps);

	//=======================================================
	// D3DXSPRITE作成
	//=======================================================
	D3DXCreateSprite(m_lpD3DDev, &m_lpSprite);

	//=======================================================
	// D3DXFONT作成
	//=======================================================
	D3DXCreateFont(m_lpD3DDev, 12, 0, 0, 1, FALSE, DEFAULT_CHARSET,
		OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE,
		"ＭＳ Ｐゴシック", &m_lpFont);
	m_lpFont->GetDesc(&m_FontDesc);

	//=======================================================
	// バックバッファとZバッファ取得
	//=======================================================
	m_lpD3DDev->GetRenderTarget(0, &m_OrgRenderTarget);
	m_lpD3DDev->GetDepthStencilSurface(&m_OrgDepthStencil);

	// 頂点フォーマットの設定
	m_lpD3DDev->SetFVF(FVF_TLVERTEX);

	timeBeginPeriod(1);

	return TRUE;
}

void CDirectX9Helper::Release()
{
	SafeRelease(m_OrgRenderTarget);
	SafeRelease(m_OrgDepthStencil);

	// D3DXSPRITE解放
	SafeRelease(m_lpSprite);
	// D3DXFONT解放
	SafeRelease(m_lpFont);

	SafeRelease(m_lpD3DDev);
	SafeRelease(m_lpD3D);
}

//================================================================================
//
// 基本的かなぁと思うレンダーステートの設定
//  ※詳細※
//    ライトON、ライトの設定は適当、法線正規化、Z判定有効、Z書き込み有効、半透明モード、
//    右回りカリング、シングルテクスチャ、フォグOFF、フォグ設定は頂点線形フォグ設定・範囲は1〜10000、
//    アルファブレンドON、ステンシルバッファON、グローシェーディング、テクスチャアドレッシングモードはWrap
//================================================================================
void CDirectX9Helper::SetState()
{
	// DirectGraphicsの詳細設定
	m_lpD3DDev->SetRenderState(D3DRS_ZENABLE, TRUE);				// Zバッファ有効
	m_lpD3DDev->SetRenderState(D3DRS_ZWRITEENABLE, TRUE);			// Zバッファに書き込み有効
	m_lpD3DDev->SetRenderState(D3DRS_STENCILENABLE, TRUE);

	m_lpD3DDev->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);		// アルファブレンド有効
	Blend_Alpha();													// 半透明モード

	m_lpD3DDev->SetRenderState(D3DRS_NORMALIZENORMALS, TRUE);		//頂点法線の自動正規化

	m_lpD3DDev->SetRenderState(D3DRS_SHADEMODE, D3DSHADE_GOURAUD);	// グローシェーディング
	m_lpD3DDev->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);		// カリング CCW=右回り=表面のみ描画

	SeparateAlphaBlendEnable(TRUE);									// α個別計算

	// テクスチャのα値のブレンディング方法の設定
	// テクスチャα合成処理の方法
	m_lpD3DDev->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);    // 以下の引数の成分を乗算する
	m_lpD3DDev->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);    // テクスチャの色を使用
	m_lpD3DDev->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE);    // 頂点の色を使用
	// 色を合成する方法
	m_lpD3DDev->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_MODULATE);    // 以下の引数の成分を乗算する
	m_lpD3DDev->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);    // テクスチャの色を使用
	m_lpD3DDev->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_DIFFUSE);    // 頂点の色を使用

	// テクスチャの設定
	SetTextureFilter_Linear(0);

	m_lpD3DDev->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);	// ラップモード
	m_lpD3DDev->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);	// ラップモード

	// ミップマップ詳細レベルを設定する。
	SetMipmapBias(-1.0f);

	// ディレクショナルライト設定
	SetDirectionalLight(0,
		&D3DXVECTOR3(0.0f, -1.0f, 0.0f),
		&D3DXCOLOR(1.0f, 1.0f, 1.0f, 1),
		&D3DXCOLOR(0.0f, 0.0f, 0.0f, 1),
		&D3DXCOLOR(1.0f, 1.0f, 1.0f, 1));

	// スペキュラ有効
	m_lpD3DDev->SetRenderState(D3DRS_SPECULARENABLE, TRUE);

	// フォグ(霧効果)
	m_lpD3DDev->SetRenderState(D3DRS_FOGENABLE, FALSE); // FogはOFF
}

void CDirectX9Helper::DefaultLight(D3DXVECTOR3 _LightDir)
{
	GET_DIRECT_HELPER.SetDirectionalLight(
		0,
		&_LightDir,				// ライトの方向
		&D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f),	// 基本色(ディフーズ RGBA)
		&D3DXCOLOR(0.3f, 0.3f, 0.3f, 0.0f),	// 環境色(アンビエント RGBA)
		&D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f)	// 反射色(スペキュラ RGBA)
		);
}

// LIGHT
D3DLIGHT9 CDirectX9Helper::GetLight(UINT LightNo)
{
	D3DLIGHT9 light;
	m_lpD3DDev->GetLight(LightNo, &light);	// LightNo番目にこのライトを設定
	return light;
}

void CDirectX9Helper::SetDirectionalLight(int LightNo, D3DXVECTOR3 *vWay, D3DXCOLOR *Dif, D3DXCOLOR *Amb, D3DXCOLOR *Spe)
{
	// ディレクショナルライト設定
	D3DLIGHT9 light;
	ZeroMemory(&light, sizeof(D3DLIGHT9));
	light.Type = D3DLIGHT_DIRECTIONAL;
	light.Diffuse = *Dif;	// ディヒューズ
	light.Ambient = *Amb;	// アンビエント
	light.Specular = *Spe;	// スペキュラ
	D3DXVec3Normalize((D3DXVECTOR3*)&light.Direction, vWay);

	m_lpD3DDev->SetLight(LightNo, &light);	// LightNo番目にこのライトを設定
	m_lpD3DDev->LightEnable(LightNo, TRUE);	// LightNo番目のライトを有効
}

void CDirectX9Helper::DrawSprite(CTexture* tex, D3DCOLOR color, D3DXMATRIX* mat)
{
	//	テクスチャーの情報が無い場合は何もせずに返す
	if (tex == NULL)
	{
		return;
	}

	//	行列を指定した場合は、行列をセットする
	if (mat)
	{
		m_lpSprite->SetTransform(mat);
	}

	m_lpSprite->Draw(tex->GetTex(), tex->GetRect(), NULL, NULL, color);
}

void CDirectX9Helper::DrawSprite(CTexture* tex, int x, int y, int w, int h, D3DCOLOR color)
{
	if (tex == NULL)return;
	CMatrix m;
	m.CreateScale((float)w / tex->GetInfo()->Width,
		(float)h / tex->GetInfo()->Height,
		0
		);
	m.Move((float)x, (float)y, 0.0f);

	m_lpSprite->SetTransform(&m);
	m_lpSprite->Draw(tex->GetTex(), tex->GetRect(), NULL, NULL, color);
}

void CDirectX9Helper::DrawSprite(CTexture* _pTex, RECT _Rect, D3DXMATRIX* _pMat, D3DCOLOR _Color)
{
	//	テクスチャーの情報が無い場合は何もせずに返す
	if (_pTex == NULL)
	{
		return;
	}

	//	行列を指定した場合は、行列をセットする
	if (_pMat)
	{
		m_lpSprite->SetTransform(_pMat);
	}

	m_lpSprite->Draw(_pTex->GetTex(), &_Rect, NULL, NULL, _Color);
}

void CDirectX9Helper::DrawFont(const char* text, D3DCOLOR color, D3DXMATRIX* mat)
{
	if (text == NULL)return;

	if (mat)m_lpSprite->SetTransform(mat);
	RECT rc = { 0, 0, 0, 0 };
	m_lpFont->DrawText(m_lpSprite, text, -1, &rc, DT_CALCRECT, color);
	m_lpFont->DrawText(m_lpSprite, text, -1, &rc, DT_LEFT, color);
}


//	アニメーションとかここでやるといいかもしれない
void CDirectX9Helper::DrawQuad(float x, float y, float w, float h, float tuCnt, float tvCnt, D3DCOLOR color)
{
	// 座標変換済み座標とテクスチャ座標
	struct VERTEX{
		float x, y, z, rhw;  // 位置
		float tu, tv;	// テクスチャ
	};

	VERTEX v[4];
	v[0].x = x;		v[0].y = y;
	v[1].x = x + w;	v[1].y = y;
	v[2].x = x;		v[2].y = y + h;
	v[3].x = x + w;	v[3].y = y + h;

	v[0].tu = 0;
	v[0].tv = 0;
	v[1].tu = tuCnt;
	v[1].tv = 0;
	v[2].tu = 0;
	v[2].tv = tvCnt;
	v[3].tu = tuCnt;
	v[3].tv = tvCnt;

	// RHW, 頂点色の設定
	for (int i = 0; i<4; i++) {
		v[i].z = 0;
		v[i].rhw = 1;
	}

	m_lpD3DDev->SetFVF(D3DFVF_XYZRHW | D3DFVF_TEX1);

	m_lpD3DDev->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, v, sizeof(VERTEX));

}

void CDirectX9Helper::DrawQuad3D(float LSize, float RSize, float TSize, float BSize, float tuCnt, float tvCnt, D3DXCOLOR color)
{
	// 座標とテクスチャ座標
	struct VERTEX{
		float x, y, z;  // 位置
		float tu, tv;	// テクスチャ
	};

	struct VERTEX_t
	{
		D3DXVECTOR3 Pos;
		D3DCOLOR    Color;
		D3DXVECTOR2 Tex;
	};



	VERTEX_t v[4];
	//v[0].x = -LSize;	v[0].y = -BSize;	// 左下
	//v[1].x = RSize;		v[1].y = -BSize;	// 右下
	//v[2].x = -LSize;	v[2].y = TSize;		// 左上
	//v[3].x = RSize;		v[3].y = TSize;		// 右上

	//v[0].tu = tuCnt;
	//v[0].tv = tvCnt;
	//v[1].tu = 0;
	//v[1].tv = tvCnt;
	//v[2].tu = tuCnt;
	//v[2].tv = 0;
	//v[3].tu = 0;
	//v[3].tv = 0;


	//v[0].Pos.x = -LSize; v[0].Pos.y = TSize;
	//v[1].Pos.x = RSize;  v[1].Pos.y = TSize;
	//v[2].Pos.x = RSize;  v[2].Pos.y = -BSize;
	//v[3].Pos.x = -LSize; v[3].Pos.y = -BSize;

	v[0].Pos.x = -100; v[0].Pos.y = -100;
	v[1].Pos.x = 100;  v[1].Pos.y = -100;
	v[2].Pos.x = 100;  v[2].Pos.y = 100;
	v[3].Pos.x = -100; v[3].Pos.y = 100;

	v[0].Tex.x = 0; v[0].Tex.y = 0;
	v[1].Tex.x = 1; v[1].Tex.y = 0;
	v[2].Tex.x = 1; v[2].Tex.y = 1;
	v[3].Tex.x = 0; v[3].Tex.y = 1;

	v[0].Color = D3DCOLOR_XRGB(255, 255, 255);
	v[1].Color = D3DCOLOR_XRGB(255, 255, 255);
	v[2].Color = D3DCOLOR_XRGB(255, 255, 255);
	v[3].Color = D3DCOLOR_XRGB(255, 255, 255);



	// RHW, 頂点色の設定
	for (int i = 0; i<4; i++) {
		v[i].Pos.z = 0;
	}

	//m_lpD3DDev->SetFVF(D3DFVF_XYZ | D3DFVF_TEX1);
	m_lpD3DDev->SetFVF(GET_DIRECT_HELPER.FVF_TLVERTEX);
	m_lpD3DDev->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, v, sizeof(VERTEX_t));

}
