#include "CMesh.h"

#include "CTextureBase.h"
#include "CEffectManager.h"


CEffectManager::CEffectManager()
{

}


CEffectManager::~CEffectManager()
{
	Release();
}


void CEffectManager::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
}


void CEffectManager::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	最初の位置を指す
	auto itEffContainer = m_listEffContainer.begin();

	while (itEffContainer != m_listEffContainer.end())
	{
		(*itEffContainer)->Update();
		++itEffContainer;
	}

}


void CEffectManager::Draw(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	auto itEffContainer = m_listEffContainer.begin();

	while (itEffContainer != m_listEffContainer.end())
	{
		(*itEffContainer)->Draw(_lpD3DDevice);
		++itEffContainer;
	}

}


void CEffectManager::Release()
{
	auto itEffContainer = m_listEffContainer.begin();

	while (itEffContainer != m_listEffContainer.end())
	{
		//	イテレータの中にあるvectorの中身を消す
		delete (*itEffContainer);
		//	消して、位置をつめる
		itEffContainer = m_listEffContainer.erase(itEffContainer);
	}
}


void CEffectManager::PushEffectManager(CTextureBase* _Object)
{
	if (_Object != NULL)
	{
		m_listEffContainer.push_back(_Object);
	}
}


CTextureBase* CEffectManager::GetItEffect(int _Id)
{
	auto itEffContainer = m_listEffContainer.begin();

	while (itEffContainer != m_listEffContainer.end())
	{
		if ((*itEffContainer)->GetListId() == _Id)
			return (*itEffContainer);

		++itEffContainer;
	}

	return nullptr;
}


void CEffectManager::SetShader(CShaderManager* _pShader)
{
	if (_pShader == nullptr)
	{
		MessageBox(NULL, "シェーダーワールドがセットされていません。このままゲームを終了します", "警告", MB_OK);
		APP.m_bEndFlag = true;
		return;
	}


	//	リストの数を変数に代入
	auto itTexContainer = m_listEffContainer.begin();

	//	リストの後尾に達していなければ
	while (itTexContainer != m_listEffContainer.end())
	{
		//	指定したオブジェクトにシェーダーワールドをセット
		(*itTexContainer)->SetBokashiShader(_pShader);

		//	次のリストへ
		++itTexContainer;
	}
}