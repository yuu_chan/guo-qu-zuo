#include "CMesh.h"
#include "Texture.h"
#include "CFileLoader.h"


CEffectWorld::CEffectWorld()
{

}


CEffectWorld::~CEffectWorld()
{
}


void CEffectWorld::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//--------------------------------------
	//	エフェクトマネージャをインスタンス化
	//--------------------------------------

	m_spEffectBaseManager = make_shared<CEffectManager>(); 


	//--------
	//	初期化
	//--------

	m_spEffectBaseManager->Init(_lpD3DDevice);


	//----------------------------------------
	//	ファイルを読み込んで、初期化処理を行う
	//----------------------------------------

	unique_ptr<CFileLoader> upLoader = make_unique<CFileLoader>();

	//	テキストファイルを読み込んで、オブジェクトをインスタンス後プッシュさせる
	upLoader->TextureTxtLoad("Data/Txt/Stage1Effect.txt", _lpD3DDevice, m_spEffectBaseManager);

	//	加算カウンターを配列の最後の要素としてコピーしておく
	auto LastId = upLoader->GetAddTextureCnt();

	//	オブジェクトに配列の要素をセットする
	for (int i = 0; i < LastId; i++)
	{
		//	先頭IDを登録
		m_spEffectBaseManager->GetItEffect(i)->SetFirstId(0);

		//	後尾IDを登録
		m_spEffectBaseManager->GetItEffect(i)->SetLastId(LastId);
	}

}


void CEffectWorld::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	m_spEffectBaseManager->Update(_lpD3DDevice);
}


void CEffectWorld::Draw(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	m_spEffectBaseManager->Draw(_lpD3DDevice);
}


void CEffectWorld::Release()
{
}
