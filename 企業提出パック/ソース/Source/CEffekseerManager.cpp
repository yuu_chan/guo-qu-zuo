//#include "CEffekseerManager.h"
//
//
//const int CEffekSeerManager::SPRITE_MAX = 255;
//
//
//void CEffekSeerManager::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
//{
//
//	//--------------------------------------
//	//	描画に必要な各データをインスタンス化
//	//--------------------------------------
//
//	m_pRender  = ::EffekseerRendererDX9::Renderer::Create(_lpD3DDevice, 255);
//
//	m_pManager = ::Effekseer::Manager::Create(SPRITE_MAX);
//
//
//	//--------------
//	//	設定の初期化
//	//--------------
//	// 描画方法の指定、独自に拡張しない限り定形文です。
//	m_pManager->SetSpriteRenderer(m_pRender->CreateSpriteRenderer());
//	m_pManager->SetRibbonRenderer(m_pRender->CreateRibbonRenderer());
//	m_pManager->SetRingRenderer(m_pRender->CreateRingRenderer());
//
//	// テクスチャ画像の読込方法の指定(パッケージ等から読み込む場合拡張する必要があります。)
//	m_pManager->SetTextureLoader(m_pRender->CreateTextureLoader());
//}
//
//
//void CEffekSeerManager::Draw(LPDIRECT3DDEVICE9 _lpD3DDevice)
//{
//	m_pRender->BeginRendering();
//
//	m_pManager->Draw();
//
//	m_pRender->EndRendering();
//}
//
//
//void CEffekSeerManager::Release()
//{
//	// エフェクト管理用インスタンスを破棄
//	m_pManager->Destroy();
//	
//	// 描画用インスタンスを破棄
//	m_pRender->Destory();
//
//}