#include "CMesh.h"
#include "CTextureBase.h"
#include "CEventFrame.h"
#include "CEffectManager.h"
#include "CEffectWorld.h"

const int CEventFrame::TEXTURE_SIZE = 3;

CEventFrame::CEventFrame()
{

}

CEventFrame::~CEventFrame()
{
	//	テクスチャ解放
	//SafeRelease(m_pTexture);
	m_pTexture->Release();
	m_pTexture = nullptr;
}

void CEventFrame::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	テクスチャを読み込む
	//m_pTexture      = GET_TEXTURE_LOAD_MANAGER.Load("Data/Graphics/Effect/black.bmp", _lpD3DDevice);
	m_pTexture->LoadTexture("Data/Graphics/Effect/black.bmp");
	m_pBlendTexture = GET_TEXTURE_LOAD_MANAGER.Load("Data/Graphics/Effect/blackBlend.bmp", _lpD3DDevice);
	

	m_Rect = { 1, 1, 2, 2 };
}

void CEventFrame::Update()
{
	//	拡大サイズをセット
	m_mWorld.CreateScale(&m_vScaleSize);

	//	座標をセット
	m_mWorld.Move(&m_vPos);

}

void CEventFrame::Draw(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	表示開始
	GET_DIRECT_HELPER.BeginSprite();

	GET_DIRECT_HELPER.Blend_Alpha();
	GET_DIRECT_HELPER.AlphaBlendEnable(false);

	//	描画1
	GET_DIRECT_HELPER.DrawSprite(m_pTexture.get(), D3DCOLOR_ARGB(255, 0, 0, 0), &m_mWorld);

	GET_DIRECT_HELPER.Blend_Alpha();
	GET_DIRECT_HELPER.AlphaBlendEnable(true);

	//	表示終了
	GET_DIRECT_HELPER.EndSprite();
}

void CEventFrame::Release()
{
	
}