#include "Shader.h"
#include "Texture.h"
#include "CFade.h"

CFade::CFade()
	:
	m_EnableFade(false)
	,
	m_AbleStart(false)
	,
	m_Load(false)
	,
	m_AbleEnd(false)
	,
	m_Alpha(0)
	,
	m_Step(0)
	,
	m_MemoryAlpha(0)
{
}


CFade::~CFade()
{
	SAFE_RELEASE(m_pTexture);
}


void CFade::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//------------
	//	画像サイズ
	//------------

	const CVector3  DEFAULT_SIZE = CVector3(640.0f, 480.0f, 0.0f);
	
	const auto		TEXTURE_SIZE = 1;

	
	//----------
	//	読み込み
	//----------

	m_pTexture->LoadTextureEx(
		"Data/Graphics/Effect/Fade.png",
		TEXTURE_SIZE,
		TEXTURE_SIZE,
		1,
		0,
		D3DFMT_UNKNOWN,
		D3DPOOL_MANAGED,
		D3DX_FILTER_NONE,
		D3DX_DEFAULT,
		NULL,					//	アルファ値指定無し
		NULL
		);


	//	万が一テクスチャが読み込めていなかった場合はエラーメッセージを出しておく
	if (m_pTexture->GetTex() == nullptr)
	{
		MessageBox(NULL, "テクスチャーが正しく読み込めていません", "警告", MB_OK);
	}

	//	行列をセット
	m_mWorld.CreateScale(&DEFAULT_SIZE);


	//	α値を設定
	const int ZERO = 0;

	m_MemoryAlpha = ZERO;

	m_Alpha = m_MemoryAlpha;
}


void CFade::Update()
{

	//------------
	//	画像サイズ
	//------------

	const CVector3 DEFAULT_SIZE = CVector3(640.0f, 480.0f, 0.0f);

	//------------
	//	α値用定数
	//------------

	const int ALPHA_UPDATE	= 2;
	const int ALPHA_MAX		= 255;
	const int ALPHA_MIN		= 0;


	//------------------------------------------
	//	フェードインフェードアウト処理を行う場合
	//------------------------------------------

	if (m_EnableFade == true)
	{
		//	α値を設定
		m_Alpha		= m_MemoryAlpha;

		//	次に進めないようにする
		m_AbleStart = false;

		//	フェードインを行う
		m_Step		= FADE::STEP::fIN;

		//	フェードインフェードアウトの処理を行う
		m_EnableFade = false;

		//	ロードが終わっていると判定させる
		m_Load		 = false;

		//	終了させない
		m_AbleEnd	 = false;
	}


	//--------------------------
	//	ステップ毎に処理を替える
	//--------------------------

	//	フェードインステップ
	if (m_Step == FADE::STEP::fIN)
	{
		//	α値を加算
		m_Alpha += ALPHA_UPDATE;
		m_MemoryAlpha = m_Alpha;

		//	限度を超えれば
		if (m_Alpha >= ALPHA_MAX)
		{
			//	値はそのまま
			m_Alpha = ALPHA_MAX;

			m_AbleEnd = true;

			//	読み込みが終われば
			if (m_Load == true)
			{
				//	次のステップへ
				m_Step = FADE::STEP::fOUT;
			}
		}
	}

	//	フェードアウトステップ
	if (m_Step == FADE::STEP::fOUT)
	{
		//	α値を減算
		m_Alpha -= ALPHA_UPDATE;
		m_MemoryAlpha = m_Alpha;

		m_AbleEnd = false;

		//	限度を下回れば
		if (m_Alpha < ALPHA_MIN)
		{
			//	値はそのまま
			m_Alpha = ALPHA_MIN;

			//	ステップは終了
			m_Step = FADE::STEP::END;
		}
	}

	//	終了
	if (m_Step == FADE::STEP::END)
	{
		//	次に進めれるように連絡をする
		m_AbleStart = true;
	}

	//	行列の更新
	m_mWorld.CreateScale(&DEFAULT_SIZE);
	m_mWorld.Move_Local(&CVector3(0.0f, 0.0f, 0.0f));
}

//	描画
void CFade::Draw(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	const int DEFAULT_COLOR = 255;

	GET_DIRECT_HELPER.BeginSprite();

	GET_DIRECT_HELPER.DrawSprite(
		m_pTexture.get(), 
		D3DCOLOR_ARGB(m_Alpha, DEFAULT_COLOR, DEFAULT_COLOR, DEFAULT_COLOR), 
		&m_mWorld);

	GET_DIRECT_HELPER.EndSprite();
}

//	解放
void CFade::Release()
{
}
