#include "Game.h"
#include "Texture.h"
#include "CFileLoader.h"


CFileLoader::CFileLoader()
{
	m_AddObjectCnt  = 0;
	m_AddTextureCnt = 0;
	m_AddGimmick	= 0;
}


CFileLoader::~CFileLoader()
{

}


/*trim------------------------------------------->

	指定された文字列を切り取り
	切り取った文字列の前後のいらない部分を削り取る

	_Str				: 切り取りたい文字列の元
	_TrimCharacterList	: いらない文字列

	return : string型

<-----------------------------------------------*/
static string trim(const string& _Str, const char* _TrimCharacterList = " \t\v\r\n")
{
	string result;

	// 左側からトリムする文字以外が見つかる位置を検索します。
	string::size_type left = _Str.find_first_not_of(_TrimCharacterList);

	if (left != string::npos)
	{
		// 左側からトリムする文字以外が見つかった場合は、同じように右側からも検索します。
		string::size_type right = _Str.find_last_not_of(_TrimCharacterList);

		// 戻り値を決定します。ここでは右側から検索しても、トリムする文字以外が必ず存在するので判定不要です。
		result = _Str.substr(left, right - left + 1);
	}

	return result;
}


vector<string> CFileLoader::GetSplit(
	string _Str,
	string _Split)
{
	//	文字を格納するための変数
	vector<string> vecRet;
	
	
	//	検索したい候補のうちのいずれかがヒットすれば
	while (_Str.find_first_of(_Split) != string::npos)
	{

		//	切り取りたい文字列を設定
		_Str = trim(_Str, " ");
		_Str = trim(_Str, "\t\v\r\n");

		//	切り取りたい文字数を取得
		int Cp = _Str.find_first_of(_Split);

		//	文字数が一つのみだった場合
		if (Cp <= 0)
		{
			Cp += 1;
		}

		//	先頭空文字が格納されないように
		if (Cp > 0)
		{
			
			//	格納
			vecRet.push_back(_Str.substr(0, Cp));

		}

		//	区切り文字が二文字以上に対応
		_Str = _Str.substr(Cp);
	}

	//	リストを返す
	return vecRet;
}


void CFileLoader::Load(
	string						_FileName,
	LPDIRECT3DDEVICE9			_lpD3DDevice,
	shared_ptr<CObjectManager>	_pObjManager
	)
{
	ifstream ifs;

	//	読み込み
	ifs.open(_FileName.c_str());

	//	読み込み失敗
	if (!ifs)
	{
		//	メッセージをだす
		MessageBox(NULL, "テキストファイルが読み込めていません", "警告", MB_OK);

		//	何もせず返す
		return;
	}


	for (;;)
	{
		string line;

		//	一行読み込む
		getline(ifs, line);

		//	読み込み終了
		if (ifs.fail())
		{
			break;
		}


		/*---------------------------------------------------------------------

		区切りたい文字を設定、vector配列に格納(切り取りたい文字は複数指定可)

		例 
			string sp = "abc";

			sp = GetSplit(line, "ac");

			spには"abc"のうちaとcが切り取られたbのみが帰ってくる

		---------------------------------------------------------------------*/
		vector<string> sp = GetSplit(line, "(),");


		//	デフォルトステータス
		CVector3 vPos				= CVector3(0.0f, 0.0f, 0.0f);
		CVector3 vScale				= CVector3(1.0f, 1.0f, 1.0f);
		CVector3 vAngle				= CVector3(0.0f, 0.0f, 0.0f);
		UINT	 Filter				= FILTER::COLLISION_OFF;
		string	 Tag				= "";
		float	 ObjectWeight		= 0.0f;
		int		 ItemResilience		= 0;
		int		 ItemId				= -1;


		//	最初のアドレスをイテレータに格納
		auto FileIt = sp.begin();


		/*-----------------------------------------------------
		
		指定されたタグが見つかった場合、そのタグ内の数値を
		用意されたステータス用の変数に代入
		数値は+2の位置に指定
		
		
		タグ
		{
			Pos				:	座標ベクトル
			Scale			:	拡大ベクトル
			Angle			:	角度ベクトル
			ItemId			:	登録するアイテムID
			Filter			:	当たり判定フィルター
			ItemResilience	:	アイテムの回復量
			ObjectWeight	:	オブジェクトの重さ設定
		}
		
		-----------------------------------------------------*/

		while (FileIt != sp.end())
		{
			if ((*FileIt) == "Pos")
			{
				FileIt += 2;
				vPos.x = static_cast<float>(atof((*FileIt).c_str()));

				FileIt += 2;
				vPos.y = static_cast<float>(atof((*FileIt).c_str()));

				FileIt += 2;
				vPos.z = static_cast<float>(atof((*FileIt).c_str()));
			}
			else if ((*FileIt) == "Scale")
			{
				FileIt += 2;
				vScale.x = static_cast<float>(atof((*FileIt).c_str()));

				FileIt += 2;
				vScale.y = static_cast<float>(atof((*FileIt).c_str()));

				FileIt += 2;
				vScale.z = static_cast<float>(atof((*FileIt).c_str()));
			}
			else if ((*FileIt) == "Angle")
			{
				FileIt += 2;
				vAngle.x = static_cast<float>(atof((*FileIt).c_str()));

				FileIt += 2;
				vAngle.y = static_cast<float>(atof((*FileIt).c_str()));

				FileIt += 2;
				vAngle.z = static_cast<float>(atof((*FileIt).c_str()));
			}
			else if ((*FileIt) == "ItemId")
			{
				FileIt += 2;
				ItemId = atoi((*FileIt).c_str());
			}
			else if ((*FileIt) == "Filter")
			{
				FileIt += 2;
				Filter = atoi((*FileIt).c_str());
			}
			else if ((*FileIt) == "ItemResilience")
			{
				FileIt += 2;
				ItemResilience = atoi((*FileIt).c_str());
			}
			else if ((*FileIt) == "Weight")
			{
				FileIt += 2;
				ObjectWeight = static_cast<float>(atof((*FileIt).c_str()));
			}


			++FileIt;
		}


		//-----------------------------------------------------
		//
		//	Classタグの中を確認して、指定したClass名があれば
		//	インスタンス化してObjectManagerにプッシュする
		//	ステータスは先に設定値を代入する
		//
		//-----------------------------------------------------
		FileIt = sp.begin();

		while (FileIt != sp.end())
		{
			if ((*FileIt) == "CPlayer")
			{
				CreateGameObject<CPlayer>(_pObjManager, GET_DIRECT_HELPER.GetDev(), ItemId, ItemResilience, Filter, ObjectWeight, Tag, vPos, vScale, vAngle);
			}
			else if ((*FileIt) == "CStage1")
			{
				CreateGameObject<CStage1>(_pObjManager, GET_DIRECT_HELPER.GetDev(), ItemId, ItemResilience, Filter, ObjectWeight, Tag, vPos, vScale, vAngle);
			}
			else if ((*FileIt) == "CStageGround")
			{
				CreateGameObject<CStageGround>(_pObjManager, GET_DIRECT_HELPER.GetDev(), ItemId, ItemResilience, Filter, ObjectWeight, Tag, vPos, vScale, vAngle);
			}
			else if ((*FileIt) == "CBox")
			{
				CreateGameObject<CBox>(_pObjManager, GET_DIRECT_HELPER.GetDev(), ItemId, ItemResilience, Filter, ObjectWeight, Tag, vPos, vScale, vAngle);
			}
			else if ((*FileIt) == "CMushRoom")
			{
				CreateGameObject<CMushRoom>(_pObjManager, GET_DIRECT_HELPER.GetDev(), ItemId, ItemResilience, Filter, ObjectWeight, Tag, vPos, vScale, vAngle);
			}
			else if ((*FileIt) == "CMushRooms")
			{
				CreateGameObject<CMushRooms>(_pObjManager, GET_DIRECT_HELPER.GetDev(), ItemId, ItemResilience, Filter, ObjectWeight, Tag, vPos, vScale, vAngle);
			}
			else if ((*FileIt) == "CApple")
			{
				CreateGameObject<CApple>(_pObjManager, GET_DIRECT_HELPER.GetDev(), ItemId, ItemResilience, Filter, ObjectWeight, Tag, vPos, vScale, vAngle);
			}
			else if ((*FileIt) == "CStage1Collision")
			{
				CreateGameObject<CStage1Collision>(_pObjManager, GET_DIRECT_HELPER.GetDev(), ItemId, ItemResilience, Filter, ObjectWeight, Tag, vPos, vScale, vAngle);
			}
			else if ((*FileIt) == "CKabo")
			{
				CreateGameObject<CKabo>(_pObjManager, GET_DIRECT_HELPER.GetDev(), ItemId, ItemResilience, Filter, ObjectWeight, Tag, vPos, vScale, vAngle);
			}
			else if ((*FileIt) == "CRock")
			{
				CreateGameObject<CRock>(_pObjManager, GET_DIRECT_HELPER.GetDev(), ItemId, ItemResilience, Filter, ObjectWeight, Tag, vPos, vScale, vAngle);
			}
			else if ((*FileIt) == "CTreeNuts")
			{
				CreateGameObject<CTreeNuts>(_pObjManager, GET_DIRECT_HELPER.GetDev(), ItemId, ItemResilience, Filter, ObjectWeight, Tag, vPos, vScale, vAngle);
			}
			else if ((*FileIt) == "CSkyMesh")
			{
				CreateGameObject<CSkyMesh>(_pObjManager, GET_DIRECT_HELPER.GetDev(), ItemId, ItemResilience, Filter, ObjectWeight, Tag, vPos, vScale, vAngle);
			}
			else if ((*FileIt) == "CTreeMultiple")
			{
				CreateGameObject<CTreeMultiple>(_pObjManager, GET_DIRECT_HELPER.GetDev(), ItemId, ItemResilience, Filter, ObjectWeight, Tag, vPos, vScale, vAngle);
			}
			else if ((*FileIt) == "CLeaf")
			{
				CreateGameObject<CLeaf>(_pObjManager, GET_DIRECT_HELPER.GetDev(), ItemId, ItemResilience, Filter, ObjectWeight, Tag, vPos, vScale, vAngle);
			}
			else if ((*FileIt) == "CAppleTree")
			{
				CreateGameObject<CAppleTree>(_pObjManager, GET_DIRECT_HELPER.GetDev(), ItemId, ItemResilience, Filter, ObjectWeight, Tag, vPos, vScale, vAngle);
			}
			else if ((*FileIt) == "CHelp")
			{
				CreateGameObject<CHelp>(_pObjManager, GET_DIRECT_HELPER.GetDev(), ItemId, ItemResilience, Filter, ObjectWeight, Tag, vPos, vScale, vAngle);
			}
			else if ((*FileIt) == "CStrawberry")
			{
				CreateGameObject<CStrawberry>(_pObjManager, GET_DIRECT_HELPER.GetDev(), ItemId, ItemResilience, Filter, ObjectWeight, Tag, vPos, vScale, vAngle);
			}
			else if ((*FileIt) == "CStrawberryIvy")
			{
				CreateGameObject<CStrawberryIvy>(_pObjManager, GET_DIRECT_HELPER.GetDev(), ItemId, ItemResilience, Filter, ObjectWeight, Tag, vPos, vScale, vAngle);
			}
			else if ((*FileIt) == "CSpringFlower")
			{
				CreateGameObject<CSpringFlower>(_pObjManager, GET_DIRECT_HELPER.GetDev(), ItemId, ItemResilience, Filter, ObjectWeight, Tag, vPos, vScale, vAngle);
			}
			else if ((*FileIt) == "CCloud")
			{
				CreateGameObject<CCloud>(_pObjManager, GET_DIRECT_HELPER.GetDev(), ItemId, ItemResilience, Filter, ObjectWeight, Tag, vPos, vScale, vAngle);
			}
			else if ((*FileIt) == "CBranch")
			{
				CreateGameObject<CBranch>(_pObjManager, GET_DIRECT_HELPER.GetDev(), ItemId, ItemResilience, Filter, ObjectWeight, Tag, vPos, vScale, vAngle);
			}
			else if ((*FileIt) == "CAirFlower")
			{
				CreateGameObject<CAirFlower>(_pObjManager, GET_DIRECT_HELPER.GetDev(), ItemId, ItemResilience, Filter, ObjectWeight, Tag, vPos, vScale, vAngle);
			}
			else if ((*FileIt) == "CSwitch")
			{
				CreateGameObject<CSwitch>(_pObjManager, GET_DIRECT_HELPER.GetDev(), ItemId, ItemResilience, Filter, ObjectWeight, Tag, vPos, vScale, vAngle);
			}
			else if ((*FileIt) == "CWindZone")
			{
				CreateGameObject<CWindZone>(_pObjManager, GET_DIRECT_HELPER.GetDev(), ItemId, ItemResilience, Filter, ObjectWeight, Tag, vPos, vScale, vAngle);
			}
			else if ((*FileIt) == "CWindHole")
			{
				CreateGameObject<CWindHole>(_pObjManager, GET_DIRECT_HELPER.GetDev(), ItemId, ItemResilience, Filter, ObjectWeight, Tag, vPos, vScale, vAngle);
			}
			else if ((*FileIt) == "CRiver")
			{
				CreateGameObject<CRiver>(_pObjManager, GET_DIRECT_HELPER.GetDev(), ItemId, ItemResilience, Filter, ObjectWeight, Tag, vPos, vScale, vAngle);
			}
			else if ((*FileIt) == "CStone")
			{
				CreateGameObject<CStone>(_pObjManager, GET_DIRECT_HELPER.GetDev(), ItemId, ItemResilience, Filter, ObjectWeight, Tag, vPos, vScale, vAngle);
			}
			else if ((*FileIt) == "CCrackWall")
			{
				CreateGameObject<CCrackWall>(_pObjManager, GET_DIRECT_HELPER.GetDev(), ItemId, ItemResilience, Filter, ObjectWeight, Tag, vPos, vScale, vAngle);
			}
			else if ((*FileIt) == "CSeesaw")
			{
				CreateGameObject<CSeesaw>(_pObjManager, GET_DIRECT_HELPER.GetDev(), ItemId, ItemResilience, Filter, ObjectWeight, Tag, vPos, vScale, vAngle);
			}
			else if ((*FileIt) == "CSeesawFoundation")
			{
				CreateGameObject<CSeesawFoundation>(_pObjManager, GET_DIRECT_HELPER.GetDev(), ItemId, ItemResilience, Filter, ObjectWeight, Tag, vPos, vScale, vAngle);
			}
			else if ((*FileIt) == "CBridge")
			{
				CreateGameObject<CBridge>(_pObjManager, GET_DIRECT_HELPER.GetDev(), ItemId, ItemResilience, Filter, ObjectWeight, Tag, vPos, vScale, vAngle);
			}
			else if ((*FileIt) == "CLargeRock")
			{
				CreateGameObject<CLargeRock>(_pObjManager, GET_DIRECT_HELPER.GetDev(), ItemId, ItemResilience, Filter, ObjectWeight, Tag, vPos, vScale, vAngle);
			}


			++FileIt;
		}
	}
}


void CFileLoader::PointLoad(
	string				_FileName,
	LPDIRECT3DDEVICE9	_lpD3DDevice,
	vector<CVector3>*	_pvecPos
	)
{
	ifstream ifs;

	//	読み込み
	ifs.open(_FileName.c_str());

	//	読み込み失敗
	if (!ifs)
	{
		//	メッセージをだす
		MessageBox(NULL, "テキストファイルが読み込めていません", "警告", MB_OK);

		//	何もせず返す
		return;
	}


	//	データを格納する
	for (;;)
	{
		string line;

		//	一行読み込む
		getline(ifs, line);

		//	読み込み終了
		if (ifs.fail())
		{
			break;
		}

		//	区切りたい文字を設定、vector配列に格納
		vector<string> sp = GetSplit(line, "(),");

		//	最初のアドレスをイテレータに格納
		auto FileIt = sp.begin();

		while (FileIt != sp.end())
		{
			//	一時的に格納するための変数
			CVector3 vPointPos = CVector3(0.0f, 0.0f, 0.0f);

			//	座標1
			if ((*FileIt) == "Point")
			{

				FileIt += 2;
				vPointPos.x = static_cast<float>(atof((*FileIt).c_str()));

				FileIt += 2;
				vPointPos.y = static_cast<float>(atof((*FileIt).c_str()));

				FileIt += 2;
				vPointPos.z = static_cast<float>(atof((*FileIt).c_str()));

				//	座標を格納
				_pvecPos->push_back(vPointPos);
			}

			//	イテレータを次へ
			++FileIt;
		}

	}

}


void CFileLoader::TextureTxtLoad(
	string							_FileName,
	LPDIRECT3DDEVICE9				_lpD3DDevice,
	shared_ptr<CEffectManager>		_spObjManager
	)
{
	ifstream ifs;

	//	読み込み
	ifs.open(_FileName.c_str());

	//	読み込み失敗
	if (!ifs)
	{
		//	メッセージを出す
		MessageBox(NULL, "エフェクト用テキストファイルが読み込めていません", "警告", MB_OK);

		//	何もせずに返す
		return;
	}


	for (;;)
	{
		string line;

		//	1行読み込む
		getline(ifs, line);

		//	読み込みが終了したかをチェック
		if (ifs.fail())
		{
			break;
		}

		//	区切りたい文字を設定、vector配列に格納(切り取りたい文字は複数指定可)
		vector<string> sp = GetSplit(line, "(),");


		//	デフォルトステータス
		CVector3 vPos			= CVector3(0.0f, 0.0f, 0.0f);
		CVector3 vScale			= CVector3(1.0f, 1.0f, 1.0f);
		CVector3 vAngle			= CVector3(0.0f, 0.0f, 0.0f);
		CVector3 vTextureSize	= CVector3(1.0f, 1.0f, 0.0f);
		int		 EffectId		= 0;
		int		 DivisionNum	= 1;


		//	先頭アドレスをイテレータに格納
		auto FileIt = sp.begin();


		/*-----------------------------------------------------

		指定されたタグが見つかった場合、そのタグ内の数値を
		用意されたステータス用の変数に代入
		数値は+2の位置に指定


		タグ
		{
		Pos				:	座標ベクトル
		Scale			:	拡大ベクトル
		Angle			:	角度ベクトル
		TextureSize		:	テクスチャのサイズ(u,v,w)
		DivisionNum		:	テクスチャの分割枚数
		EffectId		:	登録するID
		}

		-----------------------------------------------------*/

		while (FileIt != sp.end())
		{
			if ((*FileIt) == "Pos")
			{
				FileIt += 2;
				vPos.x = static_cast<float>(atof((*FileIt).c_str()));

				FileIt += 2;
				vPos.y = static_cast<float>(atof((*FileIt).c_str()));

				FileIt += 2;
				vPos.z = static_cast<float>(atof((*FileIt).c_str()));
			}
			else if ((*FileIt) == "Scale")
			{
				FileIt += 2;
				vScale.x = static_cast<float>(atof((*FileIt).c_str()));

				FileIt += 2;
				vScale.y = static_cast<float>(atof((*FileIt).c_str()));

				FileIt += 2;
				vScale.z = static_cast<float>(atof((*FileIt).c_str()));
			}
			else if ((*FileIt) == "Angle")
			{
				FileIt += 2;
				vAngle.x = static_cast<float>(atof((*FileIt).c_str()));

				FileIt += 2;
				vAngle.y = static_cast<float>(atof((*FileIt).c_str()));

				FileIt += 2;
				vAngle.z = static_cast<float>(atof((*FileIt).c_str()));
			}
			else if ((*FileIt) == "TextureSize")
			{
				FileIt += 2;
				vTextureSize.x = static_cast<float>(atof((*FileIt).c_str()));

				FileIt += 2;
				vTextureSize.y = static_cast<float>(atof((*FileIt).c_str()));

				FileIt += 2;
				vTextureSize.z = static_cast<float>(atof((*FileIt).c_str()));
			}
			else if ((*FileIt) == "DivisionNum")
			{
				FileIt += 2;
				DivisionNum = atoi((*FileIt).c_str());
			}
			else if ((*FileIt) == "EffectId")
			{
				FileIt += 2;
				EffectId = atoi((*FileIt).c_str());
			}


			++FileIt;
		}


		//-----------------------------------------------------
		//
		//	Classタグの中を確認して、指定したClass名があれば
		//	インスタンス化してEffectManagerにプッシュする
		//	ステータスは先に設定値を代入する
		//
		//-----------------------------------------------------
		FileIt = sp.begin();

		while (FileIt != sp.end())
		{
			if ((*FileIt) == "CEventFrame")
			{
				CreateTextureObject<CEventFrame>(_spObjManager, GET_DIRECT_HELPER.GetDev(), vPos, vScale, vAngle, vTextureSize, EffectId, DivisionNum);
			}
			else if ((*FileIt) == "CFade")
			{
				//CreateTextureObject<CFade>(_spObjManager, GET_DIRECT_HELPER.GetDev(), vPos, vScale, vAngle, vTextureSize, DivisionNum);
			}
			else if ((*FileIt) == "CStomach")
			{
				CreateTextureObject<CStomach>(_spObjManager, GET_DIRECT_HELPER.GetDev(), vPos, vScale, vAngle, vTextureSize, EffectId, DivisionNum);
			}
			else if ((*FileIt) == "CStomachGauge")
			{
				CreateTextureObject<CStomachGauge>(_spObjManager, GET_DIRECT_HELPER.GetDev(), vPos, vScale, vAngle, vTextureSize, EffectId, DivisionNum);
			}
			else if ((*FileIt) == "CEffect")
			{
				CreateTextureObject<CAbsorption>(_spObjManager, GET_DIRECT_HELPER.GetDev(), vPos, vScale, vAngle, vTextureSize, EffectId, DivisionNum);
			}
			else if ((*FileIt) == "CShine")
			{
				CreateTextureObject<CShine>(_spObjManager, GET_DIRECT_HELPER.GetDev(), vPos, vScale, vAngle, vTextureSize, EffectId, DivisionNum);
			}


			++FileIt;
		}
	}
}