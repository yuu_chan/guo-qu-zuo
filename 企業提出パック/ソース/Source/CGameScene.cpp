
#include "Scene.h"
#include "CGameScene.h"
#include "Shader.h"
#include "Game.h"
#include "CTextureBase.h"
#include "CEffectManager.h"
#include "CEffectWorld.h"
#include "CGoal.h"
#include "CStart.h"
#include "CFade.h"


//------------------------->
//	使用するネームスペース
//<-------------------------

using namespace OBJECT_LIST;

using namespace TEX_LIST;


CGameScene::CGameScene()
:
m_spGameWorld(nullptr)
,
m_spEffectWorld(nullptr)
,
m_pAchievementWorld(nullptr)
,
m_pGoal(nullptr)
,
m_pStart(nullptr)
{
}


CGameScene::~CGameScene()
{
	Release();
}


void CGameScene::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{

	//--------------------------
	//	各ワールドインスタンス化
	//--------------------------

	m_spGameWorld		= make_shared<CGameWorld>();

	m_spEffectWorld		= make_shared <CEffectWorld>();

	//
	m_pAchievementWorld = MyNew CAchievementWorld();

	//	シェーダーをインスタンス化
	//m_pShader			= MyNew CShaderManager();

	//	ゴールインスタンス化
	m_pGoal				= MyNew CGoal();

	//	スタートインスタンス化
	m_pStart			= MyNew CStart();

	CSearch::Init();


	//------------------
	//	各ワールド初期化
	//------------------

	//	ゲームワールドを初期化
	m_spGameWorld->Init(_lpD3DDevice);

	//	エフェクトワールドを初期化
	m_spEffectWorld->Init(_lpD3DDevice);

	//	実績
	m_pAchievementWorld->Init(_lpD3DDevice);


	//-------------------------------------------------------------------------
	//	別ワールドにワールドのポインターを渡しておく
	//	後でCWorldManagerを作成して、ワールドマネージャーだけ渡すようにしておく
	//-------------------------------------------------------------------------

	GET_WORLD.SetGameWorld(m_spGameWorld);

	GET_WORLD.SetEffectWorld(m_spEffectWorld);

	GET_HELPER.SetAchievementWorld(m_pAchievementWorld);

	//m_spGameWorld->GetObjectManager()->SetShaderManager(m_pShader);
	//
	//m_spEffectWorld->GetEffectManager()->SetShader(m_pShader);

	GET_SHADER.SetGameWorld(m_spGameWorld);

	m_pGoal->SetEffectWorld(m_spEffectWorld);
	m_pGoal->SetGameWorld(m_spGameWorld);

	m_pStart->SetEffectWorld(m_spEffectWorld);
	m_pStart->SetGameWorld(m_spGameWorld);


	//------------------------
	//	マウスポインター初期化
	//------------------------
	
	GET_MOUSE.InitMousePointer();


	//----------------------
	//	それぞれの演出初期化
	//----------------------

	//	ゴール初期化
	m_pGoal->Init(_lpD3DDevice);

	//	スタート初期化
	m_pStart->Init(_lpD3DDevice);


	//	関数名を変えておく
	GET_FADE.SetLoadEnd(true);
}


void CGameScene::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//--------------------
	//	各オブジェクト更新
	//--------------------

	//-----------------------------------------------------------
	//	スタートイベントが行われているかどうかで処理を変える
	//	フラグはStartクラスが管理しているのでそれをゲットするだけ
	//	できれば、スタート演出が終わったらクラスを破棄しておく

	//
	//	今は、スタート演出時にキー操作ができてしまうので、
	//	スタート演出時はキー操作ができないように工夫する
	//-----------------------------------------------------------

	//	スタートイベントが行われていた場合
	if (m_pStart->GetEvent() == true
		//&&
		//CSceneManager::m_NowSelectStage == STAGE_SELECT::NAME::STAGE1
		)
	{
		//	スタートイベントを行う
		m_pStart->StartEvent(_lpD3DDevice);

		GET_CAMERA.SetCamera(true);

		CVector3 vAngle;

		//	アングルを修正
		vAngle = GET_MOUSE.GetAngle();

		vAngle.y = 90.0f;
		vAngle.x = 0.0f;

		GET_MOUSE.SetAngle(vAngle);
	}
	//	スタートイベントが終了していた場合
	else if (m_pStart->GetEvent() == false
		//||
		//CSceneManager::m_NowSelectStage ==STAGE_SELECT::NAME::STAGE2
		)
	{
		CMatrix View = GET_CAMERA.GetViewMatrix();

		View.CreateInverse();

		CVector3 LightPos = View.GetZAxis();
		
		GET_DIRECT_HELPER.SetDirectionalLight(
			1,
			//&CVector3(LightPos.x, -0.5f, LightPos.z),
			&LightPos,
			&D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f),	// 基本色(ディフーズ RGBA)
			&D3DXCOLOR(0.3f, 0.3f, 0.3f, 0.0f),	// 環境色(アンビエント RGBA)
			&D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f)	// 反射色(スペキュラ RGBA)
			);

		GET_CAMERA.SetCamera(false);

		CVector3 vCamPos = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(OBJECT_LIST::ID::PLAYER)->GetPosition();



		//	カメラのポジションを更新
		GET_CAMERA.CameraUpdate(
			_lpD3DDevice,
			m_spGameWorld->GetObjectManager()->GetItObject(OBJECT_LIST::ID::PLAYER)->GetPosition(),
			NULL,
			GET_MOUSE.GetAngle().x,
			GET_MOUSE.GetAngle().y
			);

		//	マウス処理更新
		GET_MOUSE.SetMousePointer();
	}


	//--------------
	//	ワールド更新
	//--------------

	//	ゲームワールドを更新
	m_spGameWorld->Update(_lpD3DDevice);

	//
	m_pAchievementWorld->Update(_lpD3DDevice);

	//	エフェクトワールドを更新
	m_spEffectWorld->Update(_lpD3DDevice);

	//	ゴールイベント
	m_pGoal->GoalEvent(_lpD3DDevice);


	//----------------------------------------------------
	//	一度だけ、一番近いオブジェクトの距離とIDを保存する
	//----------------------------------------------------

	//	一度、データを初期化する
	CSearch::Init();

	//	プレイヤーを中心に全てのオブジェクトまでの距離を計算して、
	//	一番近いオブジェクトの距離とIDを記憶させておく
	//for (int i = GET_WORLD.GetGameWorld()->GetBegin() + 1; i < GET_WORLD.GetGameWorld()->GetEnd(); i++)
	for (int i = m_spGameWorld->GetBegin() + 1; i < m_spGameWorld->GetEnd(); i++)
	//for (auto &This : GET_WORLD.GetGameWorld()->GetObjectManager()->GetObjectContainer())
	{

		CSearch::DistanceIsShortId(
			m_spGameWorld->GetObjectManager()->GetItObject(0)->GetId(),
			i,
			m_spGameWorld
			);

	}
}


void CGameScene::Draw(LPDIRECT3DDEVICE9 _lpD3DDevice)
{

	//--------------------------
	//	屈折描画に必要な通常描画
	//--------------------------

	//	背景用描画開始
	GET_SHADER.DrawBackBegin();

	//	ゲームワールドを描画
	m_spGameWorld->Draw(_lpD3DDevice);

	//	エフェクトワールドを描画 ( ブルームをかけない )
	m_spEffectWorld->Draw(_lpD3DDevice);

	//	背景用描画終了
	GET_SHADER.DrawBackEnd();


	//----------------
	//	ミラーシェーダ
	//----------------

	GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(OBJECT_LIST::ID::RIVER)->GetMeshObject()->GetMeshState()->m_Clip = 1;

	//	ミラー用にビュー行列を渡す
	CMatrix mView = GET_CAMERA.GetViewMatrix();

	mView.CreateInverse();

	CMatrix mS;

	mS.CreateScale(1, -1, 1);

	mView *= mS;

	mView.CreateInverse();

	GET_SHADER.GetMyShader()->SetTransformView(&mView);

	//	カリングモードを裏面に変更
	GET_DIRECT_HELPER.CullMode(D3DCULL_CW);

	//	ミラー描画開始
	GET_SHADER.DrawMirrorBegin();

	//	ゲームワールドを描画
	m_spGameWorld->Draw(_lpD3DDevice);

	//	エフェクトワールドを描画 ( ブルームをかけない )
	m_spEffectWorld->Draw(_lpD3DDevice);

	//	ミラー描画終了
	GET_SHADER.DrawMirrorEnd();

	//	カリングモードを戻す
	GET_DIRECT_HELPER.CullMode(D3DCULL_CCW);

	GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(OBJECT_LIST::ID::RIVER)->GetMeshObject()->GetMeshState()->m_Clip = 0;


	//--------------------
	//	各オブジェクト描画
	//--------------------

	//	シェーダー描画開始
	GET_SHADER.DrawShaderBegin(m_spGameWorld->GetObjectManager()->GetItObject(OBJECT_LIST::ID::PLAYER)->GetPosition());

	// テクスチャ補間を線形補間にする。
	GET_DIRECT_HELPER.SetTextureFilter_Point(0);

	// 半透明モードに
	GET_DIRECT_HELPER.Blend_Alpha();


	//	ゲームワールドを描画
	m_spGameWorld->Draw(_lpD3DDevice);

	//	エフェクトワールドを描画 ( ブルームをかけない )
	m_spEffectWorld->Draw(_lpD3DDevice);


	//	シェーダー描画終了
	GET_SHADER.DrawBloomShaderEnd();

}


void CGameScene::Release()
{

	//--------------------
	//	各ワールドデリート
	//--------------------

	m_spGameWorld = nullptr;

	//	エフェクトワールドへのポインタをデリート
	m_spEffectWorld = nullptr;

	//
	Delete(m_pAchievementWorld);

	//	シェーダーのポインタをデリート
	//Delete(m_pShader);

	//	ゴール開放
	Delete(m_pGoal);

	//	スタート開放
	Delete(m_pStart);
}