#include "Scene.h"
#include "Shader.h"
#include "Game.h"
#include "CFileLoader.h"


/*---------------------->
使用するネームスペース
<----------------------*/

using namespace OBJECT_LIST;


CGameWorld::CGameWorld()
:
m_spObjectBaseManager(nullptr)
,
m_BeginId(0)
{

}


CGameWorld::~CGameWorld()
{
	m_spObjectBaseManager->Release();
}


void CGameWorld::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{

	//	オブジェクトマネージャーをインスタンス化
	m_spObjectBaseManager = make_shared<CObjectManager>();

	//	オブジェクトマネージャーを初期化
	m_spObjectBaseManager->Init(_lpD3DDevice);


	//----------------------------------------------
	//	選択されたステージ毎に初期化するものを変える
	//----------------------------------------------

	//	ステージ1が選択された場合
	if (CSceneManager::m_NowSelectStage == STAGE_SELECT::NAME::STAGE1)
	{
		//	ステージ1を呼ぶ
		StageInit("Data/Txt/Stage1.txt", _lpD3DDevice);
	}
	//	ステージ2が選択された場合
	else if (CSceneManager::m_NowSelectStage == STAGE_SELECT::NAME::STAGE2)
	{
		//	ステージ2を呼ぶ
		StageInit("Data/Txt/Stage2.txt", _lpD3DDevice);
	}
	else if (CSceneManager::m_NowSelectStage == STAGE_SELECT::NAME::STAGE3)
	{
		//	ステージ3を呼ぶ
		StageInit("Data/Txt/Stage3.txt", _lpD3DDevice);
	}
}


void CGameWorld::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	オブジェクトマネージャーを更新
	m_spObjectBaseManager->Update(_lpD3DDevice);
}


void CGameWorld::Draw(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	オブジェクトマネージャーを描画
	m_spObjectBaseManager->Draw(_lpD3DDevice);
}


void CGameWorld::Release()
{
}


void CGameWorld::StageInit(string _TxtFileName, LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	ファイル読み込みよう変数
	unique_ptr<CFileLoader> upLoader = make_unique<CFileLoader>();

	//	テキストファイルを読み込む
	upLoader->Load(_TxtFileName, _lpD3DDevice, m_spObjectBaseManager);

	//	参照カウンターをコピー
	m_EndId = upLoader->GetCnt();
}