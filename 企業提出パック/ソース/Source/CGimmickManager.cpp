#include "Game.h"


CGimmickManager::CGimmickManager()
{

}


CGimmickManager::~CGimmickManager()
{
	Release();
}


void CGimmickManager::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{

}


void CGimmickManager::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	for (auto &Data : m_listGimmcikContainer)
	{
		Data->Update(_lpD3DDevice);
	}
}


void CGimmickManager::Draw(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	for (auto &Data : m_listGimmcikContainer)
	{
		//	視錘台範囲内に入っていれば描画する
		//if (GET_HELPER.ViewFrustum(Data->GetId(), &GET_CAMERA.GetVF()) == true)
		{
			Data->Draw(_lpD3DDevice);
		}
	}
}


void CGimmickManager::Release()
{
	//for (auto &It : m_listGimmcikContainer)
	//{
	//	delete It;

	//	It = nullptr;

	//	It = m_listGimmcikContainer.erase(m_listGimmcikContainer.begin() + It);
	//}


	//------------------------------------------
	//	格納されているデータを破棄するために
	//	範囲ループではなく、イテレータを使用する
	//------------------------------------------


	//	shared_ptrのlistはeraseするだけでオブジェクトが破棄されるので
	//	delete も　nullptr　もしない

	auto ItContainer = m_listGimmcikContainer.begin();

	while (ItContainer != m_listGimmcikContainer.end())
	{
		delete (*ItContainer);

		*ItContainer = nullptr;

		ItContainer = m_listGimmcikContainer.erase(ItContainer);
	}
}


void CGimmickManager::PushGimmcikManager(CObjectBase* _Object)
{
	if (_Object != nullptr)
	{
		m_listGimmcikContainer.push_back(_Object);
	}
}


void CGimmickManager::SetShaderManager(CShaderManager* _pShader)
{
	////	nullチェック
	//if (_pShader == nullptr)
	//{
	//	MessageBox(NULL, "シェーダーのポインターがセットされていません。このままゲームを終了します", "警告", MB_OK);
	//	APP.m_bEndFlag = true;
	//	return;
	//}

	////	リストの数を変数に代入
	//auto itObjContainer = m_listGimmcikContainer.begin();

	////	リストの後尾に達していなければ
	//while (itObjContainer != m_listGimmcikContainer.end())
	//{

	//	//	指定したオブジェクトにシャドウマップシェーダのポインタをセット
	//	(*itObjContainer)->SetShader(_pShader);

	//	//	次のリストへ
	//	++itObjContainer;
	//}
}

CObjectBase* CGimmickManager::GetItObject(int _Id)
{
	//	イテレータを生成
	auto itObjContainer = m_listGimmcikContainer.begin();


	//	リストの後尾までまわす
	while (itObjContainer != m_listGimmcikContainer.end())
	{
		//	指定されたIdが入っていた場合
		if ((*itObjContainer)->GetId() == _Id)
		{
			//	指定されたIdの中身を返す
			return (*itObjContainer);
		}

		//	無ければ次へ
		++itObjContainer;
	}

	//	何も無ければNULLを返す
	return nullptr;
}