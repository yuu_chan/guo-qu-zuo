#include "Scene.h"
#include "Game.h"
#include "Texture.h"
#include "CGoal.h"

CGoal::CGoal()
{

}

CGoal::~CGoal()
{

}

void CGoal::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//------------------------------------------------------------------------------
	//	ステージ毎に代入する値が変わるので、判定分を使用する	固定値にしておきたい
	//------------------------------------------------------------------------------

	//	ステージ1が選択された場合
	if (CSceneManager::m_NowSelectStage == STAGE_SELECT::NAME::STAGE1)
	{
		//	ステージ1のゴール位置を記憶
		m_vGoalPos = CVector3(-14.500001f, 14.899998f, 12.500005f);
	}
	else if (CSceneManager::m_NowSelectStage == STAGE_SELECT::NAME::STAGE2)
	{
		//	ステージ2のゴール位置を記憶
		m_vGoalPos = CVector3(16.300001f, 10.500002f, 4.900001f);
	}

}

void CGoal::GoalEvent(LPDIRECT3DDEVICE9 _lpD3DDevice)
{

	//	ポインターが入っているかどうかを確認
	if (m_pGameWorld.lock() == nullptr
		||
		m_pEffectWorld.lock() == nullptr)
	{
		//	警告メッセージ
		MessageBox(NULL, "指定されたポインターが入っていません", "警告", MB_OK);

		//	終了させるためにフラグを立てておく
		APP.m_bEndFlag = true;

		//	何もさせずに返す
		return;
	}


	//--------------------------
	//	ゴール判定処理	距離判定
	//--------------------------

	//	距離と長さ用の変数を用意
	float	 GoalLen;

	//	今現在のプレイヤーのポジションを獲得してくる
	CVector3 vNowPlayerPos;

	//	プレイヤーの座標をゲット
	vNowPlayerPos = m_pGameWorld.lock()->GetObjectManager()->GetItObject(OBJECT_LIST::ID::PLAYER)->GetPosition();

	//	ゴールまでの距離を求める
	GoalLen = GET_HELPER.GetDistance(vNowPlayerPos, m_vGoalPos);


	//	ゴール範囲を決めておく
	const auto GOAL_ZONE = 1.5f;

	//	ゴールまでの距離が求まったら範囲内に入っているかどうかをチェックしてみる
	if (GoalLen < GOAL_ZONE && GoalLen > -GOAL_ZONE)
	{

		//	先頭IDを取得
		auto FirstId = m_pEffectWorld.lock()->GetEffectManager()->GetItEffect(0)->GetFirstId();

		//	後尾IDを取得
		auto LastId = m_pEffectWorld.lock()->GetEffectManager()->GetItEffect(0)->GetLastId();


		//	先頭から後尾までループを回す
		for (auto i = FirstId; i < LastId; i++)
		{
			//	エフェクトIDを取得する
			auto EffectId = m_pEffectWorld.lock()->GetEffectManager()->GetItEffect(i)->GetEffectId();

			//	最大拡大量
			const auto SCALE_Y_MAX = 15.0f;
			const auto SCALE_Y_MOVE = 0.5f;

			//	上側の黒い淵
			if (EffectId == TEX_LIST::ID::EVENT_FRAME_UP)
			{
				//	サイズを大きくしていく
				m_vUpScale.x = 640.0f;
				m_vUpScale.y += SCALE_Y_MOVE;
				m_vUpScale.z = 0.0f;


				//	最大量
				if (m_vUpScale.y > SCALE_Y_MAX)
				{
					//	最大量を超えたらストップ
					m_vUpScale.y = SCALE_Y_MAX;
				}


				//	最後に、値を渡しておく
				m_pEffectWorld.lock()->GetEffectManager()->GetItEffect(i)->SetScale(m_vUpScale);
			}
			//	下側の黒い淵
			else if (EffectId == TEX_LIST::ID::EVENT_FRAME_DOWN)
			{
				//	サイズを大きくしていく
				m_vDownScale.x = 640.0f;
				m_vDownScale.y -= SCALE_Y_MOVE;
				m_vDownScale.z = 0.0f;


				//	最大量
				if (m_vDownScale.y < -(SCALE_Y_MAX))
				{
					m_vDownScale.y = -(SCALE_Y_MAX);
					m_EventEnd = true;
				}


				//	最後に、値を渡しておく
				m_pEffectWorld.lock()->GetEffectManager()->GetItEffect(i)->SetScale(m_vDownScale);
			}

		}

		if (m_EventEnd == true)
		{
			m_pGameWorld.lock()->GetObjectManager()->GetItObject(OBJECT_LIST::ID::PLAYER)->SetGoalCheck(true);
		}
	}


	//	加算合成終了
	GET_DIRECT_HELPER.Blend_Alpha();

}

void CGoal::Release()
{
	
}