#include "Game.h"


CHelp::CHelp()
{

}


CHelp::~CHelp()
{

}


void CHelp::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	メッシュ読み込み
	m_Mesh->LoadSkinMesh("Data/XFile/UI/Help.x", _lpD3DDevice);

	//	スキンメッシュをセット
	m_Mesh->SetSkinMesh();

	//	再生時間設定
	m_Mesh->SetAdvanceTime(1.0f);

	//	アニメーションの切り替え時間設定
	m_Mesh->SetShiftTime(1.0f);

	//	アニメーションのループ時間設定
	m_Mesh->SetLoopTime(1.0f);

	//	アニメーション番号をセット
	m_Mesh->SetAnimationKey(0);

	//	ワールド行列を初期化
	m_mWorld.CreateMove(&m_vPos);
}


void CHelp::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	スキンメッシュを更新
	m_Mesh->UpdateSkinMesh(m_mWorld);

	//	ワールド行列に座標をセット
	m_mWorld.CreateMove(&m_vPos);

	m_mWorld.RotateY_Local(GET_CAMERA.GetAngle().y + 180.0f);

	//	再生時間
	m_Mesh->SetAdvanceTime(0.01f);

	//	アニメーションが切り替わる時間
	m_Mesh->SetShiftTime(1.0f);

	//	アニメーションのループ数(何ループに何回再生するか)
	m_Mesh->SetLoopTime(60.0f);


	//--------------------------------------------------------------------
	//	距離を測って、範囲内に入っていれば、メッセージを読むかどうかの判定
	//--------------------------------------------------------------------

	for (int i = GET_WORLD.GetGameWorld()->GetBegin(); i < GET_WORLD.GetGameWorld()->GetEnd(); i++)
	{

		//	Idを取得して、判定を行わなくていいId
		auto ObjectId = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(i)->GetId();

		//	どれくらいの範囲に入ればいいかを決めておく
		auto Range = m_Mesh->GetBounding()->GetBoundingSphereRadius() * 2.0f;

		//	自身とプレイヤーまでの距離を測って、距離が近づいているかどうかを確認
		if (ObjectId == OBJECT_LIST::ID::PLAYER 
			&& 
			GET_HELPER.RangeDecision(m_Id, i, Range))
		{

			//	Eキーメッセージを表示

			//	もし、Eキーが押された場合
			if (GET_KEY.m_Code.E == GET_KEY.PUSH
				||
				GET_KEY.m_Code.E == GET_KEY.HOLD)
			{
				//	Helpを表示
				m_vPos.y += 0.05f;
			}

		}
	}
}


void CHelp::Draw(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	ライティング開始
	_lpD3DDevice->SetRenderState(D3DRS_LIGHTING, TRUE);


	//	マイシェーダー側にワールド行列を送る
	GET_SHADER.GetMyShader()->SetConstTransformWorld(&m_mWorld);

	//	マイシェーダー描画を行う(シャドウマップ入り)
	GET_SHADER.GetMyShader()->DrawSkinMesh(m_Mesh, m_Pass);


	//	ライティング終了
	_lpD3DDevice->SetRenderState(D3DRS_LIGHTING, FALSE);
}


void CHelp::Release()
{

}

