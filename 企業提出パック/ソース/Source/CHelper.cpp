#include "Game.h"
#include "Texture.h"
#include "CGameScene.h"


//---------------->
//	namespace省略
//<----------------

using namespace OBJECT_LIST;


//------------------>
//	static変数を宣言
//<------------------

pair<int, float> CSearch::m_Search;


CSearch::CSearch()
{

}


CSearch::~CSearch()
{

}


void CSearch::Init()
{
	m_Search.second = 100.0f;
}


void CSearch::DistanceIsShortId(
	int						_SenterPointId,
	int						_TargetPointId,
	shared_ptr<CGameWorld>	_pGameWorld
	)
{
	//----------------------------------------------
	//	中心となる座標と比較する対象の座標を取得する
	//----------------------------------------------

	CVector3 vSenterPointPos = _pGameWorld->GetObjectManager()->GetItObject(_SenterPointId)->GetPosition();

	CVector3 vTargetPointPos = _pGameWorld->GetObjectManager()->GetItObject(_TargetPointId)->GetPosition();


	//------------------------------
	//	座標を比較して、距離を求める
	//------------------------------

	float Distance = GET_HELPER.GetDistance(vSenterPointPos, vTargetPointPos);
	

	//--------------------------------------------------------------------
	//	計算した座標と保存してある座標を比較して、小さければIDと距離を更新
	//--------------------------------------------------------------------

	bool Check = (Distance < m_Search.second) ? true : false;

	//	更新チェックをして、trueがでればIDと距離を更新する
	if (Check == true)
	{

		//	IDを更新
		m_Search.first = _TargetPointId;

		//	距離を更新
		m_Search.second = Distance;
	}

}


void CSearch::Distance(
	int						_SenterPointId,
	int						_TargetPointId,
	shared_ptr<CGameWorld>	_pGameWorld
	)
{
	//----------------------------------------------
	//	中心となる座標と比較する対象の座標を取得する
	//----------------------------------------------

	CVector3 vSenterPointPos = _pGameWorld->GetObjectManager()->GetItObject(_SenterPointId)->GetPosition();

	CMatrix mWorld;
	
	CRigidSupervise::GetItList(_TargetPointId)->GetMatrix(mWorld);

	CVector3 vTargetPointPos = CVector3(mWorld._41, mWorld._42, mWorld._43);


	//------------------------------
	//	座標を比較して、距離を求める
	//------------------------------

	float Distance = GET_HELPER.GetDistance(vSenterPointPos, vTargetPointPos);


	//--------------------------------------------------------------------
	//	計算した座標と保存してある座標を比較して、小さければIDと距離を更新
	//--------------------------------------------------------------------

	bool Check = (Distance < m_Search.second) ? true : false;

	//	更新チェックをして、trueがでればIDと距離を更新する
	if (Check == true)
	{

		//	IDを更新
		m_Search.first = _TargetPointId;

		//	距離を更新
		m_Search.second = Distance;
	}

}


CHelper::CHelper()
{
}


CHelper::~CHelper()
{
}


void CHelper::WallScratch(
	int			_CollisionObject_Id_1,
	int			_CollisionObject_Id_2,
	CVector3	_Ray,
	CVector3	_RayDir,
	float		_BuckPushing
	)
{

	//	フィルターを返してもらう
	UINT Filter = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetFilter();
	int ItemId = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetItemId();


	//	フィルターをチェックして、当たり判定用のフラグが立っていなければ
	if (ItemId == OBJECT_LIST::ID::BRANCH
		||
	/*	ItemId == OBJECT_LIST::ID::ROCK
		||
		ItemId == OBJECT_LIST::ID::SEESAW
		||
		ItemId == OBJECT_LIST::ID::BRIDGE
		||
		ItemId == OBJECT_LIST::ID::WOOD_BOX
		||*/
		!GET_FLAG_CALC.CheckFlag(Filter, FILTER::NAME::COLLISION_ON)
		)
	{
		//	なにもせずに返す
		return;
	}

	//-----------------------------------------------------------------------
	//	オブジェクトが吸収されていた(STOCK状態の)場合はreturnさせるようにする
	//-----------------------------------------------------------------------

	//	チェック用のステータスを生成
	int CheckState;

	//	チェック用のステータスにコリジョンを起こしたい相手側のステータスをコピーしておく
	CheckState = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetItemState();

	//　ステータスがSTOCK状態なのかどうかを調べる
	if (GET_FLAG_CALC.CheckFlag(CheckState, STOCK::STATE::DO_STOCK))
	{
		//	ステータスにSTOCKが入っていた場合は、床判定を行わないのでreturnさせる
		return;
	}


	//------------------------------------------------------------------
	//	当たり判定を行う前に、各オブジェクトの必要なデータを確保しておく
	//------------------------------------------------------------------


	//------------------------
	//	当たり判定を行いたい側
	//------------------------
	CMatrix		mMySelfWorld;
	CVector3	vMySelfPos;

	//	ID1番目のオブジェクトのワールド行列をコピー
	mMySelfWorld	= GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_1)->GetWorld();

	//	ID1番目のオブジェクトの座標をコピー
	vMySelfPos		= GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_1)->GetPosition();


	//----------------------------
	//	当たり判定を行われる相手側
	//----------------------------
	CMatrix					mOpponentWorld;
	shared_ptr<CMeshObject> mOpponentMesh;		//	ポインターを共有するので、newする必要は無い

	//	ID2番目のオブジェクトのワールド行列をコピー
	mOpponentWorld = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetWorld();

	//	ID2番目のオブジェクトのメッシュデータをコピー
	mOpponentMesh =  GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetMeshObject();


	//------------------------------------------------
	//	メッシュ同士の当たり判定を行う前に、行列の準備
	//------------------------------------------------

	//	逆行列用変数
	CMatrix mInv;

	//	相手側のワールド行列を元に逆行列を作る
	D3DXMatrixInverse(&mInv, NULL, &mOpponentWorld);

	//	ローカル座標を求める
	CVector3 vLocalPos, vWorldVec, vLocalVec;

	//	逆行列で座標を変換して、ローカル位置を求める
	D3DXVec3TransformCoord(&vLocalPos, &CVector3(vMySelfPos.x, vMySelfPos.y, vMySelfPos.z), &mInv);

	//	レイの方向を求めておく
	D3DXVec3TransformNormal(&vWorldVec, &_RayDir, &mMySelfWorld);
	D3DXVec3TransformNormal(&vLocalVec, &vWorldVec, &mInv);

	vLocalVec.Normalize();


	//------------------------
	//	向いている方向を求める
	//------------------------
	
	//	作業用変数
	//CVector3 vVec;

	//D3DXVec3TransformNormal(&vVec, &_Ray, &mMySelfWorld);


	//------------------
	//	当たり判定を行う
	//------------------
	
	//	作業用変数
	float WallDis = -1.0f;
	BOOL  Hit;
	DWORD PolyNo;


	//------------------------------------------------
	//	対象がスキンメッシュかどうかで当たりで使用する
	//	メッシュの取得先を変える
	//------------------------------------------------

	if (GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetMeshObject()->GetRootFram() != nullptr)
	{
		//	レイ発射
		D3DXIntersect(
			GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetMeshObject()->GetCont(0)->m_OriginMesh,
			&vLocalPos,
			&vLocalVec,
			&Hit,
			&PolyNo,
			NULL,
			NULL,
			&WallDis,
			NULL,
			NULL
			);
	}
	else
	{
		//	レイ発射
		D3DXIntersect(
			GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetMeshObject()->GetMesh(),
			&vLocalPos,
			&vLocalVec,
			&Hit,
			&PolyNo,
			NULL,
			NULL,
			&WallDis,
			NULL,
			NULL
			);
	}

	//	最大値
	float Limit;

	//	作業用変数
	float Dot = 0.0f;

	//	Hitしたら
	if (Hit)
	{

		//------------------------------
		//	インデックスバッファをいじる
		//------------------------------

		CVector3 vWallVec;

		//	ポリゴンの法線を取得
		vWallVec = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetMeshObject()->GetWallVec()[PolyNo];

		if (vWallVec == nullptr)
		{
			return;
		}



		//----------------
		//法線更新
		//----------------
		float mAngleX = mOpponentWorld.GetAngleX();
		float mAngleY = mOpponentWorld.GetAngleY();
		float mAngleZ = mOpponentWorld.GetAngleZ();

		CMatrix mWall;

		D3DXMatrixRotationYawPitchRoll(&mWall, mAngleY, mAngleX, mAngleZ);

		vWallVec.TransformNormal(&mOpponentWorld);

		vWallVec.Normalize();




		//	内積を求める
		Dot = D3DXVec3Dot(&-vWallVec, &vWorldVec);

		//	0で割ったら不正な値がでる
		Limit = _BuckPushing / Dot;

		//	0以下にならないように制御
		if (Limit < 0) Limit *= -1;

		//if (Limit < 0) Limit = -Limit;

		//	距離がLimitを超えようとしたら
		if (WallDis < Limit)
		{
			//	座標を更新させる
			vMySelfPos += vWallVec * ((Limit - WallDis) * Dot);
		}

		//	最後に、座標をセットする
		GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_1)->SetPosition(vMySelfPos);

	}

}


//-------------------------------------------------------------------->
//	ゲームワールド内のオブジェクト同士の足場を更新する関数
//	レイの発射地点は特にいじらないので、そのメッシュの中心点が基準
//
//	当たり判定を行う前に、IDを確認して、指定のIDなら処理を行う
//	違えばあたり判定を行わず、リターンする
//	もしくは、IDを直に入れておいて、オブジェクトのIDを動的にチェックさせるか
//
//	位置を修正する場合は、_StandAdjustmentを使用
//<--------------------------------------------------------------------
void CHelper::GameWorldStandPosition(
	int		_CollisionObject_Id_1,
	int		_CollisionObject_Id_2,
	float	_StandAdjustment,
	bool&	_Flag)
{
	//	フィルターを返してもらう
	UINT Filter = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetFilter();

	//	フィルターをチェックして、当たり判定用のフラグが立っていなければ
	if (!GET_FLAG_CALC.CheckFlag(Filter, FILTER::NAME::COLLISION_ON))
	{
		//	なにもせずに返す
		return;
	}


	//-----------------------------------------------------------------------
	//	オブジェクトが吸収されていた(STOCK状態の)場合はreturnさせるようにする
	//-----------------------------------------------------------------------

	//	チェック用のステータスを生成
	int CheckState;

	//	チェック用のステータスにコリジョンを起こしたい相手側のステータスをコピーしておく
	CheckState = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetItemState();

	//　ステータスがSTOCK状態なのかどうかを調べる
	if (GET_FLAG_CALC.CheckFlag(CheckState, STOCK::STATE::DO_STOCK))
	{
		//	ステータスにSTOCKが入っていた場合は、床判定を行わないのでreturnさせる
		return;
	}


	//------
	//	変数
	//------
	CVector3 vLocalPos;		//	レイ発射位置
	CVector3 vLocalVec;		//	レイ発射方向
	CMatrix  mInv;			//	逆行列

	BOOL	 Hit = FALSE;
	float	 Dis = -1;

	//_Flag = true;


	//----------------------------------
	//	足場更新に必要な変数の作業を行う
	//----------------------------------

	//	対象物の位置の逆行列を作る
	D3DXMatrixInverse(&mInv, NULL, &GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetWorld());

	//	レイを発射する位置を作る
	D3DXVec3TransformCoord(&vLocalPos, &GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_1)->GetPosition(), &mInv);

	//	レイの発射方向を作る	
	//  (基本的には下方向に発射、発射位置を常に変えたい場合は、CVector3(0, -1, 0)を引数に替える)
	D3DXVec3TransformNormal(&vLocalVec, &CVector3(0, -1, 0), &mInv);


	//--------------------
	//	メッシュ判定を行う
	//--------------------

	////	レイを発射させてヒットしたかどうかと、そのメッシュとの距離をとる
	//D3DXIntersect(
	//	GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetMeshObject()->GetMesh(),
	//	&vLocalPos,
	//	&vLocalVec,
	//	&Hit,
	//	NULL,
	//	NULL,
	//	NULL,
	//	&Dis,
	//	NULL,
	//	NULL);
	if (GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetMeshObject()->GetRootFram() != nullptr)
	{
		//	レイ発射
		D3DXIntersect(
			GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetMeshObject()->GetCont(0)->m_OriginMesh,
			&vLocalPos,
			&vLocalVec,
			&Hit,
			NULL,
			NULL,
			NULL,
			&Dis,
			NULL,
			NULL
			);
	}
	else
	{
		//	レイ発射
		D3DXIntersect(
			GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetMeshObject()->GetMesh(),
			&vLocalPos,
			&vLocalVec,
			&Hit,
			NULL,
			NULL,
			NULL,
			&Dis,
			NULL,
			NULL
			);
	}


	//------------------
	//	足場を更新させる
	//------------------

	//	Hitしたら
	if (Hit)
	{
		//	距離を測って、足場を更新する
		if (Dis < _StandAdjustment)
		{
			//	座標更新用変数
			CVector3 vPos;

			//	更新用変数に当たり判定を行ったオブジェクトの座標をコピーしておく
			vPos = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_1)->GetPosition();

			//	足場を更新したいオブジェクトのy座標を修正
			vPos.y += (_StandAdjustment - Dis);

			//	座標をセットする
			GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_1)->SetPosition(vPos);

			//	加速量を初期化しておく
			GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_1)->SetAccel(0);

			//	フラグをfalseにしておく
			_Flag = false;


			auto ItemId = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetItemId();

			if (ItemId == OBJECT_LIST::ID::WOOD_BOX
				||
				ItemId == OBJECT_LIST::ID::ROCK)
			{

				int State = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetState();

				State = GET_FLAG_CALC.SetData(State, 0x2000, LOGICAL::OPERATION_MODE::OR);

				GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->SetState(State);
			}
		}
	/*	else
		{
			auto ItemId = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetItemId();

			if (ItemId == OBJECT_LIST::ID::WOOD_BOX)
			{

				int State = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetState();

				State = GET_FLAG_CALC.SetData(State, 0x0fff, LOGICAL::OPERATION_MODE::AND);

				GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->SetState(State);
			}
		}*/


		if (Dis < 0.5f)
		{
			//--------------------------------------
			//	踏んだオブジェクトがバネの花だったら
			//--------------------------------------

			auto ItemId = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetItemId();

			if (ItemId == OBJECT_LIST::ID::SPRING_FLOWER)
			{
				//	ステータスを替えたものを送る
				auto State = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetState();

				State |= 0x0100;

				GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->SetState(State);
			}
		}
		else if (Dis > 0.0f && Dis < 0.5f)
		{
			auto ItemId = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetItemId();

			if (ItemId == OBJECT_LIST::ID::SPRING_FLOWER)
			{
				//	ステータスを替えたものを送る
				auto State = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetState();

				State &= 0x00ff;

				GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->SetState(State);
			}
		}
	}
	else
	{
		auto ItemId = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetItemId();

		if (ItemId == OBJECT_LIST::ID::SPRING_FLOWER)
		{
			//	ステータスを替えたものを送る
			auto State = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetState();

			State &= 0x00ff;

			GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->SetState(State);
		}

		if (ItemId == OBJECT_LIST::ID::WOOD_BOX
			||
			ItemId == OBJECT_LIST::ID::ROCK)
		{

			int State = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetState();

			State = GET_FLAG_CALC.SetData(State, 0x0fff, LOGICAL::OPERATION_MODE::AND);

			GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->SetState(State);

		}
		
	}
}


//
////------------------------------------------------------------->
////	ゲームワールド内のオブジェクト同士の判定
////	レイを好きな方向に発射し、
////	ヒットすれば、_CollisionObject_Id_1のオブジェクトに向かって
////	_CollisionObject_Id_2のオブジェクトが移動してくる
////<------------------------------------------------------------
//void CHelper::GameWorldDoAbsorption(
//	int		 _CollisionObject_Id_1,
//	int		 _CollisionObject_Id_2,
//	CVector3 _RayDir)
//{
//	//----------------------
//	//	フィルターをチェック
//	
//	//	フィルターを返してもらう
//	UINT Filter = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetFilter();
//
//	//	フィルターをチェックして、当たり判定用のフラグが立っていなければ
//	if (GET_FLAG_CALC.CheckFlag(Filter, FILTER::NAME::COLLISION_OFF)/*
//		||
//		GET_FLAG_CALC.CheckFlag(Filter, FILTER::NAME::COLLISION_GROUND)*/)
//	{
//		//	なにもせずに返す
//		return;
//	}
//
//
//	//--------------------------------->
//	//	内積を求めるためのローカル関数
//	//<---------------------------------
//	struct LocalFunction
//	{
//		//	移動量更新
//		void MovePosition(
//		CVector3	_vToVec,	//	進みたい方向
//		CVector3*	_vAngle,	//	回転軸
//		CVector3*	_vPos,		//	オブジェクトのポジション(返す)
//		CMatrix*	_mWorld,	//	ワールド行列(返す)
//		bool*		_NormalCalc	//	内積計算を行うかどうか
//		)
//		{
//			BOOL		Hit = 0;
//			float		Dot = 0;
//			float		Limit = 0;
//			float		NormalAngY = _vAngle->y;
//			CVector3	TargetMeshNormal = CVector3(0, 0, -1);
//			CMatrix	mInv;
//			CVector3	LocalPos;
//
//			//  内積を求めておく
//			if (*(_NormalCalc) == true)
//			{
//				CVector3 nowVec;
//				float Dot;
//
//				//	移動量をポジションに加算
//				_vPos->Add(&_vToVec);
//				//	回転量をセット
//				_mWorld->SetRotateY(NormalAngY / 2);
//
//				D3DXVec3TransformNormal(&nowVec, &CVector3(0, 0, 1), _mWorld);
//				D3DXVec3Normalize(&_vToVec, &_vToVec);
//				Dot = D3DXVec3Dot(&_vToVec, &nowVec);
//				Dot = D3DXToDegree(acos(Dot));
//				if (Dot >= 0.1f)
//				{
//					CVector3 Cross;
//					D3DXVec3Cross(&Cross, &nowVec, &_vToVec);
//					D3DXVec3Normalize(&Cross, &Cross);
//
//					//	外積の回転量
//					if (Dot >= 1.0f)
//						Dot = 1.0f;
//
//					if (Cross.y >= 0.9f)
//						NormalAngY += Dot;
//					else if (Cross.y <= -0.9f)
//						NormalAngY -= Dot;
//					else
//						NormalAngY += Dot;
//				}
//				//	360超えさせないようにする
//				NormalAngY = float((int)NormalAngY % 360);
//
//				_vAngle->y = NormalAngY;
//
//				_mWorld->SetRotateY(NormalAngY / 2);
//			}
//		}
//	};
//	//	ローカル関数はここまで
//
//
//	//<----------------------------
//	//	ここから吸い込み処理を行う
//
//	//------
//	//	変数
//	//------
//	float		Dis = -1;
//	BOOL		Hit;
//	CMatrix		mInv;
//	CMatrix		mInvTrans;
//	CVector3	vLocalPos;
//	CVector3	vLocalVec;
//	CVector3	vUpdatePos;
//
//
//	//------------------
//	//	当たり判定を行う
//	//------------------
//
//	//	対象物の座標を取ってくる
//	vUpdatePos = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetPosition();
//
//	//	対象物の座標を設定
//	D3DXMatrixTranslation(
//		&mInvTrans,
//		vUpdatePos.x,
//		vUpdatePos.y,
//		vUpdatePos.z);
//
//	//	逆行列に対象オブジェクトの(更新された)座標を設定
//	D3DXMatrixInverse(&mInv, NULL, &mInvTrans);
//
//	//	ローカル座標に格納する
//	D3DXVec3TransformCoord(&vLocalPos, &GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_1)->GetPosition(), &mInv);
//
//	//	発射する方向を決める
//	D3DXVec3TransformNormal(&vLocalVec, &_RayDir, &GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_1)->GetWorld());
//
//	//	対メッシュの判定を行う
//	D3DXIntersect(
//		GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetMeshObject()->GetMesh(),
//		&vLocalPos,
//		&vLocalVec,
//		&Hit,
//		NULL,
//		NULL,
//		NULL,
//		&Dis,
//		NULL,
//		NULL);
//
//
//	//------------------------
//	//	吸い込み範囲を決める
//	//	飲み込める範囲を決める
//	//------------------------
//	const auto ABSORPTION_ZONE_MAX = 3.0f;
//	const auto ABSORPTION_ZONE_MIN = 0.1f;
//	const auto STOCK_ZONE_MAX = 0.35f;
//	const auto STOCK_ZONE_MIN = 0.21f;
//
//	//----------------
//	//	吸い込める範囲
//	//----------------
//	if (Dis < ABSORPTION_ZONE_MAX && Dis >= ABSORPTION_ZONE_MIN)
//	{
//		//	作業用変数
//		CVector3 vVec;
//		CVector3 vMove;
//		float	 ObjectWeight;
//		bool	 ableNormalCalc = true;		//	Flagはわかりやすくableをつける
//
//
//		//	指定したオブジェクトの体重を持ってくる
//		ObjectWeight = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetObjectWeight();
//
//		//	制御値
//		const auto CONTROL		= 0.05f;
//		const auto MAX_WEIGHT	= 1.0f;	
//
//		//	移動速度を決める 耐久度をつけることがあるかも
//		//	(最大距離 - 現在の距離) * 制御値 * (最大体重 - 体重)
//		vMove = CVector3(
//			0.0f,
//			0.0f,
//			-((ABSORPTION_ZONE_MAX - Dis) * CONTROL * (MAX_WEIGHT - ObjectWeight)));
//
//		//	動かしたい向きと速さを求める
//		D3DXVec3TransformNormal(
//			&vVec,
//			&vMove,
//			&GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_1)->GetWorld());
//
//		//	求めた値を動かしたい対象に送る
//		GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->SetVec(vVec);
//
//
//		//----------------------------
//		//	内積を求めて向きを変更する
//		//----------------------------
//
//		//	作業用変数
//		CMatrix		mWorld;
//		CVector3	vPos;
//		CVector3	vAngle;
//
//		//	移動するオブジェクトのワールドを取ってくる
//		mWorld = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetWorld();
//
//		//	移動するオブジェクトの座標を取ってくる
//		vPos = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetPosition();
//
//		//	移動するオブジェクトのアングルを取ってくる (GetWorld()から取ってくる)
//		vAngle = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetAngle();
//
//		//vAngle = CVector3(0, vMove.z, 0);
//
//		//	取ってきたワールドを元に内積を求める
//		LocalFunction InnerProduct;
//		InnerProduct.MovePosition(
//			vVec,
//			&vAngle,
//			&vPos,
//			&mWorld,
//			&ableNormalCalc);
//
//		//	回転軸も更新されているので送る
//		GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->SetAngle(vAngle);
//
//		//	更新したワールド行列を送る
//		GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->SetWorld(mWorld);
//
//
//		//------------------------------
//		//	おなかの中に入れるタイミング
//		//------------------------------
//		if (Dis < STOCK_ZONE_MAX && Dis > STOCK_ZONE_MIN)
//		{
//			//	作業用変数
//			int State1 = 0;
//			int State2 = 0;
//
//			//	ステータスを一度確保してくる
//			State1 = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_1)->GetItemState();
//			State2 = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetItemState();
//
//			//  ステータスを変更
//			State1 = GET_FLAG_CALC.SetData((State1 & STOCK::STATE::FREE), STOCK::STATE::DO_STOCK, LOGICAL::OPERATION_MODE::OR);
//			State2 = GET_FLAG_CALC.SetData((State2 & STOCK::STATE::FREE), STOCK::STATE::DO_STOCK, LOGICAL::OPERATION_MODE::OR);
//
//			//	プレイヤーの中にアイテムIDを参照し、そのアイテムを体の中にストックされる状態にする
//			//	作業用変数
//			int ItemId = 0;
//
//			//	アイテムIDを取ってくる
//			ItemId = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetItemId();
//
//			//	ステータスにアイテムIDをプラスする
//			State1 = GET_FLAG_CALC.SetData(State1, ItemId, LOGICAL::OPERATION_MODE::OR);
//
//			//	変更したステータスを送る
//			GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_1)->SetItemState(State1);
//			GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->SetItemState(State2);
//
//
//
//			//--------------------------------------
//			//	実績用のクラスをインスタンス化させる
//			//--------------------------------------
//
//			//	ポインタが入っているかどうか
//			if (m_pAchievementWorld != nullptr)
//			{
//				LPDIRECT3DDEVICE9 lpD3D = GET_DIRECT_HELPER.GetDev();
//
//				if (ItemId == OBJECT_LIST::ID::APPLE)
//				{
//					m_pAchievementWorld->CreateObject<CAchieApple>(lpD3D, OBJECT_LIST::ID::APPLE);//CreateObject<CAchieApple>(GET_DIRECT_HELPER.GetDev(), ItemId);
//					m_pAchievementWorld->GetAchieManager()->GetItObject(0)->SetEnableAchie(true);
//				}
//			}
//
//
//			//GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_1)->GetMeshObject()->GetAnimeController()->SetAnimationTime(0.0f);
//		}
//		//	おなかの中に入らない
//		else
//		{
//			//	作業用変数
//			int State1 = 0;
//			int State2 = 0;
//
//			//	ステータスを一度確保してくる
//			State1 = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_1)->GetItemState();
//			State2 = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetItemState();
//
//			//  ステータスを変更
//			State1 = GET_FLAG_CALC.SetData((State1 & STOCK::STATE::FREE), STOCK::STATE::DO_ABSORPTION, LOGICAL::OPERATION_MODE::OR);
//			State2 = GET_FLAG_CALC.SetData((State2 & STOCK::STATE::FREE), STOCK::STATE::DO_ABSORPTION, LOGICAL::OPERATION_MODE::OR);
//
//			//	変更したステータスを送る
//			GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_1)->SetItemState(State1);
//			GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->SetItemState(State2);
//		}
//	}
//	//	吸い込める範囲に入っていなければ
//	else if (!Hit)
//	{
//		//	作業用変数
//		int State2 = 0;
//
//		//	ステータスを一度確保してくる
//		State2 = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetItemState();
//
//		//  ステータスをデフォルトの状態に変更
//		State2 = GET_FLAG_CALC.SetData((State2 & STOCK::STATE::FREE), STOCK::STATE::NO_ABSORPTION, LOGICAL::OPERATION_MODE::OR);
//
//		//	変更したステータスを送る
//		GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->SetItemState(State2);
//	}
//
//
//}


float CHelper::GetDistance(
	CVector3 _vSenterPointPos,
	CVector3 _vTargetPointPos
	)
{
	//--------------
	//	距離を求める
	//--------------

	//	長さを測るための変数
	float Distance;

	//	距離と長さを求めておく変数を用意
	CVector3 vVec;

	//	対象のオブジェクトとの距離を求める
	vVec = _vSenterPointPos - _vTargetPointPos;

	//	距離の長さに変換する
	Distance = D3DXVec3Length(&vVec);


	//	求めた距離の長さを返す
	return Distance;
}


bool CHelper::UnderDecision(
	int			_CollisionObject_Id_1,
	int			_CollisionObject_Id_2,
	float&		_UnderWaterDis,
	CVector3	_RayDir,
	CVector3	_AddPos
	)
{

	//--------------------------------------------------
	//	IDを確認して、水クラスが来ているかどうかチェック
	//--------------------------------------------------

	//	フィルターを返してもらう
	UINT Filter = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetFilter();

	//	フィルターをチェックして、当たり判定用のフラグが立っていなければ
	//if (GET_FLAG_CALC.CheckFlag(Filter, FILTER::NAME::COLLISION_OFF)
	//	||
	//	GET_FLAG_CALC.CheckFlag(Filter, FILTER::NAME::COLLISION_ON))
	if (!GET_FLAG_CALC.CheckFlag(Filter, FILTER::NAME::COLLISION_WATER))
	{
		//	なにもせずに返す
		return false;
	}


	//	判定用の変数
	bool WaterIn;		//	true(入っている) false(入っていない)

	//------------------------------------------------------------------
	//	当たり判定を行う前に、各オブジェクトの必要なデータを確保しておく
	//------------------------------------------------------------------

	//------------------------
	//	当たり判定を行いたい側
	//------------------------
	CMatrix		mMySelfWorld;
	CVector3	vMySelfPos;

	//	ID1番目のオブジェクトのワールド行列をコピー
	mMySelfWorld = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_1)->GetWorld();

	//	ID1番目のオブジェクトの座標をコピー
	vMySelfPos = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_1)->GetPosition();


	//----------------------------
	//	当たり判定を行われる相手側
	//----------------------------
	CMatrix					mOpponentWorld;
	shared_ptr<CMeshObject> mOpponentMesh;		//	ポインターを共有するので、newする必要は無い

	//	ID2番目のオブジェクトのワールド行列をコピー
	mOpponentWorld = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetWorld();

	//	ID2番目のオブジェクトのメッシュデータをコピー
	mOpponentMesh  = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetMeshObject();


	//------------------------------------------------
	//	メッシュ同士の当たり判定を行う前に、行列の準備
	//------------------------------------------------

	//	逆行列用変数
	CMatrix mInv;

	//	相手側のワールド行列を元に逆行列を作る
	D3DXMatrixInverse(&mInv, NULL, &mOpponentWorld);

	//	ローカル座標を求める
	CVector3 vLocalPos, vLocalVec;

	//	逆行列で座標を変換して、ローカル位置を求める
	D3DXVec3TransformCoord(&vLocalPos, &(vMySelfPos + _AddPos), &mInv);

	//	向きは固定させておく(Rayに切り替えるといいかもしれない)
	D3DXVec3TransformNormal(&vLocalVec, &_RayDir, &mMySelfWorld);


	//------------------
	//	当たり判定を行う
	//------------------

	//	作業用変数
	float Dis = -1.0f;
	BOOL  Hit;

	//	レイ発射
	/*D3DXIntersect(
		GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetMeshObject()->GetMesh(),
		&vLocalPos,
		&vLocalVec,
		&Hit,
		NULL,
		NULL,
		NULL,
		&Dis,
		NULL,
		NULL
		);*/
	if (GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetMeshObject()->GetRootFram() != nullptr)
	{
		//	レイ発射
		D3DXIntersect(
			GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetMeshObject()->GetCont(0)->m_OriginMesh,
			&vLocalPos,
			&vLocalVec,
			&Hit,
			NULL,
			NULL,
			NULL,
			&Dis,
			NULL,
			NULL
			);
	}
	else
	{
		//	レイ発射
		D3DXIntersect(
			GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetMeshObject()->GetMesh(),
			&vLocalPos,
			&vLocalVec,
			&Hit,
			NULL,
			NULL,
			NULL,
			&Dis,
			NULL,
			NULL
			);
	}


	//	ヒット判定が出たら
	if (Hit)
	{
		//	水深何メートルかを測る
		//if (Dis < 5.0f && Dis > -5.0f)
		WaterIn = true;
	}
	//	ヒットしていなければ
	else if (!Hit)
	{
		//	falseを返す
		WaterIn = false;
	}

	//	距離をコピーしておく
	_UnderWaterDis = Dis;


	//	変数を返す true(入っている) false(入っていない)
	return WaterIn;
}


float CHelper::RayDistanceCollision(
	int			_CollisionObject_Id_1,
	int			_CollisionObject_Id_2,
	CVector3	_RayDir,
	CVector3	_AddPos
	)
{
	//--------------------------------------------------
	//	地面と水面(川)のIDがきているかをチェックしておく
	//--------------------------------------------------

	//	チェック用の変数
	int CheckId = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetItemId();

	//	IDをチェックしてもし、指定したIDが入っていなければ
	if (CheckId != OBJECT_LIST::ID::RIVER
		&&
		CheckId != OBJECT_LIST::ID::STAGE)
	{
		//	falseを返す
		return false;
	}


	//--------------
	//	ここから本文
	//--------------

	//------------------------------------------------------------------
	//	当たり判定を行う前に、各オブジェクトの必要なデータを確保しておく
	//------------------------------------------------------------------

	//------------------------
	//	当たり判定を行いたい側
	//------------------------
	CMatrix		mMySelfWorld;
	CVector3	vMySelfPos;

	//	ID1番目のオブジェクトのワールド行列をコピー
	mMySelfWorld = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_1)->GetWorld();

	//	ID1番目のオブジェクトの座標をコピー
	vMySelfPos = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_1)->GetPosition();


	//----------------------------
	//	当たり判定を行われる相手側
	//----------------------------
	CMatrix					mOpponentWorld;
	shared_ptr<CMeshObject> mOpponentMesh;		//	ポインターを共有するので、newする必要は無い

	//	ID2番目のオブジェクトのワールド行列をコピー
	mOpponentWorld = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetWorld();

	//	ID2番目のオブジェクトのメッシュデータをコピー
	mOpponentMesh = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetMeshObject();


	//------------------------------------------------
	//	メッシュ同士の当たり判定を行う前に、行列の準備
	//------------------------------------------------

	//	逆行列用変数
	CMatrix mInv;

	//	相手側のワールド行列を元に逆行列を作る
	D3DXMatrixInverse(&mInv, NULL, &mOpponentWorld);

	//	ローカル座標を求める
	CVector3 vLocalPos, vLocalVec;

	//	逆行列で座標を変換して、ローカル位置を求める
	D3DXVec3TransformCoord(&vLocalPos, &(vMySelfPos + _AddPos), &mInv);

	//	向きは固定させておく(Rayに切り替えるといいかもしれない)
	D3DXVec3TransformNormal(&vLocalVec, &_RayDir, &mMySelfWorld);


	//------------------
	//	当たり判定を行う
	//------------------

	//	作業用変数
	float Dis = -1.0f;
	BOOL  Hit;

	//	レイ発射
	D3DXIntersect(
		GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetMeshObject()->GetMesh(),
		&vLocalPos,
		&vLocalVec,
		&Hit,
		NULL,
		NULL,
		NULL,
		&Dis,
		NULL,
		NULL
		);


	return Dis;
}


bool CHelper::Sphere(
	int _CollisionId_1,
	int _CollisionId_2)
{

	int CheckId = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionId_2)->GetItemId();

	if(CheckId == OBJECT_LIST::ID::STAGE)
	{
		return true;
	}


	//----------------------------------
	//	対象オブジェクトまでの距離を測る
	//----------------------------------

	//	自身の座標を返す
	CVector3 vMyPos = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionId_1)->GetPosition();

	//	対象の座標を返す
	CVector3 vTargetPos = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionId_2)->GetPosition();

	//	対象までの長さを返してもらう
	float Len = GetDistance(vMyPos, vTargetPos);


	//--------------
	//	スフィア判定
	//--------------

	//	対象オブジェクトの半径を返してもらう
	float ZoneSphere = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionId_2)->GetMeshObject()->GetBounding()->GetBoundingSphereRadius();

	//	取得した半径にすこし加算
	//ZoneSphere += 1.0f;

	//	対象オブジェクトの半径と距離を測って内側にいればtrue入っていなければfalseを返す
	return (Len < ZoneSphere) ? true : false;
}


void CHelper::CreateViewFrustum(CVFData* _pOut)
{
	//	射影行列のコピー
	CMatrix mProj = GET_CAMERA.GetProjMatrix();

	//	ビュー行列のコピー
	CMatrix mView = GET_CAMERA.GetViewMatrix();

	//	射影行列とビュー行列を合成するための変数
	CMatrix mVP = mView * mProj;

	//	カメラのワールド座標を取ってくる
	CMatrix mInvV = mView;
	
	//	逆行列を作る
	mInvV.CreateInverse();

	//	座標が入っている行列をとってくる
	_pOut->m_vCamPos = GET_CAMERA.GetCameraWorldPosition();//mInvV.GetPos();

	//	逆行列化する
	mVP.CreateInverse();

	//	視錘台の頂点名
	enum VERTEX
	{
		/*	頂点の順番
		A'・		・B'

		C'・		・D'
		*/

		A = 0,
		B = 1,
		C = 2,
		D = 3,
		MAX,
	};

	//	視錘台の頂点を格納するための変数
	CVector3 vFrustumVertex[VERTEX::MAX] = {
		CVector3(-1,  1, 1),		//	A'
		CVector3( 1,  1, 1),		//	B'
		CVector3(-1, -1, 1),		//	C'
		CVector3( 1, -1, 1)			//	D'
	};

	//	頂点までの長さを格納する
	CVector3 VertexLen[VERTEX::MAX]{
		CVector3(0, 0, 0),			//	A'までの距離
		CVector3(0, 0, 0),			//	B'までの距離
		CVector3(0, 0, 0),			//	C'までの距離
		CVector3(0, 0, 0)			//	D'までの距離
	};


	//	頂点の数分ループをまわす
	for (int i = 0; i < VERTEX::MAX; i++)
	{
		//	合成した行列と視錘台の頂点を合成するための射影行列用変数
		CMatrix mPVerTex;

		//	頂点を行列化する
		CMatrix mTrans;


		/*	2D頂点を行列に
			奥は1で考える

		-1, 1, 1	  1, 1, 1
			A・			B・


		-1, -1, 1,	  1, -1, 1
			C・			D・

		*/
		D3DXMatrixTranslation(&mTrans, vFrustumVertex[i].x, vFrustumVertex[i].y, vFrustumVertex[i].z);

		//	2D頂点 * 射影行列とビュー行列を合成したもの
		//		mPVerTex = mTrans * mVP;

		//	これを、3D空間として保管するためにCoordを使う
		D3DXVec3TransformCoord(&vFrustumVertex[i], &vFrustumVertex[i], &mVP);

		//	カメラと設定した頂点までの距離を測る
		VertexLen[i] = vFrustumVertex[i] - _pOut->m_vCamPos;
	}


	//--------------------------------------
	//	頂点がわかったので、頂点同士を結んで
	//	それぞれの面に対しての外積を求める
	//--------------------------------------
	//	外積を求める

	enum NVERTEX
	{
		NA = 0,
		NB = 1,
		ND = 2,
		NC = 3,

		N_MAX
	};

	CVector3 Cross[NVERTEX::N_MAX];

	/*
		 		 NB
		　		 ↑
			A'・	   B' ・

		NA <-		    → ND

			C'		   D' ・
				 ↓
				 NC
	*/

	//	NA
	D3DXVec3Cross(&_pOut->m_vN[NVERTEX::NA], &vFrustumVertex[VERTEX::C], &VertexLen[VERTEX::A]);

	//	NB
	D3DXVec3Cross(&_pOut->m_vN[NVERTEX::NB], &vFrustumVertex[VERTEX::A], &VertexLen[VERTEX::B]);

	//	ND
	D3DXVec3Cross(&_pOut->m_vN[NVERTEX::ND], &vFrustumVertex[VERTEX::B], &VertexLen[VERTEX::D]);

	//	NC
	D3DXVec3Cross(&_pOut->m_vN[NVERTEX::NC], &vFrustumVertex[VERTEX::D], &VertexLen[VERTEX::C]);


	//	正規化
	for (int i = 0; i < NVERTEX::N_MAX; i++)
	{
		D3DXVec3Normalize(&_pOut->m_vN[i], &_pOut->m_vN[i]);
	}
}


bool CHelper::ViewFrustum(int _ListId, CVFData* _pVf)
{

	int Check = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_ListId)->GetItemId();
	
	if (Check == OBJECT_LIST::ID::SKY
		||
		Check == OBJECT_LIST::ID::STAGE)
	{
		return true;
	}

	//	ターゲットまでの距離を求めるようの変数
	CVector3 vTargetVec = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_ListId)->GetPosition();

	//	距離 = ターゲットの距離 - カメラの座標
	vTargetVec = vTargetVec - _pVf->m_vCamPos;


	for (int i = 0; i < 4; i++)
	{
		//	内積を求める
		float L = 0;

		//	内積を返す
		L = D3DXVec3Dot(&vTargetVec, &_pVf->m_vN[i]);

		//	オブジェクトの半径を取得してくる
		//float r = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_ListId)->GetZone();
		float r = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_ListId)->GetMeshObject()->GetBounding()->GetBoundingSphereRadius();

		//	内積で求まった距離 > 半径
		if (L > r)
		{
			//	描画をしない
			return false;
		}

	}
	
	//	描画をする
	return true;
}


bool CHelper::RangeDecision(
	int		_ObjectId_1,
	int		_ObjectId_2,
	float	_Range
	)
{

	//----------------------------------------------------
	//	オブジェクトの座標を取得して、
	//	中心となる座標と判定したい対象までの距離までを測る
	//----------------------------------------------------

	//	中心となる座標
	CVector3 vCenterPointPos = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_ObjectId_1)->GetPosition();

	//	判定したい対象
	CVector3 vOpponentPos = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_ObjectId_2)->GetPosition();

	//	距離を保存しておく
	auto Distance = GetDistance(vCenterPointPos, vOpponentPos);
	//auto Distance = CSearch::GetSearchResult();

	//	判定内に入ったらtrueを返す
	if (Distance < _Range)
	{
		return true;
	}

	//	何も無ければfalseを返す
	return false;
}


void CHelper::RangeDoAbsorption(
	int			_ObjectId_1,
	int			_ObjectId_2,
	float		_Range,
	CVector3	_RayDir
	)
{

	//----------------------------------------------------------------------
	//	吸い込みを行わなくてもいい物に対しては、本文を通さないようにしておく
	//----------------------------------------------------------------------

	//	当たり判定を行うフィルターかどうかをチェック
	auto Check = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_ObjectId_2)->GetFilter();

	if (Check != FILTER::NAME::COLLISION_ON
		//GET_FLAG_CALC.CheckFlag(Check, 0x00ff, 0x0012)
		&&
		GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_ObjectId_2)->GetItemId() != OBJECT_LIST::ID::STAGE)
	{
		return;
	}


	//-----------------------------------------------------------
	//	指定した範囲内にオブジェクトが存在しているかどうかを確認
	//	もし、範囲外であればそのばでreturnさせる
	//-----------------------------------------------------------
	if (!RangeDecision(_ObjectId_1, _ObjectId_2, 5.0f))
	{
		return;
	}


	/*---------------------------------------------

	範囲内に入っていた場合


	内積で求めた角度が範囲内に入ったかどうかを判定
	
	内積を求めるために、二つのベクトルを作る
	1つ目は対象までの長さ
	2つ目はプレイヤーから飛ばしたベクトル

	---------------------------------------------*/

	//	中心となる座標を取得してくる
	CVector3 vCenterPointPos = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_ObjectId_1)->GetPosition();

	//	対象となる座標を取得してくる
	CVector3 vOpponentPos = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_ObjectId_2)->GetPosition();

	//	プレイヤーのワールド行列を取得する
	CMatrix mWorld = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_ObjectId_1)->GetWorld();


	//-----------------------
	//	1つ目のベクトルを取得
	//-----------------------

	//	対象の座標 - 中心となる座標 = 1つめのベクトル
	CVector3 vLenVec = vOpponentPos - vCenterPointPos;

	//	1つ目のベクトルを正規化
	vLenVec.Normalize();


	//-----------------------
	//	2つ目のベクトルを取得
	//-----------------------

	//	プレイヤーの任意の方向のベクトルを取得する
	CVector3 vRayVec;

	D3DXVec3TransformNormal(&vRayVec, &_RayDir, &mWorld);

	//	2つ目のベクトルを正規化
	vRayVec.Normalize();


	//--------------
	//	内積を求める
	//--------------

	float Dot;

	//	1つ目のベクトルと2つ目のベクトルを使う
	Dot = D3DXVec3Dot(&vLenVec, &vRayVec);


	//------------------------------------------------
	//	求めた内積が範囲内に入っているかどうかを調べる
	//------------------------------------------------

	if (Dot > _Range
		&&
		0 < Dot
		)
	{

		//--------------------------------------------------------------------------------
		//	任意のオブジェクトの物理剛体クラスにアクセスして、吸い込まれたときの力を加える
		//	力は、中心となる座標から対象の座標を引いたもので、1つ目のベクトルの-を指す
		//--------------------------------------------------------------------------------
		//if (CRigidSupervise::GetItObject(_ObjectId_2).lock() != nullptr)
		{


			if (GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_ObjectId_2)->GetItemId() == OBJECT_LIST::ID::SPRING_FLOWER)
			{
				auto thisObjectItemState = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_ObjectId_2)->GetItemState();

				thisObjectItemState = GET_FLAG_CALC.SetData((thisObjectItemState & STOCK::STATE::FREE), STOCK::STATE::DO_ABSORPTION, LOGICAL::OPERATION_MODE::OR);

				GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_ObjectId_2)->SetItemState(thisObjectItemState);
			}


			auto PlayerState = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_ObjectId_1)->GetState();

			//-----------------------------------------
			//	イチゴだった場合
			//	&&
			//	プレイヤーが吸い込み状態の場合
			//	&&
			//	イチゴの状態が落下状態ではなかった場合
			//
			//	ItemStateを吸い込まれている状態に替える
			//-----------------------------------------
			if (GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_ObjectId_2)->GetItemId() == OBJECT_LIST::ID::STRAW_BERRY
				&&
				GET_FLAG_CALC.CheckFlag(PlayerState, 0x000c, 0x0004)
				&&
				GET_FLAG_CALC.CheckFlag(GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_ObjectId_2)->GetState(), 0x00f0, 0x0010)
				||
				GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_ObjectId_2)->GetItemId() == OBJECT_LIST::ID::STRAW_BERRY_IVY
				&&
				GET_FLAG_CALC.CheckFlag(PlayerState, 0x000c, 0x0004)
				&&
				GET_FLAG_CALC.CheckFlag(GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_ObjectId_2)->GetState(), 0x00f0, 0x0010)
				)
			{
				auto thisObjectItemState = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_ObjectId_2)->GetItemState();

				thisObjectItemState = GET_FLAG_CALC.SetData((thisObjectItemState & STOCK::STATE::FREE), STOCK::STATE::DO_ABSORPTION, LOGICAL::OPERATION_MODE::OR);

				GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_ObjectId_2)->SetItemState(thisObjectItemState);
			}
			else
			{
				auto thisObjectItemState = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_ObjectId_2)->GetItemState();

				thisObjectItemState = GET_FLAG_CALC.SetData((thisObjectItemState & STOCK::STATE::FREE), STOCK::STATE::NO_ABSORPTION, LOGICAL::OPERATION_MODE::OR);

				GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_ObjectId_2)->SetItemState(thisObjectItemState);
			}

			if (GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_ObjectId_2)->GetItemId() == OBJECT_LIST::ID::STRAW_BERRY_IVY
				||
				GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_ObjectId_2)->GetItemId() == OBJECT_LIST::ID::APPLE_TREE)
			{
				return;
			}


			//------------------------------------------------------
			//	プレイヤーの方向に力を加えながらオブジェクトを飛ばす
			//	範囲内のオブジェクトすべてに適用
			//------------------------------------------------------

			CVector3 vVec = vOpponentPos - vCenterPointPos;

			CRigidSupervise::GetItObject(_ObjectId_2)->ApplyForce(-vVec * 3.5f);


			//------------------------------------------------
			//	ここからは、一番近いオブジェクトに対しての処理
			//------------------------------------------------

			//	一番近いオブジェクトの距離を測る 距離は、あらかじめ求めているものを使う
			auto Dis = CSearch::GetSearchDistance();

			//	一番近いオブジェクトのID
			auto ID = CSearch::GetSearchID();

			//	中心となる座標をもっているオブジェクトの半径の大きさ
			auto Sphere = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_ObjectId_1)->GetMeshObject()->GetBounding()->GetBoundingSphereRadius();

			//	対象となる座標をもっているオブジェクトの半径の大きさ
			auto ObjectSphere = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(ID)->GetMeshObject()->GetBounding()->GetBoundingSphereRadius();

			//	距離 - 対象オブジェクトの大きさ = 対象オブジェクトの半径の先を中心として扱う
			Dis = Dis - ObjectSphere;


			//--------------------------------------------------------------------
			//	プレイヤーの大きさに、近づけば食べたと判定
			//	一番近いIDのみで行うと、本来吸い込みを行わなくてもいい
			//	オブジェクトも反応し、吸い込んだ判定が行われるため
			//	食べた判定を行いたいID(フィルターで除外されていないID)をもってくる
			//
			//	自身のオブジェクトの大きさ > (距離 - 対象の大きさ)
			//	&&
			//	一番近いID と 食べた判定がほしいIDが一致した場合
			//--------------------------------------------------------------------
			if (Sphere > Dis
				&&
				ID == _ObjectId_2
				)
			{

				//	ステータスを一度確保してくる
				auto State1 = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_ObjectId_1)->GetItemState();
				auto State2 = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(ID)->GetItemState();

				//  ステータスを変更
				State1 = GET_FLAG_CALC.SetData((State1 & STOCK::STATE::FREE), STOCK::STATE::DO_STOCK, LOGICAL::OPERATION_MODE::OR);
				State2 = GET_FLAG_CALC.SetData((State2 & STOCK::STATE::FREE), STOCK::STATE::DO_STOCK, LOGICAL::OPERATION_MODE::OR);

				//	アイテムIDを取ってくる
				auto ItemId = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(ID)->GetItemId();

				//	ステータスにアイテムIDをプラスする
				State1 = GET_FLAG_CALC.SetData(State1, ItemId, LOGICAL::OPERATION_MODE::OR);

				//	変更したステータスを送る
				GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_ObjectId_1)->SetItemState(State1);
				GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(ID)->SetItemState(State2);


				//	飲み込まれたアイテムのIDを記憶
				CRigidSupervise::SetStockId(ID);

			}

		}
	}


}


bool CHelper::RangeDoAbsorption(
	int			_ObjectId_1,
	int			_ObjectId_2
	)
{
	CObjectBase* task1 = (CObjectBase *)CRigidSupervise::GetItList(_ObjectId_1)->GetUserPointer();
	CObjectBase* task2 = (CObjectBase *)CRigidSupervise::GetItList(_ObjectId_2)->GetUserPointer();

	if (task1 && task2)
	{

		//----------------------------------------------------------------------
		//	吸い込みを行わなくてもいい物に対しては、本文を通さないようにしておく
		//----------------------------------------------------------------------

		//	当たり判定を行うフィルターかどうかをチェック
		auto Check = task2->GetFilter();

		if (Check != FILTER::NAME::COLLISION_ON
			//!GET_FLAG_CALC.CheckFlag(Check, 0x00f0, FILTER::NAME::COLLISION_ABSORPTION)
			&&
			task2->GetItemId() != OBJECT_LIST::ID::STAGE
			||
			task2->GetItemId() == OBJECT_LIST::ID::STAGE_1_COLLISION)
		{
			return false;
		}


		//-----------------------------------------------------------
		//	指定した範囲内にオブジェクトが存在しているかどうかを確認
		//	もし、範囲外であればそのばでreturnさせる
		//-----------------------------------------------------------
		//	中心となる座標
		CVector3 vCenterPointPos = task1->GetPosition();

		//	判定したい対象
		CVector3 vOpponentPos = task2->GetPosition();

		//	距離を保存しておく
		auto Distance = GetDistance(vCenterPointPos, vOpponentPos);
		
		//	判定外ならreturn
		if (Distance > 5.0f)
		{
			return false;
		}


		/*---------------------------------------------

		範囲内に入っていた場合


		内積で求めた角度が範囲内に入ったかどうかを判定

		内積を求めるために、二つのベクトルを作る
		1つ目は対象までの長さ
		2つ目はプレイヤーから飛ばしたベクトル

		---------------------------------------------*/

		//	プレイヤーのワールド行列を取得する
		CMatrix mWorld = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_ObjectId_1)->GetWorld();


		//-----------------------
		//	1つ目のベクトルを取得
		//-----------------------

		//	対象の座標 - 中心となる座標 = 1つめのベクトル
		CVector3 vLenVec = vOpponentPos - vCenterPointPos;

		//	1つ目のベクトルを正規化
		vLenVec.Normalize();


		//-----------------------
		//	2つ目のベクトルを取得
		//-----------------------

		//	プレイヤーの任意の方向のベクトルを取得する
		CVector3 vRayVec;

		D3DXVec3TransformNormal(&vRayVec, &CVector3(0.0f, 0.0f, 1.0f), &mWorld);

		//	2つ目のベクトルを正規化
		vRayVec.Normalize();


		//--------------
		//	内積を求める
		//--------------

		float Dot;

		//	1つ目のベクトルと2つ目のベクトルを使う
		Dot = D3DXVec3Dot(&vLenVec, &vRayVec);


		//------------------------------------------------
		//	求めた内積が範囲内に入っているかどうかを調べる
		//------------------------------------------------

		if (Dot > 0.5f
			&&
			0 < Dot
			)
		{
			auto PlayerState = task1->GetState();

			//--------------------------------------------------------------------------------
			//	任意のオブジェクトの物理剛体クラスにアクセスして、吸い込まれたときの力を加える
			//	力は、中心となる座標から対象の座標を引いたもので、1つ目のベクトルの-を指す
			//--------------------------------------------------------------------------------

			/*if (task2->GetItemId() == OBJECT_LIST::ID::SPRING_FLOWER)
			{
				auto thisObjectItemState = task2->GetItemState();

				thisObjectItemState = GET_FLAG_CALC.SetData((thisObjectItemState & STOCK::STATE::FREE), STOCK::STATE::DO_ABSORPTION, LOGICAL::OPERATION_MODE::OR);

				task2->SetItemState(thisObjectItemState);
			}*/

			auto thisState = task2->GetState();

			thisState = GET_FLAG_CALC.SetData(thisState, 0x0fff, LOGICAL::OPERATION_MODE::AND);

			task2->SetState(thisState);


			//-----------------------------------------
			//	イチゴだった場合
			//	&&
			//	プレイヤーが吸い込み状態の場合
			//	&&
			//	イチゴの状態が落下状態ではなかった場合
			//
			//	ItemStateを吸い込まれている状態に替える
			//-----------------------------------------
			if (task2->GetItemId() == OBJECT_LIST::ID::STRAW_BERRY
				&&
				GET_FLAG_CALC.CheckFlag(PlayerState, 0x000c, 0x0004)
				&&
				GET_FLAG_CALC.CheckFlag(task2->GetState(), 0x00f0, 0x0010)
				||
				task2->GetItemId() == OBJECT_LIST::ID::STRAW_BERRY_IVY
				&&
				GET_FLAG_CALC.CheckFlag(PlayerState, 0x000c, 0x0004)
				&&
				GET_FLAG_CALC.CheckFlag(task2->GetState(), 0x00f0, 0x0010)
				)
			{
				auto thisObjectItemState = task2->GetItemState();

				thisObjectItemState = GET_FLAG_CALC.SetData((thisObjectItemState & STOCK::STATE::FREE), STOCK::STATE::DO_ABSORPTION, LOGICAL::OPERATION_MODE::OR);

				task2->SetItemState(thisObjectItemState);
			}
			else
			{
				auto thisObjectItemState = task2->GetItemState();

				thisObjectItemState = GET_FLAG_CALC.SetData((thisObjectItemState & STOCK::STATE::FREE), STOCK::STATE::NO_ABSORPTION, LOGICAL::OPERATION_MODE::OR);

				task2->SetItemState(thisObjectItemState);
			}

			if (task2->GetItemId() == OBJECT_LIST::ID::STRAW_BERRY_IVY
				||
				task2->GetItemId() == OBJECT_LIST::ID::APPLE_TREE
				||
				task2->GetItemId() == OBJECT_LIST::ID::BRIDGE
				||
				task2->GetItemId() == OBJECT_LIST::ID::SEESAW)
			{
				return false;
			}


			//------------------------------------------------------
			//	プレイヤーの方向に力を加えながらオブジェクトを飛ばす
			//	範囲内のオブジェクトすべてに適用
			//------------------------------------------------------

			CVector3 vVec = vOpponentPos - vCenterPointPos;

			CRigidSupervise::GetItList(_ObjectId_2)->ApplyForce(-vVec * 3.5f);


			//------------------------------------------------
			//	ここからは、一番近いオブジェクトに対しての処理
			//------------------------------------------------

			//	一番近いオブジェクトの距離を測る 距離は、あらかじめ求めているものを使う
			auto Dis = CSearch::GetSearchDistance();

			//	一番近いオブジェクトのID
			auto ID = CSearch::GetSearchID();

			//	中心となる座標をもっているオブジェクトの半径の大きさ
			auto Sphere = task1->GetMeshObject()->GetBounding()->GetBoundingSphereRadius();

			//	対象となる座標をもっているオブジェクトの半径の大きさ
			auto ObjectSphere = task2->GetMeshObject()->GetBounding()->GetBoundingSphereRadius();

			//	距離 - 対象オブジェクトの大きさ = 対象オブジェクトの半径の先を中心として扱う
			Dis = Dis - ObjectSphere;


			//--------------------------------------------------------------------
			//	プレイヤーの大きさに、近づけば食べたと判定
			//	一番近いIDのみで行うと、本来吸い込みを行わなくてもいい
			//	オブジェクトも反応し、吸い込んだ判定が行われるため
			//	食べた判定を行いたいID(フィルターで除外されていないID)をもってくる
			//
			//	自身のオブジェクトの大きさ > (距離 - 対象の大きさ)
			//	&&
			//	一番近いID と 食べた判定がほしいIDが一致した場合
			//--------------------------------------------------------------------
			if (Sphere > Dis
				&&
				ID == task2->GetId()//_ObjectId_2
				//&&
				//GET_FLAG_CALC.CheckFlag(Check, 0x00f0, FILTER::NAME::COLLISION_ABSORPTION)
				)
			{

				//	ステータスを一度確保してくる
				auto State1 = task1->GetItemState();
				auto State2 = task2->GetItemState();

				//  ステータスを変更
				State1 = GET_FLAG_CALC.SetData((State1 & STOCK::STATE::FREE), STOCK::STATE::DO_STOCK, LOGICAL::OPERATION_MODE::OR);
				State2 = GET_FLAG_CALC.SetData((State2 & STOCK::STATE::FREE), 0x1000, LOGICAL::OPERATION_MODE::OR);

				//	アイテムIDを取ってくる
				auto ItemId = task2->GetItemId();

				//	ステータスにアイテムIDをプラスする
				State1 = GET_FLAG_CALC.SetData(State1, ItemId, LOGICAL::OPERATION_MODE::OR);

				//	変更したステータスを送る
				task1->SetItemState(State1);
				task2->SetItemState(State2);


				//	飲み込まれたアイテムのIDを記憶
				CRigidSupervise::SetStockId(ID);

				return true;
			}
		}
	}

	return false;
}


float CHelper::GetPercent(
	float _WholeValues,
	float _CurrentValues
	)
{
	return ((_CurrentValues / _WholeValues) * 100);
}


bool CHelper::WindZone(
	const shared_ptr<CObjectManager> _spObjectContainer
	)
{
	/*--------------------------------------
	
	ネストを意識しながら処理を書いてみる

	boolで常に値をとって
	if文でできるだけ分ける
	
	--------------------------------------*/


	//--------------------------------
	//	Dataは風が吹いている空間を指す
	//--------------------------------

	for (auto &Data : _spObjectContainer->GetObjectContainer())
	{
		//	ネストを深くしないためにbool型で処理を行っていいかどうかを
		//	持っておく
		bool Check = GET_FLAG_CALC.CheckFlag(Data->GetFilter(), 0x000f, 0x0008);


		//	trueのものだけ処理を行う
		if (Check == true)
		{
			
			//------------------------------------------------------
			//	まず、プレイヤーがWindZoneに入っているかどうかを判定
			//------------------------------------------------------

			//	IDを検索して、ヒットすればboolにtrueを入れる
			bool ThisId = (Data->GetItemId() == ID::WIND_ZONE) ? true : false;	//	true(ヒット) : false(ノーヒット)

			if (Data->GetItemId() == ID::WIND_ZONE)
			{
				ThisId = true;
			}


			//----------------------------
			//	ヒットした場合、処理を行う
			//----------------------------
			if (ThisId == true)
			{

				//-------------------------------------------------
				//	プレイヤーと風が吹いているゾーンの距離を調べて
				//	球判定を行い、内側ならtrue外側ならfalseに分ける
				//-------------------------------------------------

				bool Zone = RangeDecision(OBJECT_LIST::ID::PLAYER, Data->GetId(), Data->GetMeshObject()->GetBounding()->GetBoundingSphereRadius());


				//--------------------------------------
				//	内側にプレイヤーがいるので処理を行う
				//--------------------------------------
				if (Zone == true)
				{
					CVector3 vVec;
					CVector3 vPos = _spObjectContainer->GetItObject(OBJECT_LIST::PLAYER)->GetPosition();

					float Weight = _spObjectContainer->GetItObject(OBJECT_LIST::PLAYER)->GetObjectWeight();

					D3DXVec3TransformNormal(&vVec, &CVector3(0.0f, 0.0f, (1.0f - Weight) * 0.1f), &Data->GetWorld());

					vPos += vVec;

					_spObjectContainer->GetItObject(OBJECT_LIST::PLAYER)->SetPosition(vPos);


					//int State = _spObjectContainer->GetItObject(OBJECT_LIST::PLAYER)->GetState();

					////	ステータスにアイテムIDをプラスする
					//State = GET_FLAG_CALC.SetData(State, 0x4000, LOGICAL::OPERATION_MODE::OR);

					//_spObjectContainer->GetItObject(OBJECT_LIST::PLAYER)->SetState(State);
				}
				//else
				//{
				//	int State = _spObjectContainer->GetItObject(OBJECT_LIST::PLAYER)->GetState();

				//	//	ステータスにアイテムIDをプラスする
				//	State = GET_FLAG_CALC.SetData(State, 0x0fff, LOGICAL::OPERATION_MODE::AND);

				//	_spObjectContainer->GetItObject(OBJECT_LIST::PLAYER)->SetState(State);
				//}

			}
		}
	}


	return false;
}


FLOAT CHelper::LenSegOnSeparateAxis(CVector3* Sep, CVector3* e1, CVector3* e2, CVector3* e3)
{
	// 3つの内積の絶対値の和で投影線分長を計算
	// 分離軸Sepは標準化されていること
	FLOAT r1 = fabs(D3DXVec3Dot(Sep, e1));
	FLOAT r2 = fabs(D3DXVec3Dot(Sep, e2));
	FLOAT r3 = e3 ? (fabs(D3DXVec3Dot(Sep, e3))) : 0;
	return r1 + r2 + r3;
}


bool CHelper::ColOBBs(COBB &obb1, COBB &obb2)
{
	// 各方向ベクトルの確保
	//// （N***:標準化方向ベクトル）
	//CVector3 NAe1 = obb1.GetDirect(0), Ae1 = NAe1 * obb1.GetLen(0);
	//CVector3 NAe2 = obb1.GetDirect(1), Ae2 = NAe2 * obb1.GetLen(1);
	//CVector3 NAe3 = obb1.GetDirect(2), Ae3 = NAe3 * obb1.GetLen(2);
	//CVector3 NBe1 = obb2.GetDirect(0), Be1 = NBe1 * obb2.GetLen(0);
	//CVector3 NBe2 = obb2.GetDirect(1), Be2 = NBe2 * obb2.GetLen(1);
	//CVector3 NBe3 = obb2.GetDirect(2), Be3 = NBe3 * obb2.GetLen(2);
	//CVector3 Interval = obb1.GetPos() - obb2.GetPos();

	//// 分離軸 : Ae1
	//FLOAT rA = D3DXVec3Length(&Ae1);
	//FLOAT rB = LenSegOnSeparateAxis(&NAe1, &Be1, &Be2, &Be3);
	//FLOAT L = fabs(D3DXVec3Dot(&Interval, &NAe1));
	//if (L > rA + rB)
	//	return false; // 衝突していない

	//// 分離軸 : Ae2
	//rA = D3DXVec3Length(&Ae2);
	//rB = LenSegOnSeparateAxis(&NAe2, &Be1, &Be2, &Be3);
	//L = fabs(D3DXVec3Dot(&Interval, &NAe2));
	//if (L > rA + rB)
	//	return false;

	//// 分離軸 : Ae3
	//rA = D3DXVec3Length(&Ae3);
	//rB = LenSegOnSeparateAxis(&NAe3, &Be1, &Be2, &Be3);
	//L = fabs(D3DXVec3Dot(&Interval, &NAe3));
	//if (L > rA + rB)
	//	return false;

	//// 分離軸 : Be1
	//rA = LenSegOnSeparateAxis(&NBe1, &Ae1, &Ae2, &Ae3);
	//rB = D3DXVec3Length(&Be1);
	//L = fabs(D3DXVec3Dot(&Interval, &NBe1));
	//if (L > rA + rB)
	//	return false;

	//// 分離軸 : Be2
	//rA = LenSegOnSeparateAxis(&NBe2, &Ae1, &Ae2, &Ae3);
	//rB = D3DXVec3Length(&Be2);
	//L = fabs(D3DXVec3Dot(&Interval, &NBe2));
	//if (L > rA + rB)
	//	return false;

	//// 分離軸 : Be3
	//rA = LenSegOnSeparateAxis(&NBe3, &Ae1, &Ae2, &Ae3);
	//rB = D3DXVec3Length(&Be3);
	//L = fabs(D3DXVec3Dot(&Interval, &NBe3));
	//if (L > rA + rB)
	//	return false;

	//// 分離軸 : C11
	//CVector3 Cross;
	//D3DXVec3Cross(&Cross, &NAe1, &NBe1);
	//rA = LenSegOnSeparateAxis(&Cross, &Ae2, &Ae3);
	//rB = LenSegOnSeparateAxis(&Cross, &Be2, &Be3);
	//L = fabs(D3DXVec3Dot(&Interval, &Cross));
	//if (L > rA + rB)
	//	return false;

	//// 分離軸 : C12
	//D3DXVec3Cross(&Cross, &NAe1, &NBe2);
	//rA = LenSegOnSeparateAxis(&Cross, &Ae2, &Ae3);
	//rB = LenSegOnSeparateAxis(&Cross, &Be1, &Be3);
	//L = fabs(D3DXVec3Dot(&Interval, &Cross));
	//if (L > rA + rB)
	//	return false;

	//// 分離軸 : C13
	//D3DXVec3Cross(&Cross, &NAe1, &NBe3);
	//rA = LenSegOnSeparateAxis(&Cross, &Ae2, &Ae3);
	//rB = LenSegOnSeparateAxis(&Cross, &Be1, &Be2);
	//L = fabs(D3DXVec3Dot(&Interval, &Cross));
	//if (L > rA + rB)
	//	return false;

	//// 分離軸 : C21
	//D3DXVec3Cross(&Cross, &NAe2, &NBe1);
	//rA = LenSegOnSeparateAxis(&Cross, &Ae1, &Ae3);
	//rB = LenSegOnSeparateAxis(&Cross, &Be2, &Be3);
	//L = fabs(D3DXVec3Dot(&Interval, &Cross));
	//if (L > rA + rB)
	//	return false;

	//// 分離軸 : C22
	//D3DXVec3Cross(&Cross, &NAe2, &NBe2);
	//rA = LenSegOnSeparateAxis(&Cross, &Ae1, &Ae3);
	//rB = LenSegOnSeparateAxis(&Cross, &Be1, &Be3);
	//L = fabs(D3DXVec3Dot(&Interval, &Cross));
	//if (L > rA + rB)
	//	return false;

	//// 分離軸 : C23
	//D3DXVec3Cross(&Cross, &NAe2, &NBe3);
	//rA = LenSegOnSeparateAxis(&Cross, &Ae1, &Ae3);
	//rB = LenSegOnSeparateAxis(&Cross, &Be1, &Be2);
	//L = fabs(D3DXVec3Dot(&Interval, &Cross));
	//if (L > rA + rB)
	//	return false;

	//// 分離軸 : C31
	//D3DXVec3Cross(&Cross, &NAe3, &NBe1);
	//rA = LenSegOnSeparateAxis(&Cross, &Ae1, &Ae2);
	//rB = LenSegOnSeparateAxis(&Cross, &Be2, &Be3);
	//L = fabs(D3DXVec3Dot(&Interval, &Cross));
	//if (L > rA + rB)
	//	return false;

	//// 分離軸 : C32
	//D3DXVec3Cross(&Cross, &NAe3, &NBe2);
	//rA = LenSegOnSeparateAxis(&Cross, &Ae1, &Ae2);
	//rB = LenSegOnSeparateAxis(&Cross, &Be1, &Be3);
	//L = fabs(D3DXVec3Dot(&Interval, &Cross));
	//if (L > rA + rB)
	//	return false;

	//// 分離軸 : C33
	//D3DXVec3Cross(&Cross, &NAe3, &NBe3);
	//rA = LenSegOnSeparateAxis(&Cross, &Ae1, &Ae2);
	//rB = LenSegOnSeparateAxis(&Cross, &Be1, &Be2);
	//L = fabs(D3DXVec3Dot(&Interval, &Cross));
	//if (L > rA + rB)
	//	return false;

	//// 分離平面が存在しないので「衝突している」
	return true;
}