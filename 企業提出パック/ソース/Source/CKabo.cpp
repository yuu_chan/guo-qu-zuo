#include "Game.h"
#include "CKabo.h"


CKabo::CKabo()
{

}

CKabo::~CKabo()
{
}

void CKabo::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	m_Mesh = GET_XFILE_LOAD_MANAGER.Load("Data/XFile/Object/Kabo.x", _lpD3DDevice);

	

	//	中心点のずれ
	m_GroundAdjustment = 2.0f;

	//	吐き出す位置
	m_SpitAdjustment = 1.5f;
}

void CKabo::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	共通の移動処理
	ObjectMove();



	m_mWorld.CreateMove(&m_vPos);
}

void CKabo::Release()
{

}