#include "Game.h"


CLargeRock::CLargeRock()
{

}


CLargeRock::~CLargeRock()
{
	//	開放処理を行う
	m_spRigidBox->Release();

	m_spRigidBox = nullptr;
}


void CLargeRock::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	メッシュ読み込み
	m_Mesh = GET_XFILE_LOAD_MANAGER.Load("Data/XFile/Object/LargeRock.x", _lpD3DDevice);


	//------------------
	//	立方体剛体を登録
	//------------------

	m_spRigidBox = make_shared<CBulletObj_Box>();

	CRigidSupervise::CreateRigidBox(m_spRigidBox, m_Mesh, m_vPos, m_Id);

	m_spRigidBox->SetUserPointer(this);

	m_spRigidBox->GetMatrix(m_mWorld);

	m_Mesh->GetMeshState()->m_EnableInk = true;
}


void CLargeRock::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	共通処理
	ObjectMove();


	if (m_UpdateTag == "PHit")
	{
		m_vPos = m_mWorld.GetPos();
		m_spRigidBox->SetMatrix(m_mWorld);
	}
	else if (m_UpdateTag == "")
	{
		m_spRigidBox->GetMatrix(m_mWorld);
		m_vPos = m_mWorld.GetPos();
	}

	//	タグを初期化しておく
	m_UpdateTag = "";
}


void CLargeRock::Release()
{

}

