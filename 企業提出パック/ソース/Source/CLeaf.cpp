#include "Game.h"

//----------------
//  コンストラクタ
//----------------
CLeaf::CLeaf()
{

}

//--------------
//  デストラクタ
//--------------
CLeaf::~CLeaf()
{

}

//--------
//  初期化
//--------
void CLeaf::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	メッシュ読み込み
	m_Mesh = GET_XFILE_LOAD_MANAGER.Load("Data/XFile/Object/Leaf.x", _lpD3DDevice);

	//	自己発行力を抑える
	m_Mesh->GetMeshState()->m_EmissivePower = 0.2f;
}

//------
//  更新
//------
void CLeaf::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{

	//	拡大サイズをセット
	m_mWorld.CreateScale(&m_vScalSize);

	//	座標をセット
	m_mWorld.Move(&m_vPos);

	//	回転軸Xをセット
	m_mWorld.RotateX_Local(m_vAng.x);

	//	回転軸Yをセット
	m_mWorld.RotateY_Local(m_vAng.y);

	//	回転軸Zをセット
	m_mWorld.RotateZ_Local(m_vAng.z);

}

//------
//  解放
//------
void CLeaf::Release()
{

}

