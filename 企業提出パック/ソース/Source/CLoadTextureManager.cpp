#include "CLoadTextureManager.h"
#include "Texture.h"


CTextureManager::CTextureManager()
{

}


CTextureManager::~CTextureManager()
{
	AllClear();
}


shared_ptr<CTexture> CTextureManager::Load(
	string				_FileName,
	LPDIRECT3DDEVICE9	_lpD3DDevice)
{
	//----------------
	//	読み込みを行う
	//----------------

	//	中身が入っているかどうかを調べる
	if (m_mapTexture.find(_FileName) != m_mapTexture.end())
	{
		//	入っていた場合、配列の中のデータを返す
		return m_mapTexture[_FileName];
	}


	//----------------------------------------
	//	中身が入っていなければ、新しく格納する
	//----------------------------------------

	//	テクスチャー型の変数をインスタンス化
	shared_ptr<CTexture> lpTexture = make_shared<CTexture>();//MyNew CTexture();

	//	読み込みを行う
	lpTexture->LoadTexture((char*)_FileName.c_str());

	m_mapTexture.insert(pair<string, shared_ptr<CTexture>>(_FileName, lpTexture));

	
	//正しくロードできなかった場合
	if (lpTexture->GetTex() == nullptr)
	{
		//	警告メッセージを出す
		MessageBox(NULL, "テクスチャーが正しく読み込まれませんでした。", "警告", MB_OK);

		//	強制終了フラグを立てる
		APP.m_bEndFlag = true;

		//	何も無いのでNULLを返す
		return nullptr;
	}


	//	正しく読み込まれればデータを返す
	return lpTexture;
}


shared_ptr<CTexture> CTextureManager::LoadEx(
	string		_FileName,
	int			_TEXTURE_WIDTH_SIZE,
	int			_TEXTURE_HEIGHT_SIZE,
	bool		_EnableCollor, 
	int			_a ,
	int			_r ,
	int			_g ,
	int			_b 
	)
{
	//--------------------------------
	//	サイズ指定などの読み込みを行う
	//--------------------------------

	//	中身が入っているかどうかを調べる
	if (m_mapTexture.find(_FileName) != m_mapTexture.end())
	{
		//	入っていた場合、配列の中のデータを返す
		return m_mapTexture[_FileName];
	}


	//----------------------------------------
	//	中身が入っていなければ、新しく格納する
	//----------------------------------------

	//	テクスチャー型の変数をインスタンス化
	shared_ptr<CTexture> lpTexture = make_shared<CTexture>();//MyNew CTexture();


	//------------------------------------
	//	アルファ指定をするかどうかを決める
	//------------------------------------

	//	アルファ値を指定しないのなら
	if (_EnableCollor == false)
	{
		//	通常読み込みを行う
		lpTexture->LoadTextureEx(
			(char*)_FileName.c_str(),
			_TEXTURE_WIDTH_SIZE,
			_TEXTURE_HEIGHT_SIZE,
			1,
			0,
			D3DFMT_UNKNOWN,
			D3DPOOL_MANAGED,
			D3DX_FILTER_NONE,
			D3DX_DEFAULT,
			NULL,					//	アルファ値指定無し
			NULL
			);
	}
	//	アルファ値を指定したなら
	else if (_EnableCollor == true)
	{
		//	通常読み込みを行う
		lpTexture->LoadTextureEx(
			(char*)_FileName.c_str(),
			_TEXTURE_WIDTH_SIZE,
			_TEXTURE_HEIGHT_SIZE,
			1,
			0,
			D3DFMT_UNKNOWN,
			D3DPOOL_MANAGED,
			D3DX_FILTER_NONE,
			D3DX_DEFAULT,
			D3DCOLOR_ARGB(_a, _r, _g, _b),					//	アルファ値指定あり
			NULL
			);
	}


	//	マップに追加
	m_mapTexture.insert(pair<string, shared_ptr<CTexture>>(_FileName, lpTexture));


	//正しくロードできなかった場合
	if (lpTexture->GetTex() == nullptr)
	{
		//	警告メッセージを出す
		MessageBox(NULL, "テクスチャーが正しく読み込まれませんでした。", "警告", MB_OK);

		//	強制終了フラグを立てる
		APP.m_bEndFlag = true;

		//	何も無いのでNULLを返す
		return nullptr;
	}


	//	正しく読み込まれればデータを返す
	return lpTexture;
}


shared_ptr<CTexture> CTextureManager::Get(string _FileName)
{
	//	読み込まれた指定の画像があれば返す
	if (m_mapTexture.find(_FileName) != m_mapTexture.end())
	{
		return m_mapTexture[_FileName];
	}

	//	無ければ何も返さない
	return nullptr;
}


void CTextureManager::AllClear()
{
	//	マップ用のイテレーターを生成
	map<string, shared_ptr<CTexture>>::iterator MapIt;

	//	一番先頭を渡す
	MapIt = m_mapTexture.begin();
	
	//	アドレスの後尾に到達するまで回す
	while (MapIt != m_mapTexture.end())
	{
		
		//	マップの二つ目のクラスがnullじゃなければ
		if (MapIt->second != nullptr)
		{
			//	現在イテレーターが指している場所のデータを破棄する
			//delete MapIt->second;

			//	破棄した後に、ゴミが入らないようにNULLを入れておく
			MapIt->second = nullptr;
		}


		//	次のアドレスへ
		++MapIt;
	}

	//	マップを破棄する
	m_mapTexture.clear();
}