#include "CLoadXFileManager.h"
#include "Game.h"


CXFileManager::CXFileManager()
{

}


CXFileManager::~CXFileManager()
{
	AllClear();
}


shared_ptr<CMeshObject> CXFileManager::Load(
	string				_FileName,
	LPDIRECT3DDEVICE9	_lpD3DDevice
	)
{
	//----------------
	//	読み込みを行う
	//----------------

	//	中身が入っているかどうかを調べる
	if (m_mapXFile.find(_FileName) != m_mapXFile.end())
	{
		//	入っていた場合、配列の中のデータを返す
		return m_mapXFile[_FileName];
	}


	//----------------------------------------
	//	中身が入っていない場合は新しく格納する
	//----------------------------------------

	shared_ptr<CMeshObject> lpXFile = make_shared<CMeshObject>();
	
	lpXFile->LoadMesh((char*)_FileName.c_str(), _lpD3DDevice);
	
	m_mapXFile.insert(pair<string, shared_ptr<CMeshObject>>(_FileName, lpXFile));


	//	正しくロードできなかった場合(lpMeshを参照させる)
	if (lpXFile->GetMesh() == nullptr)
	{
		//	警告メッセージを出す
		MessageBox(NULL, "メッシュデータが正しく読み込まれませんでした、強制終了させます", "警告", MB_OK);

		//	強制終了フラグを立てる
		APP.m_bEndFlag = true;

		//	リターンさせる
		return nullptr;
	}


	//	正しく読み込まれればデータを返す
	return lpXFile;
}


shared_ptr<CMeshObject> CXFileManager::Get(string _FileNamae)
{
	if (m_mapXFile.find(_FileNamae) != m_mapXFile.end())
	{
		return m_mapXFile[_FileNamae];
	}

	return NULL;
}


void CXFileManager::AllClear()
{
	map<string, shared_ptr<CMeshObject>>::iterator MapIt;

	MapIt = m_mapXFile.begin();

	while (MapIt != m_mapXFile.end())
	{
		if ((*MapIt).second.use_count() > 1)
		{
			MessageBox(nullptr, "開放されていないメッシュが存在しています", "警告", MB_OK);
		}

		MapIt->second = nullptr;

		++MapIt;
	}

	m_mapXFile.clear();
}