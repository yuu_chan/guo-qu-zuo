#include "CMaterialData.h"


CMaterialData::CMaterialData()
	:
	m_spMeshTex(nullptr)
	,
	m_spNormalTex(nullptr)
{
	ZeroMemory(&m_Material, sizeof(m_Material));
}


CMaterialData::~CMaterialData()
{
	Release();
}


void CMaterialData::Release()
{
	ZeroMemory(&m_Material, sizeof(m_Material));

	//	参照用テクスチャを開放
	m_spMeshTex = nullptr;

	//	法線テクスチャ開放
	m_spNormalTex = nullptr;
}


void CMaterialData::LoadTexture(const LPDIRECT3DDEVICE9 _lpD3DDevice, const char* _TexName)
{
	//	メッシュテクスチャ
	m_spMeshTex = make_shared<CTexture>();
	m_spMeshTex->LoadTexture(_TexName);

	//	法線テクスチャ
	m_spNormalTex = make_shared<CTexture>();
	string NormalTexName = ConvertExtFileName(_TexName, "normal");
	m_spNormalTex->LoadTexture(NormalTexName.c_str());

}