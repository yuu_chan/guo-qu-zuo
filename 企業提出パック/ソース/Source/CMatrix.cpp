#include "CMatrix.h"



/*================================
	CMatrixClass
================================*/

CMatrix::CMatrix()
{
	D3DXMatrixIdentity(this);
}

CMatrix::CMatrix(const D3DXMATRIX &m)
{
	_11 = m._11;
	_12 = m._12;
	_13 = m._13;
	_14 = m._14;
	_21 = m._21;
	_22 = m._22;
	_23 = m._23;
	_24 = m._24;
	_31 = m._31;
	_32 = m._32;
	_33 = m._33;
	_34 = m._34;
	_41 = m._41;
	_42 = m._42;
	_43 = m._43;
	_44 = m._44;
}

CMatrix::CMatrix(
	FLOAT _11, FLOAT _12, FLOAT _13, FLOAT _14,
	FLOAT _21, FLOAT _22, FLOAT _23, FLOAT _24,
	FLOAT _31, FLOAT _32, FLOAT _33, FLOAT _34,
	FLOAT _41, FLOAT _42, FLOAT _43, FLOAT _44)
{
	this->_11 = _11;
	this->_12 = _12;
	this->_13 = _13;
	this->_14 = _14;
	this->_21 = _21;
	this->_22 = _22;
	this->_23 = _23;
	this->_24 = _24;
	this->_31 = _31;
	this->_32 = _32;
	this->_33 = _33;
	this->_34 = _34;
	this->_41 = _41;
	this->_42 = _42;
	this->_43 = _43;
	this->_44 = _44;
}

CMatrix::~CMatrix()
{

}


/*=================================================
	行列のZ方向をLookWayの方向に向ける。
	LoockWay -> 向きたい方向ベクトルのアドレス
	Up		 -> 上方向のベクトルのアドレス
=================================================*/
void CMatrix::SetLookAt(
	const CVector3* _LookWay, 
	const CVector3* _Up)
{

	//----------
	//	変数宣言
	//----------
	CVector3 Vec, VecX, VecY, VecZ;
	CVector3 Look;


	//--------
	//	正規化
	//--------
	CVector3::Normalize(&Look, _LookWay);


	//----------------
	//  外積を計算する
	//----------------
	CVector3::Cross(&Vec, _Up, &Look);
	//	Vecを正規化する
	Vec.Normalize();
	//	Vecの長さを調べる
	if (Vec.Length() == 0)
		Vec.x = 1;


	//---------------------
	//  外積でYベクトル算出
	//---------------------
	CVector3::Cross(&VecX, &Look, &VecY);
	//	VecYを正規化する
	VecY.Normalize();
	//  VecYの長さを調べる
	if (VecY.Length() == 0)
		VecY.y = 1;


	//----------------------
	//  行列の回転部分を設定
	//----------------------
	_11 = Vec.x;
	_12 = Vec.y;
	_13 = Vec.z;

	_21 = VecY.x;
	_22 = VecY.y;
	_23 = VecY.z;

	_31 = Look.x;
	_32 = Look.y;
	_33 = Look.z;
}

/*=========================================================
	 行列のZ方向をTargetPosの位置を見るように、向ける。
　	 TargetPos   ->		見たい座標
　	 Up			 ->		上方向のベクトルのアドレス
=========================================================*/
void CMatrix::SetLookAtPos(
	const CVector3* _TargetPos, 
	const CVector3* _Up)
{
	//----------
	//	変数宣言
	//----------
	CVector3 Vec, VecX, VecY, VecZ;
	CVector3 Look;


	//--------------
	//  方向ベクトル
	//--------------
	Look.x = _TargetPos->x - _41;
	Look.y = _TargetPos->y - _42;
	Look.z = _TargetPos->z - _43;
	//	Lookを正規化
	Look.Normalize();


	//------
	//  外積
	//------
	CVector3::Cross(&Vec, _Up, &Look);
	//	Vecを正規化
	Vec.Normalize();


	//---------------------
	//  外積でYベクトル算出
	//---------------------
	CVector3::Cross(&VecY, &Look, &Vec);
	//	VecYを正規化
	VecY.Normalize();


	//----------------------
	//  行列の回転部分を設定
	//----------------------
	_11 = Vec.x;
	_12 = Vec.y;
	_13 = Vec.z;

	_21 = VecY.x;
	_22 = VecY.y;
	_23 = VecY.z;

	_31 = Look.x;
	_32 = Look.y;
	_33 = Look.z;
}
