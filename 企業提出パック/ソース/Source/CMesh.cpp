#include "CFace.h"
#include "CMesh.h"


CMyAllocateHierarchy::CMyAllocateHierarchy()
	:
	m_iBone(0)
	,
	m_cBone(0)
{

}


CMyAllocateHierarchy::~CMyAllocateHierarchy()
{

}


char* CMyAllocateHierarchy::CopyName(const char* _Name)
{
	//　名前登録
	char *Name = 0;
	if (!_Name || _Name[0] == '\0')
	{
		Name = MyNew char[1];
		Name[0] = '\0';
	}
	else
	{
		size_t Len = strlen(_Name);
		Name = MyNew char[strlen(_Name) + 1];
		strcpy_s(Name, strlen(_Name) + 1, _Name);
	}

	return Name;
}


HRESULT CMyAllocateHierarchy::CreateFrame(
	THIS_ LPCSTR _Name, 
	LPD3DXFRAME* _ppNewFrame)
{
	//　新しいフレームを作成する
	
	MYD3DXFRAME* NewFrame = MyNew MYD3DXFRAME;

	//　先に初期化しておく
	NewFrame->Name				= CopyName(_Name);
	NewFrame->pFrameFirstChild	= 0;
	NewFrame->pFrameSibling		= 0;
	NewFrame->pMeshContainer	= 0;
	D3DXMatrixIdentity(&NewFrame->TransformationMatrix);
	
	//　フレームに初期化しておいたものを入れておく。
	*_ppNewFrame = NewFrame;

	return D3D_OK;
}



HRESULT CMyAllocateHierarchy::CreateMeshContainer(
	THIS_ LPCSTR				_Name, 
	CONST D3DXMESHDATA*			_pMeshData, 
	CONST D3DXMATERIAL*			_pMaterials,
	CONST D3DXEFFECTINSTANCE*	_pEffectInstances, 
	DWORD						_NumMaterials,
	CONST DWORD*				_pAdjacencey, 
	LPD3DXSKININFO				_pSkinInfo,
	LPD3DXMESHCONTAINER*		_ppNewMeshContainer)
{

	MYD3DXMESHCONTAINER* NewContainer = MyNew MYD3DXMESHCONTAINER;
	
	//	名前登録
	NewContainer->Name = CopyName(_Name);

	NewContainer->pAdjacency = MyNew DWORD[_pMeshData->pMesh->GetNumFaces() * 3];

	memset(NewContainer->pAdjacency, 0, _pMeshData->pMesh->GetNumFaces() * 3 * sizeof(DWORD));

	NewContainer->MeshData.Type = _pMeshData->Type;

	//_pSkinInfo->ConvertToBlendedMesh(_pMeshData->pMesh, 0, _pAdjacencey, NewContainer->pAdjacency, 0, 0, &NewContainer->m_dwMaxFaceInfl,
	//	&NewContainer->m_dwNumBoneCombinations, &NewContainer->m_bCombinationTable, &NewContainer->MeshData.pMesh);

	NewContainer->m_OriginMesh = _pMeshData->pMesh;
	NewContainer->m_OriginMesh->AddRef();

	NewContainer->m_OriginMesh->CloneMeshFVF(D3DXMESH_MANAGED, NewContainer->m_OriginMesh->GetFVF(), m_CopyDevice, &NewContainer->MeshData.pMesh);

	NewContainer->MeshData.pMesh->GetAttributeTable(NULL, &NewContainer->m_BoneCnt);

	DeleteArray(NewContainer->m_pAttributeTable);

	NewContainer->m_pAttributeTable = MyNew D3DXATTRIBUTERANGE[NewContainer->m_BoneCnt];

	NewContainer->MeshData.pMesh->GetAttributeTable(NewContainer->m_pAttributeTable, NULL);



	//	マテリアル登録
	NewContainer->NumMaterials  = _NumMaterials;
	NewContainer->pMaterials	= MyNew D3DXMATERIAL[_NumMaterials];
	memcpy(NewContainer->pMaterials, _pMaterials, _NumMaterials * sizeof(D3DXMATERIAL));

	//	エフェクト登録
	NewContainer->pEffects = 0;
	if (_pEffectInstances)
	{
		NewContainer->pEffects					= MyNew D3DXEFFECTINSTANCE;
		NewContainer->pEffects->pEffectFilename	= CopyName(_pEffectInstances->pEffectFilename);
		NewContainer->pEffects->NumDefaults		= _pEffectInstances->NumDefaults;
		NewContainer->pEffects->pDefaults		= MyNew D3DXEFFECTDEFAULT[_pEffectInstances->NumDefaults];
		for (DWORD i = 0; i < _pEffectInstances->NumDefaults; i++)
		{
			D3DXEFFECTDEFAULT* Src  = _pEffectInstances->pDefaults + i;
			D3DXEFFECTDEFAULT* Dest = NewContainer->pEffects->pDefaults + i;
			Dest->NumBytes			= Src->NumBytes;
			Dest->Type				= Src->Type;
			Dest->pParamName		= CopyName(Src->pParamName);
			Dest->pValue			= MyNew char[Src->NumBytes];
			memcpy(Dest->pValue, Src->pValue, Src->NumBytes);
		}
	}


	//	テクスチャを読み込む
	//NewContainer->m_Textures = MyNew CTexture[NewContainer->NumMaterials];
	NewContainer->m_Textures.resize(NewContainer->NumMaterials);

	string strTexFile;
	for (DWORD i = 0; i < NewContainer->NumMaterials; i++)
	{
		//----------------------
		//	テクスチャを読み込む
		//----------------------

		//	ファイルパス
		strTexFile = "";

		//	ファイル名が入っていたら
		if (NewContainer->pMaterials[i].pTextureFilename)
		{
			strTexFile += NewContainer->pMaterials[i].pTextureFilename;
		}

		//	テクスチャ読み込み
		NewContainer->m_Textures[i].LoadTexture(strTexFile.c_str());
	}


	if (_pSkinInfo == nullptr)
	{
		char Mess[255];
		GetCurrentDirectory(255, Mess);

		MessageBox(NULL, "スキンメッシュの情報が入っていません。", "警告", MB_OK);
		APP.m_bEndFlag = true;
		return D3D_OK;
	}


	//　スキンメッシュ情報格納
	NewContainer->pSkinInfo = _pSkinInfo;

	_pSkinInfo->AddRef();
	*_ppNewMeshContainer = NewContainer;


	return D3D_OK;
}


HRESULT CMyAllocateHierarchy::DestroyMeshContainer(THIS_ LPD3DXMESHCONTAINER _pMeshContainerToFree)
{
	MYD3DXMESHCONTAINER* MeshContainer = (MYD3DXMESHCONTAINER*)_pMeshContainerToFree;

	//	作っておいたメッシュコンテナーを破棄する
	MeshContainer->MeshData.pMesh->Release();
	delete[] MeshContainer->Name;
	delete[] MeshContainer->pAdjacency;
	if (MeshContainer->pEffects)
	{
		for (DWORD i = 0; i < MeshContainer->pEffects->NumDefaults; i++)
		{
			D3DXEFFECTDEFAULT* EffectDefault = MeshContainer->pEffects->pDefaults + i;
			delete[] EffectDefault->pParamName;
			delete[] EffectDefault->pValue;
		}
		delete[] MeshContainer->pEffects->pDefaults;
		delete[] MeshContainer->pEffects->pEffectFilename;
		delete	 MeshContainer->pEffects;
	}
	delete[] MeshContainer->pMaterials;

	if (MeshContainer->pSkinInfo != NULL)
		MeshContainer->pSkinInfo->Release();

	if (MeshContainer->m_bCombinationTable)
		MeshContainer->m_bCombinationTable->Release();

	DeleteArray(MeshContainer->m_pAttributeTable);
	SafeRelease(MeshContainer->m_OriginMesh);

	delete MeshContainer;

	return D3D_OK;
}


HRESULT CMyAllocateHierarchy::DestroyFrame(THIS_ LPD3DXFRAME _pFrameToFree)
{

	if (_pFrameToFree != NULL)
	{
		//	子フレームを破棄
		if (_pFrameToFree->pFrameFirstChild)
			DestroyFrame(_pFrameToFree->pFrameFirstChild);
		//　兄弟フレームを破棄
		if (_pFrameToFree->pFrameSibling)
			DestroyFrame(_pFrameToFree->pFrameSibling);
		//	メッシュコンテナーを破棄
		if (_pFrameToFree->pMeshContainer)
			DestroyMeshContainer(_pFrameToFree->pMeshContainer);

		delete[] _pFrameToFree->Name;
		_pFrameToFree->Name = nullptr;
		delete   _pFrameToFree;
		_pFrameToFree = nullptr;
	}

	return D3D_OK;
}


//	コンストラクタ
CBounding::CBounding()
{

}

//	デストラクタ
CBounding::~CBounding()
{

}

//	値をセットする
void CBounding::SetBounding(LPD3DXMESH _lpMesh)
{
	//	メッシュの情報がなければなにもせず返す
	if (_lpMesh == nullptr)
	{
		return;
	}


	//------------------------------
	//	バウンディングボックスを計算
	//------------------------------

	//	頂点数を取得
	DWORD VertexCnt  = _lpMesh->GetNumVertices();

	//	1つの頂点のサイズ
	DWORD VertexSize = _lpMesh->GetNumBytesPerVertex();

	BYTE* p;


	//	頂点バッファをロックして頂点の先頭アドレスを取得させる
	HRESULT hr = _lpMesh->LockVertexBuffer(D3DLOCK_READONLY, (LPVOID *)& p);

	if (SUCCEEDED(hr))
	{

		//------------------------------
		//	バウンディングボックスを算出
		//------------------------------

		//	最大値と最小値を求める
		D3DXComputeBoundingBox(
			(CVector3 *)p,
			VertexCnt,
			VertexSize,
			&m_vBoundingBoxMin,
			&m_vBoundingBoxMax
			);

		//	長さを求める
		m_vBoundingBoxLen = m_vBoundingBoxMax - m_vBoundingBoxMin;


		//------------------------------
		//	バウンディングスフィアを算出
		//------------------------------

		//	中心点と半径を求める
		D3DXComputeBoundingSphere(
			(CVector3 *)p,
			VertexCnt,
			VertexSize,
			&m_vBoundingSphereCenter,
			&m_BoundingSphereRadius
			);

	}

	//	頂点バッファをアンロック
	_lpMesh->UnlockVertexBuffer();
}


CMeshObject::CMeshObject()
	:
	m_lpMesh(nullptr)
	,
	//m_Textures(nullptr)
	//,
	//m_Materials(nullptr)
	//,
	//m_spMaterials(nullptr)
	//,
	m_lpAdjacency(nullptr)
	,
	m_vWallVec(nullptr)
	,
	m_lpCloneMesh(nullptr)
	,
	m_pMeshState(nullptr)
	,
	m_pBounding(nullptr)
	,
	m_MaterialCnt(0)
	,
	m_FileName("")
	,
	m_MeshAlpha(0.0)
	,
	m_pMaterials(nullptr)
	,
	m_NumVertex(0)
	,
	m_pRootFrame(nullptr)
	,
	m_pCombination(nullptr)
	,
	m_pController(nullptr)
	,
	m_pHighController(nullptr)
	,
	m_AdvanceTime(0.0f)
	,
	m_ShiftTime(0.0f)
	,
	m_LoopTime(0.0f)
	,
	m_AnimationKey(0)
	,
	m_spNormalTex(nullptr)
{
	//	メッシュの透明度
	m_MeshAlpha = 1.0f;

	//	メッシュのステータスをインスタンス化
	m_pMeshState = make_shared<CMeshState>();

	//	バウンディング情報をインスタンス化
	m_pBounding = make_shared<CBounding>();
}


CMeshObject::~CMeshObject()
{
}


//	ソフトウェアスキニング
//	CPU側でスキンメッシュの頂点をいじる
void CMeshObject::ExecSoftSkinning()
{
	CMyAllocateHierarchy::MYD3DXMESHCONTAINER* _Container = m_pContainerArray[0];
	if (_Container->pSkinInfo){
		//------------------------
		//	ソフトウェアスキニング
		//------------------------

		// 影響ボーン配列作成
		vector<D3DXMATRIX> workMat;
		UINT offID;
		DWORD cBones = _Container->pSkinInfo->GetNumBones();

		for (UINT bn = 0; bn < cBones; bn++)
		{
			offID = _Container->m_BoneFrameArray[bn]->m_dwId;
			workMat.push_back(m_CombMatrixMap[offID]);
		}

		// ワールド変換行列は単位行列で
		CMatrix mI;

		// スキニング適用して、頂点更新
		PBYTE pbVerticesSrc;		// オリジナルメッシュの頂点バッファポインタ
		PBYTE pbVerticesDest;		// 現行メッシュの頂点バッファポインタ

		GET_DIRECT_HELPER.GetDev()->SetTransform(D3DTS_WORLD, &mI);
		_Container->m_OriginMesh->LockVertexBuffer(D3DLOCK_READONLY, (LPVOID*)&pbVerticesSrc);		// オリジナルメッシュの頂点バッファをロック
		_Container->MeshData.pMesh->LockVertexBuffer(0, (LPVOID*)&pbVerticesDest);					// 現行メッシュの頂点バッファをロック

		// 現在の行列に基づいて、ターゲットの頂点にソフトウェア スキニングを適用する
		_Container->pSkinInfo->UpdateSkinnedMesh(&workMat[0], NULL, pbVerticesSrc, pbVerticesDest);

		_Container->m_OriginMesh->UnlockVertexBuffer();
		_Container->MeshData.pMesh->UnlockVertexBuffer();
	}
}


void  CMeshObject::LoadSkinMesh(char* _FileName, LPDIRECT3DDEVICE9 _lpD3DDevice)
{

	//	アニメーションコントローラをインスタンス化
	m_pHighController = MyNew CHighLevelAnimeController();

	//	デバイスをコピーしておく
	m_Allocater.m_CopyDevice = _lpD3DDevice;

	//　スキンメッシュ対応のxファイルを読み込み
	if (FAILED(D3DXLoadMeshHierarchyFromX(_FileName, D3DXMESH_MANAGED, _lpD3DDevice, &m_Allocater, 0, (D3DXFRAME**)&m_pRootFrame, &m_pController)))
	{
		//	失敗すればメッセージをだす
		MessageBox(NULL, "読み込みに失敗しました", NULL, MB_OK);
		return;
	}

	//	メッシュの法線情報を取得する
	//CreateVertexData(_lpD3DDevice);
}


void CMeshObject::SetSkinMesh()
{
	//　メッシュコンテナーをルートフレームからコピーしておく
	GetMeshContainer();

	//　合成行列用の変数にコンテナーのバッファーを確保
	//	m_pCombination = (D3DXBONECOMBINATION *)m_pContainerArray[0]->m_bCombinationTable->GetBufferPointer();

	//　アニメーションコントローラの情報をアニメーション再生用変数にコピー
	m_pHighController->SetAnimationController(m_pController);

	//　アニメーションの情報をセットする。
	m_pHighController->AnimestionSet();

	//　ボーンにIDをセットしておく。
	SetFrameId(m_pRootFrame, m_pContainerArray[0]->pSkinInfo);

	//	バウンディング情報を取得
	m_pBounding->SetBounding(m_pContainerArray[0]->m_OriginMesh);

	for (UINT i = 0; i < m_pContainerArray.size(); i++)
	{
		if (m_pContainerArray[i]->pSkinInfo)
		{
			UINT bones = m_pContainerArray[i]->pSkinInfo->GetNumBones();
			
			for (UINT k = 0; k < bones; k++)
			{
				for (UINT n = 0; n < m_pFrameArray.size(); n++)
				{
					if (strcmp(m_pContainerArray[i]->pSkinInfo->GetBoneName(k), m_pFrameArray[n]->Name) == 0)
					{
						m_pContainerArray[i]->m_BoneFrameArray.push_back(m_pFrameArray[n] );
					}
				}
			}
		}
	}



	//	メッシュの情報をメッシュから、クローンメッシュへコピー
	m_pContainerArray[0]->m_OriginMesh->CloneMeshFVF(D3DXMESH_MANAGED | D3DXMESH_NPATCHES, D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1, GET_DIRECT_HELPER.GetDev(), &m_lpCloneMesh);

	//	メッシュを一度開放する
	m_pContainerArray[0]->m_OriginMesh->Release();

	//	クローンメッシュを、元のメッシュに入れなおす
	m_pContainerArray[0]->m_OriginMesh = m_lpCloneMesh;

	//	頂点数を確保
	m_NumVertex = m_pContainerArray[0]->m_OriginMesh->GetNumVertices();


	//------------------------
	//	法線テーブルを作成する
	//------------------------

	//	作業用変数
	//DWORD	PolyNo;		//	ポリゴン数
	DWORD	NumFace;

	NumFace = m_pContainerArray[0]->m_OriginMesh->GetNumFaces();

	//m_vWallVec = MyNew CVector3[NumFace];
	unique_ptr<CVector3[]> m(new CVector3[NumFace]);

	m_vWallVec = move(m);

	for (DWORD i = 0; i < NumFace; i++)
	{


		//------------------------------
		//	インデックスバッファをいじる
		//------------------------------

		//	作業用変数
		WORD* pIndex;

		//	ロックをはずす
		m_pContainerArray[0]->m_OriginMesh->LockIndexBuffer(0, (LPVOID*)&pIndex);

		//	ほしいポリゴンの頂点
		WORD VertexNo[3];

		//	PolyNo番目のポリゴンを構成する3つの頂点番号
		VertexNo[0] = *(pIndex + i * 3 + 0);
		VertexNo[1] = *(pIndex + i * 3 + 1);
		VertexNo[2] = *(pIndex + i * 3 + 2);

		//	ロックする
		m_pContainerArray[0]->m_OriginMesh->UnlockIndexBuffer();


		//------------------------------
		//	バーテックスバッファをいじる
		//------------------------------

		//	構造体変数を宣言
		CLONEVERTEX* pVertex;

		//	ロックをはずす VertexNo[n]をいじることが可能になる
		m_pContainerArray[0]->m_OriginMesh->LockVertexBuffer(0, (LPVOID*)&pVertex);


		//-------------------------------------------------
		//	ポリゴンに垂直な法線が得られることは少ないので、
		//	座標から法線を計算して、取り出す
		//-------------------------------------------------

		//	Pos[n]番目をいじる
		CVector3 vPos[3];

		vPos[0] = (pVertex + VertexNo[0])->m_Pos;
		vPos[1] = (pVertex + VertexNo[1])->m_Pos;
		vPos[2] = (pVertex + VertexNo[2])->m_Pos;

		m_pContainerArray[0]->m_OriginMesh->UnlockVertexBuffer();


		//----------------------------------
		//	三角ポリゴンに沿う二つのベクトル
		//----------------------------------

		//	ベクトル変数
		CVector3 vVec1, vVec2;

		//	一つ目のベクトルに、Posの1番目と0番目を引いた値を代入
		vVec1 = vPos[1] - vPos[0];

		//	二つ目のベクトルに、Posの2番目と0番目を引いた値を代入
		vVec2 = vPos[2] - vPos[0];


		//--------------------------------------------
		//	二つのベクトルに対して垂直なベクトル(外積)
		//--------------------------------------------

		//	外積計算用変数
		CVector3 vTmpVec;

		//	外籍を求める
		D3DXVec3Cross(&vTmpVec, &vVec1, &vVec2);

		//	正規化
		D3DXVec3Normalize(&vTmpVec, &vTmpVec);

		//	法線テーブル、WallVec(壁の法線)に代入
		m_vWallVec[i] = vTmpVec;

	}

}


void CMeshObject::UpdateSkinMesh(D3DXMATRIX _mWorld)
{


	//	アニメーション再生速度を設定
	m_pHighController->AdvanceTime(m_AdvanceTime);

	//	ループさせる時間をセット
	m_pHighController->SetLoopTime(m_AnimationKey, m_LoopTime);

	//	アニメーションを切り替える。
	m_pHighController->ChangeAnimation(m_AnimationKey);

	//	Shiftにかかる時間をセット
	m_pHighController->SetShiftTime(m_AnimationKey, m_ShiftTime);


	//　行列合成の更新
	UpDateCombMatrix(m_CombMatrixMap, m_pRootFrame, _mWorld);


	//	メッシュの情報をメッシュから、クローンメッシュへコピー
	m_pContainerArray[0]->m_OriginMesh->CloneMeshFVF(D3DXMESH_MANAGED | D3DXMESH_NPATCHES, D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1, GET_DIRECT_HELPER.GetDev(), &m_lpCloneMesh);

	//	メッシュを一度開放する
	m_pContainerArray[0]->m_OriginMesh->Release();

	//	クローンメッシュを、元のメッシュに入れなおす
	m_pContainerArray[0]->m_OriginMesh = m_lpCloneMesh;

	//	頂点数を確保
	m_NumVertex = m_pContainerArray[0]->m_OriginMesh->GetNumVertices();


	//------------------------
	//	法線テーブルを作成する
	//------------------------

	//	作業用変数
	//DWORD	PolyNo;		//	ポリゴン数
	DWORD	NumFace;

	NumFace = m_pContainerArray[0]->m_OriginMesh->GetNumFaces();


	//m_vWallVec = MyNew CVector3[NumFace];

	//for (DWORD i = 0; i < NumFace; i++)
	//{


	//	//------------------------------
	//	//	インデックスバッファをいじる
	//	//------------------------------

	//	//	作業用変数
	//	WORD* pIndex;

	//	//	ロックをはずす
	//	m_pContainerArray[0]->m_OriginMesh->LockIndexBuffer(0, (LPVOID*)&pIndex);

	//	//	ほしいポリゴンの頂点
	//	WORD VertexNo[3];

	//	//	PolyNo番目のポリゴンを構成する3つの頂点番号
	//	VertexNo[0] = *(pIndex + i * 3 + 0);
	//	VertexNo[1] = *(pIndex + i * 3 + 1);
	//	VertexNo[2] = *(pIndex + i * 3 + 2);

	//	//	ロックする
	//	m_pContainerArray[0]->m_OriginMesh->UnlockIndexBuffer();


	//	//------------------------------
	//	//	バーテックスバッファをいじる
	//	//------------------------------

	//	//	構造体変数を宣言
	//	CLONEVERTEX* pVertex;

	//	//	ロックをはずす VertexNo[n]をいじることが可能になる
	//	m_pContainerArray[0]->m_OriginMesh->LockVertexBuffer(0, (LPVOID*)&pVertex);


	//	//-------------------------------------------------
	//	//	ポリゴンに垂直な法線が得られることは少ないので、
	//	//	座標から法線を計算して、取り出す
	//	//-------------------------------------------------

	//	//	Pos[n]番目をいじる
	//	CVector3 vPos[3];

	//	vPos[0] = (pVertex + VertexNo[0])->m_Pos;
	//	vPos[1] = (pVertex + VertexNo[1])->m_Pos;
	//	vPos[2] = (pVertex + VertexNo[2])->m_Pos;

	//	m_pContainerArray[0]->m_OriginMesh->UnlockVertexBuffer();


	//	//----------------------------------
	//	//	三角ポリゴンに沿う二つのベクトル
	//	//----------------------------------

	//	//	ベクトル変数
	//	CVector3 vVec1, vVec2;

	//	//	一つ目のベクトルに、Posの1番目と0番目を引いた値を代入
	//	vVec1 = vPos[1] - vPos[0];

	//	//	二つ目のベクトルに、Posの2番目と0番目を引いた値を代入
	//	vVec2 = vPos[2] - vPos[0];


	//	//--------------------------------------------
	//	//	二つのベクトルに対して垂直なベクトル(外積)
	//	//--------------------------------------------

	//	//	外積計算用変数
	//	CVector3 vTmpVec;

	//	//	外籍を求める
	//	D3DXVec3Cross(&vTmpVec, &vVec1, &vVec2);

	//	//	正規化
	//	D3DXVec3Normalize(&vTmpVec, &vTmpVec);

	//	//	法線テーブル、WallVec(壁の法線)に代入
	//	m_vWallVec[i] = vTmpVec;

	//}

}


void CMeshObject::RecGetMeshContainer(D3DXFRAME* _Frame)
{
	//	メッシュコンテナーが見つかったら保存
	if (_Frame->pMeshContainer)
	{
		m_pContainerArray.push_back( (CMyAllocateHierarchy::MYD3DXMESHCONTAINER *)_Frame->pMeshContainer);
	}

	// Boneをプッシュバックしておく
	m_pFrameArray.push_back((CMyAllocateHierarchy::MYD3DXFRAME*)_Frame);

	//	子フレームを保存
	if (_Frame->pFrameFirstChild)
		RecGetMeshContainer(_Frame->pFrameFirstChild);

	//	兄弟フレームを保存
	if (_Frame->pFrameSibling)
		RecGetMeshContainer(_Frame->pFrameSibling);
}


void CMeshObject::GetMeshContainer()
{
	//	コンテナーを初期化
	m_pContainerArray.clear();
	//	フレームを初期化
	m_pFrameArray.clear();

	//	ルートフレームを返す
	RecGetMeshContainer(m_pRootFrame);
}


void CMeshObject::SetFrameId(CMyAllocateHierarchy::MYD3DXFRAME* _Frame, ID3DXSkinInfo* _SkinInfof)
{
	std::map<std::string, DWORD> NameToIdMap;

	//	ボーンのIDを保存しておく
	for (DWORD i = 0; i < _SkinInfof->GetNumBones(); i++)
		NameToIdMap[_SkinInfof->GetBoneName(i)] = i;

	//	ローカル構造体
	struct Create
	{
		static void FrameTraverse(std::map<std::string, DWORD> _NameToIdMap, ID3DXSkinInfo* _SkinInfo, CMyAllocateHierarchy::MYD3DXFRAME* _Frame)
		{
			if (_NameToIdMap.find(_Frame->Name) != _NameToIdMap.end())
			{
				_Frame->m_dwId = _NameToIdMap[_Frame->Name];
				_Frame->m_mOffsetMatrix = *_SkinInfo->GetBoneOffsetMatrix(_Frame->m_dwId);
			}

			if (_Frame->pFrameFirstChild)
				FrameTraverse(_NameToIdMap, _SkinInfo, (CMyAllocateHierarchy::MYD3DXFRAME*)_Frame->pFrameFirstChild);
			if (_Frame->pFrameSibling)
				FrameTraverse(_NameToIdMap, _SkinInfo, (CMyAllocateHierarchy::MYD3DXFRAME*)_Frame->pFrameSibling);
		}
	};

	Create::FrameTraverse(NameToIdMap, _SkinInfof, _Frame);
}


void CMeshObject::UpDateCombMatrix(
	std::map<DWORD, D3DXMATRIX>&		_CombMatrixMap,
	CMyAllocateHierarchy::MYD3DXFRAME*	_Frame,
	D3DXMATRIX&							_CharMatrix)
{
	//	ローカル構造体
	struct Update
	{
		static void FrameTraverse(std::map<DWORD, D3DXMATRIX>& _CombMatrixMap, D3DXMATRIX& _ParentBoneMatrix, CMyAllocateHierarchy::MYD3DXFRAME* _Frame)
		{
			D3DXMATRIX &LocalBoneMatrix = _Frame->TransformationMatrix;
			D3DXMATRIX  BoneMatrix = LocalBoneMatrix * _ParentBoneMatrix;

			if (_Frame->m_dwId != 0xffffffff)
				_CombMatrixMap[_Frame->m_dwId] = _Frame->m_mOffsetMatrix * BoneMatrix;
			if (_Frame->pFrameFirstChild)
				FrameTraverse(_CombMatrixMap, BoneMatrix, (CMyAllocateHierarchy::MYD3DXFRAME*)_Frame->pFrameFirstChild);
			if (_Frame->pFrameSibling)
				FrameTraverse(_CombMatrixMap, _ParentBoneMatrix, (CMyAllocateHierarchy::MYD3DXFRAME*)_Frame->pFrameSibling);
		}
	};

	//	行列をすべて合成
	Update::FrameTraverse(_CombMatrixMap, _CharMatrix, _Frame);
}


void CMeshObject::DrawSkinMesh(LPDIRECT3DDEVICE9	_lpD3DDevice)
{
	CMyAllocateHierarchy::MYD3DXMESHCONTAINER*	_Container;
	D3DXBONECOMBINATION*						_Combinations;
	std::map<DWORD, D3DXMATRIX>					_CombMatrixMap;

	_Container = m_pContainerArray[0];

	_Combinations = m_pCombination;

	_CombMatrixMap = m_CombMatrixMap;
		
	//	スキンメッシュ対応のメッシュを描画する
	if (_Container->pSkinInfo != NULL)
	{
		//for (DWORD i = 0; i < _Container->m_dwNumBoneCombinations; i++)
		//{
		//	DWORD boneNum = 0;
		//	DWORD AttribID = _Combinations[i].AttribId;
		//	for (DWORD j = 0; j < _Container->m_dwMaxFaceInfl; j++)
		//	{
		//		DWORD id = _Combinations[i].BoneId[j];
		//		if (id != UINT_MAX)
		//		{
		//			_lpD3DDevice->SetTransform(D3DTS_WORLDMATRIX(j), &_CombMatrixMap[id]);
		//			boneNum++;
		//		}
		//	}
		//	_lpD3DDevice->SetRenderState(D3DRS_VERTEXBLEND, boneNum - 1);
		//	_lpD3DDevice->SetMaterial(&(_Container->pMaterials[AttribID].MatD3D));
		//	_Container->MeshData.pMesh->DrawSubset(i);
		//	//	NULLが出るまでDrawする
		//	//_Container->pNextMeshContainer->MeshData.pMesh->DrawSubset(i);
		//}

		/*
		//------------------------
		//	ソフトウェアスキニング
		//------------------------

		// 影響ボーン配列作成
		vector<D3DXMATRIX> workMat;
		UINT offID;
		DWORD cBones = _Container->pSkinInfo->GetNumBones();

		for (UINT bn = 0; bn < cBones; bn++)
		{
			offID = _Container->m_BoneFrameArray[bn]->m_dwId;
			workMat.push_back(_CombMatrixMap[offID]);
		}

		// ワールド変換行列は単位行列で
		CMatrix mI;

		// スキニング適用して、頂点更新
		PBYTE pbVerticesSrc;		// オリジナルメッシュの頂点バッファポインタ
		PBYTE pbVerticesDest;		// 現行メッシュの頂点バッファポインタ

		_lpD3DDevice->SetTransform(D3DTS_WORLD, &mI);
		_Container->m_OriginMesh->LockVertexBuffer(D3DLOCK_READONLY, (LPVOID*)&pbVerticesSrc);		// オリジナルメッシュの頂点バッファをロック
		_Container->MeshData.pMesh->LockVertexBuffer(0, (LPVOID*)&pbVerticesDest);					// 現行メッシュの頂点バッファをロック

			// 現在の行列に基づいて、ターゲットの頂点にソフトウェア スキニングを適用する
			_Container->pSkinInfo->UpdateSkinnedMesh(&workMat[0], NULL, pbVerticesSrc, pbVerticesDest);

		_Container->m_OriginMesh->UnlockVertexBuffer();
		_Container->MeshData.pMesh->UnlockVertexBuffer();
		*/

		ExecSoftSkinning();

		// 描画
		/*
		for (UINT iAttrib = 0; iAttrib < _Container->m_BoneCnt; iAttrib++)
		{
			// マテリアル設定
			_lpD3DDevice->SetMaterial(&(_Container->pMaterials[_Container->m_pAttributeTable[iAttrib].AttribId].MatD3D));
			// テクスチャ設定
//			lpD3DDev->SetTexture(0, pMc->m_Materials[pMc->SoftSkinData.pAttributeTable[iAttrib].AttribId].m_lpMeshTex->GetTex());
			// サブセット描画
			_Container->MeshData.pMesh->DrawSubset(_Container->m_pAttributeTable[iAttrib].AttribId);
		}
		*/
		for (UINT iAttrib = 0; iAttrib < _Container->NumMaterials; iAttrib++)
		{
			//	マテリアル設定
			_lpD3DDevice->SetMaterial(&(_Container->pMaterials[iAttrib].MatD3D));
			
			//	テクスチャ設定
			_lpD3DDevice->SetTexture(0, _Container->m_Textures[iAttrib].GetTex());
			
			//	サブセット描画
			_Container->MeshData.pMesh->DrawSubset(iAttrib);
		}

	}
}


BOOL CMeshObject::LoadMesh(const char *lpFileName, LPDIRECT3DDEVICE9 lpD3DDev)
{

	// パス、ファイル名分解
	std::string Path;
	char szDir[_MAX_PATH];
	char szFname[_MAX_FNAME];
	char szExt[_MAX_EXT];
	_splitpath(lpFileName, NULL, szDir, szFname, szExt);
	Path = szDir;
	m_FileName = szFname;
	m_FileName += szExt;

	HRESULT hr;

	LPD3DXBUFFER	pD3DXMtrlBuffer;

	// Xファイル読み込み
	DWORD mateNum;
	hr = D3DXLoadMeshFromX(lpFileName,
		D3DXMESH_MANAGED,
		lpD3DDev,
		&m_lpAdjacency,
		&pD3DXMtrlBuffer, // マテリアルデータを格納するための変数を指定
		NULL,
		&mateNum, // マテリアル数を格納するための変数を指定
		&m_lpMesh);	// メッシュデータを格納するための変数を指定

	if (FAILED(hr))
	{
		MessageBox(NULL, "読み込みに失敗しました", NULL, MB_OK);
		return FALSE;
	}

	// マテリアル数
	m_MaterialCnt = mateNum;

	// pD3DXMtrlBuffer から、質感やテクスチャーの情報を読み取る
	D3DXMATERIAL* d3dxMaterials = (D3DXMATERIAL*)pD3DXMtrlBuffer->GetBufferPointer();


	/*
	// メッシュのマテリアルの合計数に基づいて、マテリアル配列を作成
	//m_Textures = new CTexture[mateNum];
	//m_spMaterials = make_shared<CMaterialData>()[mateNum];
	//m_pMaterials = new D3DMATERIAL9[mateNum];
	//m_pMaterials = MyNew CMaterialData[mateNum];
	//m_pMaterials = std::shared_ptr<CMaterialData[]>(new CMaterialData[mateNum]);
	//shared_ptr<CMaterialData[]> a(new CMaterialData[10]);
	//unique_ptr<CMaterialData[]> m(new CMaterialData[10]);
	*/


	unique_ptr<CMaterialData[]> m(new CMaterialData[mateNum]);

	m_pMaterials = move(m);

	// マテリアルコピー、テクスチャ読み込み
	std::string strTexFile;
	for (DWORD i = 0; i<mateNum; i++){
		// 材質
		//m_Materials[i] = d3dxMaterials[i].MatD3D;			// 質感のコピー
		//m_Materials[i].Ambient = m_Materials[i].Diffuse;	// AmbientはDiffuseのコピーを使用
		m_pMaterials[i].m_Material = d3dxMaterials[i].MatD3D;
		m_pMaterials[i].m_Material.Ambient = m_pMaterials[i].m_Material.Diffuse;



		// テクスチャ
		strTexFile = "";// Path;
		if (d3dxMaterials[i].pTextureFilename)
		{
			strTexFile += d3dxMaterials[i].pTextureFilename;
		}

		// テクスチャ読み込み
		//m_Textures[i].LoadTexture(strTexFile.c_str(), );
		m_pMaterials[i].LoadTexture(GET_DIRECT_HELPER.GetDev(), strTexFile.c_str());
	}

	// 作業用マテリアル バッファ開放
	SafeRelease(pD3DXMtrlBuffer);

	//	バウンディング情報を取得
	m_pBounding->SetBounding(m_lpMesh);

	//	メッシュの法線情報を取得する
	CreateVertexData(lpD3DDev);


	//	関数終了TRUEを返す
	return TRUE;
}


void CMeshObject::Release()
{
	//	スキンメッシュフレームを破棄
	m_Allocater.DestroyFrame(m_pRootFrame);

	//	自作アニメーションコントローラのポインターを破棄
	SAFE_DELETE(m_pHighController);

	//	アニメーションコントローラを開放
	SAFE_RELEASE(m_pController);

	//	メッシュステータスのポインターを破棄
	m_pMeshState = nullptr;

	//	面情報を開放
	m_vecFaceTbl.clear();

	//	バウンディング情報を開放
	m_pBounding = nullptr;

	//	面の法線情報を開放
	m_vWallVec = nullptr;

	//	法線テクスチャを開放
	m_spNormalTex = nullptr;

	//	マテリアルカウントを初期化
	m_MaterialCnt = 0;

	//	マテリアル情報開放
	m_pMaterials = nullptr;

	// 隣接面データ削除
	SAFE_RELEASE(m_lpAdjacency);

	// メッシュ オブジェクト解放
	SAFE_RELEASE(m_lpMesh);

	//	ファイルネームクリア
	m_FileName = "";
}


void CMeshObject::CreateVertexData(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	メッシュの情報をメッシュから、クローンメッシュへコピー
	m_lpMesh->CloneMeshFVF(D3DXMESH_MANAGED | D3DXMESH_NPATCHES, D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1, _lpD3DDevice, &m_lpCloneMesh);

	//	メッシュを一度開放する
	m_lpMesh->Release();

	//	クローンメッシュを、元のメッシュに入れなおす
	m_lpMesh = m_lpCloneMesh;

	//	頂点数を確保
	m_NumVertex = m_lpMesh->GetNumVertices();


	//------------------------
	//	法線テーブルを作成する
	//------------------------
	
	//	作業用変数
	//DWORD	PolyNo;		//	ポリゴン数
	DWORD	NumFace;

	NumFace = m_lpMesh->GetNumFaces();


	if (m_vWallVec == nullptr)
	{
		unique_ptr<CVector3[]> m(new CVector3[NumFace]);

		m_vWallVec = move(m);
	}
		//m_vWallVec = MyNew CVector3[NumFace];


	for (DWORD i = 0; i < NumFace; i++)
	{


		//------------------------------
		//	インデックスバッファをいじる
		//------------------------------

		//	作業用変数
		WORD* pIndex;

		//	ロックをはずす
		m_lpMesh->LockIndexBuffer(0, (LPVOID*)&pIndex);	

		//	ほしいポリゴンの頂点
		WORD VertexNo[3];

		//	PolyNo番目のポリゴンを構成する3つの頂点番号
		VertexNo[0] = *(pIndex + i * 3 + 0);
		VertexNo[1] = *(pIndex + i * 3 + 1);
		VertexNo[2] = *(pIndex + i * 3 + 2);

		//	ロックする
		m_lpMesh->UnlockIndexBuffer();


		//------------------------------
		//	バーテックスバッファをいじる
		//------------------------------

		//	構造体変数を宣言
		CLONEVERTEX* pVertex;

		//	ロックをはずす VertexNo[n]をいじることが可能になる
		m_lpMesh->LockVertexBuffer(0, (LPVOID*)&pVertex);


		//-------------------------------------------------
		//	ポリゴンに垂直な法線が得られることは少ないので、
		//	座標から法線を計算して、取り出す
		//-------------------------------------------------

		//	Pos[n]番目をいじる
		CVector3 vPos[3];

		vPos[0] = (pVertex + VertexNo[0])->m_Pos;
		vPos[1] = (pVertex + VertexNo[1])->m_Pos;
		vPos[2] = (pVertex + VertexNo[2])->m_Pos;

		m_lpMesh->UnlockVertexBuffer();


		//----------------------------------
		//	三角ポリゴンに沿う二つのベクトル
		//----------------------------------

		//	ベクトル変数
		CVector3 vVec1, vVec2;

		//	一つ目のベクトルに、Posの1番目と0番目を引いた値を代入
		vVec1 = vPos[1] - vPos[0];

		//	二つ目のベクトルに、Posの2番目と0番目を引いた値を代入
		vVec2 = vPos[2] - vPos[0];


		//--------------------------------------------
		//	二つのベクトルに対して垂直なベクトル(外積)
		//--------------------------------------------
		
		//	外積計算用変数
		CVector3 vTmpVec;

		//	外籍を求める
		D3DXVec3Cross(&vTmpVec, &vVec1, &vVec2);

		//	正規化
		D3DXVec3Normalize(&vTmpVec, &vTmpVec);

		//	法線テーブル、WallVec(壁の法線)に代入
		m_vWallVec[i] = vTmpVec;

	}
}


void CMeshObject::UpdateVertexData(LPDIRECT3DDEVICE9 _lpD3DDevice, int _Id)
{
	//	メッシュの情報をメッシュから、クローンメッシュへコピー
	m_lpMesh->CloneMeshFVF(D3DXMESH_MANAGED | D3DXMESH_NPATCHES, D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1, _lpD3DDevice, &m_lpCloneMesh);

	//	メッシュを一度開放する
	m_lpMesh->Release();

	//	クローンメッシュを、元のメッシュに入れなおす
	m_lpMesh = m_lpCloneMesh;

	//	頂点数を確保
	m_NumVertex = m_lpMesh->GetNumVertices();


	//------------------------
	//	法線テーブルを作成する
	//------------------------

	//	作業用変数
	//DWORD	PolyNo;		//	ポリゴン数
	DWORD	NumFace;

	NumFace = m_lpMesh->GetNumFaces();


	if (m_vWallVec == nullptr)
	{
		//m_vWallVec = MyNew CVector3[NumFace];
		unique_ptr<CVector3[]> m(new CVector3[NumFace]);

		m_vWallVec = move(m);
	}


	for (DWORD i = 0; i < NumFace; i++)
	{


		//------------------------------
		//	インデックスバッファをいじる
		//------------------------------

		//	作業用変数
		WORD* pIndex;

		//	ロックをはずす
		m_lpMesh->LockIndexBuffer(0, (LPVOID*)&pIndex);

		//	ほしいポリゴンの頂点
		WORD VertexNo[3];

		//	PolyNo番目のポリゴンを構成する3つの頂点番号
		VertexNo[0] = *(pIndex + i * 3 + 0);
		VertexNo[1] = *(pIndex + i * 3 + 1);
		VertexNo[2] = *(pIndex + i * 3 + 2);

		//	ロックする
		m_lpMesh->UnlockIndexBuffer();


		//------------------------------
		//	バーテックスバッファをいじる
		//------------------------------

		//	構造体変数を宣言
		CLONEVERTEX* pVertex;

		//	ロックをはずす VertexNo[n]をいじることが可能になる
		m_lpMesh->LockVertexBuffer(0, (LPVOID*)&pVertex);


		//-------------------------------------------------
		//	ポリゴンに垂直な法線が得られることは少ないので、
		//	座標から法線を計算して、取り出す
		//-------------------------------------------------

		//	Pos[n]番目をいじる
		CVector3 vPos[3];

		//vPos[0] = (pVertex + VertexNo[0])->m_Pos;
		//vPos[1] = (pVertex + VertexNo[1])->m_Pos;
		//vPos[2] = (pVertex + VertexNo[2])->m_Pos;

		D3DXVec3TransformNormal(&vPos[0], &(pVertex + VertexNo[0])->m_Pos, &GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_Id)->GetWorld());
		D3DXVec3TransformNormal(&vPos[1], &(pVertex + VertexNo[1])->m_Pos, &GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_Id)->GetWorld());
		D3DXVec3TransformNormal(&vPos[2], &(pVertex + VertexNo[2])->m_Pos, &GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_Id)->GetWorld());

		m_lpMesh->UnlockVertexBuffer();


		//----------------------------------
		//	三角ポリゴンに沿う二つのベクトル
		//----------------------------------

		//	ベクトル変数
		CVector3 vVec1, vVec2;

		//	一つ目のベクトルに、Posの1番目と0番目を引いた値を代入
		vVec1 = vPos[1] - vPos[0];

		//	二つ目のベクトルに、Posの2番目と0番目を引いた値を代入
		vVec2 = vPos[2] - vPos[0];


		//--------------------------------------------
		//	二つのベクトルに対して垂直なベクトル(外積)
		//--------------------------------------------

		//	外積計算用変数
		CVector3 vTmpVec;

		//	外籍を求める
		D3DXVec3Cross(&vTmpVec, &vVec1, &vVec2);

		//	正規化
		D3DXVec3Normalize(&vTmpVec, &vTmpVec);

		//	法線テーブル、WallVec(壁の法線)に代入
		m_vWallVec[i] = vTmpVec;

	}
}


void CMeshObject::DrawMesh()
{
	// マテリアルの数ぶんループ
	for (int i = 0; i < m_MaterialCnt; i++)
	{
		// i番目のマテリアルを設定
		GET_DIRECT_HELPER.GetDev()->SetMaterial(&m_pMaterials[i].m_Material);//(&m_Materials[i]);

		// i番目のテクスチャを設定
		GET_DIRECT_HELPER.GetDev()->SetTexture(0, m_pMaterials[i].m_spMeshTex->GetTex());//m_Textures[i].GetTex());

		// メッシュを描画
		m_lpMesh->DrawSubset(i);
	}
}


void CMeshObject::CreateFaceData()
{
	//	メッシュの情報が入っていなかった場合はreturnさせておく
	if (m_lpMesh == nullptr)
	{
		return;
	}


	// 頂点数
	DWORD VertexCnt = m_lpMesh->GetNumVertices();

	// １つの頂点のサイズ
	DWORD VertexSize = m_lpMesh->GetNumBytesPerVertex();

	//	リサイズさせる
	m_vecFaceTbl.resize(m_lpMesh->GetNumFaces());

	WORD*    pIndices;
	char*    pVertices;

	
	// ロック
	m_lpMesh->LockVertexBuffer(D3DLOCK_READONLY, (void**)&pVertices);
	m_lpMesh->LockIndexBuffer(D3DLOCK_READONLY, (void**)&pIndices);


	// 面情報作成
	DWORD		i;
	DWORD		dwI;
	CVector3	v1, v2;
	CVector3*	v;

	for (i = 0; i < m_vecFaceTbl.size(); i++)
	{
		// 頂点１
		dwI						= pIndices[i * 3];
		v						= (CVector3*)(pVertices + dwI*VertexSize);

		m_vecFaceTbl[i].v[0].x	= v->x;
		m_vecFaceTbl[i].v[0].y	= v->y;
		m_vecFaceTbl[i].v[0].z	= v->z;

		// 頂点２
		dwI						= pIndices[i * 3 + 1];
		v						= (CVector3*)(pVertices + dwI*VertexSize);

		m_vecFaceTbl[i].v[1].x	= v->x;
		m_vecFaceTbl[i].v[1].y	= v->y;
		m_vecFaceTbl[i].v[1].z	= v->z;

		// 頂点３
		dwI						= pIndices[i * 3 + 2];
		v						= (CVector3*)(pVertices + dwI*VertexSize);

		m_vecFaceTbl[i].v[2].x	= v->x;
		m_vecFaceTbl[i].v[2].y	= v->y;
		m_vecFaceTbl[i].v[2].z	= v->z;

		// 面の方向を求める
		m_vecFaceTbl[i].Calc_vN();

		// 最大点、最小点
		m_vecFaceTbl[i].Calc_AABB();
	}

	// アンロック
	m_lpMesh->UnlockVertexBuffer();
	m_lpMesh->UnlockIndexBuffer();
}


void CMeshObject::CreateSkinFaceData()
{
	//	メッシュの情報が入っていなかった場合はreturnさせておく
	if (m_pContainerArray[0]->m_OriginMesh== nullptr)
	{
		return;
	}


	// 頂点数
	DWORD VertexCnt = m_pContainerArray[0]->m_OriginMesh->GetNumVertices();

	// １つの頂点のサイズ
	DWORD VertexSize = m_pContainerArray[0]->m_OriginMesh->GetNumBytesPerVertex();

	//	リサイズさせる
	m_vecFaceTbl.resize(m_pContainerArray[0]->m_OriginMesh->GetNumFaces());

	WORD*    pIndices;
	char*    pVertices;


	// ロック
	m_pContainerArray[0]->m_OriginMesh->LockVertexBuffer(D3DLOCK_READONLY, (void**)&pVertices);
	m_pContainerArray[0]->m_OriginMesh->LockIndexBuffer(D3DLOCK_READONLY, (void**)&pIndices);


	// 面情報作成
	DWORD		i;
	DWORD		dwI;
	CVector3	v1, v2;
	CVector3*	v;

	for (i = 0; i < m_vecFaceTbl.size(); i++)
	{
		// 頂点１
		dwI = pIndices[i * 3];
		v = (CVector3*)(pVertices + dwI*VertexSize);

		m_vecFaceTbl[i].v[0].x = v->x;
		m_vecFaceTbl[i].v[0].y = v->y;
		m_vecFaceTbl[i].v[0].z = v->z;

		// 頂点２
		dwI = pIndices[i * 3 + 1];
		v = (CVector3*)(pVertices + dwI*VertexSize);

		m_vecFaceTbl[i].v[1].x = v->x;
		m_vecFaceTbl[i].v[1].y = v->y;
		m_vecFaceTbl[i].v[1].z = v->z;

		// 頂点３
		dwI = pIndices[i * 3 + 2];
		v = (CVector3*)(pVertices + dwI*VertexSize);

		m_vecFaceTbl[i].v[2].x = v->x;
		m_vecFaceTbl[i].v[2].y = v->y;
		m_vecFaceTbl[i].v[2].z = v->z;

		// 面の方向を求める
		m_vecFaceTbl[i].Calc_vN();

		// 最大点、最小点
		m_vecFaceTbl[i].Calc_AABB();
	}

	// アンロック
	m_pContainerArray[0]->m_OriginMesh->UnlockVertexBuffer();
	m_pContainerArray[0]->m_OriginMesh->UnlockIndexBuffer();
}