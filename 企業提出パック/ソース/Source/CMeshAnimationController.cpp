#include  "CMeshAnimationController.h"


CHighLevelAnimeController::CHighLevelAnimeController()
{
	//　初期化
	m_pAnimeController		= NULL;	
	m_CurAnimeID			= NULL;			
	m_PreAnimeID			= NULL;
	for (DWORD i = 0; i < ANIMETION_KEY_MAX; i++)
	{
		ZeroMemory(&m_Anime[i], sizeof(HLAnimationDesc));
	}

	m_AnimationTime = 0.0f;
}


CHighLevelAnimeController::~CHighLevelAnimeController()
{
}


bool CHighLevelAnimeController::SetLoopTime(UINT _uiAnimeID, FLOAT _fTime)
{
	//	トラックスピード調節値を算出
	FLOAT DefTime = (FLOAT)m_Anime[_uiAnimeID].m_pAnimeSet->GetPeriod();
	m_Anime[_uiAnimeID].m_fLoopTime = _fTime;
	m_Anime[_uiAnimeID].m_fTrackSpeed = (FLOAT)DefTime / _fTime;

	return true;
}


bool CHighLevelAnimeController::SetShiftTime(UINT _uiAnimeID, FLOAT _fInterval)
{
	//	シフト時間を登録
	m_Anime[_uiAnimeID].m_fShiftTime = _fInterval;

	return true;
}


bool CHighLevelAnimeController::ChangeAnimation(UINT _uiAnimeID)
{	
	//	異なるアニメーションであるかをチェック
	if (m_CurAnimeID == _uiAnimeID)
	{
		//	アニメーションが切り替わった
		m_ChangeDid = false;

		//	なにもせずにかえす
		return false;
	}


	//	グローバル時間を初期化する
	m_pAnimeController->ResetTime();

	//	アニメーションが切り替わった
	m_ChangeDid = true;

	//	現在のアニメーションセットの設定値を取得
	D3DXTRACK_DESC TD;  //トラックの能力
	
	//	トラック0を取得
	m_pAnimeController->GetTrackDesc(0, &TD);

	//	今のアニメーションをトラック1に移行しトラックの設定値も移行
	m_pAnimeController->SetTrackAnimationSet(1, m_Anime[m_CurAnimeID].m_pAnimeSet);
	m_pAnimeController->SetTrackDesc(1, &TD);

	//	新しいアニメーションセットをトラック0に設定
	m_pAnimeController->SetTrackAnimationSet(0, m_Anime[_uiAnimeID].m_pAnimeSet);

	//	トラック0のスピード設定
	m_pAnimeController->SetTrackSpeed(0, m_Anime[_uiAnimeID].m_fTrackSpeed);

	//	ローカル時間を初期化する(アニメーションを一番初めからやりなおす
	m_pAnimeController->SetTrackPosition(0, 0.0f);

	//	トラックの合成を許可
	m_pAnimeController->SetTrackEnable(0, true);
	m_pAnimeController->SetTrackEnable(1, true);

	m_Anime[_uiAnimeID].m_fCurWeightTime = 0.0f;

	//	現在のアニメーションを切り替え
	m_PreAnimeID = m_CurAnimeID;
	m_CurAnimeID = _uiAnimeID;

	return true;
}


bool CHighLevelAnimeController::AdvanceTime(FLOAT _fTime)
{
	if (m_ChangeDid == true)
	{
		//	グローバル時間を初期化する
		m_pAnimeController->ResetTime();

		//	アニメーションカウントを初期化する
		m_AnimationTime = 0.0f;
	}

	//	アニメーションカウンターを加算
	m_AnimationTime++;

	//	合成中か否かを判定
	m_Anime[m_CurAnimeID].m_fCurWeightTime += _fTime;
	
	if (m_Anime[m_CurAnimeID].m_fCurWeightTime <= m_Anime[m_CurAnimeID].m_fShiftTime)
	{
		//	合成中。ウェイトを算出
		FLOAT fWeight = m_Anime[m_CurAnimeID].m_fCurWeightTime / m_Anime[m_CurAnimeID].m_fShiftTime;
		
		//	ウェイトを登録
		//m_pAnimeController->KeyTrackWeight(0, 0, 0, 0, D3DXTRANSITION_LINEAR);
		m_pAnimeController->SetTrackWeight(0, fWeight);       //	現在のアニメーション
		m_pAnimeController->SetTrackWeight(1, 1 - fWeight);   //	前のアニメーション
		//m_pAnimeController->SetTrackWeight(1, 1);

		//m_pAnimeController->KeyTrackPosition(0, 0.0f, 0.0f);
	}
	else
	{
		//合成終了中。通常アニメーションをするTrack0のウェイトを最大値に
		m_pAnimeController->SetTrackWeight(0, 1.0f);    //現在のアニメーション
		m_pAnimeController->SetTrackEnable(1, false);   //前のアニメーションを無効にする
	}
	
	//	時間を更新
	m_pAnimeController->AdvanceTime(_fTime, NULL);

	return true;
}


bool CHighLevelAnimeController::AnimestionSet()
{
	DWORD AnimeStatsNum = m_pAnimeController->GetMaxNumAnimationSets();

	for (DWORD i = 0; i < AnimeStatsNum; i++)
	{
		//　アニメーションセットを取得
		if (FAILED(m_pAnimeController->GetAnimationSet(i, &m_Anime[i].m_pAnimeSet)))
			return false;
	}

	return true;
}


bool CHighLevelAnimeController::AnimationPosition(UINT _uiAnimeId)
{
	//	アニメーションの現在の位置を取得
	DOUBLE NowAnimationPosition;
	//	アニメーションの後尾を取得
	DOUBLE AnimationPeriod;

	//	グローバルタイムを取得してくる(初期化済)
	NowAnimationPosition = m_pAnimeController->GetTime();

	//NowAnimationPosition = m_Anime[_uiAnimeId].m_pAnimeSet->GetPeriodicPosition(m_pAnimeController->GetTime());


	//m_Anime[_uiAnimeId].m_pAnimeSet->GetSRT(NowAnimationPosition, _uiAnimeId, NULL, NULL, NULL);

	//	ローカル時間に変換した後、指定されたアニメーションの後尾を取得する
	AnimationPeriod = m_Anime[_uiAnimeId].m_pAnimeSet->GetPeriod();

	//	ループ数 * ピリオド
	//AnimationPeriod = m_Anime[_uiAnimeId].m_fLoopTime * AnimationPeriod;

	//	現在のアニメーションの位置と、後尾の位置を調べ、trueかfalseを返す
	//return (NowAnimationPosition > (AnimationPeriod)) ? true : false;

	if (NowAnimationPosition > AnimationPeriod)
	{
		return true;
	}
	else
	{
		return false;
	}
}


bool CHighLevelAnimeController::CheckAnimationTime(
	float		_NowAnimationTime,
	const float _ANIMATION_END_TIME)
{
	//	アニメーションタイムが最後まで到達したかどうかを判定
	if (_NowAnimationTime >= _ANIMATION_END_TIME)
	{
		return true;
	}

	return false;
}