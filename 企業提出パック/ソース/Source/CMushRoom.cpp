#include "Game.h"


CMushRoom::CMushRoom()
{
}


CMushRoom::~CMushRoom()
{
}


void CMushRoom::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	XファイルマネージャにXファイルを格納その後読み込みよう変数にコピー
	m_Mesh = GET_XFILE_LOAD_MANAGER.Load("Data/XFile/Object/MushRoom.x", _lpD3DDevice);
	//m_Mesh = GET_XFILE_LOAD_MANAGER.Load("Data/Room/Room.x", _lpD3DDevice);

	m_Mesh->ComputeTangentFrame();
	m_Pass = DRAW_SHADER::PASS::NORMAL;

	//	自己発行力を強めに設定
	m_Mesh->GetMeshState()->m_EmissivePower = 20.0f;

	//	描画パスを環境マップに変更
	m_Pass = DRAW_SHADER::PASS::SPHERE;

	m_Mesh->GetMeshState()->m_EnableInk = true;
}


void CMushRoom::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	座標をセット
	m_mWorld.CreateMove(&m_vPos);
}


void CMushRoom::Release()
{
}