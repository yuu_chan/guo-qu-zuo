#include "Game.h"

CMushRooms::CMushRooms()
{
}

CMushRooms::~CMushRooms()
{
}

void CMushRooms::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	XファイルマネージャにXファイルを格納その後読み込みよう変数にコピー
	m_Mesh = GET_XFILE_LOAD_MANAGER.Load("Data/XFile/Object/MushRooms.x", _lpD3DDevice);

	m_Mesh->GetMeshState()->m_EnableInk = true;
}

void CMushRooms::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	座標をセット
	m_mWorld.CreateMove(&m_vPos);

	//	回転軸をセット
	m_mWorld.RotateY_Local(m_vAng.y);
}

void CMushRooms::Release()
{
}