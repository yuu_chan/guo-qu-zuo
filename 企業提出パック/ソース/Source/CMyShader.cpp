#include "Shader.h"


CMyShader::CMyShader()
{
}

CMyShader::~CMyShader()
{
	Release();
}


void CMyShader::Init()
{
	//	一度、データを解放しておく
	Release();


	//----------------------------
	//	エフェクトファイルのロード
	//----------------------------

	//	エラーチェック用の変数
	LPD3DXBUFFER pErr = nullptr;
	HRESULT		 hr;

	//	ファイルをロードする
	hr = D3DXCreateEffectFromFile(
		GET_DIRECT_HELPER.GetDev(),
		"Shader/MyShader.fx",
		NULL,
		NULL,
		0,
		NULL,
		&m_Effect,
		&pErr);

	//	ファイルがきちんと入っているかどうかをチェック
	if (hr != D3D_OK)
	{
		//	無かったらreturnさせる
		MessageBox(NULL, (LPCSTR)pErr->GetBufferPointer(), "シェーダコンパイルエラー\nファイルが見つかりません", MB_OK);
		return;
	}

	//	ポイントライトの初期設定
	for (int i = 0; i < 100; i++)
	{
		m_vPointColor[i] = D3DXVECTOR4(0.0f, 0.0f, 0.0f, 0.0f);
		m_PointRadius[i] = 0.0f;
		m_vPointColor[i] = D3DXVECTOR4(0.0f, 0.0f, 0.0f, 0.0f);
	}

	//	ダミー用テクスチャを作る　saize(w * h 1ドット) color(白)
	m_TexDot.CreateTexture(1, 1, 1, 0, D3DFMT_A8R8G8B8, D3DPOOL_MANAGED);
	m_TexDot.Fill<UINT>(0xFFFFFFFF);

	//	ブルーム用にRTを生成
	m_texBloom.CreateRenderTarget(APP.m_Width, APP.m_Height, D3DFMT_A8R8G8B8);

	//	UVスクロール値を初期化
	m_vUVScroll = CVector3(0.0f, 0.0f, 0.0f);


	//----------------------
	//	環境マップを読み込む
	//----------------------
	m_SphereMap.LoadTexture("Data/Graphics/Sphere/SphereMap3.png");
	m_Effect->SetTexture("SphereTex", m_SphereMap.GetTex());


	//------------------------------------
	//	デフォルトの法線マップを作っておく
	//------------------------------------
	m_texDefNormal.CreateTexture(1, 1, 1, 0, D3DFMT_A8R8G8B8, D3DPOOL_MANAGED);
	m_texDefNormal.Fill<UINT>(D3DCOLOR_ARGB(0, 128, 128, 255));


	//------------------------------
	//	トューンテクスチャを読み込む
	//------------------------------
	m_ToonTex.LoadTexture("Data/Graphics/Toon/Toon.png");
	m_Effect->SetTexture("ToonTex", m_ToonTex.GetTex());

}


void CMyShader::Release()
{
	SAFE_RELEASE(m_Effect);
	m_TexDot.Release();
	m_ToonTex.Release();
	m_texDefNormal.Release();
}


void CMyShader::DrawMesh(shared_ptr<CMeshObject> _pMesh, int _Pass)
{

	//	テクニック選択
	m_Effect->SetTechnique("Tech");

	//	テクニック開始
	m_Effect->Begin(0, 0);

	//	パス開始
	m_Effect->BeginPass(_Pass);


	//--------
	//	ライト
	//--------

	D3DLIGHT9 Light;

	//	カメラのついてこさせるライトをセットする
	Light = GET_DIRECT_HELPER.GetLight(1);

	CVector3 LightDir = Light.Direction;

	m_Effect->SetVector("NormalLightDir", &D3DXVECTOR4(LightDir, 1));

	//	固定されたライト
	Light = GET_DIRECT_HELPER.GetLight(0);

	//	ランバート拡散照明をするかどうかをメッシュごとに判定させる
	m_Effect->SetInt("bLight", _pMesh->GetMeshState()->m_LightEnable);


/*	if (_Pass == 3)
	{

		m_Effect->SetVectorArray("PointPos", m_vPointPos, 100);

		m_Effect->SetFloatArray("PointRadius", m_PointRadius, 100);

		m_Effect->SetVectorArray("PointColor", m_vPointColor, 100);

	}

		オブジェクトの高さを取ってくる
	m_Effect->SetFloat("Height", _pMesh->GetBounding()->GetBoundingBoxMax().y);*/


	//----------------------------
	//	シャドウフォグがかかる範囲
	//----------------------------

	D3DXVECTOR2 FogRange = _pMesh->GetMeshState()->m_vFogRange;

	//	フォグの色が一番弱い位置
	m_Effect->SetFloat("FogNear", FogRange.x);

	//	フォグの色が一番強い位置
	m_Effect->SetFloat("FogFar",  FogRange.y);


	//------------------
	//	環境マップの濃さ
	//------------------

	float Reflection = _pMesh->GetMeshState()->m_Reflection;

	m_Effect->SetFloat("Reflection", Reflection);


	/*
	////	視差マップ

	//static float V = 0.0f;
	//if (GetAsyncKeyState('E'))
	//{
	//	V += 0.001f;
	//}
	//else if (GetAsyncKeyState('R'))
	//{
	//	V -= 0.001f;
	//}

	//m_Effect->SetFloat("ParallaxVal", V);

	//CFont::FontPos_t Pos{ 10, 10, 200, 200 };
	//GET_FONT.Draw(Pos, "\n\n\n\n\n\n\n\n\n\n\nFPS = %f", (float)V, 0);
	*/

	//--------------------------
	//	ミラー描画をするかどうか
	//--------------------------

	//	クリップ
	SetClip(_pMesh->GetMeshState()->m_Clip);

	//	反射
	SetRefract(_pMesh->GetMeshState()->m_Refract);


	//--------------------------------
	//	テクスチャの透明色を反映させる
	//--------------------------------
	m_Effect->SetFloat("AlphaTest", _pMesh->GetMeshState()->m_AlphaTest);


	//	マテリアルの数だけループさせておく
	for (int i = 0; i < _pMesh->GetMaterialCnt(); i++)
	{

		//	マテリアルとテクスチャーを作っておく
		D3DMATERIAL9*			pMat		=&_pMesh->GetMaterials()[i].m_Material;
		shared_ptr<CTexture>	pTex		= _pMesh->GetMaterials()[i].m_spMeshTex;
		shared_ptr<CMeshState>	pMeshState	= _pMesh->GetMeshState();

		//	ディフューズを設定する
		D3DXVECTOR4 vDif;
		vDif.x = pMat->Diffuse.r * Light.Diffuse.r;
		vDif.y = pMat->Diffuse.g * Light.Diffuse.g;
		vDif.z = pMat->Diffuse.b * Light.Diffuse.b;
		vDif.w = pMat->Diffuse.a * Light.Diffuse.a;

		m_Effect->SetVector("Diffuse", &vDif);

		//	アンビエントを設定する
		m_Effect->SetFloat("Ambient", Light.Ambient.r);

		//	スペキュラを設定する
		D3DXVECTOR4 vSpe;
		vSpe.x = pMat->Specular.r * Light.Specular.r;
		vSpe.y = pMat->Specular.g * Light.Specular.g;
		vSpe.z = pMat->Specular.b * Light.Specular.b;
		vSpe.w = pMat->Specular.a * Light.Specular.a;

		m_Effect->SetVector("Specular", &vSpe);

		//	Powerを設定する
		m_Effect->SetFloat("Power", pMat->Power);

		
		//----------------------
		//	自己発行力を設定する
		//----------------------

		m_Effect->SetFloat("EmissivePow", pMeshState->m_EmissivePower);
		
		//	自己発行力をPS側に渡す
		m_Effect->SetValue("Emissive", &pMat->Emissive, 16);


		//--------------------------
		//	スクロール値をセットする
		//--------------------------

		CVector3 UV = _pMesh->GetMeshState()->m_vUVScroll;

		SetScroll(UV.x);

		m_Effect->SetVector("ScrollUV", &D3DXVECTOR4(UV, 0));


		//------------------
		//	通常テクスチャー
		//------------------

		if (pTex->GetTex())
		{
			m_Effect->SetTexture("MeshTex", pTex->GetTex());
		}
		else
		{
			m_Effect->SetTexture("MeshTex", m_TexDot.GetTex());
		}


		//----------------------
		//	法線情報のテクスチャ
		//----------------------

		shared_ptr<CTexture> m_spTex;

		m_spTex = _pMesh->GetMaterials()[i].m_spNormalTex;

		if (m_spTex->GetTex())
		{
			m_Effect->SetTexture("NormalTex", m_spTex->GetTex());
		}
		else
		{
			m_Effect->SetTexture("NormalTex", m_texDefNormal.GetTex());
		}


		// ※これを呼ばないと、シェーダ側にうまくパラメータがセットされないよ！
		m_Effect->CommitChanges();

		// メッシュを描画
		_pMesh->GetMesh()->DrawSubset(i);
	}

	// パス終了
	m_Effect->EndPass();

	// テクニック終了
	m_Effect->End();




	//if (_pMesh->GetMeshState()->m_EnableInk == false)
	//{
	//	return;
	//}


	////	テクニック選択
	//m_Effect->SetTechnique("InkTech");

	////	テクニック開始
	//m_Effect->Begin(0, 0);

	////	パス開始
	//m_Effect->BeginPass(0);

	////	マテリアルの数だけループさせておく
	//for (int i = 0; i < _pMesh->GetMaterialCnt(); i++)
	//{

	//	//	マテリアルとテクスチャーを作っておく
	//	D3DMATERIAL9*			pMat = &_pMesh->GetMaterials()[i].m_Material;
	//	shared_ptr<CTexture>	pTex = _pMesh->GetMaterials()[i].m_spMeshTex;
	//	shared_ptr<CMeshState>	pMeshState = _pMesh->GetMeshState();

	//	//	ディフューズを設定する
	//	D3DXVECTOR4 vDif;
	//	vDif.x = pMat->Diffuse.r * Light.Diffuse.r;
	//	vDif.y = pMat->Diffuse.g * Light.Diffuse.g;
	//	vDif.z = pMat->Diffuse.b * Light.Diffuse.b;
	//	vDif.w = pMat->Diffuse.a * Light.Diffuse.a;

	//	m_Effect->SetVector("Diffuse", &vDif);

	//	//	アンビエントを設定する
	//	m_Effect->SetFloat("Ambient", Light.Ambient.r);

	//	//	スペキュラを設定する
	//	D3DXVECTOR4 vSpe;
	//	vSpe.x = pMat->Specular.r * Light.Specular.r;
	//	vSpe.y = pMat->Specular.g * Light.Specular.g;
	//	vSpe.z = pMat->Specular.b * Light.Specular.b;
	//	vSpe.w = pMat->Specular.a * Light.Specular.a;

	//	m_Effect->SetVector("Specular", &vSpe);

	//	//	Powerを設定する
	//	m_Effect->SetFloat("Power", pMat->Power);


	//	//----------------------
	//	//	自己発行力を設定する
	//	//----------------------

	//	m_Effect->SetFloat("EmissivePow", pMeshState->m_EmissivePower);

	//	//	自己発行力をPS側に渡す
	//	m_Effect->SetValue("Emissive", &pMat->Emissive, 16);


	//	//--------------------------
	//	//	スクロール値をセットする
	//	//--------------------------

	//	CVector3 UV = _pMesh->GetMeshState()->m_vUVScroll;

	//	SetScroll(UV.x);

	//	m_Effect->SetVector("ScrollUV", &D3DXVECTOR4(UV, 0));


	//	if (pTex->GetTex())
	//	{
	//		m_Effect->SetTexture("MeshTex", pTex->GetTex());
	//	}
	//	else
	//	{
	//		m_Effect->SetTexture("MeshTex", m_TexDot.GetTex());
	//	}


	//	shared_ptr<CTexture> m_spTex;

	//	m_spTex = _pMesh->GetMaterials()[i].m_spNormalTex;

	//	if (m_spTex->GetTex())
	//	{
	//		m_Effect->SetTexture("NormalTex", m_spTex->GetTex());
	//	}
	//	else
	//	{
	//		m_Effect->SetTexture("NormalTex", m_texDefNormal.GetTex());
	//	}


	//	// ※これを呼ばないと、シェーダ側にうまくパラメータがセットされないよ！
	//	m_Effect->CommitChanges();

	//	// メッシュを描画
	//	_pMesh->GetMesh()->DrawSubset(i);
	//}

	//// パス終了
	//m_Effect->EndPass();

	//// テクニック終了
	//m_Effect->End();

}


void CMyShader::DrawSkinMesh(shared_ptr<CMeshObject> _pMesh, int _Pass)
{
	//	ソフトウェアスキニングを開始する
	_pMesh->ExecSoftSkinning();

	//	単位行列をセット
	CMatrix mI;
	SetTransformWorld(&mI);

	//	メッシュコンテナーを返す
	CMyAllocateHierarchy::MYD3DXMESHCONTAINER* mc = _pMesh->GetCont(0);

	//	プレイヤーの高さを渡す
	m_Effect->SetFloat("Height", _pMesh->GetBounding()->GetBoundingSphereRadius());

	//	現在のピクセル位置
	m_Effect->SetFloat("NowPix", _pMesh->GetMeshState()->m_vNowPix.y);

	//	変更したい色を選択
	m_Effect->SetVector("ChangeColor", &_pMesh->GetMeshState()->m_vChangeColor);

	//	テクニック選択
	m_Effect->SetTechnique("Tech");

	//	テクニック開始
	m_Effect->Begin(0, 0);

	//	パス開始
	m_Effect->BeginPass(_Pass);

	//ライト情報ゲット
	D3DLIGHT9 Light = GET_DIRECT_HELPER.GetLight(0);

	//	マテリアルの数だけループさせておく
	for (DWORD i = 0; i < mc->NumMaterials; i++)
	{

		//	マテリアルとテクスチャーを作っておく
		D3DMATERIAL9*				pMat = &mc->pMaterials[i].MatD3D;
		CTexture*					pTex = &mc->m_Textures[i];
		shared_ptr<CMeshState>		pMeshState = _pMesh->GetMeshState();

		//	ディフューズを設定する
		D3DXVECTOR4 vDif;
		vDif.x = pMat->Diffuse.r * Light.Diffuse.r;
		vDif.y = pMat->Diffuse.g * Light.Diffuse.g;
		vDif.z = pMat->Diffuse.b * Light.Diffuse.b;
		vDif.w = pMat->Diffuse.a * Light.Diffuse.a;

		m_Effect->SetVector("Diffuse", &vDif);

		//	アンビエントを設定する
		m_Effect->SetFloat("Ambient", Light.Ambient.r);

		//	スペキュラを設定する
		D3DXVECTOR4 vSpe;
		vSpe.x = pMat->Specular.r * Light.Specular.r;
		vSpe.y = pMat->Specular.g * Light.Specular.g;
		vSpe.z = pMat->Specular.b * Light.Specular.b;
		vSpe.w = pMat->Specular.a * Light.Specular.a;

		m_Effect->SetVector("Specular", &vSpe);

		//	Powerを設定する
		m_Effect->SetFloat("Power", pMat->Power);


		//	エミッシブ(自己発行力をセット)

		m_Effect->SetFloat("EmissivePow", pMeshState->m_EmissivePower);

		m_Effect->SetValue("Emissive", &pMat->Emissive, 16);


		//	テクスチャの情報を取得
		if (pTex->GetTex())
		{
			//	テクスチャの情報が入っていた場合は、テクスチャーを送る
			m_Effect->SetTexture("MeshTex", pTex->GetTex());
		}
		else
		{
			//	入っていなかった場合は、読み込み時に作ったテクスチャーを送る
			m_Effect->SetTexture("MeshTex", m_TexDot.GetTex());
		}

		m_Effect->SetTexture("NormalTex", m_texDefNormal.GetTex());


		// ※これを呼ばないと、シェーダ側にうまくパラメータがセットされないよ！
		m_Effect->CommitChanges();

		// メッシュを描画
		mc->MeshData.pMesh->DrawSubset(i);
	}

	// パス終了
	m_Effect->EndPass();

	// テクニック終了
	m_Effect->End();





	////	テクニック選択
	//m_Effect->SetTechnique("InkTech");

	////	テクニック開始
	//m_Effect->Begin(0, 0);

	////	パス開始
	//m_Effect->BeginPass(0);

	////ライト情報ゲット
	////D3DLIGHT9 Light = GET_DIRECT_HELPER.GetLight(0);

	////	マテリアルの数だけループさせておく
	//for (DWORD i = 0; i < mc->NumMaterials; i++)
	//{

	//	//	マテリアルとテクスチャーを作っておく
	//	D3DMATERIAL9*				pMat = &mc->pMaterials[i].MatD3D;
	//	CTexture*					pTex = &mc->m_Textures[i];
	//	shared_ptr<CMeshState>		pMeshState = _pMesh->GetMeshState();

	//	//	ディフューズを設定する
	//	D3DXVECTOR4 vDif;
	//	vDif.x = pMat->Diffuse.r * Light.Diffuse.r;
	//	vDif.y = pMat->Diffuse.g * Light.Diffuse.g;
	//	vDif.z = pMat->Diffuse.b * Light.Diffuse.b;
	//	vDif.w = pMat->Diffuse.a * Light.Diffuse.a;

	//	m_Effect->SetVector("Diffuse", &vDif);

	//	//	アンビエントを設定する
	//	m_Effect->SetFloat("Ambient", Light.Ambient.r);

	//	//	スペキュラを設定する
	//	D3DXVECTOR4 vSpe;
	//	vSpe.x = pMat->Specular.r * Light.Specular.r;
	//	vSpe.y = pMat->Specular.g * Light.Specular.g;
	//	vSpe.z = pMat->Specular.b * Light.Specular.b;
	//	vSpe.w = pMat->Specular.a * Light.Specular.a;

	//	m_Effect->SetVector("Specular", &vSpe);

	//	//	Powerを設定する
	//	m_Effect->SetFloat("Power", pMat->Power);


	//	//	エミッシブ(自己発行力をセット)

	//	m_Effect->SetFloat("EmissivePow", pMeshState->m_EmissivePower);

	//	m_Effect->SetValue("Emissive", &pMat->Emissive, 16);


	//	//	テクスチャの情報を取得
	//	if (pTex->GetTex())
	//	{
	//		//	テクスチャの情報が入っていた場合は、テクスチャーを送る
	//		m_Effect->SetTexture("MeshTex", pTex->GetTex());
	//	}
	//	else
	//	{
	//		//	入っていなかった場合は、読み込み時に作ったテクスチャーを送る
	//		m_Effect->SetTexture("MeshTex", m_TexDot.GetTex());
	//	}

	//	m_Effect->SetTexture("NormalTex", m_texDefNormal.GetTex());


	//	// ※これを呼ばないと、シェーダ側にうまくパラメータがセットされないよ！
	//	m_Effect->CommitChanges();

	//	// メッシュを描画
	//	mc->MeshData.pMesh->DrawSubset(i);
	//}

	//// パス終了
	//m_Effect->EndPass();

	//// テクニック終了
	//m_Effect->End();
}


void CMyShader::AlphaDrawMesh(shared_ptr<CMeshObject> _pMesh, float _alpha)
{

	//	テクニック選択
	m_Effect->SetTechnique("Tech");

	//	テクニック開始
	m_Effect->Begin(0, 0);

	//	パス開始
	m_Effect->BeginPass(0);

	//ライト情報ゲット
	D3DLIGHT9 Light = GET_DIRECT_HELPER.GetLight(0);

	//	マテリアルの数だけループさせておく
	for (int i = 0; i < _pMesh->GetMaterialCnt(); i++)
	{

		//	マテリアルとテクスチャーを作っておく
		D3DMATERIAL9* pMat = &_pMesh->GetMaterials()[i].m_Material;
		//CTexture*	  pTex = &_pMesh->GetTextures()[i];
		shared_ptr<CTexture>	pTex = _pMesh->GetMaterials()[i].m_spMeshTex;

		//	ディフューズを設定する
		D3DXVECTOR4 vDif;
		vDif.x = pMat->Diffuse.r * Light.Diffuse.r;
		vDif.y = pMat->Diffuse.g * Light.Diffuse.g;
		vDif.z = pMat->Diffuse.b * Light.Diffuse.b;
		vDif.w = (pMat->Diffuse.a - _alpha) * Light.Diffuse.a;

		m_Effect->SetVector("Diffuse", &vDif);

		//	アンビエントを設定する
		m_Effect->SetFloat("Ambient", Light.Ambient.r);

		//	スペキュラを設定する
		D3DXVECTOR4 vSpe;
		vSpe.x = pMat->Specular.r * Light.Specular.r;
		vSpe.y = pMat->Specular.g * Light.Specular.g;
		vSpe.z = pMat->Specular.b * Light.Specular.b;
		vSpe.w = pMat->Specular.a * Light.Specular.a;

		m_Effect->SetVector("Specular", &vSpe);

		//	Powerを設定する
		m_Effect->SetFloat("Power", pMat->Power);

		//	自己発行力を設定する
		m_Effect->SetValue("Emissive", &pMat->Emissive, 16);

		if (pTex->GetTex())
		{
			m_Effect->SetTexture("MeshTex", pTex->GetTex());
		}
		else
		{
			m_Effect->SetTexture("MeshTex", m_TexDot.GetTex());
		}

		// ※これを呼ばないと、シェーダ側にうまくパラメータがセットされないよ！
		m_Effect->CommitChanges();

		// メッシュを描画
		_pMesh->GetMesh()->DrawSubset(i);
	}

	// パス終了
	m_Effect->EndPass();

	// テクニック終了
	m_Effect->End();
}