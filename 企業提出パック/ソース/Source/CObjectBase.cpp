#include "Game.h"
#include "Scene.h"


//-------------------------
//	static const 値を初期化
//-------------------------

//	重量を扱う
const float CObjectBase::GRAVITY_SPEED		= 0.025f;
const float CObjectBase::GRAVITY_SPEED_MAX	= (GRAVITY_SPEED * 13);;

//	オブジェクトの最大体重
const float CObjectBase::MAX_WEIGHT			= 1.0f;

//	Y座標吐き出されたときの移動速度
const float CObjectBase::SPIT_MOVE_Y_SPEED		= 0.025f;
const float CObjectBase::SPIT_MOVE_Y_SPEED_MAX	= (SPIT_MOVE_Y_SPEED * 13);

//	Z座標吐き出されたときの移動速度
const float CObjectBase::SPIT_MOVE_Z_SPEED		= 0.025f;
const float CObjectBase::SPIT_MOVE_Z_SPEED_MAX	= (SPIT_MOVE_Z_SPEED * 13);


CObjectBase::CObjectBase()
	:
	m_Mesh(nullptr)
	,
	m_vVec(CVector3(0.0f, 0.0f, 0.0f))
	,
	m_vPos(CVector3(0.0f, 0.0f, 0.0f))
	,
	m_vAng(CVector3(0.0f, 0.0f, 0.0f))
	,
	m_vScalSize(CVector3(0.0f, 0.0f, 0.0f))
	,
	m_State(0)
	,
	m_Id(0)
	,
	m_Accel(0)
	,
	m_GroundAdjustment(1.0f)
	,
	m_ObjectWeight(0.0)
	,
	m_SpitAdjustment(0.0f)
	,
	m_Pass(0)
	,
	m_ItemState(0)
	,
	m_MaxCnt(0)
	,
	m_EnableColl(false)
	,
	m_EnableDestruction(false)
	,
	m_EnableGetWorld(false)
	,
	m_GoalCheck(false)
	,
	m_Filter(0)
	,
	m_mWorld(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
	,
	m_RegisterTag("")
	,
	m_ItemResilience(0)
	,
	m_ItemId(0)
	,
	m_UpdateTag("")
	,
	m_Cnt(0)
{

	//	アイテムステータスを初期化(吸い込んでいない状態にしておく)
	m_ItemState = GET_FLAG_CALC.SetData(m_ItemState, (STOCK::STATE::FREE | STOCK::STATE::NO_ABSORPTION), LOGICAL::OPERATION_MODE::OR);

	//	メッシュのポインタを確保
	m_Mesh = make_shared<CMeshObject>();

}


CObjectBase::~CObjectBase()
{
	//	メッシュクラスを解放
	m_Mesh = nullptr;
}


void CObjectBase::ObjectMove(bool _EnableGroundCheck, bool _EnableStep)
{

	//----------------------------------------------
	//	もしアイテムがストックされている状態だったら
	//----------------------------------------------

	UINT Check = 0x0f00;

	//	ストックされているかをチェック
	if (GET_FLAG_CALC.CheckFlag(m_ItemState, Check, STOCK::STATE::DO_STOCK))
	{
		CVector3 vPos = CVector3(0.0f, -100.0f, 0.0);

		//	座標を外に設定しておく
		m_mWorld.CreateMove(&vPos);

		//	自身のIDとストックされたときに記憶されたIDが一致した場合は
		//	オブジェクトをステージ外にセットしておく
		if (m_Id == CRigidSupervise::GetStockId())
		{
			//	行列をセットする
			CRigidSupervise::GetItObject(m_Id)->SetMatrix(m_mWorld);

			//	質量を0に変換させる
			CRigidSupervise::GetItObject(m_Id)->SetMass(0.0f);

			//	ステータスを初期化
			m_State = GET_FLAG_CALC.SetData(m_State, Check, LOGICAL::OPERATION_MODE::AND);

			//	拡大サイズを戻す
			const CVector3 DefaultSize = CVector3(1.0f, 1.0f, 1.0f);

			m_vScalSize = DefaultSize;
		}
	}


	//	チェックするデータを変更
	Check = 0x1000;

	//	ストックされているかをチェック
	if (GET_FLAG_CALC.CheckFlag(m_ItemState, Check))
	{

		//----------
		//	木箱なら
		//----------
		if (m_ItemId == OBJECT_LIST::ID::WOOD_BOX)
		{
			//	バレットからワールド行列を取得しない
			m_EnableGetWorld = false;

			//	当たり判定をしないので一時的にフィルターをOFFに
			m_Filter = FILTER::NAME::COLLISION_ON;


			//----------------------------------------------------------------------
			//	プレイヤーの座標からオブジェクトの座標を引くと方向と力をもらえるので
			//	正規化してから、オブジェクトの座標に加算していく
			//----------------------------------------------------------------------

			//	プレイヤーの行列
			CMatrix  mMat = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(OBJECT_LIST::ID::PLAYER)->GetWorld();

			CVector3 vVec;


			//	プレイヤーの行列から座標を取得
			vVec = mMat.GetPos();

			//	力と方向を取得して正規化
			vVec = vVec - m_vPos;

			vVec.Normalize();


			//	移動量として座標に加算していく

			float MoveAdd  = 0.09f;
			float AngleAdd = 10.0f;

			m_vPos   += (vVec * MoveAdd);

			m_vAng.x += AngleAdd;


			//	オブジェクトとプレイヤーとの距離を測る
			auto Dis = GET_HELPER.GetDistance(mMat.GetPos(), m_vPos);

			const CVector3 SCALE_SIZE = CVector3(0.05f, 0.05f, 0.05f);

			//	オブジェクトの拡大サイズを小さくしていく
			m_vScalSize -= SCALE_SIZE;


			//	最小サイズを設定しておいて、それ以下になる場合はサイズを止めておく
			const CVector3 SMOLL_SIZE = CVector3(0.0f, 0.0f, 0.0f);

			if (m_vScalSize.z < SMOLL_SIZE.z)
			{
				m_vScalSize = SMOLL_SIZE;
			}


			//	一定距離内に入れば、アイテムがストックされた状態に変更される
			float CertainDisZoon = 0.6f;

			if (Dis < CertainDisZoon)
			{
				m_ItemState = GET_FLAG_CALC.SetData((m_ItemState & STOCK::STATE::FREE), STOCK::STATE::DO_STOCK, LOGICAL::OPERATION_MODE::OR);
			}

		}
		//--------------------------------------------------------
		//	果物系統のオブジェクトならなにもしないでストック状態へ
		//--------------------------------------------------------
		else
		{
			m_ItemState = GET_FLAG_CALC.SetData((m_ItemState & STOCK::STATE::FREE), STOCK::STATE::DO_STOCK, LOGICAL::OPERATION_MODE::OR);
		}
	}
	else
	{
		//	バレットワールドの行列を取得できるように変更
		m_EnableGetWorld = true;

		//	当たり判定フィルダーを通すように変更
		m_Filter = FILTER::NAME::COLLISION_ON;
	}


	//------------------------------
	//	もし吐き出し処理を行われたら
	//------------------------------

	//	行うかどうかをチェック(true : 行う)
	if (_EnableStep == true)
	{

		//	吐き出しを行っているかチェック
		if (GET_FLAG_CALC.CheckFlag(m_ItemState, STOCK::STATE::DO_SPIT)
			&&
			m_Id == CRigidSupervise::GetStockId())
		{

			m_UpdateTag = "";

			//--------------------------------
			//	処理を幾つかのステップに分ける
			//--------------------------------
			enum STEP
			{
				CHECK = 0x000f,	//	フラグチェック用

				INIT  = 0x0000,	//	初期化
				MOVE  = 0x0001,	//	移動
				END   = 0x0002,	//	終了
			};

			//--------------------------------------------------
			//	STEP1(INIT)		:	座標の初期化
			//
			//	STEP2(MOVE)		:	オブジェクトを移動させる
			//
			//	STEP3(END)		:	移動終了
			//--------------------------------------------------


			//	STEP1
			if (GET_FLAG_CALC.CheckFlag(m_State, STEP::CHECK, STEP::INIT))
			{

				//------------------------------------------------
				//	力をかける方向と
				//	オブジェクトが吐き出されたときの座標を設定する
				//------------------------------------------------

				//	プレイヤーのワールド行列を取得する
				CMatrix mWorld = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(OBJECT_LIST::ID::PLAYER)->GetWorld();

				auto  Cnt    = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(OBJECT_LIST::PLAYER)->GetMaxCnt();

				float Power  = 0.0f;

				float half   = 0.5f;

				float Weight = 1.2f;

				Power		 = Cnt * half * (Weight - m_ObjectWeight);

				//	力をかける方向は、プレイヤーのz方向+1
				D3DXVec3TransformNormal(&m_vVec, &CVector3(0.0f, Power * half, Power), &mWorld);

				m_vVec.Normalize();


				//----------------------------------------------------------------------
				//	オブジェクトによって、吐き出す位置を少し変更する
				//	プレイヤーの手前にオブジェクトを設置する
				//	プレイヤーのY軸から+メッシュのバウンディングスフィアのサイズをセット
				//----------------------------------------------------------------------

				CMatrix mRotY;

				//	プレイヤーのワールド行列から、Y軸を取得
				D3DXMatrixRotationY(&mRotY, D3DXToRadian(mWorld.GetAngleY()));

				CVector3 Pos;

				if (m_ItemId == OBJECT_LIST::ID::WOOD_BOX)
				{
					const float Z = 2.2f;

					//	設置したい座標を設定
					D3DXVec3TransformCoord(
						&Pos,
						&CVector3(
						0.0f,
						m_Mesh->GetBounding()->GetBoundingBoxMax().y,
						m_Mesh->GetBounding()->GetBoundingBoxMax().z * Z),
						&mRotY);
				}
				else
				{
					const float Z = 2.0f;

					//	設置したい座標を設定
					D3DXVec3TransformCoord(
						&Pos,
						&CVector3(
						0.0f,
						0.0f,
						m_Mesh->GetBounding()->GetBoundingSphereRadius() * Z),
						&mRotY);
				}


				//	プレイヤーのワールド行列から座標を取得して
				//	設置したい座標と加算する
				m_vPos   = mWorld.GetPos();

				m_vPos   = m_vPos + Pos;

				//	座標以外のデータも一致させておくために、一度行列をコピー
				m_mWorld = mWorld;

				//	座標をセットする
				m_mWorld.CreateMove(&m_vPos);

				//	当たり判定をOFFにする
				m_EnableColl = false;


				//------------------------------------------
				//	物理ワールドに行列をセットし、
				//	オブジェクトにかかる力を一度リセットする
				//------------------------------------------

				CRigidSupervise::GetItObject(m_Id)->SetMatrix(m_mWorld);

				CRigidSupervise::GetItObject(m_Id)->GetBody()->activate();

				CRigidSupervise::GetItObject(m_Id)->SetMass(1.0f);

				CRigidSupervise::GetItObject(m_Id)->ClearForces();

				//	移動方向 * 量 
				CRigidSupervise::GetItObject(m_Id)->ApplyImpulse(m_vVec * Power);


				//	ステータスをMOVEに変更
				m_State = GET_FLAG_CALC.SetData(m_State, STEP::MOVE, LOGICAL::OPERATION_MODE::OR);
			}

			//	STEP2
			else if (
				GET_FLAG_CALC.CheckFlag(m_State, STEP::CHECK, STEP::MOVE)
				&&
				m_EnableColl == true)
			{
				//if (m_EnableColl == true)
				{
					//	ステータスを変更する
					m_State		= GET_FLAG_CALC.SetData(m_State, STEP::END, LOGICAL::OPERATION_MODE::OR);

					m_ItemState = GET_FLAG_CALC.SetData(m_State, STOCK::STATE::NO_ABSORPTION, LOGICAL::OPERATION_MODE::OR);

				}
			}


		}
	}

}


void CObjectBase::Draw(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//-----------------------------------------------------
	//	特に、指定が無い場合はこっちの描画関数が呼び出される
	//	シェーダー描画をしたくない場合は、オブジェクト毎に
	//	オーバーロードした描画関数を作る
	//-----------------------------------------------------

	//	アイテムが体内に入っていない場合は
	if (!GET_FLAG_CALC.CheckFlag(m_ItemState, STOCK::STATE::DO_STOCK))
		//	描画をする(シャドウ入り)
		DrawShader(_lpD3DDevice);

}


void CObjectBase::DrawShader(LPDIRECT3DDEVICE9 _lpD3DDevice)
{

	//------------------------------------------------------
	//	シャドウマップとメッシュの描画を行う
	//
	//	描画手順は三つ
	//	
	//	�@	影の情報を作るためにモデルまでの距離を測るための
	//		シャドウマップテクスチャを作成する(深度マップ)
	//		型はR32F型で生成 (2D空間描画)
	//
	//	�A	�@の手順で作成した影の情報をシェーダ側に渡す
	//
	//	�B	全キャラクターを描画する (3D空間描画)
	//
	//	手順�@はシェーダーマネージャー側でする
	//	手順�Bは次の関数でする
	//
	//	描画を二度行うので少々処理が重くなる可能性がある
	//
	//	関数は、CObjectBase型の描画関数で呼ばれる
	//
	//------------------------------------------------------


	//--------------
	//	手順�Bを行う
	//--------------

	//	ライティング開始
	_lpD3DDevice->SetRenderState(D3DRS_LIGHTING, TRUE);


	//	ワールド行列をマイシェーダーに渡す
	GET_SHADER.GetMyShader()->SetConstTransformWorld(&m_mWorld);


	//	オブジェクト自身の描画を行う
	GET_SHADER.GetMyShader()->DrawMesh(m_Mesh, m_Pass);


	//	ライティング終了
	_lpD3DDevice->SetRenderState(D3DRS_LIGHTING, FALSE);


}


void CObjectBase::UpdateBulletWorld(bool _Enable)
{
	//	取得する場合は、物理ワールドからワールド行列を取得してくる
	if (_Enable == true)
	{
		//m_mWorld = 
	}
	//	取得しない場合は、物理ワールドにワールド行列をセットする
	else if (_Enable == false)
	{

	}

	//CBulletObj_Box* box = new CBulletObj_Box();
	//m_spRegid[0] = box;


	//CBulletObj_Base* rb =  m_spRegid[0];

	//CBulletRigid_Base* aaa;
	//aaa->ApplyForce();

	//CBulletObj_Sphere* sph = dynamic_cast<CBulletObj_Sphere*>(rb);
	//if (sph){
	//	sph->
	//}

	//CBulletObj_Box* box = dynamic_cast<CBulletObj_Box*>(rb);
	//if (box){
	//	box->
	//}
}


//--------------------------->
//	static変数初期化
//<---------------------------

//	剛体が入っているリスト
list<weak_ptr<CBulletRigid_Base>> CRigidSupervise::m_lweRigidSupervise;

//	IDのコピー
int	CRigidSupervise::m_StockId	= -1;

//	後尾のID
int CRigidSupervise::m_EndId	= 0;


//----------------------------------------
//	扱う質量のある程度のデータを決めておく
//----------------------------------------

//	デフォルト値
const float CRigidSupervise::MASS_DEFAULT = 1.0f;

//	最小値
const float CRigidSupervise::MASS_MIN	  = 0.0f;


void CRigidSupervise::CreateRigidSphere(
	weak_ptr<CBulletObj_Sphere>		_RigidSphere,
	shared_ptr<CMeshObject>			_Mesh,
	CVector3						_vPos,
	int								_RegistId,
	float							_SphereSize,
	float							_mass
	)
{
	float DefaultState = -1.0f;

	//	指定されたサイズがデフォルト引数ではない場合
	bool Check = (_SphereSize > DefaultState) ? true : false;		//	true : 指定サイズ false : メッシュサイズ


	//--------------------------
	//	物理ワールドへ剛体を登録
	//--------------------------
	if (Check == false)
	{
		//	物理ワールドへ球剛体を登録する
		_RigidSphere.lock()->Create(
			&GET_BULLET_WORLD.m_pBulletWorld,	//	物理世界
			_vPos,
			_Mesh->GetBounding()->GetBoundingSphereRadius(),
			_mass
			);
	}
	else if (Check == true)
	{
		//	物理ワールドへ球剛体を登録する
		_RigidSphere.lock()->Create(
			&GET_BULLET_WORLD.m_pBulletWorld,	//	物理世界
			_vPos,
			_SphereSize,
			_mass
			);
	}


	//------------
	//	初期化処理
	//------------

	//	摩擦設定
	_RigidSphere.lock()->SetFriction(MASS_DEFAULT);

	//	スリープ状態に入らないようにする
	_RigidSphere.lock()->SetNonSleep();

	//	リストへプッシュする
	PushRigid<CBulletObj_Sphere>(_RegistId, _RigidSphere);


	_RigidSphere.lock()->getShape()->setMargin(0.001f);
}


void CRigidSupervise::CreateRigidBox(
	weak_ptr<CBulletObj_Box>	_RigidBox,
	shared_ptr<CMeshObject>		_Mesh,
	CVector3					_vPos,
	int							_RegistId,
	CVector3					_vScaleSize,
	float						_mass
	)
{
	float DefaultState = -1.0f;

	//	指定されたサイズがデフォルト引数ではない場合
	bool Check = (_vScaleSize.z > DefaultState) ? true : false;		//	true : 指定サイズ false : メッシュサイズ


	//--------------------------
	//	物理ワールドへ剛体を登録
	//--------------------------
	if (Check == false)
	{
		//	物理ワールドへ球剛体を登録する
		_RigidBox.lock()->Create(
			&GET_BULLET_WORLD.m_pBulletWorld,
			_vPos,
			_Mesh->GetBounding()->GetBoundingBoxMax(),
			_mass
			);
	}
	else if (Check == true)
	{
		//	物理ワールドへ球剛体を登録する
		_RigidBox.lock()->Create(
			&GET_BULLET_WORLD.m_pBulletWorld,
			_vPos,
			_vScaleSize,
			_mass
			);
	}


	//------------
	//	初期化処理
	//------------

	//	摩擦設定
	_RigidBox.lock()->SetFriction(MASS_DEFAULT);

	//	スリープ状態に入らないようにする
	_RigidBox.lock()->SetNonSleep();

	//	リストへプッシュする
	PushRigid<CBulletObj_Box>(_RegistId, _RigidBox);


	_RigidBox.lock()->GetShape()->setMargin(0.001f);
}


void CRigidSupervise::CreateRigidCapsule(
	weak_ptr<CBulletObj_Capsule>	_spCapsule,
	shared_ptr<CMeshObject>			_spMesh,
	CMatrix							_mMatrix,
	CVector3						_vPos,
	float							_Height,
	int								_RegistId,
	CVector3						_vScaleSize,
	float							_mass
	)
{
	float DefaultState = -1.0f;

	//	指定されたサイズがデフォルト引数ではない場合
	bool Check = (_vScaleSize.z > DefaultState) ? true : false;		//	true : 指定サイズ false : メッシュサイズ


	_mMatrix.CreateMove(&_vPos);

	//--------------------------
	//	物理ワールドへ剛体を登録
	//--------------------------
	if (Check == false)
	{
		//	物理ワールドへ球剛体を登録する
		_spCapsule.lock()->Create(
			&GET_BULLET_WORLD.m_pBulletWorld,
			_mMatrix,
			_spMesh->GetBounding()->GetBoundingSphereRadius() / 9.0f,
			_Height,
			_mass
			);
	}
	//else if (Check == true)
	//{
	//	//	物理ワールドへ球剛体を登録する
	//	_spCapsule.lock()->Create(
	//		&GET_BULLET_WORLD.m_pBulletWorld,
	//		_vPos,
	//		_vScaleSize,
	//		_mass
	//		);
	//}


	//------------
	//	初期化処理
	//------------

	//	摩擦設定
	_spCapsule.lock()->SetFriction(MASS_DEFAULT);

	//	スリープ状態に入らないようにする
	_spCapsule.lock()->SetNonSleep();

	//	リストへプッシュする
	PushRigid<CBulletObj_Capsule>(_RegistId, _spCapsule);


	_spCapsule.lock()->getShape()->setMargin(0.001f);
}


void CRigidSupervise::CreateRigidCompound(
	weak_ptr<CBulletObj_Capsule>	_spCapsule,
	shared_ptr<CMeshObject>			_Mesh,
	CVector3						_vPos,
	CVector3						_Scale,
	CBulletObj_Compound*			_RigidCompound
	)
{

}


void CRigidSupervise::CreateRigidMesh(
	weak_ptr<CBulletObj_Mesh>	_RigidMesh,
	shared_ptr<CMeshObject>		_Mesh,
	CVector3					_vPos,
	int							_RegistId,
	CVector3					_vScaleSize,
	float						_mass
	)
{
	float DefaultState = -1.0f;

	//	指定されたサイズがデフォルト引数ではない場合
	bool Check = (_vScaleSize.z > DefaultState) ? true : false;		//	true : 指定サイズ false : メッシュサイズ


	//--------------------------
	//	物理ワールドへ剛体を登録
	//--------------------------
	if (Check == false)
	{
		//	物理ワールドへ球剛体を登録する
		_RigidMesh.lock()->CreateFromMesh(
			&GET_BULLET_WORLD.m_pBulletWorld,
			_Mesh->GetMesh(),
			_vPos
			);
	}
	else if (Check == true)
	{
		////	物理ワールドへ球剛体を登録する
		//_RigidMesh.lock()->Create(
		//	&GET_BULLET_WORLD.m_pBulletWorld,
		//	_vPos,
		//	_vScaleSize,
		//	_mass
		//	);
	}


	//------------
	//	初期化処理
	//------------

	//	摩擦設定
	_RigidMesh.lock()->SetFriction(MASS_DEFAULT);

	//	質量設定
	_RigidMesh.lock()->SetMass(_mass);

	//	スリープ状態に入らないようにする
	_RigidMesh.lock()->SetNonSleep();

	//	リストへプッシュする
	PushRigid<CBulletObj_Mesh>(_RegistId, _RigidMesh);


	_RigidMesh.lock()->GetShape()->setMargin(0.001f);
}