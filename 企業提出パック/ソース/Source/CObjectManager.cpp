#include "Game.h"


CObjectManager::CObjectManager()
{

}


CObjectManager::~CObjectManager()
{
	Release();
}


void CObjectManager::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
}


void CObjectManager::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	/*
	//	イテレータを生成最初の位置を指す
	//auto itObjContainer = m_listObjContainer.begin();

	//	破棄フラグがでた場所を記憶しておく
	//list<int> DestructionId;

	int  DestructionId		= 0;
	bool EnableDestruction	= false;	//	true(破棄する命令があった) false(破棄する命令が無かった)

	
	//while (itObjContainer != m_listObjContainer.end())
	//{

	//	//	更新中に、オブジェクトの破棄命令が
	//	//	でていないかを確認してみる
	//	if ((*itObjContainer)->GetEnableDestruction() == true)
	//	{
	//		//	破棄する命令がでた場所を記憶
	//		DestructionId = (*itObjContainer)->GetId();

	//		//	破棄処理を行う
	//		EnableDestruction = true;
	//	}

	//	//	コンテナーのクラスの更新処理を呼ぶ
	//	(*itObjContainer)->Update(_lpD3DDevice);
	//	++itObjContainer;
	//}
	

	if (EnableDestruction == true)
	{
		ItObjectKill(DestructionId);

		//GET_WORLD.GetGameWorld()->DeleteEndId();

		//CRigidSupervise::DeleteItList();
	}
	*/


	for (auto &This : m_listObjContainer)
	{
		This->Update(_lpD3DDevice);
	}

}



void CObjectManager::Draw(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	/*
	//	イテレータを生成
	auto itObjContainer = m_listObjContainer.begin();

	while (itObjContainer != m_listObjContainer.end())
	{
		//	視錘台範囲内に入っていれば描画する
		if (GET_HELPER.ViewFrustum((*itObjContainer)->GetId(), &GET_CAMERA.GetVF()) == true)
		{
			(*itObjContainer)->Draw(_lpD3DDevice);
		}

		++itObjContainer;
	}
	*/


	/*
	
		参照にすることで、コンテナの中身のコピーを防ぐ

		ポインターであれば、問題はないが
		もし通常の変数だった場合は、コピーが帰ってくると
		中身を書き換えることができないので
		&を使用して参照体を渡す
	
	*/
	for (auto &This : m_listObjContainer)
	{
		if (GET_HELPER.ViewFrustum(This->GetId(), &GET_CAMERA.GetVF()) == true)
		{
			This->Draw(_lpD3DDevice);
		}
	}
}


void CObjectManager::Release()
{
	/*------------------------------------------->
	
		範囲ループはイテレータと違って
		コンテナの中身そのものを指すため
		削除する場合は、すこし手間がかかるので
		コンテナからオブジェクトを破棄する場合は
		イテレーターを使用する
	
	<-------------------------------------------*/

	//	イテレータを生成
	auto itObjContainer = m_listObjContainer.begin();


	while (itObjContainer != m_listObjContainer.end())
	{
		//	イテレータの中にあるvectorの中身を消す
		//delete (*itObjContainer);
	
		//	消したアドレスにNULLを入れておく
		*itObjContainer = nullptr;
		
		//	消して、位置をつめる
		itObjContainer = m_listObjContainer.erase(itObjContainer);
	}
}


void CObjectManager::PushObjectManager(shared_ptr<CObjectBase> _spObject)
{
	if (_spObject != nullptr)
	{
		m_listObjContainer.push_back(_spObject);
	}
}


shared_ptr<CObjectBase> CObjectManager::GetItObject(int _Id)
{

	/*
	//	イテレータを生成
	auto itObjContainer = m_listObjContainer.begin();


	//	リストの後尾までまわす
	while (itObjContainer != m_listObjContainer.end())
	{
		//	指定されたIdが入っていた場合
		if ((*itObjContainer)->GetId() == _Id)
		{
			//	指定されたIdの中身を返す
			return (*itObjContainer);
		}

		//	無ければ次へ
		++itObjContainer;
	}

	//	何も無ければNULLを返す
	return nullptr;
	*/



	for (auto &This : m_listObjContainer)
	{
		if (This->GetId() == _Id)
		{
			return This;
		}
	}

	return nullptr;
}


void CObjectManager::SetShaderManager(CShaderManager* _pShader)
{
	////	nullチェック
	//if (_pShader == nullptr)
	//{
	//	MessageBox(NULL, "シェーダーのポインターがセットされていません。このままゲームを終了します", "警告", MB_OK);
	//	APP.m_bEndFlag = true;
	//	return;
	//}

	////	リストの数を変数に代入
	//auto itObjContainer = m_listObjContainer.begin();

	////	リストの後尾に達していなければ
	//while (itObjContainer != m_listObjContainer.end())
	//{

	//	//	指定したオブジェクトにシャドウマップシェーダのポインタをセット
	//	(*itObjContainer)->SetShader(_pShader);

	//	//	次のリストへ
	//	++itObjContainer;
	//}
}


void CObjectManager::ItObjectKill(int _ObjectIt)
{
	////	先頭IDを取得
	//auto itObjContainer = m_listObjContainer.begin();

	//while (itObjContainer != m_listObjContainer.end())
	//{
	//	//	破棄したいIDとコンテナのIDが一致したら
	//	//	指定したオブジェクトを破棄して、コンテナ内をつめる
	//	//	ゲームワールド内のオブジェクトの数を管理している変数も-1しておく
	//	if (_ObjectIt == (*itObjContainer)->GetId())
	//	{
	//		//delete *itObjContainer;

	//		*itObjContainer = nullptr;

	//		itObjContainer  = m_listObjContainer.erase(itObjContainer);

	//		//	後尾IDを更新
	//		GET_WORLD.GetGameWorld()->DeleteEndId();


	//		bool EnableUpdateId = false; // true(更新開始) : false(更新しない)

	//		//	登録されているオブジェクトのIDを更新する
	//		//for (int i = _ObjectIt; i < GET_WORLD.GetGameWorld()->GetEnd(); i++, ++itObjContainer)
	//		for (auto &It : GET_WORLD.GetGameWorld()->GetObjectManager()->GetObjectContainer())
	//		{
	//			if (It->GetId() == _ObjectIt + 1)
	//			{
	//				EnableUpdateId = true;
	//			}

	//			if (EnableUpdateId == true)
	//			{
	//				It->SetId(It->GetId() - 1);
	//			}
	//		}

	//		CRigidSupervise::SetStockId(-1);

	//		return;
	//	}
	//	//	違えば次のコンテナへ
	//	else
	//	{
	//		++itObjContainer;
	//	}
	//}


	////------------------------------
	////	オブジェクト内のIDを更新する
	////------------------------------

	//itObjContainer = m_listObjContainer.begin();

	//int i = 0;

	//while (itObjContainer != m_listObjContainer.end())
	//{

	//	(*itObjContainer)->SetId(i);

	//	i++;

	//}


	////auto It =  CRigidSupervise::GetItList().lock().begin;



}