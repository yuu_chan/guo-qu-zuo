#include "Scene.h"
#include "Font.h"
#include "CPicture.h"
#include "CLoadXFileManager.h"


CPicture::CPicture()
{
}


CPicture::~CPicture()
{
}


void CPicture::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	if (m_EffectId == SELECT_PICTURE::STAGE_1)
	{

		//	メッシュを読み込む
		m_Mesh = GET_XFILE_LOAD_MANAGER.Load("Data/XFile/Object/Stage1Picture.x", _lpD3DDevice);
	
	}
	else if (m_EffectId == SELECT_PICTURE::STAGE_2)
	{

		//	メッシュを読み込む
		m_Mesh = GET_XFILE_LOAD_MANAGER.Load("Data/XFile/Object/Stage2Picture.x", _lpD3DDevice);

	}
}


void CPicture::Update()
{
	//--------------------
	//	行列を更新しておく
	//--------------------

	const CVector3 SIZE = CVector3(8.0f, 8.0f, 8.0f);

	m_mWorld.CreateScale(&SIZE);

	m_mWorld.Move(&m_vPos);
}


void CPicture::Draw(LPDIRECT3DDEVICE9 _lpD3DDevice)
{

	//--------------
	//	メッシュ描画
	//--------------

	_lpD3DDevice->SetRenderState(D3DRS_LIGHTING, TRUE);

	_lpD3DDevice->SetTransform(D3DTS_WORLD, &m_mWorld);

	m_Mesh->DrawMesh();

	_lpD3DDevice->SetRenderState(D3DRS_LIGHTING, FALSE);

}


//	開放
void CPicture::Release()
{

}
