#include "Game.h"
#include "Scene.h"


//-------------------------
//	static const 値を初期化
//-------------------------

const float CPlayer::JUMP_CHAGE = 0.02f;

const float CPlayer::JUMP_GAUGE_MAX = CPlayer::JUMP_CHAGE * 10.0f;


CPlayer::CPlayer()
	:
	m_AnimationTime(0)
	,
	m_AnimationPeriod(false)
	,
	m_Grav(0)
	,
	m_StockCnt(0)
	,
	m_NormalAngY(0)
	,
	m_WaterInCnt(0)
	,
	m_NormalCalc(false)
	,
	m_jumpFlg(false)
	,
	m_EnableSpit(false)
	,
	m_spRigidSphere(nullptr)
	,
	m_WaterWeight(0.0f)
	,
	m_WaterFlow(0.0f)
	,
	m_SpitCnt(0)
	,
	m_ChageJump(0.0f)
	,
	m_AbleChage(false)
{
}


CPlayer::~CPlayer()
{
	m_spRigidSphere->Release();
	//m_Mesh = nullptr;//Release();
	m_Mesh->Release();
	m_Mesh = nullptr;
}


void CPlayer::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//--------------------
	//　スキンメッシュ準備
	//--------------------

	//	ファイルロード
	m_Mesh->LoadSkinMesh("Data/XFile/Character/Player.x", _lpD3DDevice);
	
	//	スキンメッシュをセット
	m_Mesh->SetSkinMesh();

	//	アニメーション再生時間設定
	m_Mesh->SetAdvanceTime(1.0f);

	//	アニメーションの切り替え時間設定
	m_Mesh->SetShiftTime(1.0f);

	//	アニメーションのループ時間設定
	m_Mesh->SetLoopTime(1.0f);

	//	プレイヤーアニメーション初期化
	m_Mesh->SetAnimationKey(ANIMATION_KEY::STAND);

	//	当たり判定用の面を生成
	m_Mesh->CreateSkinFaceData();

	//----------------------
	//　プレイヤー情報初期化
	//----------------------

	//	加速量初期化
	m_Accel = 0.0f;

	//	一度初期化してから生存フラグを立てておく
	m_State = GET_FLAG_CALC.SetFlag(m_State, STATE::INIT, STATE::DEFAULT);

	//	お腹の中に何も入っていない状態にしておく
	m_ItemState = GET_FLAG_CALC.SetData(m_ItemState, (STOCK::STATE::FREE | STOCK::STATE::NO_ABSORPTION), LOGICAL::OPERATION_MODE::OR);

	//	中心点へのぶれ値を設定
	m_GroundAdjustment = 0.42f;

	//	水中カウンター初期化
	m_WaterInCnt = 1;

	//	選択するパスを指定
	m_Pass = 0;

	m_Mesh->GetMeshState()->m_EmissivePower = 0.2f;


	//----------------------
	//	物理エンジンへの登録
	//----------------------

	m_spRigidSphere = make_shared<CBulletObj_Sphere>();

	CRigidSupervise::CreateRigidSphere(m_spRigidSphere, m_Mesh, m_vPos, m_Id);

	//	このタスクをUserPointerに記憶
	m_spRigidSphere->SetUserPointer(this);

	//	物理世界の行列取得
	m_spRigidSphere->GetMatrix(m_mWorld);


	m_Pass = 2;
}


//------
//	更新
//------
void CPlayer::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{

	//	移動速度
	auto				Speed = 0.11f;

	//	壁刷りを行うかどうか(四方キーで制御)
	bool				WallScratch = false;	//	true(行う) false(行わない)

	//	テンプレート方向
	CVector3			vTmpVec = CVector3(0, 0, 0);

	//	移動方向
	CVector3			vToVec = CVector3(0, 0, 0);

	//	レイを飛ばす方向
	CVector3			vRayDir = CVector3(0, 0, 1);


	//	現在ステージ2が選ばれたいたら
	if (CSceneManager::m_NowSelectStage == STAGE_SELECT::NAME::STAGE3)
	{
		//	ステージ2用処理を行う
		Stage2Update();
	}


	//if (GET_FLAG_CALC.CheckFlag(m_ItemState, STOCK::STATE::CHECK))


	//--------------------------------------
	//	ここからはどのステージでも共通の処理
	//--------------------------------------

	//	ゴール判定
	if (m_GoalCheck == false)
	{

		//--------------------------------------------------------------------------------------------
		//	何もしない状態ならスタンドを常に読んでおいてそれに+でなにかつける?
		//	m_AnimationKey = STAND + 変数(吸い込んだアイテムによって変動)
		//	スタンドや歩行のアクションにすべての頂点影響を設けてないため、アニメーションが引き継がれる
		//	で、各々のアイテムのSTAND状態にするとか
		//--------------------------------------------------------------------------------------------

		//	もし岩が入っていたら
		if (GET_FLAG_CALC.CheckFlag(m_ItemState, STOCK::STATE::CHECK, STOCK::STATE::ROCK))
		{	
			//	アニメーション再生時間
			m_Mesh->SetAdvanceTime(1.0f);

			//	アニメーションの切り替わる時間
			m_Mesh->SetShiftTime(1.0f);

			//	アニメーションのループ数(何ループに何回再生するか)
			m_Mesh->SetLoopTime(60.0f);

			//	再生するアニメーションを変更
			m_Mesh->SetAnimationKey(ANIMATION_KEY::STAND);

		}
		else if (GET_FLAG_CALC.CheckFlag(m_ItemState, 0x00ff, OBJECT_LIST::ID::SPRING_FLOWER))
		{
			//m_State = GET_FLAG_CALC.SetData(m_State, STATE::ACTION_SPRING_ON, LOGICAL::OPERATION_MODE::OR);

			//m_Mesh->SetAnimationKey(ANIMATION_KEY::EAT);

			//m_StockCnt++;

			////	再生時間
			//m_Mesh->SetAdvanceTime(0.5f);

			////	アニメーションが切り替わる時間
			//m_Mesh->SetShiftTime(30.0f);

			////	アニメーションのループ数(何ループに何回再生するか)
			//m_Mesh->SetLoopTime(60.0f);

			//if (m_StockCnt > 100)
			//{
			//	//m_ItemState = GET_FLAG_CALC.SetData((m_ItemState & STOCK::STATE::FREE), STOCK::STATE::NO_ABSORPTION, LOGICAL::OPERATION_MODE::OR);

			//	for (int i = GET_WORLD.GetGameWorld()->GetBegin(); i < GET_WORLD.GetGameWorld()->GetEnd(); i++)
			//	{
			//		auto Id = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(i)->GetId();

			//		if (Id == CRigidSupervise::GetStockId())
			//		{
			//			////	お腹の中に何も入っていない状態にしておく
			//			m_ItemState = GET_FLAG_CALC.SetData(((m_ItemState & 0x0000) | STOCK::STATE::NO_ABSORPTION), 0x0f00, LOGICAL::OPERATION_MODE::AND);

			//			m_State = GET_FLAG_CALC.SetData(m_State, STATE::ACTION_SPRING_ON, LOGICAL::OPERATION_MODE::OR);

			//			GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(Id)->SetItemState(STOCK::STATE::NO_ABSORPTION);

			//			break;
			//		}
			//	}
			//}

		}
	/*	else if (!GET_FLAG_CALC.CheckFlag(m_ItemState, 0x00ff, OBJECT_LIST::ID::SPRING_FLOWER))
		{
			m_State = GET_FLAG_CALC.SetData(m_State, 0xffef, LOGICAL::OPERATION_MODE::AND);
		}*/
		//	もし木箱が入っていたら
		else if (GET_FLAG_CALC.CheckFlag(m_ItemState, STOCK::STATE::CHECK, STOCK::STATE::WOOD_BOX))
		{
			
			//	アニメーションが最後まで到達しなければ
			if (m_AnimationPeriod == false)
			{
				//	アニメーションの5番目をセット
				m_Mesh->SetAnimationKey(ANIMATION_KEY::SWALLOW);

				m_AnimationPeriod = true;

				//	再生時間
				m_Mesh->SetAdvanceTime(0.5f);

				//	アニメーションが切り替わる時間
				m_Mesh->SetShiftTime(60.0f);

				//	アニメーションのループ数(何ループに何回再生するか)
				m_Mesh->SetLoopTime(60.0f);
			}
			//	最後まで到達すれば
			else if (m_AnimationPeriod == true)
			{
				m_Mesh->SetAnimationKey(ANIMATION_KEY::STOCK);

				//	自身に掛かる重力を変更
				m_ObjectWeight = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(CRigidSupervise::GetStockId())->GetObjectWeight();

				m_Grav = 0.1f * m_ObjectWeight;
			}

			
			//GET_WORLD.GetGameWorld()->GetObjectManager()->ItObjectKill(CRigidSupervise::GetStockId());
		}
		//	果物系
		else if (GET_FLAG_CALC.CheckFlag(m_ItemState, STOCK::STATE::CHECK, STOCK::STATE::APPLE)
				 ||
				 GET_FLAG_CALC.CheckFlag(m_ItemState, 0x00ff, STOCK::STATE::STRAW_BERRY)
				 )
		{
			m_Mesh->SetAnimationKey(ANIMATION_KEY::EAT);

			m_StockCnt++;

			//	再生時間
			m_Mesh->SetAdvanceTime(0.5f);

			//	アニメーションが切り替わる時間
			m_Mesh->SetShiftTime(20.0f);

			//	アニメーションのループ数(何ループに何回再生するか)
			m_Mesh->SetLoopTime(60.0f);

			if (m_StockCnt > 100)
			{
				//m_ItemState = GET_FLAG_CALC.SetData((m_ItemState & STOCK::STATE::FREE), STOCK::STATE::NO_ABSORPTION, LOGICAL::OPERATION_MODE::OR);

				for (int i = GET_WORLD.GetGameWorld()->GetBegin(); i < GET_WORLD.GetGameWorld()->GetEnd(); i++)
				{
					auto Id = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(i)->GetId();

					if (Id == CRigidSupervise::GetStockId())
					{
						//	アイテムの回復量
						auto Resilience = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(i)->GetItemResilience();

						//	体力を回復させる
						CStuffedMeter::m_StuffedMeter += Resilience;

						//GET_WORLD.GetGameWorld()->GetObjectManager()->ItObjectKill(i);


						////	一度初期化してから生存フラグを立てておく
						//m_State = GET_FLAG_CALC.SetFlag(m_State, STATE::INIT, STATE::DEFAULT);

						////	お腹の中に何も入っていない状態にしておく
						m_ItemState = GET_FLAG_CALC.SetData(((m_ItemState & 0x0000) | STOCK::STATE::NO_ABSORPTION), 0x0f00, LOGICAL::OPERATION_MODE::AND);


						GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(Id)->SetItemState(STOCK::STATE::NO_ABSORPTION);


						break;
					}
				}
				//}
			}
		}
		else
		{
			m_StockCnt = 0;
		}


		//----------
		//	移動処理
		//----------

		//	Wキーが押された
		//	&&
		//	ステータスがデフォルトである
		//	||
		//	Wキーが押された
		//	&&
		//	ステータスが浮力移動中である
		if (GET_KEY.m_Code.W == GET_KEY.HOLD)
		{
			//	壁刷りを行う
			WallScratch = true;

			//	アニメーションを歩行に変更
			m_Mesh->SetAnimationKey(ANIMATION_KEY::WALK);

			//	移動量を設定
			auto Move = (Speed - (m_Grav + m_WaterWeight));

			//	0よりも小さければ無理やり整数に変更させる
			if (Move < 0){ Move *= -1; }

			//	移動量用の変数に求めた移動量を格納
			m_vVec.Set(&CVector3(0.0f, 0.0f, Move));

			//	レイを飛ばす方向を決める
			vRayDir = CVector3(0, 0, 1);


			//----------------------------
			//	向きにあわせて移動量を作る
			//----------------------------

			//	回転行列
			CMatrix mRot;

			//	方向を求める
			D3DXMatrixRotationY(&mRot, D3DXToRadian(m_NormalAngY));

			//	vTmpVecに向きと移動量を格納
			D3DXVec3TransformCoord(&vTmpVec, &m_vVec, &mRot);

			//	加算させる
			vToVec += vTmpVec;

			//	内積計算を開始させる
			m_NormalCalc = true;

			//	移動量を加える
			MovePosition(vToVec);

		}
		//	Sキーが押された
		//	&&
		//	ステータスがデフォルトである
		//	||
		//	Sキーが押された
		//	&&
		//	ステータスが浮力移動中である
		else if (GET_KEY.m_Code.S == GET_KEY.HOLD)
		{
			//	壁刷りを行う
			WallScratch = true;


			m_Mesh->SetAnimationKey(ANIMATION_KEY::WALK);


			//	移動量を設定
			m_vVec.Set(&CVector3(0.0f, 0.0f, (-Speed + m_Grav) + m_WaterWeight));
			vRayDir = CVector3(0, 0, -1);

			//　向きにあわせて移動量を作る
			CMatrix mCanrad;
			D3DXMatrixRotationY(&mCanrad, D3DXToRadian(m_NormalAngY));
			D3DXVec3TransformCoord(&vTmpVec, &m_vVec, &mCanrad);
			vToVec += vTmpVec;
			m_NormalCalc = 1;



			//	移動量を加える
			MovePosition(vToVec);

		}
		//	Dキーが押された
		//	&&
		//	ステータスがデフォルトである
		//	||
		//	Dキーが押された
		//	&&
		//	ステータスが浮力移動中である
		else if (GET_KEY.m_Code.D == GET_KEY.HOLD)
		{
			//	壁刷りを行う
			WallScratch = true;


			m_Mesh->SetAnimationKey(ANIMATION_KEY::WALK);

			//	移動量を設定
			m_vVec.Set(&CVector3((Speed - m_Grav) - m_WaterWeight, 0.0f, 0.0f));
			vRayDir = CVector3(0, 1, 0);

			//　向きにあわせて移動量を作る
			CMatrix mCanrad;
			D3DXMatrixRotationY(&mCanrad, D3DXToRadian(m_NormalAngY));
			D3DXVec3TransformCoord(&vTmpVec, &m_vVec, &mCanrad);
			vToVec += vTmpVec;
			m_NormalCalc = 1;

			//	移動量を加える
			MovePosition(vToVec);

		}
		//	Aキーが押された
		//	&&
		//	ステータスがデフォルトである
		//	||
		//	Aキーが押された
		//	&&
		//	ステータスが浮力移動中である
		else if (GET_KEY.m_Code.A == GET_KEY.HOLD)
		{
			//	壁刷りを行う
			WallScratch = true;
			
			m_Mesh->SetAnimationKey(ANIMATION_KEY::WALK);


			//	移動量を設定
			m_vVec.Set(&CVector3((-Speed + m_Grav) + m_WaterWeight, 0.0f, 0.0f));
			vRayDir = CVector3(0, -1, 0);

			//　向きにあわせて移動量を作る
			CMatrix mCanrad;
			D3DXMatrixRotationY(&mCanrad, D3DXToRadian(m_NormalAngY));
			D3DXVec3TransformCoord(&vTmpVec, &m_vVec, &mCanrad);
			vToVec += vTmpVec;
			m_NormalCalc = 1;

			//	移動量を加える
			MovePosition(vToVec);

		}


		//	壁ずり判定がtrueなら壁刷りを行う
		if (WallScratch == true)
		{
			//	レイの発射位置を決める
			WallScratchUpdatePosition(CVector3(0, 0, 1));
		}


		//--------------
		//	ジャンプ処理
		//--------------

		//	キーが押し込まれたら
		//	&&
		//	フラグが移動状態もしくはデフォルトの場合
		//	&&
		//	ジャンプフラグがたっていなければ
		if (GET_KEY.m_Code.Space == GET_KEY.PUSH
			&&
			!GET_FLAG_CALC.CheckFlag((m_State & (STATE::ACTION_ABSORPTION_ON | STATE::DEFAULT)), STATE::ACTION_ABSORPTION_ON)
			&&
			!GET_FLAG_CALC.CheckFlag((m_State & (STATE::ACTION_SPRING_ON | STATE::DEFAULT)), STATE::ACTION_SPRING_ON)
			&&
			m_jumpFlg == false
			&&
			m_AbleChage == false)
		{
			//	加速量を設定
			m_Accel = GRAVITY_SPEED_MAX;

			//	ジャンプ中へ変更
			m_jumpFlg = true;

			//m_vScalSize.y -= 0.5f;

			//	SEを鳴らす
			//SOUNDPLAYER.PlayTheSound(SE::NAME::JUMP, true);
		}
		if (GET_KEY.m_Code.Space == GET_KEY.HOLD
			&&
			GET_FLAG_CALC.CheckFlag((m_State & (STATE::ACTION_SPRING_ON | STATE::DEFAULT)), STATE::ACTION_SPRING_ON)
			&&
			m_jumpFlg == false
			&&
			m_AbleChage == false)
		{
			m_ChageJump += JUMP_CHAGE;

			if (m_ChageJump >= JUMP_GAUGE_MAX)
			{
				m_ChageJump = JUMP_GAUGE_MAX;
			}
		}
		else if (GET_KEY.m_Code.Space == GET_KEY.PULL
				&&
				GET_FLAG_CALC.CheckFlag((m_State & (STATE::ACTION_SPRING_ON | STATE::DEFAULT)), STATE::ACTION_SPRING_ON)
				&&
				m_AbleChage == false
				)
		{
			m_Accel = GRAVITY_SPEED_MAX + m_ChageJump;

			m_jumpFlg = true;

			m_AbleChage = true;
		}
		else
		{
			m_ChageJump = 0.0f;
			m_AbleChage = m_jumpFlg;
		}


		//--------------
		//	吸い込み処理
		//--------------

		//	ステータスがデフォルトの場合
		//	&&
		//	アイテムがストックされていなければ
		//	&&
		//	キーが押されこまれていたら
		//	||
		//	ステータスがデフォルトの場合
		//	&&
		//	アイテムが吸い込まれている状態
		//	&&
		//	キーが押されこまれていたら
		if (/*GET_FLAG_CALC.CheckFlag(m_State, STATE::DEFAULT)
			&&
			GET_FLAG_CALC.CheckFlag(m_ItemState, STOCK::STATE::NO_ABSORPTION)
			&&
			GET_KEY.m_Code.MouseLeft == GET_KEY.HOLD
			&&
			m_EnableSpit == false
			||
			GET_FLAG_CALC.CheckFlag(m_State, STATE::DEFAULT)
			&&
			GET_FLAG_CALC.CheckFlag(m_ItemState, STOCK::STATE::DO_ABSORPTION)
			&&
			GET_KEY.m_Code.MouseLeft == GET_KEY.HOLD
			&&
			m_EnableSpit == false*/
			CheckActionKey() == true
			)
		{

			//　吸い込みアニメーション開始
			m_Mesh->SetAnimationKey(ANIMATION_KEY::ABSORPTION);

			//	アニメーションの再生速度を決めておく
			m_Mesh->SetAdvanceTime(1.5f);

			//　ステータスを吸い込み状態へ変更
			m_State = GET_FLAG_CALC.SetData(m_State, STATE::ACTION_ABSORPTION_ON, LOGICAL::OPERATION_MODE::OR);

			//	吸い込みアクション
			AbsorptionAction(vRayDir);

			//	吐き出し溜めカウンターオフ
			m_SpitCnt = 0;

			m_MaxCnt = 0;

			m_UpdateTag = "Abso";

		}
		//	体内のアイテムがストックされていた場合
		//	&&
		//	体内のアイテムが空気ではなかった場合
		//	&&
		//	キーがプッシュされたら
		else if (
			GET_FLAG_CALC.CheckFlag(m_ItemState, STOCK::STATE::DO_STOCK)
			&&
			GET_KEY.m_Code.MouseLeft == GET_KEY.HOLD			
			&&
			m_EnableSpit == false
			)
		{

			//	アニメーション再生時間
			m_Mesh->SetAdvanceTime(0.5f);

			//	アニメーションの切り替わる時間
			m_Mesh->SetShiftTime(1.0f);

			//	アニメーションのループ数(何ループに何回再生するか)
			m_Mesh->SetLoopTime(60.0f);

			//	再生するアニメーションを変更
			m_Mesh->SetAnimationKey(12);


			m_UpdateTag = "";


			m_MaxCnt += 1.0f;

			if (m_MaxCnt >= 50.0f)
			{
				m_MaxCnt = 50.0f;

				//	再生するアニメーションを変更
				m_Mesh->SetAnimationKey(13);
			}

		}
		else if (
			GET_FLAG_CALC.CheckFlag(m_ItemState, STOCK::STATE::DO_STOCK)
			&&
			GET_KEY.m_Code.MouseLeft == GET_KEY.PULL
			&&
			m_EnableSpit == false
			)
		{
			m_ItemState = GET_FLAG_CALC.SetData((m_ItemState & STOCK::STATE::FREE), STOCK::STATE::DO_SPIT, LOGICAL::OPERATION_MODE::OR);
		}
		//	体内のアイテムが吐き出された場合
		//	&&
		//	キーが離されたら
		//  ||
		//	キーが離されたら
		else if (
			GET_FLAG_CALC.CheckFlag(m_ItemState, STOCK::STATE::DO_SPIT)
			&&
			GET_KEY.m_Code.MouseLeft == GET_KEY.FREE			
			||
			GET_KEY.m_Code.MouseLeft == GET_KEY.FREE			
/*			||
			GET_FLAG_CALC.CheckFlag(m_ItemState, STOCK::STATE::DO_SPIT)
			&&
			GET_KEY.m_Code.MouseRight == GET_KEY.FREE
			||
			GET_KEY.m_Code.MouseRight == GET_KEY.FREE*/)
		{

			//	吸い込みアクションをしていない場合はOFFに
			m_State = GET_FLAG_CALC.SetData(m_State, STATE::ACTION_ABSORPTION_OFF, LOGICAL::OPERATION_MODE::AND);

			//	吸い込んだ状態担っていた場合
			//　デフォルトに戻しておく
			//	吐き出しが行われたら
			//	1フレームだけ128になるので、その128で吐き出された処理をかくといい
			//	そうすると、吐き出されるとすぐに吸い込まれる処理を防げる
			if (//GET_FLAG_CALC.CheckFlag(m_ItemState, STOCK::STATE::DO_SPIT)
				//||
				GET_FLAG_CALC.CheckFlag(m_ItemState, STOCK::STATE::DO_ABSORPTION))
			{
				//	通常状態(吸い込んでいない)に戻す
				m_ItemState = GET_FLAG_CALC.SetData((m_ItemState & STOCK::STATE::FREE), (STOCK::STATE::FREE | STOCK::STATE::NO_ABSORPTION), LOGICAL::OPERATION_MODE::OR);
			}

			if (GET_FLAG_CALC.CheckFlag(m_ItemState, STOCK::STATE::DO_SPIT))
			{
				//	吐き出しアクション
				SpitAction();
			}

			if (GET_FLAG_CALC.CheckFlag(m_ItemState, STOCK::STATE::DO_STOCK))
			{
				m_EnableSpit = false;
			}
		}
		/*else if (GET_KEY.m_Code.MouseLeft == GET_KEY.FREE && m_EnableSpit == true)
		{
			m_EnableSpit = false;
		}*/


		//----------------------
		//	座標や、各行列を更新
		//----------------------


		if (GET_FLAG_CALC.CheckFlag(m_State, 0xf000, 0x4000))
		{
			m_spRigidSphere->SetMass(1.0f);

			m_spRigidSphere->ApplyForce(CVector3(0.0f, 0.0f, 1.0f));

			m_spRigidSphere->GetMatrix(m_mWorld);
		}
		if (!GET_FLAG_CALC.CheckFlag(m_State, 0xf000, 0x4000))
		{
			//　行列更新
			UpdateMatrix();
		}

		//	ジャンプ
		StandPosition();

		GET_HELPER.WindZone(GET_WORLD.GetGameWorld()->GetObjectManager());

		//	キャラクターのアニメーションや行列などを更新
		m_Mesh->UpdateSkinMesh(m_mWorld);

		//	アニメーションカウンターを返させる
		//m_AnimationTime = m_Mesh->GetAnimeController()->GetAnimationTime();

		//	特に変更がなければ通常状態へ
		if (GET_FLAG_CALC.CheckFlag(m_ItemState, 0xffff, STOCK::STATE::NO_ABSORPTION))
		{

			//	アニメーション再生時間
			m_Mesh->SetAdvanceTime(1.0f);

			//	アニメーションの切り替わる時間
			m_Mesh->SetShiftTime(1.0f);

			//	アニメーションのループ数(何ループに何回再生するか)
			m_Mesh->SetLoopTime(60.0f);

			//	再生するアニメーションを変更
			m_Mesh->SetAnimationKey(ANIMATION_KEY::STAND);

			//	重力はかけない
			m_Grav = 0.0f;

			m_ObjectWeight = 0.2f;

			//	アニメーションタイムを初期化
			m_AnimationTime = 0.0f;

		}

		//	ライフゲージ更新
		CStuffedMeter::UpdateStuffedMeter();

		//　何もしていない場合は、ステータスをデフォルトにしておく
		m_State = GET_FLAG_CALC.SetData(m_State, 0xffff, LOGICAL::OPERATION_MODE::AND);


	}
	//	ゴールしていたら
	else if (m_GoalCheck == true)
	{
		//	再生時間
		m_Mesh->SetAdvanceTime(0.5f);

		//	アニメーションが切り替わる時間
		m_Mesh->SetShiftTime(20.0f);

		//	アニメーションのループ数(何ループに何回再生するか)
		m_Mesh->SetLoopTime(60.0f);

		//	アニメーションを切り替える
		m_Mesh->SetAnimationKey(ANIMATION_KEY::CLEAR);

		//	スキンメッシュを更新させる
		m_Mesh->UpdateSkinMesh(m_mWorld);
	}


	if (GetAsyncKeyState('G'))
	{
		m_vPos.y = 20.0f;
	}
}


void CPlayer::Draw(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	ライティング開始
	_lpD3DDevice->SetRenderState(D3DRS_LIGHTING, TRUE);


	//	マイシェーダー側にワールド行列を送る
	GET_SHADER.GetMyShader()->SetConstTransformWorld(&m_mWorld);

	//	マイシェーダー描画を行う(シャドウマップ入り)
	GET_SHADER.GetMyShader()->DrawSkinMesh(m_Mesh, m_Pass);


	//	ライティング終了
	_lpD3DDevice->SetRenderState(D3DRS_LIGHTING, FALSE);
}


void CPlayer::Release()
{
}


void CPlayer::UpdateMatrix()
{

	//	Y軸はカメラと一致させておくが、移動した場合のみ
	m_NormalAngY = GET_MOUSE.GetAngle().y;


	////	FPS視点に変更した場合視点と同じ位置に回転する
	//if (GET_FLAG_CALC.CheckFlag(GET_CAMERA.CameraMode, GET_CAMERA.Mode::FPS, LOGICAL::OPERATION_MODE::AND))
	//{
	//	//	FPS視点が選択された場合、すべてのY軸はカメラと一致させておく
	//	m_NormalAngY = m_vAng.y = GET_MOUSE.GetAngle().y;
	//}


	//	拡大座標
	m_mWorld.CreateScale(&m_vScalSize);

	//	座標をセット
	m_mWorld.Move(&m_vPos);

	//	Y軸をセット
	m_mWorld.RotateY_Local(m_vAng.y);

	//	X軸をセット
	m_mWorld.RotateX_Local(m_vAng.x);

	//	Z軸をセット
	m_mWorld.RotateZ_Local(m_vAng.z);

	//	ワールドをセット
	m_spRigidSphere->SetMatrix(m_mWorld);

	//	質量を0にしておく
	m_spRigidSphere->SetMass(0.0f);


}


//----------
//	通常移動
//----------
void CPlayer::MovePosition(CVector3 _vToVec)
{
	//  内積を求めてる
	if (m_NormalCalc == true)
	{
		//	現在向いている方向
		CVector3 NowVec;

		//	内積を求めさせる
		float Dot;

		//	移動量をポジションに加算
		m_vPos.Add(&_vToVec);

		//	回転量をセット
		m_mWorld.CreateRotateY(m_vAng.y);

		//	向いている方向を出す
		D3DXVec3TransformNormal(&NowVec, &CVector3(0, 0, 1), &m_mWorld);

		//	正規化を行う
		D3DXVec3Normalize(&_vToVec, &_vToVec);

		//	内積を求める
		Dot = D3DXVec3Dot(&_vToVec, &NowVec);

		//	角度に変換する
		Dot = D3DXToDegree(acos(Dot));

		//	内積がある程度の値を超えていたら
		if (Dot >= 0.1f)
		{
			//	外積を求める変数
			CVector3 Cross;

			//	外積を求める
			D3DXVec3Cross(&Cross, &NowVec, &_vToVec);

			//	正規化する
			D3DXVec3Normalize(&Cross, &Cross);

			//	内積の回転量
			if (Dot >= 10)
				//	超えないようにする
				Dot = 10.0f;

			//	外積がある程度超えれば
			if (Cross.y >= 0.9f)
				//	Y軸に内積を加算
				m_vAng.y += Dot;
			//	外積がある程度超えれば
			else if (Cross.y <= -0.9f)
				//	Y軸に内積を減算
				m_vAng.y -= Dot;
			else
				//	Y軸に内積を加算
				m_vAng.y += Dot;
		}
		//	360超えさせないようにする
		m_vAng.y = float((int)m_vAng.y % 360);
	}
}


void CPlayer::StandPosition()
{

	if (GET_FLAG_CALC.CheckFlag(m_State, STATE::ACTION_SPRING_ON, STATE::ACTION_SPRING_ON))
	{
		//	加算する
		m_vPos.y += m_Accel;

		if (m_AbleChage == true)
		{
			//	加速量更新	(0.025f + アイテムの重量)
			m_Accel -= (GRAVITY_SPEED)-m_ChageJump;

			//	加速量の制限
			if (m_Accel <= -(GRAVITY_SPEED_MAX - m_ChageJump))
				m_Accel = -(GRAVITY_SPEED_MAX - m_ChageJump);

		}
		else if (m_AbleChage == false)
		{
			//	加速量更新	(0.025f + アイテムの重量)
			m_Accel -= (GRAVITY_SPEED);

			//	加速量の制限
			if (m_Accel <= -(GRAVITY_SPEED_MAX))
				m_Accel = -(GRAVITY_SPEED_MAX);
		}
	}
	else if (!GET_FLAG_CALC.CheckFlag(m_State, STATE::ACTION_ABSORPTION_ON, STATE::ACTION_ABSORPTION_ON))
	{
		//	加算する
		m_vPos.y += m_Accel;

		//	加速量更新	(0.025f + アイテムの重量)
		m_Accel -= ((GRAVITY_SPEED + m_Grav));

		//	加速量の制限
		if (m_Accel <= -(GRAVITY_SPEED_MAX + (m_Grav * 5)))
			m_Accel = -(GRAVITY_SPEED_MAX + (m_Grav * 5));
	}

	//	リストの先頭から後尾までループをまわす
	for (int i = GET_WORLD.GetGameWorld()->GetBegin() + 1; i < GET_WORLD.GetGameWorld()->GetEnd(); i++)
	//for (auto &It : GET_WORLD.GetGameWorld()->GetObjectManager()->GetObjectContainer())
	{
		//	対象オブジェクトとの床判定を行う
		GET_HELPER.GameWorldStandPosition(m_Id, i, m_GroundAdjustment, m_jumpFlg);
	}

}


void CPlayer::AbsorptionAction(CVector3 _RayDir)
{

	/*m_ItemState = GET_FLAG_CALC.SetData(m_ItemState, STOCK::STATE::DO_ABSORPTION, LOGICAL::OPERATION_MODE::OR);
*/

	//------------------------------------------------------
	//	コリジョン側で当たり判定とステータス管理をしてくれる
	//------------------------------------------------------

	//	ステージ以外
	//for (int i = GET_WORLD.GetGameWorld()->GetBegin() + 2; i < GET_WORLD.GetGameWorld()->GetEnd(); i++)
	for (int i = m_Id + 1; i < CRigidSupervise::GetEnd(); i++)
	{
		bool Check = false;

		//	コリジョンをまわす
		//GET_HELPER.RangeDoAbsorption(m_Id, i, 0.8f, CVector3(0, 0, 1));
		Check = GET_HELPER.RangeDoAbsorption(m_Id, i);

		if (Check == true)
		{
			m_EnableSpit = true;
		}
	}

}


void CPlayer::SwallowAction()
{
	////	アニメーションの状態判定
	//for (auto i = m_FirstId; i < m_LastId; i++)
	//{

	//}
}


void CPlayer::SpitAction()
{
	
	for (auto i = GET_WORLD.GetGameWorld()->GetBegin() + 1; i < GET_WORLD.GetGameWorld()->GetEnd(); i++)
	{
		//	アニメーションの状態判定
		Spit(m_Id, i);
	}

	//	デフォルトの姿に戻す
	//m_Mesh->SetAnimationKey(0);

}


//---------------------------------------------------
//		プレイヤーが吐き出し処理を行った際に
//		吐き出されるために必要な情報を
//		お互いのオブジェクトに情報を渡す
//		それ以外のものが入ってきた場合はreturnを返す
//
//
//		あくまで、座標をセットするだけで、
//		アイテムの挙動は別で行う
//---------------------------------------------------
void CPlayer::Spit(
	int _CollisionObject_Id_1,
	int _CollisionObject_Id_2)
{


	//----------------------------------------------------
	//	STOCK以外の情報が入っていた場合は
	//	STOCK状態のアイテムが今体内に入っているアイテム
	//	なので、他のアイテムのステータスが変わることはない
	//	returnさせておく
	//----------------------------------------------------

	//	ステータスをコピーするために変数を用意
	int CheckState;

	//	ステータスをコピーする
	CheckState = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetItemState();

	//	STOCK以外が入っていないかチェックする
	if (!GET_FLAG_CALC.CheckFlag(CheckState, STOCK::STATE::DO_STOCK))
		return;

	//----------------------
	//	アニメーションの更新
	//----------------------

	m_SpitCnt++;

	//	アニメーションの5番目をセット
	m_Mesh->SetAnimationKey(ANIMATION_KEY::SPIT);

	//	再生時間
	m_Mesh->SetAdvanceTime(0.5f);

	//	アニメーションが切り替わる時間
	m_Mesh->SetShiftTime(10.0f);

	//	アニメーションのループ数(何ループに何回再生するか)
	m_Mesh->SetLoopTime(60.0f);

	if (m_SpitCnt > 70)
	{

		//--------------
		//	吐き出し処理
		//--------------

		//--------------------------------------------------
		//	  STOCKが入っていた場合
		//
		//	�@STOCKされているアイテムの座標を更新する
		//
		//	�ASTOCKされているアイテムの回転軸を更新する
		//
		//	�Bステータスは最後に吐き出された状態に戻しておく
		//--------------------------------------------------

		//--------------
		//	�@座標を更新
		//--------------

		//	座標を更新させるために必要な変数
		CVector3 UpdatePos = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_1)->GetPosition();

		//	座標を少し手前に置くために向きと座標を作る
		CVector3 SetPos;

		//	吐き出す位置の修正値を決めておく
		float	 SpitAdjustment = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetSpitAdjustment();

		//	吐き出す側の向きをあわせておく
		D3DXVec3TransformNormal(&SetPos, &CVector3(0, 0, 1.0f + SpitAdjustment), &GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_1)->GetWorld());

		//	求めた二つの変数を足し合わせる
		UpdatePos = UpdatePos + SetPos;

		//	座標を相手側に返す
		GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->SetPosition(UpdatePos);


		//----------------
		//	�A回転軸を更新
		//----------------

		//	回転軸を更新させるために必要な変数
		CMatrix UpdateRotation = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_1)->GetWorld();

		//	回転軸をセット
		UpdateRotation.CreateRotateY(m_vAng.y);

		//	回転軸を返す
		GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->SetWorld(UpdateRotation);

		//	Angleも渡しておく
		GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->SetAngle(m_vAng);


		//--------------------------------------------------------------
		//	�B最後にステータスを変更し、相手側のオブジェクトと自身に送る
		//--------------------------------------------------------------

		//	作業用変数
		int State1 = 0;
		int State2 = 0;

		//	ステータスを一度確保してくる
		State1 = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_1)->GetItemState();
		State2 = GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->GetItemState();

		//  ステータスを変更
		State1 = GET_FLAG_CALC.SetData((State1 & STOCK::STATE::FREE), STOCK::STATE::NO_ABSORPTION, LOGICAL::OPERATION_MODE::OR);
		State2 = GET_FLAG_CALC.SetData((State2 & STOCK::STATE::FREE), STOCK::STATE::DO_SPIT, LOGICAL::OPERATION_MODE::OR);

		//	変更したステータスを送る
		GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_1)->SetItemState(State1);
		GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(_CollisionObject_Id_2)->SetItemState(State2);

		//	アニメーションのフラグをfalseにもどす
		m_AnimationPeriod = false;

	}

}


void CPlayer::WallScratchUpdatePosition(CVector3 _RayDir)
{
	
	////	リストの先頭から後尾まで回す
	for (int i = GET_WORLD.GetGameWorld()->GetBegin() + 2; i < GET_WORLD.GetGameWorld()->GetEnd(); i++)
	{

		if (CSceneManager::m_NowSelectStage == STAGE_SELECT::NAME::STAGE1)
		{
			//	壁ずりを行う
			GET_HELPER.WallScratch(m_Id, i, CVector3( 0, 0,  1), CVector3( 0, 0,  1), 0.6f);
			//GET_HELPER.WallScratch(m_Id, i, CVector3( 0, 0, -1), CVector3( 0, 0, -1), 0.3f);
			//GET_HELPER.WallScratch(m_Id, i, CVector3( 1, 0,  0), CVector3( 1, 0,  0), 0.1f);
			//GET_HELPER.WallScratch(m_Id, i, CVector3(-1, 0,  0), CVector3(-1, 0,  0), 0.1f);
		}


		if (CSceneManager::m_NowSelectStage == STAGE_SELECT::NAME::STAGE2)
		{
			GET_HELPER.WallScratch(m_Id, i, CVector3(0, 0, 1), CVector3(0, 0, 1), 0.3f);
			//GET_HELPER.WallScratch(m_Id, i, CVector3(0, 0, -1), CVector3(0, 0, -1), 0.3f);
			//GET_HELPER.WallScratch(m_Id, i, CVector3(1, 0, 0), CVector3(1, 0, 0), 0.2f);
			//GET_HELPER.WallScratch(m_Id, i, CVector3(-1, 0, 0), CVector3(-1, 0, 0), 0.2f);
		}
	}
	


	//------------------------------------------------------------
	//	ゲームワールド内のオブジェクト(プレイヤーを除く)全てまわす
	//------------------------------------------------------------
	for (int i = GET_WORLD.GetGameWorld()->GetBegin() + 1; i < GET_WORLD.GetGameWorld()->GetEnd(); i++)
	{
		CVector3	vPos	= CVector3(0, 0, 0);
		bool		Check	= false;


		//----------------------------------------------
		//	面テーブルが保存されているかどうかを確認して
		//	入っていれば当たり判定を行う信号を出す
		//----------------------------------------------
		if (GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(i)->GetMeshObject()->GetFace().size() >= 1)
		{
			Check = true;
		}


		//--------------------------------------
		//	当たり判定が行われる信号がでていれば
		//	当たり判定を行う
		//--------------------------------------
		if (Check == true)
		{
			CCollision::FaceToSphere(
				&GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(i)->GetMeshObject()->GetFace()[0],
				GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(i)->GetMeshObject()->GetFace().size(),
				&m_vPos,
				m_Mesh->GetBounding()->GetBoundingSphereRadius() - 0.5f,
				&vPos,
				i
				);

			m_mWorld.CreateMove(&vPos);
		}
	}
}


void CPlayer::Stage2Update()
{

	//	オブジェクトリストの先頭から後尾までまわす
	for (auto &This : GET_WORLD.GetGameWorld()->GetObjectManager()->GetObjectContainer())
	{

		//----------------------------------
		//	水中にいれば、ゆっくりと沈ませる
		//----------------------------------

		bool WaterIn;

		float UnderWater;

		//	水中にいるかどうかを判定させる
		WaterIn = GET_HELPER.UnderDecision(m_Id, This->GetId(), UnderWater, CVector3(0, 1, 0), CVector3(0.0f, -0.4f, 0.0f));

		if (WaterIn == true)
		{
			
			m_Accel = -0.005f;

			break;
		}

	}


	//	今は必要ない
	/*
	//for (auto i = GET_WORLD.GetGameWorld()->GetBegin() + 1; i < GET_WORLD.GetGameWorld()->GetEnd(); i++)
	//{
	//	//----------
	//	//	水中処理

	//	//	水中判定用の変数
	//	bool WaterIn;	//	true(水中) false(水中にはいない)

	//	//	水深
	//	float UnderWater = 0.0f;

	//	//	水中判定を行う
	//	WaterIn = GET_HELPER.UnderDecision(m_Id, i, UnderWater, CVector3(0, 1, 0), CVector3(0.0f, -0.4f, 0.0f));

	//	//	水の中に入ったら
	//	//	&&
	//	//	ストックされているアイテムが重い物ではなかった場合
	//	if (WaterIn == true
	//		&&
	//		!GET_FLAG_CALC.CheckFlag(m_ItemState, STOCK::STATE::CHECK, STOCK::STATE::ROCK))
	//	{

	//		//	オブジェクトのIDが川であること
	//		if (OBJECT_LIST::ID::RIVER == GET_WORLD.GetGameWorld()->GetObjectManager()->GetItObject(i)->GetItemId())
	//		{
	//			//	川に流される量を決めておく
	//			m_WaterFlow = 0.09f;

	//			//	加算
	//			//m_vPos.x += m_WaterFlow;
	//		}

	//		//	水中での動きを制限
	//		m_WaterFlow = 0.08f;

	//		//	落下速度を変更
	//		m_Accel = -0.005f;

	//		//	ループを終了させる
	//		break;
	//	}
	//	//	水に入っていなければ
	//	else if (WaterIn == false)
	//	{
	//		//	水中の重みを消しておく
	//		m_WaterFlow = 0.0f;
	//	}
	//}
	*/
}


const int	CStuffedMeter::STUFFED_METER_MAX = 105;

const int	CStuffedMeter::STUFFED_METER_MIN = 0;

int			CStuffedMeter::m_StuffedMeter	 = CStuffedMeter::STUFFED_METER_MAX;

int			CStuffedMeter::m_Recovery		 = 0;

int			CStuffedMeter::m_DownMeter		 = 1;


void CStuffedMeter::UpdateStuffedMeter()
{
	if (APP.m_FrameCnt % 80 == 0)
	{
		//	体力減少更新
		m_StuffedMeter -= (m_DownMeter - m_Recovery);
	}


	//	回復力が更新された場合、回復した後に0に戻す
	if (m_Recovery > 0
		||
		m_Recovery < 0)
	{
		m_Recovery = 0;
	}

	if (m_StuffedMeter < STUFFED_METER_MIN)
	{
		m_StuffedMeter = STUFFED_METER_MIN;
	}
	else if (m_StuffedMeter > STUFFED_METER_MAX)
	{
		m_StuffedMeter = STUFFED_METER_MAX;
	}
}


bool CPlayer::CheckActionKey()
{
	//------------------------
	//	キーが押されたかどうか
	//------------------------

	if (GET_KEY.m_Code.MouseLeft != GET_KEY.HOLD)
	{
		return false;
	}

	if (GET_KEY.m_Code.Space == GET_KEY.HOLD)
	{
		return false;
	}


	//--------------------------------------
	//	条件が合うフラグがたっているかどうか
	//--------------------------------------

	if (!GET_FLAG_CALC.CheckFlag(m_State, STATE::DEFAULT))
	{
		return false;
	}

	if (!GET_FLAG_CALC.CheckFlag(m_ItemState, STOCK::STATE::NO_ABSORPTION))
	{
		return false;
	}

	if (m_jumpFlg == true)
	{
		return false;
	}

	if (m_EnableSpit == true)
	{
		return false;
	}



	return true;
}