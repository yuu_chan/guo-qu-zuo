#include "Game.h"


CRiver::CRiver()
{

}


CRiver::~CRiver()
{

}


void CRiver::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	m_Mesh = GET_XFILE_LOAD_MANAGER.Load("Data/XFile/Object/River.x", _lpD3DDevice);

	m_Mesh->GetMeshState()->m_Refract = 1;
}


void CRiver::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	m_mWorld.CreateMove(&m_vPos);
}


void CRiver::Draw(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	GET_SHADER.GetWaterShader()->DrawMesh(m_Mesh, 0);
}


void CRiver::Release()
{

}

