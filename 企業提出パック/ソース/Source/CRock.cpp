#include "Shader.h"
#include "Game.h"

CRock::CRock()
{
}

CRock::~CRock()
{
	if (m_UseType == SIZE::SMALL)
	{
		m_spRigidSphere->Release();
	}
	else if (m_UseType == SIZE::LARGE)
	{
		m_spRigidBox->Release();
	}
}

void CRock::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	登録するサイズを決める(重さが1.0fかどうか)
	m_UseType = (m_ObjectWeight <= 1.0f && m_ObjectWeight > 0.9f) ? m_UseType = SIZE::LARGE : m_UseType = SIZE::SMALL;


	//----------------------
	//	最小サイズの岩を登録
	//----------------------
	if (m_UseType == SIZE::SMALL)
	{
		//	メッシュを読み込み
		m_Mesh = GET_XFILE_LOAD_MANAGER.Load("Data/XFile/Object/Rock.x", _lpD3DDevice);

		m_spRigidSphere = make_shared<CBulletObj_Sphere>();

		CRigidSupervise::CreateRigidSphere(m_spRigidSphere, m_Mesh, m_vPos, m_Id, m_Mesh->GetBounding()->GetBoundingSphereRadius() / 2.0f);

		m_spRigidSphere->SetUserPointer(this);

		m_spRigidSphere->GetMatrix(m_mWorld);


		//m_spRigidSphere->GetBody()->
		m_spRigidSphere->getShape()->setMargin(0.001f);
	}
	//----------------------
	//	最大サイズの岩を登録
	//----------------------
	else if (m_UseType == SIZE::LARGE)
	{
		//	メッシュを読み込み
		m_Mesh = GET_XFILE_LOAD_MANAGER.Load("Data/XFile/Object/LargeRock.x", _lpD3DDevice);

		m_spRigidBox = make_shared<CBulletObj_Box>();

		CRigidSupervise::CreateRigidBox(m_spRigidBox, m_Mesh, m_vPos, m_Id);

		m_spRigidBox->SetUserPointer(this);

		m_spRigidBox->GetMatrix(m_mWorld);
	}

	m_Mesh->GetMeshState()->m_EnableInk = true;
}

void CRock::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{

	//	共通の更新処理
	ObjectMove();


	//----------------------------
	//	サイズによって命令を変える
	//----------------------------

	if (m_UseType == SIZE::SMALL)
	{
		m_spRigidSphere->GetMatrix(m_mWorld);
	}
	else if (m_UseType == SIZE::LARGE)
	{
		if (m_UpdateTag == "PHit")
		{
			m_spRigidBox->GetMatrix(m_mWorld);
			m_vPos = m_mWorld.GetPos();
			m_spRigidBox->SetMatrix(m_mWorld);
		}
		if (!GET_FLAG_CALC.CheckFlag(m_State, 0x2000, 0x2000))
		{
			m_spRigidBox->GetMatrix(m_mWorld);
		}
		else
		{
			m_spRigidBox->SetMatrix(m_mWorld);
		}
	}


	//	座標を取得しておく
	m_vPos = m_mWorld.GetPos();

}

void CRock::Release()
{
}