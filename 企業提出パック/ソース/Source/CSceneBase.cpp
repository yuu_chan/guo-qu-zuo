#include "CSceneBase.h"
#include "Scene.h"
#include "CFade.h"


CSceneBase::CSceneBase()
{

}


CSceneBase::~CSceneBase()
{

}


int CSceneBase::EnableNextScene(int _NowScene, bool& _Back, bool _Enable)
{
	//	エラーチェック
	if (_NowScene > SCENE::NAME::MAX + 1)
	{
		//	メッセージをだす
		MessageBox(NULL, "予定外の数値が入力されました", "警告", MB_OK);
		
		//	何もないという情報を返す
		return SCENE::NAME::NOT;
	}


	//------------
	//	シーン移動

	//	エンターキーが押されれば
	if (GET_KEY.m_Code.Return == GET_KEY.PUSH)
	{
		//	一つ前のシーンへ移動するためのフラグを初期化
		_Back = false;

		//	現在タイトルシーンにいれば
		if (_NowScene == SCENE::NAME::NOW_TITLE)
		{
			//_NowScene = SCENE::NAME::STAGE_SELECT_INIT;

			//	フェードインフェードアウトを行う
			GET_FADE.SetEnableFade(true);

			//	SEを鳴らす
			SOUNDPLAYER.PlayTheSound(SE::NAME::CLICK, true);
		}
		//	現在ステージセレクトにいれば
		else if (_NowScene == SCENE::NAME::NOW_STAGE_SELECT && CStageSelectScene::m_AbleNext == true)
		{
			//_NowScene = SCENE::NAME::GAME_INIT;

			//	フェードインフェードアウトを行う
			GET_FADE.SetEnableFade(true);

			//	SEを鳴らす
			SOUNDPLAYER.PlayTheSound(SE::NAME::CLICK, true);
		}
	}

	if (_NowScene == SCENE::NAME::NOW_STAGE_SELECT && CStageSelectScene::m_AbleNext == true)
	{
		//_NowScene = SCENE::NAME::GAME_INIT;

		//	フェードインフェードアウトを行う
		GET_FADE.SetEnableFade(true);

		//	SEを鳴らす
		SOUNDPLAYER.PlayTheSound(SE::NAME::CLICK, true);
	}

	//	ここまで
	//------------


	//	ゲームシーンでゲームクリアかなんかが着たら
	if (_Enable == true)
	{
		//	タイトルへ戻す
		_NowScene = SCENE::NAME::TITLE_INIT;
	}
	
	//	現在のシーン(変更済み)を返す
	return _NowScene;
}