#include "CSceneBase.h"
#include "Scene.h"
#include "CSceneManager.h"
#include "CFade.h"
#include "Game.h"


//	現在選んでいるステージを初期化
int CSceneManager::m_NowSelectStage = STAGE_SELECT::NAME::STAGE1;


CSceneManager::CSceneManager()
	:
	m_NowScene(0)
{
}


CSceneManager::~CSceneManager()
{
	Release();

	GET_FADE.Release();
	GET_XFILE_LOAD_MANAGER.AllClear();
	GET_TEXTURE_LOAD_MANAGER.AllClear();
}


void CSceneManager::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	フェードインフェードアウト用テクスチャ初期化
	GET_FADE.Init(_lpD3DDevice);



	//--------------------
	//	現在のシーンを設定
	//--------------------
	m_NowScene  = SCENE::NAME::TITLE_INIT;

	m_SceneStep = SCENE::STEP::INIT;

}


void CSceneManager::Proc(LPDIRECT3DDEVICE9 _lpD3DDevice)
{

	//------------
	//	作業用変数
	//------------

	//	セットするライトの方向
	CVector3 LightDir = CVector3(1, -1, 0);

	//	正規化
	D3DXVec3Normalize(&LightDir, &LightDir);


	//----------
	//	初期設定
	//----------

	//	ライトの設定をデフォルトにしておく
	GET_DIRECT_HELPER.DefaultLight(LightDir);

	//	アルファブレンドを可能にしておく
	GET_DIRECT_HELPER.AlphaBlendEnable(TRUE);

	//	アルファブレンドの設定をしておく
	GET_DIRECT_HELPER.Blend_Alpha();


	//---------------------------------------------------------
	//	Enterキーが押されると次のシーンの情報がかえってくる
	//	(ゲームシーンでの処理は別で、ゴールフラグかなんかを返す
	//---------------------------------------------------------
	m_NowScene = CSceneBase::EnableNextScene(m_NowScene, m_AbleBack);


	//	初期化  更新描画  開放のいずれかを行う
	SceneProcess(_lpD3DDevice);


	GET_FADE.Update();
	GET_FADE.Draw(_lpD3DDevice);

}


void CSceneManager::Release()
{

	//	使用したコンテナを開放させる
	auto It = m_vspScene.begin();

	while (It != m_vspScene.end())
	{
		*It = nullptr;

		It = m_vspScene.erase(It);
	}

}


void CSceneManager::SceneProcess(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	const int UPDATE = 0;


	if (m_SceneStep == SCENE::STEP::INIT)
	{
		//	シーン初期化
		SceneInit();

		m_vspScene[UPDATE]->Init(_lpD3DDevice);


		//	更新描画へ移る
		const int NEXT = 1;

		m_SceneStep += NEXT;
	}
	else if (m_SceneStep == SCENE::STEP::UPDATE)
	{
		//	シーン更新
		SceneUpdate();

		m_vspScene[UPDATE]->Update(_lpD3DDevice);
		m_vspScene[UPDATE]->Draw(_lpD3DDevice);
	}
	else if (m_SceneStep == SCENE::STEP::RELEASE)
	{
		m_vspScene[UPDATE]->Release();

		CRigidSupervise::DeleteItList();

		//	シーン開放
		SceneRelease();

		//	初期化ステップへ変更させておく
		m_SceneStep = SCENE::STEP::INIT;
	}



	//	エスケープキー管理
	if (GET_KEY.m_Code.Escape == GET_KEY.PUSH)
	{
		//	シーン毎に変更
		if (m_NowScene == SCENE::NAME::NOW_TITLE)
		{
			//	メッセージ取得用変数
			int Message;

			//	メッセージを返させる
			Message = MessageBox(NULL, "ゲームを終了しますか?", "", MB_YESNO);

			//	YESが帰ってきたら
			if (Message == IDYES)
			{
				//	ゲームを終了する
				APP.m_CloseFlag = true;
			}
			//	NOが帰ってきたら
			else if (Message == IDNO)
			{
				//	なにもしない
			}
		}

		if (m_NowScene == SCENE::NAME::NOW_STAGE_SELECT && m_AbleBack == false)
		{
			//	前のシーンへ
			GET_FADE.SetEnableFade(true);
		}

		if (m_NowScene == SCENE::NAME::NOW_GAME)
		{
			//	前のシーンへ移動
			GET_FADE.SetEnableFade(true);
		}

		m_AbleBack = true;
	}


	if (m_NowScene == SCENE::NAME::NOW_STAGE_SELECT  && m_AbleBack == true)
	{

		if (GET_FADE.GetAbleEnd() == true)
		{
			KillScene();

			m_NowScene = SCENE::NAME::TITLE_INIT;
			m_SceneStep = SCENE::STEP::INIT;

			GET_FADE.SetLoadEnd(true);
		}
	}
	if (m_NowScene == SCENE::NAME::NOW_GAME && m_AbleBack == true)
	{
		if (GET_FADE.GetAbleEnd() == true)
		{
			KillScene();

			m_NowScene  = SCENE::NAME::STAGE_SELECT_INIT;
			m_SceneStep = SCENE::STEP::INIT;

			GET_FADE.SetLoadEnd(true);
		}
	}
}


void CSceneManager::SceneInit()
{
	if (m_NowScene == SCENE::NAME::TITLE_INIT)
	{
		CreateScene<CTitleScene>();

		//	現在タイトル
		m_NowScene = SCENE::NAME::NOW_TITLE;
		SOUNDPLAYER.PlayTheSound(BGM::NAME::TITLE_AND_SELECT, true);
	}
	else if (m_NowScene == SCENE::NAME::STAGE_SELECT_INIT)
	{
		CreateScene<CStageSelectScene>();

		//	現在ステージセレクト
		SOUNDPLAYER.PlayTheSound(BGM::NAME::TITLE_AND_SELECT, true);
		SOUNDPLAYER.PlayTheSound(BGM::NAME::GAME, false);
		m_NowScene = SCENE::NAME::NOW_STAGE_SELECT;
	}
	else if (m_NowScene == SCENE::NAME::GAME_INIT)
	{
		CreateScene<CGameScene>();

		//	現在ゲーム
		m_NowScene = SCENE::NAME::NOW_GAME;
		SOUNDPLAYER.PlayTheSound(BGM::NAME::TITLE_AND_SELECT, false);
		SOUNDPLAYER.PlayTheSound(BGM::NAME::GAME, true);
	}
}


void CSceneManager::SceneUpdate()
{

	//	一度だけ通す
	static bool Init = false;

	if (m_NowScene == SCENE::NOW_TITLE)
	{

		if (GET_FADE.GetAbleEnd() == true && m_AbleBack == false)
		{
			m_NowScene = SCENE::TITLE_RELEASE;
			m_SceneStep = SCENE::STEP::RELEASE;
		}

	}
	else if (m_NowScene == SCENE::NOW_STAGE_SELECT)
	{
		if (GET_FADE.GetStep() == FADE::STEP::END)
		{
			m_AbleBack = false;
		}

		//	ライトの方向を設定
		CVector3 LightDir = CVector3(0.5f, -0.3f, 1.0f);

		//	正規化する
		LightDir.Normalize();


		GET_DIRECT_HELPER.DefaultLight(LightDir);

		GET_DIRECT_HELPER.AlphaBlendEnable(TRUE);

		GET_DIRECT_HELPER.Blend_Alpha();

		if (GET_FADE.GetAbleEnd() == true && m_AbleBack == false)
		{
			m_NowScene	= SCENE::NAME::STAGE_SELECT_RELEASE;
			m_SceneStep = SCENE::STEP::RELEASE;
			Init		= false;
		}
	}
	else if (m_NowScene == SCENE::NOW_GAME)
	{

		for (auto &This : GET_WORLD.GetGameWorld()->GetObjectManager()->GetObjectContainer())
		{
			bool Player = (This->GetId() == OBJECT_LIST::ID::PLAYER) ? true : false;

			if (Player == true
				&&
				This->GetGoalCheck() == true)
			{
				if (Init == false)
				{
					Init = true;
					GET_FADE.SetEnableFade(Init);
				}

				GET_FADE.SetLoadEnd(true);

				if (GET_FADE.GetStep() == FADE::STEP::fOUT)
				{
					m_NowScene = SCENE::NAME::GAME_RELEASE;

					m_SceneStep = SCENE::STEP::RELEASE;

					m_AbleBack = false;
				}
			}
		}
	}
}


void CSceneManager::SceneRelease()
{
	//	現在のシーンを終了
	KillScene();

	if (m_NowScene == SCENE::TITLE_RELEASE)
	{
		m_NowScene = SCENE::STAGE_SELECT_INIT;


		//GET_XFILE_LOAD_MANAGER.AllClear();
		//GET_TEXTURE_LOAD_MANAGER.AllClear();
	}
	else if (m_NowScene == SCENE::STAGE_SELECT_RELEASE)
	{
		m_NowScene = SCENE::GAME_INIT;


		//GET_XFILE_LOAD_MANAGER.AllClear();
		//GET_TEXTURE_LOAD_MANAGER.AllClear();
	}
	else if (m_NowScene == SCENE::GAME_RELEASE)
	{
		m_NowScene = SCENE::STAGE_SELECT_INIT;

		//マネージャーを最後に解放しておくようにする
		//GET_XFILE_LOAD_MANAGER.AllClear();
		//GET_TEXTURE_LOAD_MANAGER.AllClear();
		SOUNDPLAYER.PlayTheSound(BGM::NAME::GAME, false);
		SOUNDPLAYER.PlayTheSound(BGM::NAME::TITLE_AND_SELECT, true);
	}
}


void CSceneManager::KillScene()
{
	auto It = m_vspScene.begin();

	(*It) = nullptr;

	m_vspScene.erase(It);
}