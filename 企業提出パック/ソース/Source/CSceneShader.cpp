#include "Shader.h"


/*---------------------------------------------->

レンダーターゲットをセットする関数

<----------------------------------------------*/
static void SetRenderTarget(CTexture _Texture, LPDIRECT3DSURFACE9 _lpDepthStencil)
{
	LPDIRECT3DSURFACE9 NowRT;

	if (SUCCEEDED(_Texture.GetTex()->GetSurfaceLevel(0, &NowRT)))
	{
		//	現在のRTの情報をセットする
		GET_DIRECT_HELPER.GetDev()->SetRenderTarget(0, NowRT);

		//	現在のRTを開放
		NowRT->Release();

		//	Zバッファを情報をセットする
		GET_DIRECT_HELPER.GetDev()->SetDepthStencilSurface(_lpDepthStencil);
	}
}


/*------------------------------>

RTの初期化

<------------------------------*/
static void ResetRenderTarget(LPDIRECT3DSURFACE9 _lpRenderTarget, LPDIRECT3DSURFACE9 _lpDepthStencil)
{
	//	RTを取得
	GET_DIRECT_HELPER.GetDev()->SetRenderTarget(0, _lpRenderTarget);

	//	Zバッファを取得
	GET_DIRECT_HELPER.GetDev()->SetDepthStencilSurface(_lpDepthStencil);
}


CSceneShader::CSceneShader()
{
	//	Heightマップ初期化
	for (auto i = 0; i < 2; i++)
	{
		m_HeightTex[i].Release();
	}

	//	法線マップ初期化
	m_NormalTex.Release();
}


CSceneShader::~CSceneShader()
{
	//	デバイスの中にある必要の無いRTを開放させておく
	OnLostDevice();

	//	開放
	Release();
}


void CSceneShader::Init()
{
	//	エラーチェック用の変数
	LPD3DXBUFFER pErr = nullptr;
	HRESULT		 hr;

	//	ファイルをロードする
	hr = D3DXCreateEffectFromFile(
		GET_DIRECT_HELPER.GetDev(),
		"Shader/SceneShader.fx",
		NULL,
		NULL,
		0,
		NULL,
		&m_Effect,
		&pErr);

	//	ファイルがきちんと入っているかどうかをチェック
	if (hr != D3D_OK)
	{
		//	無かったらreturnさせる
		MessageBox(NULL, (LPCSTR)pErr->GetBufferPointer(), "シェーダコンパイルエラー\nファイルが見つかりません", MB_OK);
		return;
	}


	//	デバイスリセット
	OnResetDevice();

}


void CSceneShader::Draw(shared_ptr<CTexture> _spTexture, int& _Pass, CMatrix& _mMatrix)
{

	//--------------
	//	波を描画する
	//--------------

	DefaultSetState();


	//	ライトの情報ゲット
	D3DLIGHT9 Light = GET_DIRECT_HELPER.GetLight(1);

	CVector3 LightDir = Light.Direction;

	SetLightDir(&LightDir);


	//	テクニックの選択
	m_Effect->SetTechnique("Wave");

	m_Effect->SetVector("WaveDiffuse", &D3DXVECTOR4(0.0f, 0.0f, 1.0f, 0.7f));

	m_Effect->SetVector("WaveSpecular", &D3DXVECTOR4(0.0f, 0.0f, 1.0f, 1.0f));

	m_Effect->SetFloat("WavePower", 100.0f);

	m_Effect->SetTexture("NormalTex", m_NormalTex.GetTex());

	m_Effect->SetMatrix("WView", &GET_CAMERA.GetViewMatrix());

	CMatrix mVP, mV, mP;

	mV = GET_CAMERA.GetViewMatrix();

	mP = GET_CAMERA.GetProjMatrix();

	mVP = mV * mP;

	m_Effect->SetMatrix("WVProj", &mVP);

	CVector3 mLightWVPos = GET_CAMERA.GetCameraWorldPosition();

	m_Effect->SetVector("LightWVPos", &D3DXVECTOR4(mLightWVPos, 1.0f));

	m_Effect->SetVector("LightColor", &D3DXVECTOR4(Light.Diffuse.r, Light.Diffuse.g, Light.Diffuse.b, Light.Diffuse.a));//D3DXVECTOR4(0.0f, 0.0f, 1.0f, 1.0f));

	//	テクニック開始
	m_Effect->Begin(0, 0);

	//	パスを設定
	m_Effect->BeginPass(0);




	//	テクセルサイズ
	D3DXVECTOR2 TexelSize;

	const float TEX_SIZE = 1.0f;

	TexelSize.x = TEX_SIZE / _spTexture->GetInfo()->Width;
	TexelSize.y = TEX_SIZE / _spTexture->GetInfo()->Height;

	m_Effect->SetValue("TexelSize", &TexelSize, 8);

	m_Effect->SetTexture("InputTex", _spTexture->GetTex());


	//------------------
	//	テクスチャの描画
	//------------------

	GET_DIRECT_HELPER.BeginSprite();

	GET_DIRECT_HELPER.DrawSprite(_spTexture.get(), D3DCOLOR_ARGB(255, 255, 255, 255), &_mMatrix);

	GET_DIRECT_HELPER.EndSprite();


	//	パス終了
	m_Effect->EndPass();

	//	テクニック終了
	m_Effect->End();
}


void CSceneShader::Release()
{
	SAFE_RELEASE(m_Effect);
}


void CSceneShader::OnLostDevice()
{
	for (auto i = 0; i < 2; i++)
	{
		m_HeightTex[i].Release();
	}

	m_NormalTex.Release();
}


void CSceneShader::OnResetDevice()
{
	//----------------------------------------------
	//	ハイトマップを作る前にデバイスをリセットする
	//----------------------------------------------

	//	デバイスをリセット
	m_Effect->OnResetDevice();

	GET_DIRECT_HELPER.GetDev()->GetRenderTargetData(0, GET_DIRECT_HELPER.m_OrgRenderTarget);
	GET_DIRECT_HELPER.GetDev()->GetDepthStencilSurface(&GET_DIRECT_HELPER.m_OrgDepthStencil);


	//--------------------------
	//	�@ハイトマップを作成する
	//--------------------------
	for (auto i = 0; i < 2; i++)
	{
		//	テクスチャーの情報が入っていうるかどうかを調べる
		if (!m_HeightTex[i].GetTex())
		{
			const auto TEXTURE_SIZE = 256;

			//	ハイトマップを生成する
			m_HeightTex[i].CreateTexture(
				TEXTURE_SIZE,
				TEXTURE_SIZE,
				1,
				D3DUSAGE_RENDERTARGET,
				D3DFMT_G16R16F,
				D3DPOOL_DEFAULT
				);
		}
	}


	//--------------------
	//	�A深度マップを生成
	//--------------------

	//	法線マップの情報が入っているかどうかを調べる
	if (!m_NormalTex.GetTex())
	{
		const	auto TEXTURE_SIZE = 256;

		//	法線マップを生成する
		m_NormalTex.CreateTexture(
			TEXTURE_SIZE,
			TEXTURE_SIZE,
			1,
			D3DUSAGE_RENDERTARGET,
			D3DFMT_G16R16F,
			D3DPOOL_DEFAULT
			);
	}


	//--------------------------------------
	//	�B高度マップと法線マップをクリアする
	//--------------------------------------


	//--------------------
	//	RTにマップをセット

	//	ハイトマップ
	for (auto i = 0; i < 2; i++)
	{
		//	テクスチャ情報が格納されていた場合
		if (m_HeightTex[i].GetTex())
		{
			//	RTを切り替える
			SetRenderTarget(m_HeightTex[i], NULL);

			//	デバイスをクリア
			GET_DIRECT_HELPER.Clear(0, NULL, D3DCLEAR_TARGET, 0, 1, 0);
		}
	}

	//	法線マップ
	if (m_NormalTex.GetTex())
	{
		SetRenderTarget(m_NormalTex, NULL);
		GET_DIRECT_HELPER.Clear(0, NULL, D3DCLEAR_TARGET, 0, 1, 0);
	}

	//	ここまで
	//--------------------


	//	RTをリセット
	GET_DIRECT_HELPER.ResetDepthStencil();
	GET_DIRECT_HELPER.ResetRenderTarget();
}


void CSceneShader::UpdateMap()
{
	//	テクスチャーサイズ
	const auto TEXTURE_SIZE = 256;

	//	タイマー加算
	m_Time++;

	//----------------
	//	波の動きを行う
	//----------------

	m_TextureTime = m_Time;


	//----------------
	//	新しい波の生成
	//----------------

	m_Effect->SetVector(
		"AddWavePos",
		&D3DXVECTOR4(
		m_vCreateWavePos.x,
		m_vCreateWavePos.y,
		0.0f,
		0.0f
		));


	//	0.05fがきれいに見える
	m_Effect->SetFloat("AddWaveRad", static_cast<float>(Rand.Real1()) * 0.01f);

	//	-0.5f
	m_Effect->SetFloat("AddWaveVelocity", static_cast<float>(Rand.Real1()) - 0.5f);



	//------------------
	//	高度マップの更新
	//------------------

	//	RTに高度マップをセット(前の高度マップをはる
	SetRenderTarget(m_HeightTex[m_TextureTime % 2], NULL);

	//	セットした高度マップの配列と逆側を見る(新しい高度マップをはる
	m_Effect->SetTexture("HeightTex", m_HeightTex[1 - m_TextureTime % 2].GetTex());

	//	テクニック選択
	m_Effect->SetTechnique("Height");

	//	波の高さ	0.08fがいいかんじかも？
	m_Effect->SetFloat("Spring", 0.08f);

	//	テクニック開始
	m_Effect->Begin(0, 0);

	//	パス設定
	m_Effect->BeginPass(0);

	//	ポリゴン描画
	GET_DIRECT_HELPER.DrawQuad(
		0,
		0,
		(float)TEXTURE_SIZE,
		(float)TEXTURE_SIZE
		);

	//	パス終了
	m_Effect->EndPass();

	//	テクニック終了
	m_Effect->End();


	//------------------
	//	法線マップの更新
	//------------------

	//	RTに法線マップを貼り付ける
	SetRenderTarget(m_NormalTex, NULL);

	m_Effect->SetTexture("HeightTex", m_HeightTex[m_TextureTime % 2].GetTex());

	//	テクニック選択
	m_Effect->SetTechnique("Normal");

	//	テクニック開始
	m_Effect->Begin(0, 0);

	//	パス設定
	m_Effect->BeginPass(0);

	//	ポリゴン描画
	GET_DIRECT_HELPER.DrawQuad(
		0,
		0,
		(float)TEXTURE_SIZE,
		(float)TEXTURE_SIZE
		);

	//	パス終了
	m_Effect->EndPass();

	//	テクニック終了
	m_Effect->End();

	GET_DIRECT_HELPER.ResetDepthStencil();
	GET_DIRECT_HELPER.ResetRenderTarget();
}