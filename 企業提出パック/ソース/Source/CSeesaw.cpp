#include "Game.h"


CSeesaw::CSeesaw()
{

}


CSeesaw::~CSeesaw()
{
}


void CSeesaw::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	シーソの部分
	m_Mesh = GET_XFILE_LOAD_MANAGER.Load("Data/XFile/Object/Seesaw.x", _lpD3DDevice);


	//----------------------
	//	立方体剛体を登録する
	//----------------------

	m_spRigidBox = make_shared<CBulletObj_Box>();

	CRigidSupervise::CreateRigidBox(m_spRigidBox, m_Mesh, m_vPos, m_Id, m_Mesh->GetBounding()->GetBoundingBoxMax(), 0.0f);

	m_spRigidBox->SetUserPointer(this);

	m_spRigidBox->SetMass(0.0f);

	m_Mesh->GetMeshState()->m_EnableInk = true;
}


void CSeesaw::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{

	//	重さを初期化しておく
	m_ObjectWeight = 0.0f;


	//----------------------
	//	球判定を行って範囲内にオブジェクトがあるかをチェック
	//	範囲内にオブジェクトがあった場合、自身の座標　- 対象の座標
	//	差分のうちx座標が+か-かを判定

	for (auto &This : CRigidSupervise::GetRigidList())
	{

		//	対象オブジェクトのポインターを一時的に使用
		CObjectBase* ThisObj = (CObjectBase *)This.lock()->GetUserPointer();

		if (ThisObj)
		{

			//------------------------------------------
			//	対象までの距離をとってきて球判定を行う
			//	範囲内なら距離を使ってそのまま処理を行う
			//	範囲外なら次へ
			//------------------------------------------

			CVector3 vLen = m_vPos - ThisObj->GetPosition();

			float Len = D3DXVec3Length(&vLen);

			//	球範囲内かどうかを返させる
			bool Check = (Len < m_Mesh->GetBounding()->GetBoundingSphereRadius()) ? true : false;


			//------------------------------------------------------
			//	範囲内にオブジェクトがあり
			//	かつ
			//	登録されているタグが指定タグと一致した場合処理を実行
			//------------------------------------------------------
			if (Check == true
				&&
				ThisObj->GetUpdateTag() == "NowSeesaw")
			{
				//	シーソーの上にのっているオブジェクトの重さを取る
				float ThisWeight = ThisObj->GetObjectWeight();

				//	m_ObjectWeightは一番大きい物がのっているという判定に使う
				bool EnableSlope = (ThisWeight > m_ObjectWeight) ? true : false;//m_ObjectWeight = ThisWeight : m_ObjectWeight = m_ObjectWeight;

				//	更新していいので更新
				if (EnableSlope == true)
				{
					//	重さを更新
					m_ObjectWeight = ThisWeight;

					//	加速量として距離を保存
					m_vVec.x = Len * (ThisObj->GetObjectWeight() + 0.2f) * 0.1f;

					//	傾く角度をここで決める
					m_SlopeDegree = Len * 6.0f;

					//	現在オブジャクトがどっちにあるかを返す
					m_Direction = (vLen.z < 0.0f) ? RIGHT: LEFT;
				}
			}
			else
			{
				//m_Direction = DIRECTION::CENTER;
			}


			if (ThisObj->GetUpdateTag() == "NowSeesaw")
			{
				ThisObj->SetUpdateTag("");
			}
		}
		else
		{
			//m_vVec.x = 0.0f;
		}

	}


	//	シーソーの傾きを更新
	UpdateSeesaw(m_Direction);


	//------------
	//	行列を更新
	//------------

	m_mWorld.CreateRotateX(m_vAng.x);

	m_mWorld.RotateY(m_vAng.y);

	m_mWorld.Move(&m_vPos);

	m_spRigidBox->SetMatrix(m_mWorld);

	m_vPos = m_mWorld.GetPos();

}


void CSeesaw::Release()
{

}


void CSeesaw::UpdateSeesaw(const int _Direction)
{

	//--------------------------------------------
	//	オブジェクトのある場所によって傾きを替える
	//--------------------------------------------

	if (_Direction == DIRECTION::CENTER)
	{
		if (m_vAng.x < 0.0f)
		{
			m_vAng.x += 0.05f;
		}
		else if (m_vAng.x > 0.0f)
		{
			m_vAng.x -= 0.05f;
		}
	}
	else if (_Direction == DIRECTION::RIGHT)
	{
		m_vAng.x += m_vVec.x;

		if (m_vAng.x >= m_SlopeDegree)
		{
			m_vAng.x = m_SlopeDegree;
		}
	}
	else if (_Direction == DIRECTION::LEFT)
	{
		m_vAng.x -= m_vVec.x;

		if (m_vAng.x <= -(m_SlopeDegree))
		{
			m_vAng.x = -(m_SlopeDegree);
		}
	}

}