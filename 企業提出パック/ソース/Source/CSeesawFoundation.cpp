#include "CSeesawFoundation.h"


CSeesawFoundation::CSeesawFoundation()
{

}


CSeesawFoundation::~CSeesawFoundation()
{

}


void CSeesawFoundation::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	m_Mesh = GET_XFILE_LOAD_MANAGER.Load("Data/XFile/Object/SeesawFoundation.x", _lpD3DDevice);

	m_Mesh->GetMeshState()->m_EnableInk = true;
}


void CSeesawFoundation::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//----------------
	//	行列を更新する
	//----------------

	m_mWorld.CreateRotateX(m_vAng.x);

	m_mWorld.RotateY(m_vAng.y);

	m_mWorld.Move(&m_vPos);

	m_vPos = m_mWorld.GetPos();
}


void CSeesawFoundation::Release()
{

}

