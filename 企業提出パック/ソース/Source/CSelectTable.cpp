#include "Scene.h"
#include "CLoadXFileManager.h"
#include "CSelectTable.h"


CSelectTable::CSelectTable()
	:
	m_pMesh(nullptr)
{
	//	メッシュのポインタを確保
	m_pMesh = MyNew CMeshObject();
}


CSelectTable ::~CSelectTable()
{
	SAFE_DELETE(m_pMesh);
}


void CSelectTable::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	メッシュを読み込む
	m_pMesh->LoadMesh("Data/XFile/Object/SelectMap.x", _lpD3DDevice);
}

void CSelectTable::Update()
{
	//	行列を更新する
	m_mWorld.CreateMove(&m_vPos);
}

void CSelectTable::Draw(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	ライティング開始
	_lpD3DDevice->SetRenderState(D3DRS_LIGHTING, TRUE);


	_lpD3DDevice->SetTransform(D3DTS_WORLD, &m_mWorld);

	//	メッシュの描画
	m_pMesh->DrawMesh();

	//	ライティング終了
	_lpD3DDevice->SetRenderState(D3DRS_LIGHTING, FALSE);
}

//	開放
void CSelectTable::Release()
{

}