#include "CShaderBase.h"


CShaderBase::CShaderBase()
	:
	m_Effect(nullptr)	//	エフェクトのポインタを初期化
{
	//	Texを初期化
	m_TexDot.Release();	
}


CShaderBase::~CShaderBase()
{
}


void CShaderBase::DefaultSetState(
	CVector3	_CamPos,
	CMatrix		_mProj,
	CMatrix		_mView	
	)
{
	//	登録しているライトから方向をもらう
	CVector3 LightDir = GET_DIRECT_HELPER.GetLight(0).Direction;
	
	//	シェーダー側にライトの方向をセットさせておく
	SetLightDir(&LightDir);

	//	シェーダ側に射影行列をセット
	SetConstTransformProj(&_mProj);

	//	シェーダ側にビュー行列をセット
	SetConstTransformView(&_mView);

	//	シェーダ側にカメラのポジションをセット
	SetCamPos(&_CamPos);

	//	念のためクリップを0にしておく
	
	const int CLIPP_OFF = 0;

	SetClip(CLIPP_OFF);
}


void CShaderBase::Release()
{
	//	エフェクトを開放
	SafeRelease(m_Effect);

	//	Texを開放
	m_TexDot.Release();
}
