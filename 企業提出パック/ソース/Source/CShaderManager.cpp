#include "Shader.h"
#include "Texture.h"
#include "Font.h"
#include "Game.h"


//-------------
//	namespace略
//-------------

using namespace DRAW_SHADER;


CShaderManager::CShaderManager()
:
m_spShadowMapShader(nullptr)
,
m_spMyShader(nullptr)
,
m_spBokashiShader(nullptr)
,
m_spWaterReflection(nullptr)
,
m_spWaterShader(nullptr)
,
m_spBack(nullptr)
,
m_spSceneShader(nullptr)
{
	//	横幅初期化
	m_CreateShadowWidthRange = 0.0f;

	//	縦幅初期化
	m_CreateShadowHeightRange = 0.0f;
}


CShaderManager::~CShaderManager()
{
	Release();
}


void CShaderManager::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//----------------------------------------
	//	各シェーダーポインターをインスタンス化
	//----------------------------------------
	
	//	シャドウマップを初期化
	m_spShadowMapShader	= CreateShaderObject<CShadowMapShader>();

	//	ぼかしを初期化
	m_spBokashiShader	= CreateShaderObject<CBokashiShader>();

	//	マイシェーダーを初期化
	m_spMyShader		= CreateShaderObject<CMyShader>();

	//	水面シェーダーを初期化
	m_spWaterShader		= CreateShaderObject<CWaterShader>();

	//	シーンシェーダ
	m_spSceneShader		 = CreateShaderObject<CSceneShader>();


	//------------------------
	//	RT類のポインタを初期化
	//------------------------

	//	ミラーのテクスチャーをインスタンス化
	m_spWaterReflection = make_shared<CTexture>();

	//	ミラーのテクスチャーを作る
	m_spWaterReflection->CreateRenderTarget(APP.WINDOW_WIDTH_SIZE, APP.WINDOW_HEIGHT_SIZE, D3DFMT_A8R8G8B8);

	//	背景のテクスチャーをインスタンス化
	m_spBack			= make_shared<CTexture>();

	//	背景のテクスチャーを作る
	m_spBack->CreateRenderTarget(APP.WINDOW_WIDTH_SIZE, APP.WINDOW_HEIGHT_SIZE, D3DFMT_A8R8G8B8);
}


void CShaderManager::Release()
{

	//----------
	//	シェーダ
	//----------

	m_spMyShader		= nullptr;
	m_spWaterShader		= nullptr;
	m_spBokashiShader	= nullptr;
	m_spShadowMapShader	= nullptr;
	m_spSceneShader		= nullptr;


	//------
	//	RT類
	//------

	m_spWaterReflection = nullptr;
	m_spBack			= nullptr;
}


void CShaderManager::DrawShaderBegin()
{
	//------------------------------------------------------
	//	各シェーダーに必ず必要なステータスをセットさせておく
	//------------------------------------------------------

	m_spShadowMapShader->DefaultSetState();

	m_spMyShader->DefaultSetState();

	m_spBokashiShader->DefaultSetState();

	m_spWaterShader->DefaultSetState();

	m_spSceneShader->DefaultSetState();


	//-------------------------------------------------------------------
	//	深度バッファを作成 (第二引数はライトの方向とかぶらないようにする)
	//-------------------------------------------------------------------

	CreateShadowMap(
		CVector3(0.0f, 0.0f, 0.0f),
		CVector3(0, 1, 0)
		);


	//----------------------------
	//	ブルームの設定をセットする
	//----------------------------

	SetBloom();


	//----------
	//	波を作る
	//----------

	m_spWaterShader->UpdateMap();
}


void CShaderManager::DrawShaderBegin(CVector3 _vPos)
{
	//------------------------------------------------------
	//	各シェーダーに必ず必要なステータスをセットさせておく
	//------------------------------------------------------

	m_spShadowMapShader->DefaultSetState();

	m_spMyShader->DefaultSetState();

	m_spBokashiShader->DefaultSetState();

	m_spWaterShader->DefaultSetState();

	m_spSceneShader->DefaultSetState();


	//-------------------------------------------------------------------
	//	深度バッファを作成 (第二引数はライトの方向とかぶらないようにする)
	//-------------------------------------------------------------------

	CreateShadowMap(_vPos, CVector3(0, 1, 0));


	//----------------------------
	//	ブルームの設定をセットする
	//----------------------------
	
	SetBloom();


	//----------
	//	波を作る
	//----------
	
	m_spWaterShader->UpdateMap();


	//------------------------------------
	//	ミラーで使用したRT情報を消しておく
	//------------------------------------
	GET_DIRECT_HELPER.Clear(true, true, false, D3DCOLOR_ARGB(255, 0, 0, 0));

}


void CShaderManager::DrawBloomShaderEnd()
{
	//--------------------
	//	ブルーム描画を行う
	//--------------------
	DrawBloom();

	//--------------------
	//	デバッグ表示をする
	//--------------------
	ShaderDebugSprite();

	//----------------------------------------------------
	//	ブルームが合成されたレンダーターゲットをセットする
	//----------------------------------------------------
	GET_DIRECT_HELPER.GetDev()->SetRenderTarget(1, NULL);
}


//------------------------------------------------------>
//	シャドウマップとメッシュの描画を行う
//
//	描画手順は三つ
//	
//	�@	影の情報を作るためにモデルまでの距離を測るための
//		シャドウマップテクスチャを作成する(深度マップ)
//		型はR32F型で生成 (2D空間描画)
//
//	�A	�@の手順で作成した影の情報をシェーダ側に渡す
//
//	�B	全キャラクターを描画する (3D空間描画)
//
//	手順�@と�Aは次の関数でする
//	手順�Bはオブジェクト自身が行う
//
//	描画を二度行うので少々処理が重くなる可能性がある。
//
//
//	ライトの座標(震度マップを作る)はプレイヤーに依存させてもいいかも
//
//<------------------------------------------------------
void CShaderManager::CreateShadowMap(CVector3 _vLightPos, CVector3 _CameraHead)
{
	//------------------------------------------------
	//	手順�@を行う前に、ライトのポジションを確保する
	//------------------------------------------------

	//	ライトのポジションを決める
	CVector3	vLightPos = CVector3(_vLightPos.x, 2.0f, _vLightPos.z);

	//	大体の距離を決めておく
	const auto	LIGHT_DIRECTION = 30.0f;

	//	ライトの座標をセットする
	vLightPos -= (CVector3 &)GET_DIRECT_HELPER.GetLight(0).Direction * LIGHT_DIRECTION;


	//---------------------------
	//	手順�@ 深度マップ描画開始
	//---------------------------

	//	描画開始
	m_spShadowMapShader->Begin(&vLightPos, _CameraHead, m_CreateShadowWidthRange, m_CreateShadowHeightRange);


	//--------------------------------------
	//	ゲームワールドの深度マップを作成する
	//--------------------------------------

	//	ゲームワールドのポインターが入っているかどうかを確認
	if (m_spGameWorld.lock() != nullptr)
	{
		//	シャドウマップを作る範囲の値を決める
		m_CreateShadowWidthRange  = 30.0f;
		m_CreateShadowHeightRange = 30.0f;
		

		//	先頭IDを確保してくる
		auto i = GET_WORLD.GetGameWorld()->GetBegin();//m_pGameWorld.lock()->GetObjectManager()->GetItObject(OBJECT_LIST::ID::PLAYER)->GetFirstId();

		//	後尾IDを確保しておく
		auto LastId = m_spGameWorld.lock()->GetEnd();


		//------------------------------
		//	プレイヤーの深度マップを作る
		//------------------------------

		//	プレイヤーのワールド行列をシャドウマップシェーダー側にセット
		m_spShadowMapShader->SetTransformWorld(&m_spGameWorld.lock()->GetObjectManager()->GetItObject(OBJECT_LIST::ID::PLAYER)->GetWorld());

		//	深度マップに描画する
		m_spShadowMapShader->DrawSkinMesh(m_spGameWorld.lock()->GetObjectManager()->GetItObject(OBJECT_LIST::ID::PLAYER)->GetMeshObject());

		//	プレイヤー以外の深度マップを作る
		while (i < LastId)
		{
			//	当たり判定用の壁の影を出さないのでそれ以外をまわす
			int Check;

			//	IDを記憶させる
			Check = m_spGameWorld.lock()->GetObjectManager()->GetItObject(i)->GetItemId();

			//	当たり判定用意外が入っていれば
			if (Check != OBJECT_LIST::ID::STAGE_1_COLLISION
				&&
				!GET_FLAG_CALC.CheckFlag(m_spGameWorld.lock()->GetObjectManager()->GetItObject(i)->GetItemState(), STOCK::STATE::DO_STOCK)
				//)
				&&
				GET_HELPER.ViewFrustum(i, &GET_CAMERA.GetVF()) == true)
			{
				//	ワールド行列をシャドウマップシェーダー側にセット
				m_spShadowMapShader->SetTransformWorld(&m_spGameWorld.lock()->GetObjectManager()->GetItObject(i)->GetWorld());

				//	深度マップに描画する
				m_spShadowMapShader->DrawMesh(m_spGameWorld.lock()->GetObjectManager()->GetItObject(i)->GetMeshObject(), 0);
			}

			//	次へ
			i++;
		}

	}


	//----------------------------------------
	//	ステージセレクトの深度マップを作成する
	//----------------------------------------

	//	ステージセレクトのポインターが入っているかを確認
	if (m_spSelectWorld != nullptr)
	{
		m_CreateShadowWidthRange  = 10.0f;
		m_CreateShadowHeightRange = 10.0f;

		//	シャドウマップ側にワールド行列を送る
		m_spShadowMapShader->SetTransformWorld(&m_spSelectWorld->GetSelectManager()->GetItSelect(0)->GetWorld());

		//	深度マップに描画する
		m_spShadowMapShader->DrawMesh(m_spSelectWorld->GetSelectManager()->GetItSelect(0)->GetMesh(), 0);
	}

	//	深度マップ描画終了
	m_spShadowMapShader->End();


	//------------------------------
	//	手順�Aを行う
	//------------------------------

	m_spMyShader->SetShadowMapDate(
		&m_spShadowMapShader->GetMatrix(),	//	ライト用の行列を返す(ビュー行列 * 射影行列)
		&m_spShadowMapShader->GetTexture()	//	深度マップを返す
		);

}


void CShaderManager::SetBloom()
{
	//--------------------------------------------------
	//	ブルームを行うまえにRT0とRT1に情報を受け渡しする
	//--------------------------------------------------

	//	レンダーターゲットを記憶させるために変数を作る
	LPDIRECT3DSURFACE9 NowRT;

	//	レンダーターゲットを記憶させておく
	GET_DIRECT_HELPER.GetDev()->GetRenderTarget(USE_RT::COLOR, &NowRT);

	//	ブルームにレンダーターゲットをセット
	m_spMyShader->GetBloom().SetRenderTarget(USE_RT::BLOOM);

	//	レンダーターゲット[0](バックバッファ)はNULLにしないので記憶させておく
	GET_DIRECT_HELPER.Clear(true, false, false, D3DCOLOR_ARGB(255, 0, 0, 0), 1, 0);

	//	記憶しておいたRT0をセット
	GET_DIRECT_HELPER.GetDev()->SetRenderTarget(USE_RT::COLOR, NowRT);

	//	レンダーターゲットをセットしたので記憶させておいた分は開放させておく
	NowRT->Release();

	//	RT1をブルームにセットする
	m_spMyShader->GetBloom().SetRenderTarget(USE_RT::BLOOM);
}


void CShaderManager::DrawBloom()
{
	//--------------------------------------------------------------
	//	RT0(通常描画) と　RT1(自己発効のみを抽出) の描画を合成させる
	//--------------------------------------------------------------

	//	ぼかしシェーダーでRT同士のデータを受け渡しさせる
	m_spBokashiShader->GenerateBokasi(&m_spMyShader->GetBloom());

	//	加算合成を行う
	GET_DIRECT_HELPER.Blend_Add();

	//	ぼかしシェーダーでブルームを描画させる
	m_spBokashiShader->Draw2D(&m_spBokashiShader->GetTexBokashi(1), 0, 0, APP.m_Width, APP.m_Height);

	//	以降加算合成をしないので通常の状態に戻させる
	GET_DIRECT_HELPER.Blend_Alpha();
}


void CShaderManager::ShaderDebugSprite()
{
	//-------------
	// デバッグ表示
	//-------------

	//	表示開始
	GET_DIRECT_HELPER.BeginSprite();

	//	描画位置
	CMatrix m;

	//	画像サイズ制御
	const auto SIZE = 150.0f;

	//	描画位置初期化
	m.CreateMove(0, 0, 0);


	if (GetAsyncKeyState('1'))
	{
		//----------------
		//	深度マップ描画
		//----------------

		//	サイズを指定
		m.Scale_Local(
			SIZE / m_spShadowMapShader->GetTexture().GetInfo()->Width,
			SIZE / m_spShadowMapShader->GetTexture().GetInfo()->Height,
			1);

		//	深度マップを描画
		GET_DIRECT_HELPER.DrawSprite(
			&m_spShadowMapShader->GetTexture(), 
			D3DCOLOR_ARGB(255, 255, 0, 0), 
			&m);

	}
	else if (GetAsyncKeyState('2'))
	{
		//-----------------------------------------------
		//	ライトブルーム描画(Emmisivが出ている部分のみ)
		//-----------------------------------------------

		//	サイズを指定
		m.Scale_Local(
			SIZE / m_spShadowMapShader->GetTexture().GetInfo()->Width,
			SIZE / m_spShadowMapShader->GetTexture().GetInfo()->Height,
			1);

		//	深度マップを描画
		GET_DIRECT_HELPER.DrawSprite(
			&m_spMyShader->GetBloom(),
			D3DCOLOR_ARGB(255, 255, 255, 255),
			&m);

	}
	else if (GetAsyncKeyState('6'))
	{
		//----------------
		//	ミラー描画
		//----------------

		//	サイズを指定
		m.Scale_Local(
			SIZE / m_spWaterReflection->GetInfo()->Width,
			SIZE / m_spWaterReflection->GetInfo()->Height,
			1);

		//	深度マップを描画
		GET_DIRECT_HELPER.DrawSprite(
			m_spWaterReflection.get(),
			D3DCOLOR_ARGB(255, 255, 255, 255),
			&m);
	}
	else if (GetAsyncKeyState('7'))
	{
		//----------------
		//	ミラー描画
		//----------------

		//	サイズを指定
		m.Scale_Local(
			SIZE / m_spBack->GetInfo()->Width,
			SIZE / m_spBack->GetInfo()->Height,
			1);

		//	深度マップを描画
		GET_DIRECT_HELPER.DrawSprite(
			m_spBack.get(),
			D3DCOLOR_ARGB(255, 255, 255, 255),
			&m);
	}


	//	水面シェーダーのデバッグ表示
	m_spWaterShader->TexDraw();

	//	表示終了
	GET_DIRECT_HELPER.EndSprite();
}


void CShaderManager::DrawMirrorBegin()
{
	const int COLOR_MAX = 255;
	const int COLOR_MIN = 0;

	//--------------
	//	RTを保存する
	//--------------

	GET_DIRECT_HELPER.GetDev()->GetRenderTarget(USE_RT::COLOR, &m_MirrorRT);
	m_spWaterReflection->SetRenderTarget(USE_RT::COLOR);
	GET_DIRECT_HELPER.Clear(true, true, false, D3DCOLOR_ARGB(COLOR_MAX, COLOR_MIN, COLOR_MIN, COLOR_MIN));
}


void CShaderManager::DrawMirrorEnd()
{
	const int CLIP_OFF = 0;


	//------------------------------------
	//	RTを開放
	//	クリップ処理を終了
	//	テクスチャをシェーダーに送る
	//------------------------------------

	//	RT開放
	GET_DIRECT_HELPER.GetDev()->SetRenderTarget(USE_RT::COLOR, m_MirrorRT);
	m_MirrorRT->Release();

	//	クリップオフ
	m_spMyShader->SetClip(CLIP_OFF);
	m_spWaterShader->SetClip(CLIP_OFF);

	//	ミラー用のテクスチャをシェーダに送る
	m_spMyShader->SetMirrorTex(m_spWaterReflection);
	m_spWaterShader->SetMirrorTex(m_spWaterReflection);
}


void CShaderManager::DrawBackBegin()
{
	const int COLOR_MAX = 255;
	const int COLOR_MIN = 0;

	//----------
	//	RTを保存
	//----------

	GET_DIRECT_HELPER.GetDev()->GetRenderTarget(USE_RT::COLOR, &m_BackRT);
	m_spBack->SetRenderTarget(USE_RT::COLOR);
	GET_DIRECT_HELPER.Clear(true, true, false, D3DCOLOR_ARGB(COLOR_MAX, COLOR_MIN, COLOR_MIN, COLOR_MIN));
}


void CShaderManager::DrawBackEnd()
{
	//------------------------------
	//	RTを開放
	//	テクスチャをシェーダ側に送る
	//------------------------------

	//	RT開放
	GET_DIRECT_HELPER.GetDev()->SetRenderTarget(USE_RT::COLOR, m_BackRT);
	m_BackRT->Release();

	//	背景用のテクスチャを送る
	m_spMyShader->SetBackTex(m_spBack);
	m_spWaterShader->SetBackTex(m_spBack);
}