#include "Shader.h"


CShadowMapShader::CShadowMapShader()
{

}


CShadowMapShader::~CShadowMapShader()
{
	Release();
}


void CShadowMapShader::Init()
{
	//	念のためにはじめに中身を掃除しておく
	Release();


	//	エフェクトファイルのロード
	LPD3DXBUFFER pErr = nullptr;
	HRESULT		 hr;
	
	hr = D3DXCreateEffectFromFile(
		GET_DIRECT_HELPER.GetDev(),
		"Shader/ShadowMap.fx",
		NULL,
		NULL,
		0,
		NULL,
		&m_Effect,
		&pErr
		);

	//	読み込めていなければ
	if (hr != D3D_OK)
	{
		//	メッセージを出して
		MessageBox(NULL, (LPCSTR)pErr->GetBufferPointer(), "シャドウマップのエフェクトファイルが読み込めていません", MB_OK);
	
		//	ウィンドウ終了フラグを立てて
		APP.m_bEndFlag = true;

		//	返す
		return;
	}


	//	生成するテクスチャのサイズを決めておく
	const auto CREATE_SIZE = 1024;

	//	シャドウマップ用のテクスチャを作成する
	m_TexShadow.CreateRenderTarget(CREATE_SIZE, CREATE_SIZE, D3DFMT_R32F);

	//	深度テクスチャを作成
	m_ZBuffer.CreateDepthStencil(CREATE_SIZE, CREATE_SIZE, D3DFMT_D24S8, GET_DIRECT_HELPER.GetDev());
}


void CShadowMapShader::Begin(
	CVector3*	 _vLightPos, 
	CVector3	 _vCameraHead,
	const float  _ProjWidthSize,
	const float  _ProjHeightSize
	)
{
	//------------------
	//	ビュー行列を作成
	//------------------

	//	ライトの射影行列
	CMatrix	mLightView;

	//	ライトの方向
	CVector3	LightDir	= GET_DIRECT_HELPER.GetLight(0).Direction;

	//	ターゲットのポシジョン
	CVector3	TargetPos	= *_vLightPos + LightDir;

	//	カメラをセット
	D3DXMatrixLookAtLH(&mLightView, _vLightPos, &TargetPos, &_vCameraHead);

	//	シェーダー側にLightViewをセット
	m_Effect->SetMatrix("mLV", &mLightView);


	//--------------------------------------------------------------
	//	正射影行列を作成
	//	正射影行列はプレイヤーの近くに(ついてこさせるようにする)置く
	//--------------------------------------------------------------

	//	ライトの正射影行列
	CMatrix mLightProj;

	//	正射影行列の範囲
	D3DXMatrixOrthoLH(&mLightProj, _ProjWidthSize, _ProjHeightSize, 1, 100);

	//	シェーダー側にLightPointをセットする
	m_Effect->SetMatrix("mLP", &mLightProj);


	//--------------------------------
	//	合成済みの行列を記憶させておく
	//--------------------------------
	m_LVP = mLightView * mLightProj;


	//---------------------------------
	//	行列のRTとZバッファを記憶させる
	//---------------------------------
	GET_DIRECT_HELPER.GetDev()->GetRenderTarget(0, &m_pNowTarget);
	GET_DIRECT_HELPER.GetDev()->GetDepthStencilSurface(&m_pNowZBuffer);


	//---------------------------
	//	RTとZバッファを切り替える
	//---------------------------
	m_TexShadow.SetRenderTarget(0);
	m_ZBuffer.SetDepthStencil(GET_DIRECT_HELPER.GetDev());

	//	画面をクリアする
	GET_DIRECT_HELPER.Clear(true, true, false, D3DCOLOR_ARGB(255, 255, 255, 255), 1, 0);

}


void CShadowMapShader::End()
{
	//-------------------------
	//	元のRTとZバッファに戻す
	//-------------------------
	GET_DIRECT_HELPER.GetDev()->SetRenderTarget(0, m_pNowTarget);
	GET_DIRECT_HELPER.GetDev()->SetDepthStencilSurface(m_pNowZBuffer);
	SafeRelease(m_pNowTarget);
	SafeRelease(m_pNowZBuffer);
}


//-------------------------------------------->
//	シェーダに対応させたメッシュを描画させる
//<--------------------------------------------
void CShadowMapShader::DrawMesh(shared_ptr<CMeshObject> _pMesh, int _Pass)
{

	//	テクニック選択
	m_Effect->SetTechnique("Tech");

	//	テクニック開始
	m_Effect->Begin(0, 0);

	//	パス開始
	m_Effect->BeginPass(_Pass);

	//	マテリアルの数分ループ
	for (int i = 0; i < _pMesh->GetMaterialCnt(); i++)
	{
		//	シェーダー側のテクスチャを取ってきて、α値が透明指定されているなら、α値を適用
		//CTexture* pTex = &_pMesh->GetTextures()[i];
		shared_ptr<CTexture> pTex = _pMesh->GetMaterials()[i].m_spMeshTex;

		//	αテスト
		m_Effect->SetTexture("MeshTex", pTex->GetTex());

		//	ステート変化をレンダリング前にデバイス側に伝えておく
		m_Effect->CommitChanges();

		//	メッシュを描画する
		_pMesh->GetMesh()->DrawSubset(i);
	}

	//	パスを終了する
	m_Effect->EndPass();

	//	テクニックを終了する
	m_Effect->End();
}


void CShadowMapShader::DrawSkinMesh(shared_ptr<CMeshObject> _pMesh)
{
	//	スキニングを行う
	_pMesh->ExecSoftSkinning();

	//	単位行列をセット
	CMatrix mI;
	SetTransformWorld(&mI);

	//	コンテナを返させる
	CMyAllocateHierarchy::MYD3DXMESHCONTAINER* mc = _pMesh->GetCont(0);

	//	テクニック選択
	m_Effect->SetTechnique("Tech");

	//	テクニック開始
	m_Effect->Begin(0, 0);

	//	パス開始
	m_Effect->BeginPass(0);

	//	マテリアルの数分ループ
	for (DWORD i = 0; i < mc->NumMaterials; i++)
	{

		//	シェーダー側のテクスチャを取ってきて、α値が透明指定されているなら、α値を適用
		CTexture* pTex = &mc->m_Textures[i];

		//	αテスト
		m_Effect->SetTexture("MeshTex", pTex->GetTex());

		//	ステート変化をレンダリング前にデバイス側に伝えておく
		m_Effect->CommitChanges();

		// メッシュを描画
		mc->MeshData.pMesh->DrawSubset(i);
	}

	//	パスを終了する
	m_Effect->EndPass();

	//	テクニックを終了する
	m_Effect->End();
}


void CShadowMapShader::Release()
{
	SafeRelease(m_Effect);
}