#include "CMesh.h"
#include "Shader.h"
#include "Texture.h"


CShine::CShine()
{
}

CShine::~CShine()
{
}

void CShine::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	D3DXCOLOR m_Color = D3DXCOLOR(255, 255, 255, 255);

	m_pTexture = GET_TEXTURE_LOAD_MANAGER.LoadEx(
		"Data/Graphics/Effect/Shine.png",
		static_cast<int>(m_vTextureSize.x),
		static_cast<int>(m_vTextureSize.y),
		true,
		static_cast<int>(m_Color.a),
		static_cast<int>(m_Color.r),
		static_cast<int>(m_Color.g),
		static_cast<int>(m_Color.b)
		);


	//for (int i = 0; i < POS::MAX; i++)
	//{
	//	//	初期化
	//	m_v[i] = CreateVerTex();
	//}

	////	左上
	//m_v[POS::LEFT_TOP] = CreateVerTex(
	//	D3DCOLOR_ARGB(255, 255, 255, 255), 
	//	CVector3(m_EffectSetPos.x - (TEXTURE_SET_WIDTH_SIZE / 2), m_EffectSetPos.y + (TEXTURE_SET_HEIGHT_SIZE / 2), 0),
	//	D3DXVECTOR2(0, 0)
	//	);
	////	右上
	//m_v[POS::RIGHT_TOP] = CreateVerTex(
	//	D3DCOLOR_ARGB(255, 255, 255, 255),
	//	CVector3(m_EffectSetPos.x + (TEXTURE_SET_WIDTH_SIZE / 2), m_EffectSetPos.y + (TEXTURE_SET_HEIGHT_SIZE / 2), 0),
	//	D3DXVECTOR2(0, 0)
	//	);
	////	右下
	//m_v[POS::RIGHT_BOTTOM] = CreateVerTex(
	//	D3DCOLOR_ARGB(255, 255, 255, 255),
	//	CVector3(m_EffectSetPos.x + (TEXTURE_SET_WIDTH_SIZE / 2), m_EffectSetPos.y - (TEXTURE_SET_HEIGHT_SIZE / 2), 0),
	//	D3DXVECTOR2(0, 0)
	//	);
	////	左下
	//m_v[POS::LEFT_BOTTOM] = CreateVerTex(
	//	D3DCOLOR_ARGB(255, 255, 255, 255),
	//	CVector3(m_EffectSetPos.x - (TEXTURE_SET_WIDTH_SIZE / 2), m_EffectSetPos.y - (TEXTURE_SET_HEIGHT_SIZE / 2), 0),
	//	D3DXVECTOR2(0, 0)
	//	);

}

void CShine::Update()
{

	//m_Num++;
	//if (m_Num > TEXTURE_NUM_MAX - 1){ m_Num = 0; }

	////	左上
	//m_v[POS::LEFT_TOP].Tex = D3DXVECTOR2(TEXTURE_WIDTH_SIZE  * (m_Num % TEXTURE_WIDTH_NUM) / TEXTURE_WIDTH_MAX_SIZE,
	//							 		    TEXTURE_HEIGHT_SIZE * (m_Num / TEXTURE_HEIGHT_NUM) / TEXTURE_HEIGHT_MAX_SIZE);
	////	右上					   
	//m_v[POS::RIGHT_TOP].Tex = D3DXVECTOR2(TEXTURE_WIDTH_SIZE  * (m_Num % TEXTURE_WIDTH_NUM + 1) / TEXTURE_WIDTH_MAX_SIZE,
	//										TEXTURE_HEIGHT_SIZE * (m_Num / TEXTURE_HEIGHT_NUM) / TEXTURE_HEIGHT_MAX_SIZE);
	////	右下					   
	//m_v[POS::RIGHT_BOTTOM].Tex = D3DXVECTOR2(TEXTURE_WIDTH_SIZE  * (m_Num % TEXTURE_WIDTH_NUM + 1) / TEXTURE_WIDTH_MAX_SIZE,
	//										TEXTURE_HEIGHT_SIZE * (m_Num / TEXTURE_HEIGHT_NUM + 1) / TEXTURE_HEIGHT_MAX_SIZE);
	////	左下					   
	//m_v[POS::LEFT_BOTTOM].Tex = D3DXVECTOR2(TEXTURE_WIDTH_SIZE  * (m_Num % TEXTURE_WIDTH_NUM) / TEXTURE_WIDTH_MAX_SIZE,
	//										TEXTURE_HEIGHT_SIZE * (m_Num / TEXTURE_HEIGHT_NUM + 1) / TEXTURE_HEIGHT_MAX_SIZE);


	////	行列合成
	//SetTransMatrix(&m_mTrans, CVector3(-14 + m_EffectSetPos.x, 14 + m_EffectSetPos.y, 2 + m_EffectSetPos.z));



	//m_DivisionNum
}

void CShine::Draw(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	_lpD3DDevice->SetTexture(0, m_pTexture->GetTex());
	//_lpD3DDevice->SetFVF(GET_DIRECT_HELPER.FVF_TLVERTEX);
	//_lpD3DDevice->SetTransform(D3DTS_WORLD, &m_mTrans);




	//	ジェネレートぼかしは最後に入れる
	//	テクスチャーをぼかしたい場合はテクスチャーもブルームの範囲に入れておく

	//_lpD3DDevice->SetTransform(World)
	//GET_DIRECT_HELPER.DrawQuad3D()

	m_mWorld.CreateMove(&CVector3(0, 2, 0));

	_lpD3DDevice->SetTransform(D3DTS_WORLD, &m_mWorld);

	GET_DIRECT_HELPER.DrawQuad3D(0, static_cast<float>(m_pTexture->GetInfo()->Width), 0, static_cast<float>(m_pTexture->GetInfo()->Height), 1, 1, D3DCOLOR_XRGB(255, 255, 255));


/*
	m_pShader->GetBokashiShader()->Draw3D(
		m_pTexture, 
		0, 
		0, 
		m_pTexture->GetInfo()->Width, 
		m_pTexture->GetInfo()->Height, 
		m_DivisionNumU / m_vTextureSize.x,
		m_DivisionNumV / m_vTextureSize.y);

	m_pShader->GetBokashiShader()->DrawXBokasi(m_pTexture, 0, 0, m_pTexture->GetInfo()->Width, m_pTexture->GetInfo()->Height);
	m_pShader->GetBokashiShader()->DrawYBokasi(m_pTexture, 0, 0, m_pTexture->GetInfo()->Width, m_pTexture->GetInfo()->Height);
	
	m_pShader->GetBokashiShader()->GenerateBokasi(m_pTexture);

	m_pShader->GetBokashiShader()->Draw3D(
		&m_pShader->GetBokashiShader()->GetTexBokashi(1), 
		0, 
		0, 
		m_pTexture->GetInfo()->Width, 
		m_pTexture->GetInfo()->Height,
		m_DivisionNumU / m_vTextureSize.x,
		m_DivisionNumV / m_vTextureSize.y);

*/
	_lpD3DDevice->SetTexture(0, NULL);
}

void CShine::Release()
{
	m_pTexture->Release();
}