#include "Game.h"
#include "btBulletCollisionCommon.h"

CSkyMesh::CSkyMesh()
{

}

CSkyMesh::~CSkyMesh()
{
}

void CSkyMesh::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	XファイルマネージャにXファイルを格納その後読み込みよう変数にコピー
	m_Mesh = GET_XFILE_LOAD_MANAGER.Load("Data/XFile/Stage/Sky.x", _lpD3DDevice);

	//	自己発行の力
	m_Mesh->GetMeshState()->m_EmissivePower = 0.4f;

	//	フォグの距離
	m_Mesh->GetMeshState()->m_vFogRange = D3DXVECTOR2(20000, 30000);

	//	ライト処理をオフにする
	m_Mesh->GetMeshState()->m_LightEnable = 0;
}

void CSkyMesh::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{

	//	スカイスフィアを回転
	m_vAng.y += 0.03f;
	if (m_vAng.y > 360)
		m_vAng.y = 0;

	//	座標を設定	
	//m_mWorld.CreateMove(&m_vPos);

	m_vScalSize.Set(5, 5, 5);

	m_mWorld.CreateScale(&m_vScalSize);

	m_mWorld.Move(&m_vPos);

	//	回転量を設定
	m_mWorld.RotateY(m_vAng.y);

}

void CSkyMesh::Release()
{
}