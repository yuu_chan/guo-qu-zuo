#include "CDirectX9.h"
#include "CSystem.h"
#include "CSound.h"

void CSound::Init()
{
	//	サウンドを追加
	DirectSoundCreate8(NULL, &m_lpDSound, NULL);

	//	協調レベルを設定
	m_lpDSound->SetCooperativeLevel(m_CopyHwnd, DSSCL_PRIORITY);

	//  プライマリバッファの作成
	//	DSBUFFERDESC構造体を設定
	DSBUFFERDESC DsbDesc;
	ZeroMemory(&DsbDesc, sizeof(DSBUFFERDESC));
	DsbDesc.dwSize = sizeof(DSBUFFERDESC);
	
	//	プライマリバッファ
	DsbDesc.dwFlags = DSBCAPS_CTRLVOLUME | DSBCAPS_CTRLPAN | DSBCAPS_PRIMARYBUFFER;
	DsbDesc.dwBufferBytes = 0;
	DsbDesc.lpwfxFormat = NULL;

	//　バッファを作る
	m_lpDSound->CreateSoundBuffer(&DsbDesc, &m_lpSPrimary, NULL);

	//	プライマリバッファのWaveフォーマットを設定
	//	優先協調レベル以上の協調レベルが設定されている必要がある
	WAVEFORMATEX PcmWF;
	ZeroMemory(&PcmWF, sizeof(WAVEFORMATEX));
	PcmWF.wFormatTag = WAVE_FORMAT_PCM;
	PcmWF.nChannels = 2;					//　2チャンネル(ステレオ)
	PcmWF.nSamplesPerSec = 44100;			//	サンプリングレート 44.1kHz
	PcmWF.nBlockAlign = 4;			
	PcmWF.nAvgBytesPerSec = PcmWF.nSamplesPerSec * PcmWF.nBlockAlign;
	PcmWF.wBitsPerSample = 16;				//	16bit
	m_lpSPrimary->SetFormat(&PcmWF);

	CoInitialize(NULL);
}

void CSound::Load(int _Id, char* _FileName)
{
	LoadWave(m_lpSSecond[_Id], _FileName);

	LPDIRECTSOUNDBUFFER lpSTmp;
	
	m_lpDSound->DuplicateSoundBuffer(m_lpSSecond[_Id], &lpSTmp);
	lpSTmp->QueryInterface(IID_IDirectSoundBuffer8, (LPVOID *)& m_lpSSecond[_Id]);
	lpSTmp->Release();
	
	m_lpSPrimary->GetVolume(&PVol);
	m_lpSPrimary->GetPan(&PPan);
}

void CSound::PlayTheSound(
	int  _SoundNo,
	bool _AblePlay
	)
{
	/*if (_SoundNo == )
	{
		m_lpSSecond[1]->SetVolume(-500);
		m_lpSSecond[1]->Play(0, 0, 0);
	}

	if (CKeyCode::m_Code.W == CKeyCode::HOLD && CPlayer::walkFlg == 1)
	{
		m_lpSSecond[2]->SetVolume(0);
		m_lpSSecond[2]->Play(0, 0, DSBPLAY_LOOPING);
	}
	else if (CKeyCode::m_Code.W == CKeyCode::PULL && CPlayer::walkFlg == 0)
	{
		m_lpSSecond[2]->Stop();
	}*/


	//-----------------------
	//	ゲーム中に再生するBGM
	//-----------------------
	if (_SoundNo == BGM::NAME::GAME)
	{

		if (_AblePlay == true)
		{
			m_lpSSecond[0]->SetVolume(-100);
			m_lpSSecond[0]->Play(0, 0, DSBPLAY_LOOPING);
		}
		else if (_AblePlay == false)
		{
			m_lpSSecond[0]->Stop();
		}

	}
	else if (_SoundNo == BGM::NAME::TITLE_AND_SELECT)
	{

		if (_AblePlay == true)
		{
			m_lpSSecond[1]->SetVolume(-100);
			m_lpSSecond[1]->Play(0, 0, DSBPLAY_LOOPING);
		}
		else if (_AblePlay == false)
		{
			m_lpSSecond[1]->Stop();
		}

	}


	////----
	////	SE
	////----
	//if (_SoundNo == SE::NAME::JUMP)
	//{

	//	if (_AblePlay == true)
	//	{
	//		m_lpSSecond[2]->SetVolume(-800);
	//		m_lpSSecond[2]->Play(0, 0, 0);
	//	}
	//	else if (_AblePlay == false)
	//	{
	//		m_lpSSecond[2]->Stop();
	//	}

	//}
	//else if (_SoundNo == SE::NAME::WALK)
	//{

	//	if (_AblePlay == true)
	//	{
	//		/*m_lpSSecond[3]->SetVolume(-800);
	//		m_lpSSecond[3]->Play(0, 0, 0);*/
	//	}
	//	else if (_AblePlay == false)
	//	{
	//		m_lpSSecond[3]->Stop();
	//	}

	//}
	//else if (_SoundNo == SE::NAME::NOMIKOMU)
	//{

	//	if (_AblePlay == true)
	//	{
	//		m_lpSSecond[4]->SetVolume(-800);
	//		m_lpSSecond[4]->Play(0, 0, 0);
	//	}
	//	else if (_AblePlay == false)
	//	{
	//		m_lpSSecond[4]->Stop();
	//	}

	//}
	//else if (_SoundNo == SE::NAME::ITEM_FALL)
	//{

	//	if (_AblePlay == true)
	//	{
	//		m_lpSSecond[5]->SetVolume(-800);
	//		m_lpSSecond[5]->Play(0, 0, 0);
	//	}
	//	else if (_AblePlay == false)
	//	{
	//		m_lpSSecond[5]->Stop();
	//	}

	//}
	//else if (_SoundNo == SE::NAME::CLICK)
	//{

	//	if (_AblePlay == true)
	//	{
	//		m_lpSSecond[6]->SetVolume(-800);
	//		m_lpSSecond[6]->Play(0, 0, 0);
	//	}
	//	else if (_AblePlay == false)
	//	{
	//		m_lpSSecond[6]->Stop();
	//	}

	//}


}

void CSound::Release()
{
	CoUninitialize();
	m_lpSPrimary->SetVolume(PVol);
	m_lpSPrimary->SetPan(PPan);

	for (int i = 0; i < SOUND_NUM_MAX; i++)
	{
		if (m_lpSSecond[i] != NULL)
		{
			m_lpSSecond[i]->Release();
		}
	}

	m_lpSPrimary->Release();
	m_lpDSound->Release();
}