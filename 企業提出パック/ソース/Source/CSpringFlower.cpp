#include "Game.h"
#include "Scene.h"

CSpringFlower::CSpringFlower()
{

}


CSpringFlower::~CSpringFlower()
{
	m_spRigidBox->Release();
}


void CSpringFlower::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	メッシュ読み込み
	m_Mesh = GET_XFILE_LOAD_MANAGER.Load("Data/XFile/Object/SpringFlower.x", _lpD3DDevice);

	m_spRigidBox = make_shared<CBulletObj_Box>();

	CRigidSupervise::CreateRigidBox(m_spRigidBox, m_Mesh, m_vPos, m_Id, CVector3(0.5f, 0.5f, 0.5f));

	m_spRigidBox->SetMass(0.0f);

	//	このタスクをUserPointerに登録する
	m_spRigidBox->SetUserPointer(this);


	m_spRigidBox->GetMatrix(m_mWorld);

	m_AbleNext = false;
}


void CSpringFlower::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	ObjectMove();

	//	座標を取得
	m_vPos = m_mWorld.GetPos();



	if (GET_FLAG_CALC.CheckFlag(m_ItemState, 0x0fff, STOCK::STATE::DO_ABSORPTION)
		||
		GET_FLAG_CALC.CheckFlag(m_ItemState, 0x0fff, STOCK::STATE::DO_STOCK)
		||
		GET_FLAG_CALC.CheckFlag(m_ItemState, 0x0fff, STOCK::STATE::DO_SPIT))
	{
		m_spRigidBox->SetMass(1.0f);
		m_spRigidBox->GetMatrix(m_mWorld);
	}
	else
	{

		////	Y軸を調べて、0じゃなければ0になるように修正する
		//m_vAng.y = m_mWorld.GetAngleY();

		//if (m_vAng.y > 1.0f)
		//{
		//	m_vAng.y -= 1.0f;
		//}


		//	質量をセット
		m_spRigidBox->SetMass(0.0f);

		if (GET_FLAG_CALC.CheckFlag(m_State, 0x0100, 0x0100))
		{
			
		}
		else
		{
			
		}

		if (m_AbleNext == true)
		{
			m_vScalSize.y -= 0.005f;

			if (m_vScalSize.y <= 0.5f)
			{
				m_vScalSize.y = 0.5f;
				m_AbleNext = false;
			}
		}
		else if (m_AbleNext == false)
		{
			m_vScalSize.y += 0.005f;

			if (m_vScalSize.y >= 1.0f)
			{
				m_vScalSize.y = 1.0f;

				m_AbleNext = true;
			}
		}


		//	行列をセット
		m_mWorld.CreateScale(&m_vScalSize);

		m_mWorld.Move(&m_vPos);
	}

}


void CSpringFlower::Release()
{

}

