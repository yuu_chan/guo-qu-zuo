#include "Game.h"
#include "Scene.h"

CStage1::CStage1()
{
}


CStage1::~CStage1()
{
	m_spRigidBox->Release();

	m_spRigidMesh->Release();

	m_spRigidBox = nullptr;

	m_spRigidMesh = nullptr;
}


void CStage1::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//----------------------------------------------
	//	選択ステージによって読み込むステージを変える
	//----------------------------------------------

	if (CSceneManager::m_NowSelectStage == STAGE_SELECT::STAGE1)
	{
		m_Mesh = GET_XFILE_LOAD_MANAGER.Load("Data/XFile/Stage/Stage1.x", _lpD3DDevice);

		m_Mesh->ComputeTangentFrame();

		m_Mesh->CreateFaceData();
	}
	else if (CSceneManager::m_NowSelectStage == STAGE_SELECT::STAGE2)
	{
		//m_Mesh = GET_XFILE_LOAD_MANAGER.Load("Data/XFile/SkinMesh/room.x", _lpD3DDevice);
		m_Mesh = GET_XFILE_LOAD_MANAGER.Load("Data/XFile/Stage/Stage2.x", _lpD3DDevice);

		m_Mesh->ComputeTangentFrame();

		m_Mesh->CreateFaceData();
	}
	else if (CSceneManager::m_NowSelectStage == STAGE_SELECT::STAGE3)
	{
		m_Mesh = GET_XFILE_LOAD_MANAGER.Load("Data/XFile/Stage/Stage6.x", _lpD3DDevice);
	}

	
	//----------------
	//	剛体を登録する
	//----------------

	m_spRigidBox = make_shared<CBulletObj_Box>();

	//	地面生成
	//	物理ワールドへの登録
	CRigidSupervise::CreateRigidBox(m_spRigidBox, m_Mesh, m_vPos, m_Id, CVector3(0.0f, 0.0f, 0.0f), 0.0f);

	//	メッシュ剛体
	m_spRigidMesh = make_unique<CBulletObj_Mesh>();

	m_spRigidMesh->CreateFromMesh(
		&GET_BULLET_WORLD.m_pBulletWorld,
		m_Mesh->GetMesh(),
		m_vPos
		);

	//	メッシュ剛体のユーザーポインタを登録しておく
	m_spRigidMesh->SetUserPointer(this);

	m_Pass = DRAW_SHADER::PASS::NORMAL;

	m_Mesh->GetMeshState()->m_EnableInk = true;

	/*const int REFRACT_ON = 1;

	m_Mesh->GetMeshState()->m_Refract = REFRACT_ON;*/
}


void CStage1::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	行列を取得してくる
	m_spRigidMesh->GetMatrix(m_mWorld);

	m_vPos = m_mWorld.GetPos();
}


void CStage1::Release()
{
}