#include "Game.h"
#include "Scene.h"

CStage1Collision::CStage1Collision()
{

}

CStage1Collision::~CStage1Collision()
{
	m_spRigidBox->Release();

	m_spRigidMesh->Release();

	m_spRigidBox = nullptr;

	m_spRigidMesh = nullptr;
}

void CStage1Collision::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	
	if (CSceneManager::m_NowSelectStage == STAGE_SELECT::STAGE1)
	{
		m_Mesh = GET_XFILE_LOAD_MANAGER.Load("Data/XFile/Stage/Stage1Collision.x", _lpD3DDevice);
	}
	else if (CSceneManager::m_NowSelectStage == STAGE_SELECT::STAGE2)
	{
		m_Mesh = GET_XFILE_LOAD_MANAGER.Load("Data/XFile/Stage/Stage2Collision.x", _lpD3DDevice);
	}


	//----------------
	//	剛体を登録する
	//----------------

	m_spRigidBox = make_shared<CBulletObj_Box>();

	//	地面生成
	//	物理ワールドへの登録
	CRigidSupervise::CreateRigidBox(m_spRigidBox, m_Mesh, m_vPos, m_Id, CVector3(0.0f, 0.0f, 0.0f), 0.0f);

	//	メッシュ剛体
	m_spRigidMesh = make_unique<CBulletObj_Mesh>();

	m_spRigidMesh->CreateFromMesh(
		&GET_BULLET_WORLD.m_pBulletWorld,
		m_Mesh->GetMesh(),
		m_vPos
		);

	//	メッシュ剛体のユーザーポインタを登録しておく
	m_spRigidMesh->SetUserPointer(this);


	m_mWorld.CreateMove(&m_vPos);
}

void CStage1Collision::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{

	//	行列を取得してくる
	m_spRigidMesh->GetMatrix(m_mWorld);

	m_vPos = m_mWorld.GetPos();
}

void CStage1Collision::Release()
{
}