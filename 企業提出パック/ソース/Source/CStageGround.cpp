#include "CStageGround.h"
#include "Scene.h"


CStageGround::CStageGround()
{

}


CStageGround::~CStageGround()
{
	m_spRigidMesh->Release();

	m_spRigidBox->Release();
}


void CStageGround::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{

	//	メッシュを読み込む
	m_Mesh = GET_XFILE_LOAD_MANAGER.Load("Data/XFile/Stage/Stage1Ground.x", _lpD3DDevice);



	//	立方体剛体
	m_spRigidBox = make_shared<CBulletObj_Box>();

	//	地面
	CRigidSupervise::CreateRigidBox(m_spRigidBox, m_Mesh, m_vPos, m_Id, CVector3(1.0f, 1.0f, 1.0f), 0.0f);


	//	メッシュ剛体
	m_spRigidMesh = make_shared<CBulletObj_Mesh>();

	//CRigidSupervise::CreateRigidMesh(
	//	m_spRigidMesh,
	//	m_Mesh,
	//	m_vPos,
	//	m_Id
	//	);

	m_spRigidMesh->CreateFromMesh(
		&GET_BULLET_WORLD.m_pBulletWorld,
		m_Mesh->GetMesh(),
		m_vPos
		);

	m_spRigidMesh->SetUserPointer(this);

}


void CStageGround::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	m_spRigidMesh->GetMatrix(m_mWorld);
}


void CStageGround::Release()
{

}

