#include "Font.h"


CStageSelectManager::CStageSelectManager()
{

}


CStageSelectManager::~CStageSelectManager()
{
	Release();
}


void CStageSelectManager::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
}


void CStageSelectManager::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	最初の位置を指す
	auto itSelectContainer = m_listSelectContainer.begin();

	while (itSelectContainer != m_listSelectContainer.end())
	{
		(*itSelectContainer)->Update();
		++itSelectContainer;
	}

}


void CStageSelectManager::Draw(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	auto itSelectContainer = m_listSelectContainer.begin();

	while (itSelectContainer != m_listSelectContainer.end())
	{
		(*itSelectContainer)->Draw(_lpD3DDevice);
		++itSelectContainer;
	}

}


void CStageSelectManager::Release()
{
	auto itSelectContainer = m_listSelectContainer.begin();

	while (itSelectContainer != m_listSelectContainer.end())
	{
		//	イテレータの中にあるvectorの中身を消す
		delete (*itSelectContainer);

		//	消したアドレスにNULLを入れておく
		*itSelectContainer = NULL;

		//	消して、位置をつめる
		itSelectContainer = m_listSelectContainer.erase(itSelectContainer);
	}
}


void CStageSelectManager::PushSelectManager(CTextureBase* _Object)
{
	if (_Object != NULL)
	{
		m_listSelectContainer.push_back(_Object);
	}
}


CTextureBase* CStageSelectManager::GetItSelect(int _Id)
{
	auto itSelectContainer = m_listSelectContainer.begin();

	while (itSelectContainer != m_listSelectContainer.end())
	{
		if ((*itSelectContainer)->GetListId() == _Id)
			return (*itSelectContainer);

		++itSelectContainer;
	}

	return 0;
}