#include "Font.h"
#include "Shader.h"
#include "CStageSelectScene.h"
#include "CFade.h"
#include "CSceneManager.h"


bool CStageSelectScene::m_AbleNext = false;

CStageSelectScene::CStageSelectScene()
	:
	m_spSelectWorld(nullptr)
	,
	m_NowMatrix(m_NowMatrix.CreateInverse())
	,
	m_StartMatrix(m_StartMatrix.CreateInverse())
	,
	m_EndMatrix(m_EndMatrix.CreateInverse())
	,
	m_Animation(0)
	,
	m_EnableAnimation(false)
{

}


CStageSelectScene::~CStageSelectScene()
{
	//	セレクトワールド削除
	m_spSelectWorld = nullptr;

	Release();
}


void CStageSelectScene::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{

	//	セレクトワールドをインスタンス化
	//m_pSelectWorld = MyNew CStageSelectWorld();
	m_spSelectWorld = make_shared<CStageSelectWorld>();


	//	セレクトワールド初期化
	m_spSelectWorld->Init(_lpD3DDevice);

	//	フェードアウトを行う
	GET_FADE.SetLoadEnd(true);

	//	マウスの初期化
	GET_MOUSE.InitMousePointer();


	//--------------------------------------
	//	マップの情報が格納されているクラスを
	//	インスタンス化してリストへ
	//--------------------------------------

	//---------------
	//	マップ1の情報
	//---------------
	shared_ptr<CSelectStateBase> spSelectMap1		= make_shared<CSelectStateBase>(CVector3(-4.5f, 2.0f, 1.0f),  CSelectStateBase::SelectionSelect::STAGE_1,		CSelectStateBase::SelectionSelect::STAGE_1,			90.0f);
	shared_ptr<CSelectStateBase> spSelectMap1Item	= make_shared<CSelectStateBase>(CVector3(-5.0f, 5.0f, 10.0f), CSelectStateBase::SelectionSelect::STAGE_1_ITEM,	CSelectStateBase::SelectionSelect::STAGE_1_ITEM,	0.0f);


	//---------------
	//	マップ2の情報
	//---------------
	shared_ptr<CSelectStateBase> spSelectMap2		= make_shared<CSelectStateBase>(CVector3(3.5f, 2.0f, -4.0f), CSelectStateBase::SelectionSelect::STAGE_2,		CSelectStateBase::SelectionSelect::STAGE_2,			90.0f);
	shared_ptr<CSelectStateBase> spSelectMap2Item	= make_shared<CSelectStateBase>(CVector3(1.0f, 5.0f, 10.0f), CSelectStateBase::SelectionSelect::STAGE_2_ITEM,	CSelectStateBase::SelectionSelect::STAGE_2_ITEM,	0.0f);


	//-----------------------------------
	//	生成されたクラスをプッシュしていく
	//-----------------------------------
	m_spMapState.push_back(spSelectMap1);
	m_spMapState.push_back(spSelectMap1Item);
	m_spMapState.push_back(spSelectMap2);
	m_spMapState.push_back(spSelectMap2Item);


	//	アニメーション可能にしておく
	m_EnableAnimation = true;

	//	アニメーションタイムをマックスで初期化
	const float MAX = 1.0f;
	m_Animation		= MAX;


	//	一番初めに見るのは、ステージ1
	m_Now = CSceneManager::m_NowSelectStage;


	//	一番初めに指すべき地点を指定しておく
	for (auto& It : m_spMapState)
	{
		if (It->GetListId() == m_Now)
		{
			m_NowMatrix = It->GetWorld();

			m_StartMatrix = It->GetWorld();

			m_EndMatrix = It->GetWorld();

			break;
		}
	}

}


void CStageSelectScene::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{

	//	マウス処理更新
	GET_MOUSE.SetMousePointer();

	//	カメラのポジションを更新
	GET_CAMERA.CameraUpdate(
		_lpD3DDevice,
		m_NowMatrix.GetPos(),
		NULL,
		m_NowMatrix.GetAngleY()
		);

	//	ライトの情報を変更する
	GET_DIRECT_HELPER.SetDirectionalLight(
		1,
		&CVector3(0.0f, 0.0f, 0.0f),
		&D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f),	// 基本色(ディフーズ RGBA)
		&D3DXCOLOR(0.3f, 0.3f, 0.3f, 0.0f),	// 環境色(アンビエント RGBA)
		&D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f)	// 反射色(スペキュラ RGBA)
		);


	//	セレクトワールド更新
	m_spSelectWorld->Update(_lpD3DDevice);


	//------------------------------------------
	//	クオータニオンのアニメーションを更新する
	//------------------------------------------

	float AnimationSpeed		= 0.01f;
	const float ANIMATION_INIT	= 0.0f;
	const float ANIMATION_MAX	= 1.0f;
	
	//	アニメーション更新
	m_Animation += AnimationSpeed;


	if (GET_KEY.m_Code.Up == GET_KEY.PUSH
		&&
		m_Animation > ANIMATION_MAX)
	{
		//	クオータニオンを行う
		m_EnableAnimation = true;

		for (auto& It : m_spMapState)
		{
			if (It->GetListId() == CSelectStateBase::STAGE_1
				&&
				m_Now == CSelectStateBase::STAGE_2)
			{
				m_EndMatrix = It->GetWorld();
			}
			
			if (It->GetListId() == CSelectStateBase::STAGE_2
				&&
				m_Now == CSelectStateBase::STAGE_2)
			{
				m_StartMatrix = It->GetWorld();
			}
		}

		//	現在以降のステージが無ければアニメーションをしない
		if (m_Now == CSelectStateBase::STAGE_1)
		{
			m_EnableAnimation = false;
		}


		//	アニメーションを初期化
		m_Animation = ANIMATION_INIT;

		//	現在選択されているステージが1
		CSceneManager::m_NowSelectStage = STAGE_SELECT::NAME::STAGE1;

		//	現在はステージ1を見ている
		m_Now = CSelectStateBase::STAGE_1;
	}
	else if (GET_KEY.m_Code.Dwon == GET_KEY.PUSH
			&&
			m_Animation > ANIMATION_MAX)
	{
		//	クオータニオンを行う
		m_EnableAnimation = true;

		for (auto& This : m_spMapState)
		{
			if (This->GetListId() == CSelectStateBase::STAGE_1
				&&
				m_Now == CSelectStateBase::STAGE_1)
			{
				m_StartMatrix = This->GetWorld();
			}
			
			if (This->GetListId() == CSelectStateBase::STAGE_2
				&&
				m_Now == CSelectStateBase::STAGE_1)
			{
				m_EndMatrix = This->GetWorld();
			}
		}

		//	現在以降のステージが無ければアニメーションをしない
		if (m_Now == CSelectStateBase::STAGE_2)
		{
			m_EnableAnimation = false;	
		}


		//	アニメーションを初期化
		m_Animation = ANIMATION_INIT;

		//	現在選択されているステージが2
		CSceneManager::m_NowSelectStage = STAGE_SELECT::NAME::STAGE2;

		//	現在はステージ2を見ている
		m_Now = CSelectStateBase::STAGE_2;
	}


	//	アニメーションが一定を超えればMAXでとめておく
	if (m_Animation > ANIMATION_MAX)
	{
		m_Animation = ANIMATION_MAX;
	}

	if (m_EnableAnimation == true)
	{
		//	クオータニオンを更新
		GET_CAMERA.D3DLMatrixAnimation(&m_NowMatrix, &m_StartMatrix, &m_EndMatrix, m_Animation);
	}
	else
	{
		m_Animation = ANIMATION_MAX;
	}


	//----------------------------------------------
	//	if文をできる限り少なくするように変更しておく
	//----------------------------------------------
	if (GET_KEY.m_Code.Return == GET_KEY.PUSH)
	{

		if (m_Now == CSelectStateBase::STAGE_1)
		{
			m_AbleNext = true;

			//m_Now = CSelectStateBase::STAGE_1_ITEM;


			//for (auto& It : m_spMapState)
			//{
			//	if (It->GetListId() == CSelectStateBase::STAGE_1)
			//	{
			//		m_StartMatrix = It->GetWorld();
			//	}
			//	else if (It->GetListId() == CSelectStateBase::STAGE_1_ITEM)
			//	{
			//		m_EndMatrix = It->GetWorld();
			//	}
			//}


			//m_Animation = 0.0f;
		}
		else if (m_Now == CSelectStateBase::STAGE_2)
		{
			m_AbleNext = true;

			//m_Now = CSelectStateBase::STAGE_2_ITEM;

			//for (auto& It : m_spMapState)
			//{
			//	if (It->GetListId() == CSelectStateBase::STAGE_2)
			//	{
			//		m_StartMatrix = It->GetWorld();
			//	}
			//	else if (It->GetListId() == CSelectStateBase::STAGE_2_ITEM)
			//	{
			//		m_EndMatrix = It->GetWorld();
			//	}
			//}


			//m_Animation = 0.0f;
		}


		//if (m_Now == CSelectStateBase::STAGE_1_ITEM
		//	&&
		//	m_Animation >= 1.0f
		//	||
		//	m_Now == CSelectStateBase::STAGE_2_ITEM
		//	&&
		//	m_Animation >= 1.0f
		//	)
		//{
		//	m_AbleNext = true;
		//}


	}
	else
	{
		m_AbleNext = false;
	}




}


void CStageSelectScene::Draw(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//----------------------
	//	各オブジェクトを描画
	//----------------------

	//	シェーダー描画開始
	GET_SHADER.DrawShaderBegin();

	//	セレクトワールド描画
	m_spSelectWorld->Draw(_lpD3DDevice);

	//	ブルーム描画
	GET_SHADER.DrawBloomShaderEnd();

}


void CStageSelectScene::Release()
{
	//	セレクトワールド開放
	m_spSelectWorld = nullptr;


	//	使用したlistの中身をすべて破棄させる
	auto It = m_spMapState.begin();

	while (It != m_spMapState.end())
	{
		*It = nullptr;

		It = m_spMapState.erase(It);
	}
}