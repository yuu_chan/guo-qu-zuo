#include "Font.h"


CStageSelectWorld::CStageSelectWorld()
{

}


CStageSelectWorld::~CStageSelectWorld()
{
	//Delete(m_SelectManager);
	m_SelectManager = nullptr;
}


void CStageSelectWorld::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//--------------------------------------
	//	エフェクトマネージャをインスタンス化
	//--------------------------------------
	//m_SelectManager = MyNew CStageSelectManager();
	m_SelectManager = make_shared<CStageSelectManager>();


	//--------
	//	初期化
	//--------
	m_SelectManager->Init(_lpD3DDevice);


	//----------------------------------------------
	//	格オブジェクトをインスタンス化後タスクに格納
	//----------------------------------------------
	
	CPicture*	pStage1Picture = m_SelectManager->CreateSelectTask<CPicture>();

	CPicture*	pStage2Picture = m_SelectManager->CreateSelectTask<CPicture>();
	
	//	メニュー本
	CSelectTable* SelectTable = m_SelectManager->CreateSelectTask<CSelectTable >();


	//--------
	//	初期化
	//--------

	CVector3 vPos = CVector3(-5.0f, 7.0f, 15.0f);

	pStage1Picture->SetEffectId(CPicture::SELECT_PICTURE::STAGE_1);

	pStage1Picture->SetPosition(vPos);

	pStage1Picture->Init(_lpD3DDevice);


	vPos = CVector3(1.0f, 7.0f, 15.0f);

	pStage2Picture->SetEffectId(CPicture::SELECT_PICTURE::STAGE_2);

	pStage2Picture->SetPosition(vPos);

	pStage2Picture->Init(_lpD3DDevice);


	SelectTable->Init(_lpD3DDevice);

}

void CStageSelectWorld::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	m_SelectManager->Update(_lpD3DDevice);
}

void CStageSelectWorld::Draw(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	m_SelectManager->Draw(_lpD3DDevice);
}

void CStageSelectWorld::Release()
{
	m_SelectManager->Release();
}
