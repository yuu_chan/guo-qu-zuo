#include "Scene.h"
#include "Game.h"
#include "Texture.h"
#include "CStart.h"


CStart::CStart()
	:
	m_vEndPos(0.0f, 0.0f, 0.0f)
{
}


CStart::~CStart()
{

}


void CStart::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//------------------------------------------------------------------------------
	//	ステージ毎に代入する値が変わるので、判定分を使用する	固定値にしておきたい
	//------------------------------------------------------------------------------

	//	ステージ1が選択された場合
	if (CSceneManager::m_NowSelectStage == STAGE_SELECT::NAME::STAGE1)
	{
		//	座標を決める
		m_vStartPos = CVector3(-30.0f, 0.42f, -17.5f);

		//	プレイヤーの初期位置をセットする
		m_pGameWorld.lock()->GetObjectManager()->GetItObject(OBJECT_LIST::ID::PLAYER)->SetPosition(m_vStartPos);

		//	アングルの初期化
		m_vMoveAngle = CVector3(15.0f, 200.0f, 0.0f);

		//	ステップ初期化
		m_Step = 0;

		//	スタート終了位置
		m_vEndPos.x = 5.0f;

		//	スタートイベントを行う
		m_NowEvent = true;
	}
	else if (CSceneManager::m_NowSelectStage == STAGE_SELECT::NAME::STAGE2)
	{		
		//	座標を決める
		m_vStartPos = CVector3(15.149996f, 0.400000f, -15.399998f);

		//	プレイヤーの初期位置をセットする
		m_pGameWorld.lock()->GetObjectManager()->GetItObject(OBJECT_LIST::ID::PLAYER)->SetPosition(m_vStartPos);

		//	アングルの初期化
		m_vMoveAngle = CVector3(15.0f, 200.0f, 0.0f);

		//	ステップ初期化
		m_Step = 0;

		//	スタート終了位置
		m_vEndPos.x = -5.0f;

		//	スタートイベントを行う
		m_NowEvent = true;
	}

}


void CStart::StartEvent(LPDIRECT3DDEVICE9 _lpD3DDevice)
{

	//--------------
	//	NULLチェック
	//--------------

	//	ポインターが入っているかどうかを確認
	if (m_pGameWorld.lock() == nullptr
		||
		m_pEffectWorld.lock() == nullptr)
	{
		//	警告メッセージ
		MessageBox(NULL, "指定されたポインターが入っていません", "警告", MB_OK);

		//	終了させるためにフラグを立てておく
		APP.m_bEndFlag = true;

		//	何もさせずに返す
		return;
	}


	//------------------------------------------------------->
	//	ここから本文
	//-------------->

	enum STEP
	{
		MOVE		= 0,
		FRAME		= 1,
		GAME_START  = 2,
	};


	//--------------
	//	カメラの設定
	//--------------

	//	カメラのアングルを決める
	CVector3 vAngle;

	//	アングルをコピーしておく
	vAngle = m_pGameWorld.lock()->GetObjectManager()->GetItObject(OBJECT_LIST::ID::PLAYER)->GetAngle();

	//	イベント中はこっちでカメラをいじる
	if (m_NowEvent == true)
	{

		//	カメラの位置を修正
		vAngle.y = vAngle.y - m_vMoveAngle.y;
		vAngle.x = vAngle.x + m_vMoveAngle.x;
		vAngle.z = 0.0f;

		//	最後のステップなら
		if (m_Step == STEP::FRAME)
		{
			m_vMoveAngle.y -= 10.0f;

			if (m_vMoveAngle.y < 0)
			{
				//	アングルを初期化
				m_vMoveAngle.x = 0.0f;
				m_vMoveAngle.y = 0.0f;

			}
		}

		//	カメラに座標をセットする
		GET_CAMERA.CameraUpdate(
			_lpD3DDevice,
			m_pGameWorld.lock()->GetObjectManager()->GetItObject(OBJECT_LIST::ID::PLAYER)->GetPosition(),
			NULL,
			vAngle.x,
			vAngle.y,
			0.5f
			);

	}


	//------------------
	//	プレイヤーの移動
	//------------------

	//	更新するプレイヤーの座標を決める
	CVector3 vNowPlayerPos;

	//	プレイヤーの座標をコピーしておく
	vNowPlayerPos = m_pGameWorld.lock()->GetObjectManager()->GetItObject(OBJECT_LIST::ID::PLAYER)->GetPosition();

	//	プレイヤーのワールド行列とY軸
	CMatrix mPlayerWorld;
	CMatrix mRotY;

	//	ワールド行列をコピーしておく
	mPlayerWorld = m_pGameWorld.lock()->GetObjectManager()->GetItObject(OBJECT_LIST::ID::PLAYER)->GetWorld();

	float AngY = mPlayerWorld.GetAngleY();

	mRotY.CreateRotateY(AngY);


	//------------------------------------------------
	//	向いている方向から、移動したい座標を取ってくる
	//------------------------------------------------
	float Speed = 0.05f;

	//	方向用の変数
	CVector3 vVec;

	//	移動方向を出す
	D3DXVec3TransformCoord(&vVec, &CVector3(0.0f, 0.0f, Speed), &mRotY);

	//	プレイヤーの座標を更新
	vNowPlayerPos += vVec;


	//------------------------------------------------
	//	スタート演出を終える座標までの距離の長さを算出
	//------------------------------------------------

	if (m_Step == STEP::MOVE)
	{

		//	スタート終了座標までの距離用変数
		float StartCorner;
		float Corner = 5.0f;

		//	スタート演出の終わる場所
		CVector3 vStartCorner = m_vStartPos;

		vStartCorner = vStartCorner + m_vEndPos;//CVector3(vStartCorner.x + Corner, vStartCorner.y, vStartCorner.z);

		//	距離を算出させる
		StartCorner = GET_HELPER.GetDistance(vNowPlayerPos, vStartCorner);

		//	プレイヤーの座標が一定を越したら
		float Line = 0.5f;

		if (StartCorner < Line && StartCorner > -(Line))
		{
			//	次のステップへ
			m_Step = STEP::FRAME;
		}
		//	越していない場合は座標を更新させる
		else
		{
			//	アニメーション更新
			m_pGameWorld.lock()->GetObjectManager()->GetItObject(OBJECT_LIST::ID::PLAYER)->GetMeshObject()->SetAnimationKey(2);

			//	更新したプレイヤーの座標を送る
			m_pGameWorld.lock()->GetObjectManager()->GetItObject(OBJECT_LIST::ID::PLAYER)->SetPosition(vNowPlayerPos);
		}

	}
	

	//------------
	//	エフェクト
	//------------

	//	先頭IDを取得
	auto FirstId = m_pEffectWorld.lock()->GetEffectManager()->GetItEffect(0)->GetFirstId();

	//	後尾IDを取得
	auto LastId = m_pEffectWorld.lock()->GetEffectManager()->GetItEffect(0)->GetLastId();

	//	先頭から後尾までループを回す
	for (auto i = FirstId; i < LastId; i++)
	{

		//	エフェクトIDを取得する
		auto EffectId = m_pEffectWorld.lock()->GetEffectManager()->GetItEffect(i)->GetEffectId();

		//	最大拡大量
		const auto SCALE_Y_MAX = 15.0f;
		const auto SCALE_Y_MOVE = 0.5f;


		//------------------------------------------------
		//	スタート演出を行っているときは黒幕をかけておく
		//------------------------------------------------

		//	一番初めのステップなら
		if (m_Step == STEP::MOVE)
		{

			//	上側の黒い淵
			if (EffectId == TEX_LIST::ID::EVENT_FRAME_UP)
			{
				//	初期サイズはマックス
				m_vUpScale.x = 640.0f;
				m_vUpScale.y = SCALE_Y_MAX;
				m_vUpScale.z = 0.0f;

				//	最後に、値を渡しておく
				m_pEffectWorld.lock()->GetEffectManager()->GetItEffect(i)->SetScale(m_vUpScale);
			}
			//	下側の黒い淵
			else if (EffectId == TEX_LIST::ID::EVENT_FRAME_DOWN)
			{
				//	初期サイズはマックス
				m_vDownScale.x = 640.0f;
				m_vDownScale.y = -(SCALE_Y_MAX);
				m_vDownScale.z = 0.0f;

				//	最後に、値を渡しておく
				m_pEffectWorld.lock()->GetEffectManager()->GetItEffect(i)->SetScale(m_vDownScale);
			}

		}
		//	次のステップが着たら
		else if (m_Step == STEP::FRAME)
		{

			//	上側の黒い淵
			if (EffectId == TEX_LIST::ID::EVENT_FRAME_UP)
			{
				//	サイズ更新
				m_vUpScale.x = 640.0f;
				m_vUpScale.y -= SCALE_Y_MOVE;
				m_vUpScale.z = 0.0f;

				//	最少量
				if (m_vUpScale.y < 0)
				{
					//	超えたらストップ
					m_vUpScale.y = 0;
				}

				//	最後に、値を渡しておく
				m_pEffectWorld.lock()->GetEffectManager()->GetItEffect(i)->SetScale(m_vUpScale);
			}
			//	下側の黒い淵
			else if (EffectId == TEX_LIST::ID::EVENT_FRAME_DOWN)
			{
				//	サイズ更新
				m_vDownScale.x = 640.0f;
				m_vDownScale.y += SCALE_Y_MOVE;
				m_vDownScale.z = 0.0f;
				
				//	最少量
				if (m_vDownScale.y > 0)
				{
					//	超えたらストップ
					m_vDownScale.y = 0;

					//  次のステップへ(ループの最後なので一応こっち側に記述)
					m_Step = STEP::GAME_START;
				}

				//	最後に、値を渡しておく
				m_pEffectWorld.lock()->GetEffectManager()->GetItEffect(i)->SetScale(m_vDownScale);
			}

		}

	}


	//	ステップが最後までいったら
	if (m_Step == STEP::GAME_START)
	{
		//	ゲームを開始
		m_NowEvent = false;

		//	アングルを送っておく
		m_pGameWorld.lock()->GetObjectManager()->GetItObject(OBJECT_LIST::ID::PLAYER)->SetAngle(vAngle);
	}
}


//	開放
void CStart::Release()
{

}