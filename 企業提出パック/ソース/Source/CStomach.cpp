#include "Texture.h"
#include "Game.h"


CStomach::CStomach()
{

}


CStomach::~CStomach()
{
	m_pTexture->Release();
	m_pTexture = nullptr;
}


void CStomach::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	テクスチャのサイズを指定
	const auto TEXTURE_WIDTH_SIZE  = 204;
	const auto TEXTURE_HEIGHT_SIZE = 132;

	//	テクスチャを読み込む
	m_pTexture->LoadTextureEx(
		"Data/Graphics/UI/Stomach.bmp",
		TEXTURE_WIDTH_SIZE,
		TEXTURE_HEIGHT_SIZE,
		1,
		0,
		D3DFMT_UNKNOWN,
		D3DPOOL_MANAGED,
		D3DX_FILTER_NONE,
		D3DX_DEFAULT,
		D3DCOLOR_ARGB(255, 255, 255, 255),
		NULL
		);

	//	もし読み込めなかった場合はメッセージを出す
	if (m_pTexture->GetTex() == nullptr)
	{
		MessageBox(NULL, "テクスチャーが正しく読み込めていません->CStomach", "警告", MB_OK);
	}
}


void CStomach::Update()
{
	//	ワールド行列に移動ベクトルをセット
	m_mWorld.CreateMove(&CVector3((float)APP.m_Width - m_pTexture->GetInfo()->Width, (float)APP.m_Height - m_pTexture->GetInfo()->Height, 0));

	/*m_mWorld.CreateScale(&CVector3(0.8f, 0.8f, 0.0f));
	m_mWorld.Move(&CVector3(0.0f, 0.0f, 0.0f));*/
}


void CStomach::Draw(LPDIRECT3DDEVICE9 _lpD3DDevce)
{
	GET_DIRECT_HELPER.BeginSprite();
	
	GET_DIRECT_HELPER.DrawSprite(m_pTexture.get(), D3DCOLOR_ARGB(255, 255, 255, 255), &m_mWorld);
	
	GET_DIRECT_HELPER.EndSprite();
}


void CStomach::Release()
{

}

