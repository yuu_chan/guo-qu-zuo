#include "Texture.h"
#include "Game.h"


CStomachGauge::CStomachGauge()
{

}


CStomachGauge::~CStomachGauge()
{
	m_pTexture->Release();
	m_pTexture = nullptr;
}


void CStomachGauge::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	テクスチャのサイズを指定
	const auto TEXTURE_WIDTH_SIZE = 204;
	const auto TEXTURE_HEIGHT_SIZE = 132;

	//	画像を読み込む
	m_pTexture->LoadTextureEx(
		"Data/Graphics/UI/StomachGauge.bmp",
		TEXTURE_WIDTH_SIZE,
		TEXTURE_HEIGHT_SIZE,
		1,
		0,
		D3DFMT_UNKNOWN,
		D3DPOOL_MANAGED,
		D3DX_FILTER_NONE,
		D3DX_DEFAULT,
		D3DCOLOR_ARGB(255, 255, 255, 255),
		NULL
		);

	//	もし画像が読み込めなかった場合はメッセージを出す
	if (m_pTexture->GetTex() == nullptr)
	{
		MessageBox(NULL, "テクスチャーが正しく読み込めていません->CStomach", "警告", MB_OK);
	}

	//	満腹メーターの初期設定
	m_Meter = CStuffedMeter::m_StuffedMeter;

	//	色の指定
	m_Color = D3DXCOLOR(255.0f, 255.0f, 255.0f, 255.0f);
}

void CStomachGauge::Update()
{

	//	満腹メーターの最大値　- 現在値 = 描画するメーターの幅
	m_Meter = CStuffedMeter::STUFFED_METER_MAX - CStuffedMeter::m_StuffedMeter;


	//	ワールド行列に移動ベクトルをセット
	m_mWorld.CreateMove(
		&CVector3(
		static_cast<float>(APP.m_Width - m_pTexture->GetInfo()->Width),
		static_cast<float>(APP.m_Height - m_pTexture->GetInfo()->Height) + m_Meter, 
		0)
		);


	//	現在何%かを取得
	static_cast<float>(m_Residue) = GET_HELPER.GetPercent(static_cast<float>(CStuffedMeter::STUFFED_METER_MAX), static_cast<float>(CStuffedMeter::m_StuffedMeter));


	//	80%以下〜40%以上なら黄色に変化
	if (m_Residue <= 80
		&&
		m_Residue >= 40)
	{
		m_Color.b -= 1.0f;

		if (m_Color.b < 0.0f)
		{
			m_Color.b = 0.0f;
		}
	}
	//	40%以下〜0%なら赤に変化
	else if (
		m_Residue <= 40
		&&
		m_Residue >= 0
		)
	{
		m_Color.g -= 1.0f;

		if (m_Color.g < 0.0f)
		{
			m_Color.g = 0.0f;
		}
	}


}


void CStomachGauge::Draw(LPDIRECT3DDEVICE9 _lpD3DDevce)
{
	GET_DIRECT_HELPER.BeginSprite();

	RECT Rec = { 0, 0, m_pTexture->GetInfo()->Width, m_pTexture->GetInfo()->Height };

	//	満腹度メーター分Topに加算していく
	Rec.top += m_Meter;

	GET_DIRECT_HELPER.DrawSprite(m_pTexture.get(), Rec, &m_mWorld, m_Color);


	GET_DIRECT_HELPER.EndSprite();
}


void CStomachGauge::Release()
{

}

