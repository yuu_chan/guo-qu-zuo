#include "Game.h"


CStone::CStone()
{

}


CStone::~CStone()
{

}


void CStone::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	m_Mesh = GET_XFILE_LOAD_MANAGER.Load("Data/XFile/Object/Stone.x", _lpD3DDevice);

	m_spRigidBox = make_shared<CBulletObj_Box>();

	CRigidSupervise::CreateRigidBox(m_spRigidBox, m_Mesh, m_vPos, m_Id);

	m_spRigidBox->SetUserPointer(this);

	m_spRigidBox->SetMass(0.0f);

	m_State = GET_FLAG_CALC.SetData(m_State, 0x0001, LOGICAL::OPERATION_MODE::OR);
}


void CStone::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	
	const float MOVE = 0.01f;

	if (GET_FLAG_CALC.CheckFlag(m_State, 0x0001))
	{
		m_vPos.y += MOVE;

		if (m_vPos.y >= 0.0f)
		{
			m_vPos.y = 0.0f;
		}

		//m_vPos = CVector3(0.0f, 0.0f, 0.0f);

		m_mWorld.CreateMove(&m_vPos);

		m_spRigidBox->SetMatrix(m_mWorld);
	}
	else if (GET_FLAG_CALC.CheckFlag(m_State, 0x0002))
	{
		m_vPos.y -= MOVE;

		if (m_vPos.y < -(m_Mesh->GetBounding()->GetBoundingBoxMax().y))
		{
			m_vPos.y = -(m_Mesh->GetBounding()->GetBoundingBoxMax().y);
		}

		//m_vPos = CVector3(0.0f, -m_Mesh->GetBounding()->GetBoundingBoxMax().y, 0.0f);

		m_mWorld.CreateMove(&m_vPos);

		m_spRigidBox->SetMatrix(m_mWorld);
	}


	//----------------------------------------
	//	自分が下に移動したら隣の岩を上にあげる
	//----------------------------------------

	for (auto &This : CRigidSupervise::GetRigidList())
	{
		CObjectBase* task = (CObjectBase*) This.lock()->GetUserPointer();

		if (task
			&&
			task->GetId() != m_Id
			&&
			task->GetItemId() == OBJECT_LIST::ID::STONE
			&&
			task->GetRegisterTag() == m_RegisterTag
			&&
			GET_FLAG_CALC.CheckFlag(task->GetState(), 0x0002, 0x0002))
		{
			int State = task->GetState();

			State = GET_FLAG_CALC.SetData((State & 0x0000), 0x0001, LOGICAL::OPERATION_MODE::OR);

			task->SetState(State);
		}
	}

}


void CStone::Release()
{

}

