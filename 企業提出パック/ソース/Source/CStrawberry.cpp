#include "Game.h"
#include "CGameScene.h"


CStrawberry::CStrawberry()
{

}


CStrawberry::~CStrawberry()
{
	m_spRigidSphere->Release();
}


void CStrawberry::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	XファイルマネージャにXファイルを格納その後読み込みよう変数にコピー
	m_Mesh = GET_XFILE_LOAD_MANAGER.Load("Data/XFile/Object/Strawberry.x", _lpD3DDevice);

	m_spRigidSphere = make_shared<CBulletObj_Sphere>();

	//	球剛体を登録する
	CRigidSupervise::CreateRigidSphere(m_spRigidSphere, m_Mesh, m_vPos, m_Id, m_Mesh->GetBounding()->GetBoundingSphereRadius() / 2);

	//	このタスクをUserPointerに記憶
	m_spRigidSphere->SetUserPointer(this);


	//	ステータスを落下していないに変化
	m_State = GET_FLAG_CALC.SetData(m_State, 0x0010, LOGICAL::OPERATION_MODE::OR);

}


void CStrawberry::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{

	//	共通処理
	ObjectMove();


	//	落下すれば、アイテムは物理ワールドから行列をもらえる
	if (GET_FLAG_CALC.CheckFlag(m_State, 0x00f0, 0x0020))
	{
		//	オブジェクトにかける力を一度クリアしておく
		m_spRigidSphere->ClearForces();

		//	行列を取得する
		m_spRigidSphere->GetMatrix(m_mWorld);

		//	座標取得
		m_vPos = m_mWorld.GetPos();


		//	質量を1にしておく
		m_spRigidSphere->SetMass(1.0f);

		m_spRigidSphere->ApplyTorque(CVector3(10.0f, 0.0f, 10.0f));

		//	ステータスを落下に変化
		m_State = GET_FLAG_CALC.SetData((m_State & 0x000f), 0x0030, LOGICAL::OPERATION_MODE::OR);
	}
	else if (GET_FLAG_CALC.CheckFlag(m_State, 0x00f0, 0x0010))
	{
		//	座標を指定して
		m_mWorld.CreateMove(&m_vPos);

		//	行列を物理ワールドにセットする
		m_spRigidSphere->SetMatrix(m_mWorld);


		//	質量を0にしておく
		m_spRigidSphere->SetMass(0.0f);
	}
	else if (GET_FLAG_CALC.CheckFlag(m_State, 0x00f0, 0x0030))
	{
		//	行列を取得する
		m_spRigidSphere->GetMatrix(m_mWorld);

		//	座標取得
		m_vPos = m_mWorld.GetPos();
	}

}


void CStrawberry::Release()
{

}


void CStrawberry::CreateJoint()
{

	m_spJoint = make_shared<CBulletJoint_Point>();

	//	ジョイントを作成する
	m_spJoint->Create(
		&GET_BULLET_WORLD.m_pBulletWorld,
		*(CRigidSupervise::GetItObject(m_Id)->GetBody()),
		*(CRigidSupervise::GetItObject(m_Id + 1)->GetBody()),
		CVector3(0.0f, 0.0f, 0.0f),
		CVector3(0.0f, 0.0f, 0.0f)
		);
}