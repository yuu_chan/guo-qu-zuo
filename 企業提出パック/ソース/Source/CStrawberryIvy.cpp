#include "Game.h"
#include "CGameScene.h"


CStrawberryIvy::CStrawberryIvy()
{

}


CStrawberryIvy::~CStrawberryIvy()
{
	m_spMesh->Release();
}


void CStrawberryIvy::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{

	//	XファイルマネージャにXファイルを格納その後読み込みよう変数にコピー
	m_Mesh->LoadMesh("Data/XFile/Object/StrawberryIvy.x", _lpD3DDevice);
	//GET_XFILE_LOAD_MANAGER.Load("Data/XFile/Object/StrawberryIvy.x", _lpD3DDevice);


	m_spMesh = make_shared<CBulletObj_Mesh>();

	CRigidSupervise::CreateRigidMesh(m_spMesh, m_Mesh, m_vPos, m_Id);


	//	タスクを登録する
	m_spMesh->SetUserPointer(this);

	//m_mWorld.CreateRotateY(m_vAng.y);

	//m_mWorld.Move(&m_vPos);

	m_mWorld.CreateScale(&m_vScalSize);

	m_mWorld.RotateY(m_vAng.y);

	m_mWorld.Move(&m_vPos);

	m_spMesh->SetMatrix(m_mWorld);


	m_spMesh->GetMatrix(m_mWorld);

	m_vPos = m_mWorld.GetPos();

	//	ステータスを落下していないに変化
	m_State = GET_FLAG_CALC.SetData(m_State, 0x0010, LOGICAL::OPERATION_MODE::OR);


	m_Pass = 2;
}


void CStrawberryIvy::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	共通処理
	ObjectMove();



	//	イチゴが取れたら蔦のα値を消す
	//if (GET_FLAG_CALC.CheckFlag(m_State, 0x00f0, 0x0020))
	//{
	//	for (int i = 0; i < m_Mesh->GetMaterialCnt(); i++)
	//	{
	//		D3DMATERIAL9* pMat = &m_Mesh->GetMaterials()[i];


	//		pMat->Diffuse.a -= 0.05f;

	//		if (pMat->Diffuse.a < 0.0f)
	//		{
	//			pMat->Diffuse.a = 0.0f;

	//			m_vPos.y = -100.0f;

	//			m_mWorld.CreateMove(&m_vPos);

	//			m_spMesh->SetMatrix(m_mWorld);
	//		}
	//	}
	//}

}


void CStrawberryIvy::Release()
{

}

