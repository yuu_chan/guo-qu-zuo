#include "CSurface.h"

BOOL CSurface::CreateDepthStencil(
	UINT				_Width, 
	UINT				_Height, 
	D3DFORMAT			_Format, 
	D3DMULTISAMPLE_TYPE _MultiSample, 
	DWORD				_MultisampleQuality, 
	BOOL				_Discard,
	LPDIRECT3DDEVICE9  _lpD3DDevice)
{
	Release();

	HRESULT hr = _lpD3DDevice->CreateDepthStencilSurface(
		_Width, 
		_Height, 
		_Format, 
		_MultiSample, 
		_MultisampleQuality, 
		_Discard, &m_lpSur, NULL);
	
	if (hr != D3D_OK)return FALSE;

	m_lpSur->GetDesc(&m_Desc);

	return TRUE;
}

// �Z�k��
BOOL CSurface::CreateDepthStencil(
	UINT				_Width, 
	UINT				_Height, 
	D3DFORMAT			_Format,
	LPDIRECT3DDEVICE9	_lpD3DDevice)
{
	return CreateDepthStencil(_Width, _Height, _Format, D3DMULTISAMPLE_NONE, 0, FALSE, _lpD3DDevice);
}

void CSurface::SetDepthStencil(LPDIRECT3DDEVICE9  _lpD3DDevice)
{
	_lpD3DDevice->SetDepthStencilSurface(m_lpSur);
}

void CSurface::Release()
{
	SafeRelease(m_lpSur);
	ZeroMemory(&m_Desc, sizeof(m_Desc));
}
