#include "Game.h"


CSwitch::CSwitch()
{

}


CSwitch::~CSwitch()
{

}


void CSwitch::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	m_Mesh = GET_XFILE_LOAD_MANAGER.Load("Data/XFile/Object/Switch.x", _lpD3DDevice);



	m_spRigidBox = make_shared<CBulletObj_Box>();

	CRigidSupervise::CreateRigidBox(m_spRigidBox, m_Mesh, m_vPos, m_Id, m_Mesh->GetBounding()->GetBoundingBoxMax(), 0.0f);

	m_spRigidBox->SetUserPointer(this);


	m_vRememberPos = m_vPos;
}


void CSwitch::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{

	const float MOVE_MIN		= 0.2f;
	const float MOVE_MAX		= 1.0f;
	const float MOVE_UPDATE		= 0.01f;

	//--------------------------------------
	//	スイッチが押し込まれたので、縮小して
	//	縮小しきったら、次へ
	//--------------------------------------
	if (m_EnableColl == true)
	{

		m_vPos.y -= MOVE_UPDATE;

		if (m_vPos.y < (m_vRememberPos.y - MOVE_MIN))
		{
			m_vPos.y = (m_vRememberPos.y - MOVE_MIN);


			m_EnableSwitch = true;
		}

		m_mWorld.CreateScale(&m_vScalSize);

		m_mWorld.RotateY(m_vAng.y);

		m_mWorld.Move(&m_vPos);

	}
	//--------------------------------------------------
	//	縮小されていた場合は、元のサイズに戻るようにする
	//	それいがいは、特に変化もなし
	//--------------------------------------------------
	else if (m_EnableColl == false)
	{

		if (m_vPos.y < (m_vRememberPos.y + MOVE_MAX))
		{
			m_vPos.y += MOVE_UPDATE;

			if (m_vPos.y >= m_vRememberPos.y)
			{
				m_vPos.y = m_vRememberPos.y;

				m_EnableSwitch = false;
			}
		}


		m_mWorld.CreateScale(&m_vScalSize);

		m_mWorld.RotateY(m_vAng.y);

		m_mWorld.Move(&m_vPos);
	}



	//	質量0	物理ワールド側に行列をセットしておく
	m_spRigidBox->SetMass(0.0f);
	m_spRigidBox->SetMatrix(m_mWorld);


	//--------------------------------------------------------------
	//
	//	スイッチが押されたら、連結されているオブジェクトの処理を開始
	//	bool型の変数を入れて、falseならなにもせずに返す
	//						  trueなら処理を実行させる
	//
	//--------------------------------------------------------------

	function<void(bool)> ProcessRun = 
	[&]								//	全キャプチャ
	(const bool _EnableSwitch)		//	bool型の変数を代入
	{

		if (_EnableSwitch == false)
		{
		/*	for (auto &It : GET_WORLD.GetGameWorld()->GetGimmcikManager()->GetGimmickContainer())
			{

				if (m_ItemId + 1 == It->GetItemId())
				{
					It->SetPosition(CVector3(-5.249999, 3.999998f, 0.000000));
				}
				else if (m_ItemId + 1 == It->GetItemId())
				{
					It->SetPosition(CVector3(-8.849999, 3.999998f, 0.000000));
				}
				else if (m_ItemId + 2 == It->GetItemId())
				{
					It->SetPosition(CVector3(-12.300001, 3.999998f, 0.000000));
				}
			}*/

			return;
		}

	};



	ProcessRun(m_EnableSwitch);




}


void CSwitch::Release()
{

}

