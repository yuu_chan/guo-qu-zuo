#include <stdio.h>
#include "CSystem.h"
#include "CDirectX9.h"

/*------------------->

static変数の値を設定

<-------------------*/

CKeyCode::Code_t CKeyCode::m_Code;


CKeyCode::CKeyCode()
{
}


CKeyCode::~CKeyCode()
{
}


short CKeyCode::GetKeyState(short _KeyCode, short _KeyState)
{
	//　キーの情報を取得
	if (GetAsyncKeyState(_KeyCode) & 0x8000)
	{
		//　キーの状態が真でかつ、キーの状態フラグがFREEなら
		if (_KeyState == FREE)		
			return PUSH;			//　PUSHを返す
		else
			return HOLD;			//　HOLDを返す
	}
	else
	{
		//　キーの状態が偽でかつ、キーの状態フラグがHOLDだったら
		if (_KeyState == HOLD)
			return PULL;			//　PULLを返す
		else
			return FREE;			//　FREEを返す
	}
}


void CKeyCode::UpdateKeyState()
{
	//　各キー状態の取得
	m_Code.A			= GetKeyState('A', m_Code.A);
	m_Code.D			= GetKeyState('D', m_Code.D);
	m_Code.S			= GetKeyState('S', m_Code.S);
	m_Code.W			= GetKeyState('W', m_Code.W);
	m_Code.E			= GetKeyState('E', m_Code.E);

	m_Code.Up			= GetKeyState(VK_UP,		m_Code.Up);
	m_Code.Dwon			= GetKeyState(VK_DOWN,		m_Code.Dwon);
	m_Code.Left			= GetKeyState(VK_LEFT,		m_Code.Left);
	m_Code.Right		= GetKeyState(VK_RIGHT,		m_Code.Right);
	m_Code.Escape		= GetKeyState(VK_ESCAPE,	m_Code.Escape);
	m_Code.Space		= GetKeyState(VK_SPACE,		m_Code.Space);
	
	m_Code.MouseLeft	= GetKeyState(VK_LBUTTON,	m_Code.MouseLeft);
	m_Code.MouseRight	= GetKeyState(VK_RBUTTON,	m_Code.MouseRight);

	m_Code.Return		= GetKeyState(VK_RETURN,	m_Code.Return);

	return ;
}


CMousePointer::CMousePointer()
	:
	m_CopyHwnd(nullptr)
{

}


CMousePointer::~CMousePointer()
{
}


void CMousePointer::InitMousePointer()
{
	const long  WIDTH_SIZE  = 640;			//	横サイズ640
	const long  HEIGHT_SIZE = 480;			//	縦サイズ480

	//	センシ値はデフォルトで4.0f
	m_MouseSpeed = DEFAULT_MOUSE_SPEED;	

	//	センサーに格納しておく
	m_MouseSensor = m_MouseSpeed;

	//	マウスポインタの座標を決めておく
	m_BasePointer = { WIDTH_SIZE / 2, HEIGHT_SIZE / 2 };	

	//	マウスポインタをセットする
	ClientToScreen(m_CopyHwnd, &m_BasePointer);
	SetCursorPos(m_BasePointer.x, m_BasePointer.y);

	//  マウスポインタを消しておく
	ShowCursor(FALSE);

	m_Ang.x = 0.0f;
}


void CMousePointer::SetMousePointer()
{
	const float MOUSE_ANGLE_X_MAX  = 40;		//	マウスの向きの限界値
	const float MOUSE_ANGLE_X_DOWN_MAX = 15.0f;
	const float MOUSE_ANGLE_Y_MAX  = 360;
	POINT		MousePointer;				//　マウスポインタの情報
	
	//	現在のマウスポインタの位置を確保しておく
	GetCursorPos(&MousePointer);	

	//	マウスの向きを計算
	m_Ang.x += (MousePointer.y - m_BasePointer.y) / m_MouseSensor; 
	m_Ang.y += (MousePointer.x - m_BasePointer.x) / m_MouseSensor;
	
	//	限界値を超えないようにしておく
	if (m_Ang.x > MOUSE_ANGLE_X_MAX){ m_Ang.x = MOUSE_ANGLE_X_MAX; }
	if (m_Ang.x < -MOUSE_ANGLE_X_DOWN_MAX){ m_Ang.x = -MOUSE_ANGLE_X_DOWN_MAX; }
	if (m_Ang.y > MOUSE_ANGLE_Y_MAX){ m_Ang.y = 0; }
	if (m_Ang.y < -MOUSE_ANGLE_Y_MAX){ m_Ang.y = 0; }
	
	//　マウスポインタセット
	SetCursorPos(m_BasePointer.x, m_BasePointer.y);
}


CFont::CFont()
	:
	m_lpFont(nullptr)
{
}


CFont::~CFont()
{
	SafeRelease(m_lpFont);
}


void CFont::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	const int INIT_WIDTH  = 0;
	const int INIT_HEIGHT = 10;
	const int INIT_WEIGHT = 500;
	const int INIT_LEVEL  = 1;

	D3DXCreateFont(_lpD3DDevice, INIT_WIDTH, INIT_HEIGHT, INIT_WEIGHT, INIT_LEVEL, FALSE, SHIFTJIS_CHARSET, 0, 0, 0, NULL, &m_lpFont);
	m_lpFont->OnResetDevice();
}


void CFont::Draw(FontPos_t _Pos, const char* _Format, float _data, char _Locale...)
{
	RECT Rect = { _Pos.m_Left, _Pos.m_Top, _Pos.m_Right, _Pos.m_Bottom };

	sprintf_s(m_Buffer, sizeof(m_Buffer), _Format, _data);
	m_lpFont->DrawText(NULL, m_Buffer, -1, &Rect, 
		DT_LEFT | DT_NOCLIP, D3DCOLOR_XRGB(255, 255, 255));
}


void CFont::Release()
{
}