#include "CTexture.h"


CTexture::CTexture()
	:
	m_lpSur(nullptr)
	,
	m_lpTex(nullptr)
{
	m_lpTex = nullptr;
	m_lpSur = nullptr;
	ZeroMemory(&m_Info, sizeof(m_Info));
	SetRect(&m_rc, 0, 0, 0, 0);
}


CTexture::~CTexture()
{
	//Release();
}


BOOL CTexture::LoadTexture(LPCTSTR pSrcFile)
{
	
	Release();

	HRESULT hr;
	hr = D3DXCreateTextureFromFile(GET_DIRECT_HELPER.GetDev(), pSrcFile, &m_lpTex);
	if (hr != S_OK)
	{
		//MessageBox(NULL, "テクスチャの読み込みに失敗しました", NULL, MB_OK);
		return FALSE;
	}

	// サーフェイスアドレス取得
	hr = m_lpTex->GetSurfaceLevel(0, &m_lpSur);
	if (hr != D3D_OK)
	{
		MessageBox(NULL, "読み込みに失敗しました", NULL, MB_OK);
		return FALSE;
	}

	D3DXGetImageInfoFromFile(pSrcFile, &m_Info);
	D3DSURFACE_DESC desc;
	m_lpSur->GetDesc(&desc);

	m_Info.MipLevels = m_lpTex->GetLevelCount();
	m_Info.Format = desc.Format;

	SetRect(&m_rc, 0, 0, m_Info.Width, m_Info.Height);

	m_lpTex->GetLevelDesc(0, &desc);

	return TRUE;
}


BOOL CTexture::LoadTextureEx(
	LPCTSTR pSrcFile,
	UINT Width,
	UINT Height,
	UINT MipLevels,
	DWORD Usage,
	D3DFORMAT Format,
	D3DPOOL Pool,
	DWORD Filter,
	DWORD MipFilter,
	D3DCOLOR ColorKey,
	PALETTEENTRY *pPalette
	)
{
	
	//Release();

	m_strName = pSrcFile;

	HRESULT hr;

	// テクスチャ読み込み
	hr = D3DXCreateTextureFromFileEx(GET_DIRECT_HELPER.GetDev(),
		pSrcFile,
		Width,
		Height,
		MipLevels,
		Usage,
		Format,
		Pool,
		Filter,
		MipFilter,
		ColorKey,		//カラーキー ARGBで指定。0で無し。 
		&m_Info,
		pPalette,
		&m_lpTex);
	if (hr != D3D_OK){
		return FALSE;
	}

	// サーフェイスアドレス取得
	hr = m_lpTex->GetSurfaceLevel(0, &m_lpSur);
	if (hr != D3D_OK){
		return FALSE;
	}

	D3DSURFACE_DESC desc;
	m_lpTex->GetLevelDesc(0, &desc);

	SetRect(&m_rc, 0, 0, m_Info.Width, m_Info.Height);


	return TRUE;
}


BOOL CTexture::CreateTexture(
	UINT Width,
	UINT Height,
	UINT MipLevels,
	DWORD Usage,
	D3DFORMAT Format,
	D3DPOOL Pool
	){

	Release();

	HRESULT hr;
	hr = D3DXCreateTexture(GET_DIRECT_HELPER.GetDev(), Width, Height, MipLevels, Usage, Format, Pool, &m_lpTex);
	if (hr != D3D_OK){
		return FALSE;
	}

	// サーフェイスアドレス取得
	hr = m_lpTex->GetSurfaceLevel(0, &m_lpSur);
	if (hr != D3D_OK){
		return FALSE;
	}

	D3DSURFACE_DESC desc;
	m_lpSur->GetDesc(&desc);

	m_Info.Width = desc.Width;
	m_Info.Height = desc.Height;
	m_Info.MipLevels = m_lpTex->GetLevelCount();
	m_Info.Format = desc.Format;

	SetRect(&m_rc, 0, 0, m_Info.Width, m_Info.Height);

	m_lpTex->GetLevelDesc(0, &desc);

	return TRUE;
}


void CTexture::SetTexture(DWORD StageNo)
{
	GET_DIRECT_HELPER.GetDev()->SetTexture(StageNo, m_lpTex);
}


HRESULT CTexture::SetRenderTarget(DWORD Index)
{
	return GET_DIRECT_HELPER.GetDev()->SetRenderTarget(Index, m_lpSur);
}


void CTexture::Release()
{
	SAFE_RELEASE(m_lpSur);// サーフェス解放
	SAFE_RELEASE(m_lpTex);// テクスチャー解放
	m_strName.clear();

	ZeroMemory(&m_Info, sizeof(m_Info));
	SetRect(&m_rc, 0, 0, 0, 0);
}
