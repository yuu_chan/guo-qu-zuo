#include "CMesh.h"
#include "Shader.h"
#include "Game.h"
#include "Texture.h"


CTextureBase::CTextureBase()
	:
	m_pTexture(nullptr)
	,
	m_pBlendTexture(nullptr)
{
	m_pTexture = make_shared<CTexture>();

	m_pBlendTexture = make_shared<CTexture>();

	//m_pTexture = MyNew CTexture();
}


CTextureBase::~CTextureBase()
{
	//SAFE_RELEASE(m_Mesh);
	m_Mesh = nullptr;
	//SAFE_RELEASE(m_pTexture);

	SAFE_RELEASE(m_pBlendTexture);
	//SAFE_DELETE(m_pBlendTexture);
	
	//SAFE_DELETE(m_pTexture);
	m_pBlendTexture = nullptr;
	m_pTexture		= nullptr;


	//SAFE_DELETE(m_pBlendTexture);

}