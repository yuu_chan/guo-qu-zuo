#include "CThread.h"


CThread::CThread()
	:
	m_EndFlag(false)
{

}


CThread::~CThread()
{
	m_EndFlag = false;
}


void CThread::Init()
{
	//m_Thread;
}


void CThread::Update()
{
	//--------------------------------------------------
	//	スレッドのコンストラクタに、直接関数を作っておく
	//--------------------------------------------------


	/*LoadThread(Lambda関数)-------------------------->

		読み込みスレッド開始
		※別スレッドで稼動
		※読み込み関数のみ行う


		[&]		:	全参照キャプチャ
		()		:	引数なし

		return	:	戻り値なし

	<------------------------------------------------*/
	thread LoadThread(
		[&]
		()
		{

			//================
			//	データ読み込み
			//================


			//	エンドフラグをtrueに
			m_EndFlag = true;
		}
	);


	/*DrawThread(Lambda関数)------------------------->
	
		描画スレッド開始
		※別スレッドで稼動
		※描画のみを行う


		[&]		:	全参照キャプチャ
		()		:	引数なし

		return	:	戻り値なし

	<-----------------------------------------------*/
	thread DrawThread(
		[&]
		()
		{
			int cnt = 0;

			while (1)
			{
				//	アルファ値
				float alpha = sinf(D3DXToRadian(cnt * 10));
				alpha = alpha * 0.5f + 0.5f;



				Sleep(32);

				//	ロードスレッドが終了しているなら終わる
				if (m_EndFlag)
				{
					break;
				}

				cnt++;
			}

		}
	);


	//	読み込みスレッドの終了まで待機する
	LoadThread.join();

	//	描画スレッドまで待機する
	DrawThread.join();
}


void CThread::Release()
{
}