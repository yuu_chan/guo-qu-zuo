#include "Texture.h"
#include "CTitleFont.h"


//------------------
//	定数の値を決める
//------------------

//	フォントの横幅
const float CTitleFont::FONT_WIDTH_SIZE  = 640;

//	フォントの縦幅
const float CTitleFont::FONT_HEIGHT_SIZE = 480;


CTitleFont::CTitleFont()
{

}

CTitleFont::~CTitleFont()
{
	m_pTexture->Release();
	m_pTexture = nullptr;
}


//	初期化
void CTitleFont::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//m_pTexture = GET_TEXTURE_LOAD_MANAGER.LoadEx(
	//	"Data/Graphics/Title/Title.png",
	//	(int)FONT_WIDTH_SIZE,
	//	(int)FONT_HEIGHT_SIZE,
	//	true,
	//	255,
	//	255,
	//	255,
	//	255
	//	);

	m_pTexture = make_shared<CTexture>();
	m_pTexture->LoadTextureEx(
		"Data/Graphics/Title/Title.png", 
		static_cast<UINT>(FONT_WIDTH_SIZE), 
		static_cast<UINT>(FONT_HEIGHT_SIZE), 
		1,
		0,
		D3DFMT_UNKNOWN,
		D3DPOOL_MANAGED,
		D3DX_FILTER_NONE,
		D3DX_DEFAULT,
		NULL,
		NULL);
}


//	更新
void CTitleFont::Update()
{
	m_mWorld.CreateMove(0.0f, 0.0f, 0.0f);
}


//	描画
void CTitleFont::Draw(LPDIRECT3DDEVICE9 _lpD3DDevice)
{

	_lpD3DDevice->SetTexture(0, m_pTexture->GetTex());
	
	GET_DIRECT_HELPER.BeginSprite();

	GET_DIRECT_HELPER.DrawSprite(m_pTexture.get(), D3DCOLOR_ARGB(255, 255, 255, 255), &m_mWorld);

	GET_DIRECT_HELPER.EndSprite();
	
	_lpD3DDevice->SetTexture(0, NULL);
}


//	開放
void CTitleFont::Release()
{

}
