#include "Font.h"


CTitleManager::CTitleManager()
{

}


CTitleManager::~CTitleManager()
{
	Release();
}


void CTitleManager::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
}


void CTitleManager::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	最初の位置を指す
	auto itTitleContainer = m_listTitleContainer.begin();

	while (itTitleContainer != m_listTitleContainer.end())
	{
		(*itTitleContainer)->Update();
		++itTitleContainer;
	}

}


void CTitleManager::Draw(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	auto itTitleContainer = m_listTitleContainer.begin();

	while (itTitleContainer != m_listTitleContainer.end())
	{
		(*itTitleContainer)->Draw(_lpD3DDevice);
		++itTitleContainer;
	}

}


void CTitleManager::Release()
{
	auto itTitleContainer = m_listTitleContainer.begin();

	while (itTitleContainer != m_listTitleContainer.end())
	{
		//	イテレータの中にあるvectorの中身を消す
		delete (*itTitleContainer);

		//	消したアドレスにNULLを入れておく
		*itTitleContainer = NULL;

		//	消して、位置をつめる
		itTitleContainer = m_listTitleContainer.erase(itTitleContainer);
	}
}


void CTitleManager::PushTitleManager(CTextureBase* _Object)
{
	if (_Object != NULL)
	{
		m_listTitleContainer.push_back(_Object);
	}
}



CTextureBase* CTitleManager::GetItTitle(int _Id)
{
	auto itTitleContainer = m_listTitleContainer.begin();

	while (itTitleContainer != m_listTitleContainer.end())
	{
		if ((*itTitleContainer)->GetListId() == _Id)
			return (*itTitleContainer);

		++itTitleContainer;
	}

	return 0;
}