#include "Font.h"
#include "CTitleScene.h"
#include "CFade.h"


CTitleScene::CTitleScene()
	:
	m_pTitleWorld(nullptr)
	,
	m_Ang(0.0f)
{

}


CTitleScene::~CTitleScene()
{
	m_pTitleWorld->Release();
	m_pTitleWorld = nullptr;
}


void CTitleScene::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	タイトルワールドをインスタンス化
	m_pTitleWorld = make_shared<CTitleWorld>();

	//	タイトルワールドを初期化
	m_pTitleWorld->Init(_lpD3DDevice);
}


void CTitleScene::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//GET_CAMERA.CameraUpdate(
	//	_lpD3DDevice,
	//	CVector3(0.0f, 0.8f, -2.0f),
	//	NULL,
	//	15,
	//	m_Ang
	//	);

	//m_Ang++;

	//if (m_Ang > 360.0f)
	//{
	//	m_Ang = 0;
	//}

	//	タイトルワールドを更新
	m_pTitleWorld->Update(_lpD3DDevice);
}


void CTitleScene::Draw(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	タイトルワールドを描画
	m_pTitleWorld->Draw(_lpD3DDevice);
}


void CTitleScene::Release()
{
}