#include "Font.h"


CTitleWorld::CTitleWorld()
{

}


CTitleWorld::~CTitleWorld()
{
	m_TitleManager = nullptr;
}


void CTitleWorld::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//--------------------------------------
	//	エフェクトマネージャをインスタンス化
	//--------------------------------------
	//m_TitleManager = MyNew CTitleManager();
	m_TitleManager = make_shared<CTitleManager>();


	//--------
	//	初期化
	//--------
	m_TitleManager->Init(_lpD3DDevice);


	//	クリック文字をインスタンス化
	CClickFont* pClick = m_TitleManager->CreateTitleTask<CClickFont>();

	//	タイトル文字をインスタンス化
	CTitleFont* pTitle = m_TitleManager->CreateTitleTask<CTitleFont>();


	pTitle->Init(_lpD3DDevice);
	pClick->Init(_lpD3DDevice);
}


void CTitleWorld::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	m_TitleManager->Update(_lpD3DDevice);
}


void CTitleWorld::Draw(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	m_TitleManager->Draw(_lpD3DDevice);
}


void CTitleWorld::Release()
{
	m_TitleManager->Release();
}
