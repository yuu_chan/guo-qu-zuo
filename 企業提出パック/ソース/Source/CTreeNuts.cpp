#include "Game.h"
#include "CTreeNuts.h"


//---------------------------------->
//	static const 値を初期化
//<----------------------------------

//	拡大行列の最大値を決める
const float CTreeNuts::SCALE_MAX_SIZE	= 1.0f;

//	拡大行列の拡大する量を決める
const float CTreeNuts::SCALE_AMOUNT		= 0.01f;


CTreeNuts::CTreeNuts()
{

}

CTreeNuts::~CTreeNuts()
{
	
}

void CTreeNuts::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	//	魚の餌を読み込み
	m_Mesh = GET_XFILE_LOAD_MANAGER.Load("Data/XFile/Object/Fruit.x", _lpD3DDevice);

	//	座標を記憶用変数に保存
	m_vMemoryPos = m_vPos;

	//	落下処理を行わないのでfalseを代入
	m_EnableFall = false;

	//	ステータス初期化
	m_State = 10;

	m_GroundAdjustment = 2.0f;
}

void CTreeNuts::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{

	//	落下処理は、状態によって行うので変数にしておく
	ObjectMove(m_EnableFall, true);


	//------------------------
	//	ステップ毎に処理をする
	//------------------------

	//	ステップ用に列挙体を生成
	enum STEP
	{
		INIT			= 10,//= 0x0010,	//	初期化
		GROW			= 11,//= 0x0020,	//	育つ
		FERMENT			= 12,//= 0x0040,	//	熟成
		FALL			= 13,//= 0x0080,	//	落ちる
		SOMEONE_HAS_EAT	= 14,	//	魚が食べた
		FELL_IN_WATER   = 15,	//	水中に落ちた場合
	};

	//-------------------------------------------------------
	//	STEP1	:	ステータスや座標などを初期化しておく
	//
	//	STEP2	:	木の実が育つ
	//
	//	STEP3	:	熟成したら、落下の準備(揺れたり)
	//
	//	STEP4	:	熟成した木の実が落下する
	//
	//	分岐ステップ
	//	STEP5A	:	誰かが食べた場合一定時間たつとSTEP2へ戻る
	//
	//	STEP5B	:	誰も食べずに木の実が(水中に)落ちた場合
	//-------------------------------------------------------

	//	STEP1
	if (m_State == STEP::INIT)
	{
		//	落下処理を行うひつようが無いので、変数にfalseを代入
		m_EnableFall = false;

		//	座標を記憶した座標にコピー
		m_vPos = m_vMemoryPos;

		//	拡大行列を0.0fに変更
		m_vScalSize = CVector3(0.0f, 0.0f, 0.0f);

		//	次のステップへ行く
		m_State = STEP::GROW;
	}
	//	STEP2
	else if (m_State == STEP::GROW)
	{
		//	拡大行列を、マックスまで加算する
		m_vScalSize += CVector3(SCALE_AMOUNT, SCALE_AMOUNT, SCALE_AMOUNT);

		//	どこでもいいので、サイズが最大値を超えたら
		if (m_vScalSize.x > SCALE_MAX_SIZE)
		{
			//	最大サイズで止める
			m_vScalSize = CVector3(SCALE_MAX_SIZE, SCALE_MAX_SIZE, SCALE_MAX_SIZE);

			//	次のステップへ
			m_State = STEP::FERMENT;
		}
	}
	//	STEP3
	else if (m_State == STEP::FERMENT)
	{
		//	フレームカウントが100で割り切れたら
		if (APP.m_FrameCnt % 500 == 0)
		{
			//	次のステップへ
			m_State = STEP::FALL;
		}
	}
	//	STEP4
	else if (m_State == STEP::FALL)
	{
		//	落下処理を行うために落下処理を制御していた変数をtrueに
		m_EnableFall = true;

		////	吸い込み処理を行えるように
		//m_State = 0;


		//------------------------------------------------------
		//	水との当たり判定を行い、水中にいるかどうかを判断する
		//------------------------------------------------------
		bool WaterIn;		//	true(水中) false(水中にはいない)

		//	リストの先頭から後尾まで回す
		for (auto i = GET_WORLD.GetGameWorld()->GetBegin() + 1; i < GET_WORLD.GetGameWorld()->GetEnd(); i++)
		{
			float UnderWater;

			//	上方向にレイを発射して、水中にいるかどうかを判断する
			WaterIn = GET_HELPER.UnderDecision(m_Id, i, UnderWater, CVector3(0, -1, 0), CVector3(0.0f, 3.0f, 0.0f));
		}

		//	水中にいることがわかったら
		if (WaterIn == true)
		{
			//	水中にいる状態へ
			m_State = STEP::FELL_IN_WATER;
			//m_EnableFall = false;
		}
	}
	//	STEP5A(CFish内にてステータス変更)
	else if (m_State == STEP::SOMEONE_HAS_EAT)
	{
		//	落下処理を行うひつようが無いので、変数にfalseを代入
		m_EnableFall = false;

		//	座標を記憶した座標にコピー
		m_vPos = m_vMemoryPos;

		//	拡大行列を0.0fに変更
		m_vScalSize = CVector3(0.0f, 0.0f, 0.0f);

		//	一定時間たつと
		if (APP.m_FrameCnt % 100 == 0)
		{
			//	初期化させる
			m_State = STEP::GROW;
		}
	}
	//	STEP5B
	else if (m_State == STEP::FELL_IN_WATER)
	{
		//m_Accel = 0.0f;
	}

	
	//	拡大サイズをセット
	m_mWorld.CreateScale(&m_vScalSize);

	//	移動座標をセット
	m_mWorld.Move(&m_vPos);

}

void CTreeNuts::Release()
{

}