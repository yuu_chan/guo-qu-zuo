#include "CVector3.h"

/*==============================
	VECTOR3型サポートクラス
==============================*/

CVector3::CVector3(){
	x = 0;
	y = 0;
	z = 0;
}

CVector3::CVector3(float _x, float _y, float _z)
{
	x = _x;
	y = _y;
	z = _z;
}

CVector3::CVector3(const D3DXVECTOR3& _In)
{
	x = _In.x;
	y = _In.y;
	z = _In.z;
}

CVector3::~CVector3()
{

}

/*===========================================
		キャラクターが決められた向きに
		向きながら移動する

		vTargetDir	->	ターゲットの方向
		MaxAng		->  角度回転量
===========================================*/
void CVector3::Homing(
	const CVector3* _vTargetDir, 
	float _MaxAng)
{
	// 方向
	CVector3 vWay(x, y, z);
	vWay.Normalize();
	// 敵への方向
	CVector3 vTar = *_vTargetDir;
	vTar.Normalize();


	// 内積で角度を求める
	float dot = CVector3::Dot2(&vWay, &vTar);

	if (dot < 1.0f)
	{
		float Deg;
		CVector3 crs;
		// 正反対のときは適当に曲げる
		if (dot == -1.0f)
		{
			crs.Set(0, 1, 0);
			Deg = _MaxAng;
		}
		else
		{
			// 角度制限
			Deg = D3DXToDegree(acos(dot));
			if (Deg > _MaxAng)
				Deg = _MaxAng;

			// 外積
			CVector3::Cross(&crs, &vWay, &vTar);
			// 正規化
			crs.Normalize();
			if (crs.Length() == 0){
				crs.y = 1;
			}
		}

		// 回転行列作成
		D3DXMATRIX mRota;
		D3DXMatrixRotationAxis(&mRota, &crs, ToRadian(Deg));

		TransformNormal(&mRota);
	}
}