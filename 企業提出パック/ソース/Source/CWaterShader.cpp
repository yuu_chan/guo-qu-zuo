#include "Shader.h"
#include "CWaterShader.h"
#include "Rand.h"


//	ランダム管理
CRand Rand;


/*---------------------------------------------->

レンダーターゲットをセットする関数

<----------------------------------------------*/
static void SetRenderTarget(CTexture _Texture, LPDIRECT3DSURFACE9 _lpDepthStencil)
{
	LPDIRECT3DSURFACE9 NowRT;

	if (SUCCEEDED(_Texture.GetTex()->GetSurfaceLevel(0, &NowRT)))
	{
		//	現在のRTの情報をセットする
		GET_DIRECT_HELPER.GetDev()->SetRenderTarget(0, NowRT);

		//	現在のRTを開放
		NowRT->Release();

		//	Zバッファを情報をセットする
		GET_DIRECT_HELPER.GetDev()->SetDepthStencilSurface(_lpDepthStencil);
	}
}


/*------------------------------>

RTの初期化

<------------------------------*/
static void ResetRenderTarget(LPDIRECT3DSURFACE9 _lpRenderTarget, LPDIRECT3DSURFACE9 _lpDepthStencil)
{
	//	RTを取得
	GET_DIRECT_HELPER.GetDev()->SetRenderTarget(0, _lpRenderTarget);

	//	Zバッファを取得
	GET_DIRECT_HELPER.GetDev()->SetDepthStencilSurface(_lpDepthStencil);
}


CWaterShader::CWaterShader()
{
	//	Heightマップ初期化
	for (auto i = 0; i < 2; i++)
	{
		m_HeightTex[i].Release();
	}

	//	法線マップ初期化
	m_NormalTex.Release();


	m_vLightWVPos = D3DXVECTOR4(0.0f, 0.0f, 0.0f, 1.0f);

	m_vLightColor = D3DXVECTOR4(0.0f, 0.0f, 0.0f, 1.0f);

	//m_vFrom = CVector3(0.0f, 0.0f, 0.0f);

	//m_vLookAt = CVector3(0.0f, 0.0f, 0.0f);

	//SAFE_RELEASE(m_lpRenderTarget);

	//SAFE_RELEASE(m_lpDepthStencil);

	m_Time = 0;

	m_TextureTime = 0;

	m_Mode = 1;
}


CWaterShader::~CWaterShader()
{
	//	デバイスの中にある必要の無いRTを開放させておく
	OnLostDevice();
}


void CWaterShader::Init()
{
	//----------------------------
	//	エフェクトファイルのロード
	//----------------------------

	//	エラーチェック用の変数
	LPD3DXBUFFER pErr = nullptr;
	HRESULT		 hr;

	//	ファイルをロードする
	hr = D3DXCreateEffectFromFile(
		GET_DIRECT_HELPER.GetDev(),
		"Shader/Water.fx",
		NULL,
		NULL,
		0,
		NULL,
		&m_Effect,
		&pErr);

	//	ファイルがきちんと入っているかどうかをチェック
	if (hr != D3D_OK)
	{
		//	無かったらreturnさせる
		MessageBox(NULL, (LPCSTR)pErr->GetBufferPointer(), "シェーダコンパイルエラー\nファイルが見つかりません", MB_OK);
		return;
	}


	m_TexDot.CreateTexture(1, 1, 1, 0, D3DFMT_A8R8G8B8, D3DPOOL_MANAGED);
	m_TexDot.Fill<UINT>(0xFFFFFFFF);

	OnResetDevice();

	m_Effect->SetFloat("AddWaveVelocity", 100.0f);
}


void CWaterShader::Release()
{

}



/*------------------------------------------------------------------->

	描画では、波の動きを更新させる

	�C	新しい波を発生させるためにパラメータを設定する。

	�D	高度マップを更新する。
		前回に作成したテクスチャを入力用としてシェーダーに設定する。

	�E	法線マップを更新する

	�F	波の描画を行う

<-------------------------------------------------------------------*/
void CWaterShader::DrawMesh(shared_ptr<CMeshObject> _pMesh, int _Pass)
{

	//--------------
	//	波を描画する
	//--------------

	DefaultSetState();


	//ライト情報ゲット
	D3DLIGHT9 Light = GET_DIRECT_HELPER.GetLight(1);

	CVector3 LightDir = Light.Direction;

	SetLightDir(&LightDir);

	//	テクニックの選択
	m_Effect->SetTechnique("Wave");

	m_Effect->SetVector("WaveDiffuse", &D3DXVECTOR4(0.0f, 0.0f, 1.0f, 0.7f));
	
	m_Effect->SetVector("WaveSpecular", &D3DXVECTOR4(0.0f, 0.0f, 1.0f, 1.0f));

	m_Effect->SetFloat("WavePower", 100.0f);

	m_Effect->SetTexture("NormalTex", m_NormalTex.GetTex());

	m_Effect->SetMatrix("WView", &GET_CAMERA.GetViewMatrix());

	SetRefract(_pMesh->GetMeshState()->m_Refract);
	SetClip(_pMesh->GetMeshState()->m_Clip);


	CMatrix mVP, mV, mP;

	mV = GET_CAMERA.GetViewMatrix();

	mP = GET_CAMERA.GetProjMatrix();

	mVP = mV * mP;

	m_Effect->SetMatrix("WVProj", &mVP);

	CVector3 mLightWVPos = GET_CAMERA.GetCameraWorldPosition();

	m_Effect->SetVector("LightWVPos", &D3DXVECTOR4(mLightWVPos, 1.0f));

	m_Effect->SetVector("LightColor", &D3DXVECTOR4(Light.Diffuse.r, Light.Diffuse.g, Light.Diffuse.b, Light.Diffuse.a));//D3DXVECTOR4(0.0f, 0.0f, 1.0f, 1.0f));


	//	スクロール量
	m_Effect->SetVector("ScrollUV", &D3DXVECTOR4(m_vUVScroll, 1.0f));

	//	テクニック開始
	m_Effect->Begin(0, 0);

	//	パスを設定
	m_Effect->BeginPass(0);

	//	マテリアルの数だけループさせておく
	for (int i = 0; i < _pMesh->GetMaterialCnt(); i++)
	{

		//	マテリアルとテクスチャーを作っておく
		//D3DMATERIAL9*				pMat = &_pMesh->GetMaterials()[i];
		D3DMATERIAL9*				pMat = &_pMesh->GetMaterials()[i].m_Material;
		//CTexture*					pTex = &_pMesh->GetTextures()[i];
		shared_ptr<CTexture>		pTex = _pMesh->GetMaterials()[i].m_spMeshTex;
		shared_ptr<CMeshState>		pMeshState = _pMesh->GetMeshState();

		//	ディフューズを設定する
		D3DXVECTOR4 vDif;
		vDif.x = pMat->Diffuse.r * Light.Diffuse.r;
		vDif.y = pMat->Diffuse.g * Light.Diffuse.g;
		vDif.z = pMat->Diffuse.b * Light.Diffuse.b;
		vDif.w = pMat->Diffuse.a * Light.Diffuse.a;

		m_Effect->SetVector("Diffuse", &vDif);


		//	自己発行力を渡す
		m_Effect->SetFloat("EmissivePow", pMeshState->m_EmissivePower);
		m_Effect->SetValue("Emissive", &pMat->Emissive, 16);


		//	スペキュラを設定する
		D3DXVECTOR4 vSpe;
		vSpe.x = pMat->Specular.r * Light.Specular.r;
		vSpe.y = pMat->Specular.g * Light.Specular.g;
		vSpe.z = pMat->Specular.b * Light.Specular.b;
		vSpe.w = pMat->Specular.a * Light.Specular.a;

		// 環境光
		m_Effect->SetVector("Ambient", &D3DXVECTOR4(Light.Ambient.r, Light.Ambient.g, Light.Ambient.b, 1));

		m_Effect->SetVector("Specular", &vSpe);

		m_Effect->SetFloat("Power", pMat->Power);

		if (pTex->GetTex())
		{
			m_Effect->SetTexture("MeshTex", pTex->GetTex());
		}
		else
		{
			m_Effect->SetTexture("MeshTex", m_TexDot.GetTex());
		}

		// ※これを呼ばないと、シェーダ側にうまくパラメータがセットされないよ！
		m_Effect->CommitChanges();

		// メッシュを描画
		_pMesh->GetMesh()->DrawSubset(i);
	}

	//	パスを終了
	m_Effect->EndPass();

	//	テクニック終了
	m_Effect->End();

}


void CWaterShader::OnLostDevice()
{
	//m_lpRenderTarget->Release();
	//m_lpDepthStencil->Release();

	for (auto i = 0; i < 2; i++)
	{
		m_HeightTex[i].Release();
	}

	m_NormalTex.Release();
}


void CWaterShader::TexDraw()
{	

	GET_DIRECT_HELPER.AlphaBlendEnable(false);

	//	描画位置
	CMatrix m;

	//	画像サイズ制御
	const auto SIZE = 150.0f;

	//	描画位置初期化
	m.CreateMove(0, 0, 0);

	//if (GetAsyncKeyState('3'))
	//{
	///*	m_Effect->SetTexture("HeightTex", m_HeightTex[0].GetTex());

	//	m_Effect->SetTechnique("ShowHeight");

	//	m_Effect->Begin(0, 0);

	//	m_Effect->BeginPass(0); 
	//	*/
	//	m.Scale_Local(
	//		SIZE / m_HeightTex[0].GetInfo()->Width,
	//		SIZE / m_HeightTex[0].GetInfo()->Height,
	//		1);

	//	GET_DIRECT_HELPER.DrawSprite(
	//		&m_HeightTex[0],
	//		D3DCOLOR_ARGB(255, 255, 0, 0),
	//		&m);

	//	//m_Effect->EndPass();

	//	//m_Effect->End();
	//}
	//if (GetAsyncKeyState('4'))
	//{
	//	m_Effect->SetTexture("HeightTex", m_HeightTex[1].GetTex());

	//	m_Effect->SetTechnique("ShowVelocity");

	//	m_Effect->Begin(0, 0);

	//	m_Effect->BeginPass(0);

	//	m.Scale_Local(
	//		SIZE / m_HeightTex[1].GetInfo()->Width,
	//		SIZE / m_HeightTex[1].GetInfo()->Height,
	//		1);

	//	GET_DIRECT_HELPER.DrawSprite(
	//		&m_HeightTex[0],
	//		D3DCOLOR_ARGB(255, 255, 0, 0),
	//		&m);

	//	m_Effect->EndPass();

	//	m_Effect->End();
	//}
	//if (GetAsyncKeyState('5'))
	//{
	//	m_Effect->SetTexture("NormalTex", m_NormalTex.GetTex());

	//	m.Scale_Local(
	//		SIZE / m_NormalTex.GetInfo()->Width,
	//		SIZE / m_NormalTex.GetInfo()->Height,
	//		1);

	//	GET_DIRECT_HELPER.DrawSprite(
	//		&m_NormalTex,
	//		D3DCOLOR_ARGB(255, 255, 255, 255),
	//		&m);
	//}
}


/*--------------------------------------------------------------------->

	波にしようする高さ、速度、法線マップを作成する
	D3DXFMT_G16R16Fを使用
	RとGの輝度として格納する

	�@	高度(Height)マップを作成する

	�A	法線マップを作成する

	�B	高度マップと法線マップをクリアする
		波の発生していない状態から始めるため、わざとクリアさせておく
		波が発生している状態からはじめる場合は、高度マップにあらかじめ
		波の形を描画しておく

<---------------------------------------------------------------------*/
void CWaterShader::OnResetDevice()
{
	//----------------------------------------------
	//	ハイトマップを作る前にデバイスをリセットする
	//----------------------------------------------

	//	デバイスをリセット
	m_Effect->OnResetDevice();

	GET_DIRECT_HELPER.GetDev()->GetRenderTargetData(0, GET_DIRECT_HELPER.m_OrgRenderTarget);
	GET_DIRECT_HELPER.GetDev()->GetDepthStencilSurface(&GET_DIRECT_HELPER.m_OrgDepthStencil);
	

	//--------------------------
	//	�@ハイトマップを作成する
	//--------------------------
	for (auto i = 0; i < 2; i++)
	{
		//	テクスチャーの情報が入っていうるかどうかを調べる
		if (!m_HeightTex[i].GetTex())
		{
			const auto TEXTURE_SIZE = 256;
		
			//	ハイトマップを生成する
			m_HeightTex[i].CreateTexture(
				TEXTURE_SIZE,
				TEXTURE_SIZE,
				1,
				D3DUSAGE_RENDERTARGET,
				D3DFMT_G16R16F,
				D3DPOOL_DEFAULT
				);
		}
	}


	//--------------------
	//	�A深度マップを生成
	//--------------------

	//	法線マップの情報が入っているかどうかを調べる
	if (!m_NormalTex.GetTex())
	{
		const	auto TEXTURE_SIZE = 256;

		//	法線マップを生成する
		m_NormalTex.CreateTexture(
			TEXTURE_SIZE,
			TEXTURE_SIZE,
			1,
			D3DUSAGE_RENDERTARGET,
			D3DFMT_G16R16F,
			D3DPOOL_DEFAULT
			);
	}

	
	//--------------------------------------
	//	�B高度マップと法線マップをクリアする
	//--------------------------------------
	

	//--------------------
	//	RTにマップをセット
	
	//	ハイトマップ
	for (auto i = 0; i < 2; i++)
	{
		//	テクスチャ情報が格納されていた場合
		if (m_HeightTex[i].GetTex())
		{
			//	RTを切り替える
			SetRenderTarget(m_HeightTex[i], NULL);

			//	デバイスをクリア
			GET_DIRECT_HELPER.Clear(0, NULL, D3DCLEAR_TARGET, 0, 1, 0);
		}
	}

	//	法線マップ
	if (m_NormalTex.GetTex())
	{
		SetRenderTarget(m_NormalTex, NULL);
		GET_DIRECT_HELPER.Clear(0, NULL, D3DCLEAR_TARGET, 0, 1, 0);
	}

	//	ここまで
	//--------------------


	//	RTをリセット
	GET_DIRECT_HELPER.ResetDepthStencil();
	GET_DIRECT_HELPER.ResetRenderTarget();
	//ResetRenderTarget(m_lpRenderTarget, m_lpDepthStencil);
}


void CWaterShader::Update()
{
	
}


void CWaterShader::UpdateMap()
{
	//	テクスチャーサイズ
	const auto TEXTURE_SIZE = 256;

	//	タイマー加算
	m_Time++;


	//----------------
	//	波の動きを行う
	//----------------

	m_TextureTime = m_Time;


	//----------------
	//	新しい波の生成
	//----------------
	
	//	方向ベクトルをセット
	//m_Effect->SetVector(
	//	"AddWavePos", &D3DXVECTOR4(Rand.Real1(), Rand.Real1(), 0.0f, 0.0f));

	m_Effect->SetVector(
		"AddWavePos", 
		&D3DXVECTOR4(
		static_cast<float>(Rand.Real1()),
		static_cast<float>(Rand.Real1()),
		0.0f, 
		0.0f
		));


	//	0.05fがきれいに見える
	m_Effect->SetFloat("AddWaveRad", static_cast<float>(Rand.Real1()) * 0.01f);

	//	-0.5f
	m_Effect->SetFloat("AddWaveVelocity", static_cast<float>(Rand.Real1()) - 0.5f);


	//------------------
	//	高度マップの更新
	//------------------

	//	RTに高度マップをセット(前の高度マップをはる
	SetRenderTarget(m_HeightTex[m_TextureTime % 2], NULL);

	//	セットした高度マップの配列と逆側を見る(新しい高度マップをはる
	m_Effect->SetTexture("HeightTex", m_HeightTex[1 - m_TextureTime % 2].GetTex());

	//	テクニック選択
	m_Effect->SetTechnique("Height");

	//	波の高さ	0.08fがいいかんじかも？
	m_Effect->SetFloat("Spring", 0.04f);

	//	テクニック開始
	m_Effect->Begin(0, 0);

	//	パス設定
	m_Effect->BeginPass(0);

	//	ポリゴン描画
	GET_DIRECT_HELPER.DrawQuad(
		0,
		0,
		(float)TEXTURE_SIZE,
		(float)TEXTURE_SIZE
		);

	//	パス終了
	m_Effect->EndPass();

	//	テクニック終了
	m_Effect->End();


	//------------------
	//	法線マップの更新
	//------------------

	//	RTに法線マップを貼り付ける
	SetRenderTarget(m_NormalTex, NULL);

	m_Effect->SetTexture("HeightTex", m_HeightTex[m_TextureTime % 2].GetTex());

	//	テクニック選択
	m_Effect->SetTechnique("Normal");

	//	テクニック開始
	m_Effect->Begin(0, 0);

	//	パス設定
	m_Effect->BeginPass(0);

	//	ポリゴン描画
	GET_DIRECT_HELPER.DrawQuad(
		0,
		0,
		(float)TEXTURE_SIZE,
		(float)TEXTURE_SIZE
		);

	//	パス終了
	m_Effect->EndPass();

	//	テクニック終了
	m_Effect->End();

	GET_DIRECT_HELPER.ResetDepthStencil();
	GET_DIRECT_HELPER.ResetRenderTarget();
}