#include "Game.h"


CWindHole::CWindHole()
{

}


CWindHole::~CWindHole()
{

}


void CWindHole::Init(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	m_Mesh = GET_XFILE_LOAD_MANAGER.Load("Data/XFile/Stage/WindHole.x", _lpD3DDevice);

	//m_mWorld.CreateMove(&m_vPos);

	m_spRigidBox = make_shared<CBulletObj_Box>();

	CRigidSupervise::CreateRigidBox(m_spRigidBox, m_Mesh, m_vPos, m_Id, m_Mesh->GetBounding()->GetBoundingBoxMax() / 2.0f, 0.0f);

	m_spRigidBox->SetUserPointer(this);

	m_mWorld.CreateMove(&m_vPos);
}


void CWindHole::Update(LPDIRECT3DDEVICE9 _lpD3DDevice)
{
	m_spRigidBox->SetMatrix(m_mWorld);
}


void CWindHole::Release()
{

}

