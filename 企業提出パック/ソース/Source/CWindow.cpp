#include "CDirectX9.h"
#include "CGameScene.h"
#include "Game.h"
#include "CCamera.h"
#include "Font.h"
#include "CWindow.h"
#include "CSceneManager.h"


/*-------------------->

static変数の値を設定

<--------------------*/

//	横
const int CWindow::WINDOW_WIDTH_SIZE  = 640;

//	縦
const int CWindow::WINDOW_HEIGHT_SIZE = 480;


CWindow::CWindow()
	:
	m_hWnd(nullptr),
	m_hInst(nullptr)
{
	//--------------------
	//	初期化を行っておく
	//--------------------
	ZeroMemory(m_DefDir, sizeof(m_DefDir));

	m_FullScreen	= FALSE;
	m_CloseFlag		= false;
	m_bEndFlag		= false;
	m_Width			= 0;
	m_Height		= 0;
	m_FrameCnt		= 0;


	//	現在のシーンを初期化
	m_NowScene = 0;
}


CWindow::~CWindow()
{

}


BOOL CWindow::InitWindow(
	HINSTANCE	_hInstance, 
	int			_Width, 
	int			_Height, 
	BOOL		_bFullScreen)
{

	// カレントディレクトリ取得
	GetCurrentDirectory(256, m_DefDir);
	m_hInst = _hInstance;

	//===============================
	// メインウィンドウ作成
	//===============================
	//ウィンドウクラスの定義
	WNDCLASSEX wc;												//ウィンドウクラスの定義用
	wc.cbSize			= sizeof(WNDCLASSEX);					//構造体のサイズ
	wc.style			= 0;									//スタイル
	wc.lpfnWndProc		= &CWindow::callWindowProc;				//ウインドウ関数
	wc.cbClsExtra		= 0;									//エキストラクラス情報 
	wc.cbWndExtra		= 0;									//エキストラウィンドウ情報
	wc.hInstance		= m_hInst;								//インスタンスハンドル
	wc.hIcon			= LoadIcon(NULL, IDI_APPLICATION);		//ラージアイコン
	wc.hIconSm			= LoadIcon(NULL, IDI_APPLICATION);		//スモールアイコン 
	wc.hCursor			= LoadCursor(NULL, IDC_ARROW);			//マウスカーソル
	wc.hbrBackground	= (HBRUSH)GetStockObject(BLACK_BRUSH);	//背景色 
	wc.lpszMenuName		= NULL;									//メインメニュー名
	wc.lpszClassName	= "Window";								//ウィンドウクラス名

	//ウィンドウクラスの登録
	if (!RegisterClassEx(&wc))
		return 0;

	//ウィンドウの作成
	m_hWnd = CreateWindow(
		"Window",				//作成するウィンドウ名
		"Hobit Is Eat",			//ウィンドウのタイトル
		WS_OVERLAPPEDWINDOW,	//ウィンドウタイプを標準タイプに	
		CW_USEDEFAULT,			//ウィンドウの位置（Ｘ座標）
		CW_USEDEFAULT,			//ウィンドウの位置（Ｙ座標）						
		WINDOW_WIDTH_SIZE,		//ウィンドウの幅
		WINDOW_HEIGHT_SIZE,		//ウィンドウの高さ			
		NULL,					//親ウィンドウのハンドル
		NULL,					//メニューのハンドル
		m_hInst,				//インスタンスハンドル 
		NULL);					//追加情報

	//ウィンドウの表示&更新
	SetClientSize(m_hWnd, _Width, _Height);
	timeBeginPeriod(1);

	//ウィンドウの表示
	ShowWindow(m_hWnd, SW_SHOW);
	
	//ウィンドウの更新
	UpdateWindow(m_hWnd);

	//	フラグを初期化
	m_CloseFlag = false;
	m_bEndFlag  = false;

	//	TRUEを返す
	return TRUE;
}


int CWindow::Loop()
{

	//----------
	//	変数宣言
	//----------

	//	メッセージ構造体
	MSG				  msg;

	//	シーンクラス
	CSceneManager*	  pScene	= MyNew CSceneManager();

	//	Fps計算用
	CFps*			  time		= MyNew CFps();

	//	FPS制御用現在時刻を覚える
	DWORD			  prevTime	= timeGetTime();

	//	デバイスを保持しておくために使用する変数
	LPDIRECT3DDEVICE9 lpD3DDevice;


	//---------------
	// Direct3D初期化
	//---------------

	//	DirectXを初期化
	if (GET_DIRECT_HELPER.Init(m_hWnd, m_Width, m_Height, D3DFMT_X8R8G8B8, FALSE) == FALSE)
	{
		//	失敗したらそのまま終了させる
		MessageBox(m_hWnd, "Direct3D初期化ミス", "", MB_OK);
		return FALSE;
	}

	// 基本的なレンダーステート設定
	GET_DIRECT_HELPER.SetState();

	//	HWNDコピー
	GET_MOUSE.SetHwnd(GetHwnd());

	//	デバイスコピー
	lpD3DDevice = GET_DIRECT_HELPER.GetDev();

	//	フォントを初期化
	GET_FONT.Init(lpD3DDevice);

	//	シーン初期化
	pScene->Init(lpD3DDevice);

	//	物理ワールドを初期化
	GET_BULLET_WORLD.m_pBulletWorld.Init();

	//	サウンドクラスにHwndを送る
	SOUNDPLAYER.SetHwnd(GetHwnd());

	//	サウンドクラスを初期化
	SOUNDPLAYER.Init();

	//	ゲームで使用するBGMをロード
	SOUNDPLAYER.Load(0, "Data/Sound/BGM/BGM1.wav");

	//	タイトルとセレクトで使用するBGMをロード
	SOUNDPLAYER.Load(1, "Data/Sound/BGM/BGM2.wav");

	//	SEをロード
	SOUNDPLAYER.Load(6, "Data/Sound/SE/Click.wav");

	SOUNDPLAYER.Load(5, "Data/Sound/SE/bofu.wav");

	SOUNDPLAYER.Load(2, "Data/Sound/SE/jump12.wav");

	SOUNDPLAYER.Load(4, "Data/Sound/SE/Nomikomu.wav");

	SOUNDPLAYER.Load(3, "Data/Sound/SE/Walk.wav");

	//	シェーダーの初期化をする
	GET_SHADER.Init(lpD3DDevice);



	//------------------
	//	メッセージループ
	//------------------
	while (1)
	{

		//-----------------------------
		//  Escape押したら終了フラグON。
		//-----------------------------
		if (m_CloseFlag)
		{
			//	終了フラグON
			m_bEndFlag = true;
			DestroyWindow(m_hWnd);
		}

		//--------------------------------------------
		//  終了フラグが立っているなら、ループから出る
		//--------------------------------------------
		if (m_bEndFlag)
		{
			//	メッセージループを抜ける
			break; 
		}

		//----------------
		//  メッセージ取得
		//----------------
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			//----------------
			//	メッセージ処理
			//----------------
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		//------------
		//  ゲーム処理
		//------------
		else
		{
			
			//---------
			//  FPS計算
			//---------


			//-----------
			//	60FPS制御
			//-----------
			
			//// スレッドスリープ
			DWORD newTime	= timeGetTime();
			DWORD fps		= time->FpsProc();
			if (newTime - prevTime < 16)
			{
				Sleep(newTime - prevTime);
			}
			else
			{
				prevTime = newTime;
			}

			if (GetAsyncKeyState('X'))
			{
				APP.m_bEndFlag = true;
			}


			//------------
			//  処理・描画
			//------------

			// 描画開始
			GET_DIRECT_HELPER.GetDev()->BeginScene();

			// 画面クリア
			GET_DIRECT_HELPER.Clear(true, true, true, D3DCOLOR_XRGB(100, 100, 255), 1, 0);

			//	デバイスコピー
			lpD3DDevice = GET_DIRECT_HELPER.GetDev();

			//	物理ワールド更新
			GET_BULLET_WORLD.m_pBulletWorld.StepSimulation(1.0f / 60, 1);

			//	キー情報更新
			GET_KEY.UpdateKeyState();

			//	シーン管理
			pScene->Proc(lpD3DDevice);

			//	サウンド情報更新
			//lpSoundBgm->Proc();	
			
			{
			//CFont::FontPos_t Pos{ 10, 10, 200, 200 };
			//GET_FONT.Draw(Pos, "\n\n\n\n\n\n\n\n\n\n\nFPS = %f", (float)fps, 0);
			}


			//	描画終了
			GET_DIRECT_HELPER.GetDev()->EndScene();

			//	画面描画
			GET_DIRECT_HELPER.GetDev()->Present(NULL, NULL, NULL, NULL);

			//	フレームカウンターを加算
			m_FrameCnt++;
		}
	}


	//----------
	//	開放処理
	//----------

	//	timeポインタをデリート
	Delete(time);

	//	シーン全体をデリート
	Delete(pScene);

	//	XFileマネージャーをクリア
	GET_XFILE_LOAD_MANAGER.AllClear();

	//	Textureマネージャーをクリア
	GET_TEXTURE_LOAD_MANAGER.AllClear();

	//	サウンドを開放
	SOUNDPLAYER.Release();


	// プログラム終了
	return 0;
}


LRESULT CALLBACK CWindow::callWindowProc(HWND _hWnd, UINT _Message, WPARAM _wParam, LPARAM _lParam)
{
	return APP.WindowProc(_hWnd, _Message, _wParam, _lParam);
}


LRESULT CWindow::WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	
	//------------------------------
	//	メッセージによって処理を選択
	//------------------------------

	switch (message){

		//	ウィンドウのサイズ
		case WM_SIZE:
		{

			//	作業用変数
			RECT rcWnd, rcCli;

			// ウィンドウのRECT取得
			GetWindowRect(hWnd, &rcWnd);

			// クライアント領域のRECT取得
			GetClientRect(hWnd, &rcCli);

			m_Width = rcCli.right;
			
			m_Height = rcCli.bottom;
		}

		break;

		// ウィンドウを閉じた時
		case WM_CLOSE:

			//	クローズ用の変数をtrueへ
			m_CloseFlag = true;

			break;

		//	指定のキーが押されたら
		case WM_KEYDOWN:

			////ESCでプログラム終了
			//if (GetAsyncKeyState(VK_ESCAPE) & 0x8000)
			//{
			//	//  ウィンドウを破棄
			//	DestroyWindow(m_hWnd);

			//	//	クローズ用の変数をtrueへ
			//	m_CloseFlag = true;
			//}

			break;

		default:
		{

			//不要なメッセージの後始末
			return DefWindowProc(hWnd, message, wParam, lParam);
		
		}
	}

	return 0;
}


void CWindow::SetClientSize(HWND hWnd, int w, int h)
{

	//	作業用変数
	RECT rcWnd, rcCli;

	// ウィンドウのRECT取得
	GetWindowRect(hWnd, &rcWnd);

	// クライアント領域のRECT取得
	GetClientRect(hWnd, &rcCli);

	// ウィンドウの余白を考えて、クライアントのサイズを指定サイズにする。
	MoveWindow(hWnd,
		rcWnd.left,					// X座標
		rcWnd.top,					// Y座標
		w + (rcWnd.right - rcWnd.left) - (rcCli.right - rcCli.left),
		h + (rcWnd.bottom - rcWnd.top) - (rcCli.bottom - rcCli.top),
		TRUE);
}
