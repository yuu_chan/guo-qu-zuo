#include "CWindow.h"


//メイン
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInst, LPSTR lpszArgs, int nWinMode)
{
	// メモリリークを知らせる
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	if (APP.InitWindow(hInstance, CWindow::WINDOW_WIDTH_SIZE, CWindow::WINDOW_HEIGHT_SIZE, FALSE) == FALSE)
	{
		return 0;
	}

	//_CrtDumpMemoryLeaks();
	//_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	return APP.Loop();
}