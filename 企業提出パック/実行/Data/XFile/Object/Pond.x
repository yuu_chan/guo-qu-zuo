xof 0302txt 0064
template Header {
 <3D82AB43-62DA-11cf-AB39-0020AF71E433>
 WORD major;
 WORD minor;
 DWORD flags;
}

template Vector {
 <3D82AB5E-62DA-11cf-AB39-0020AF71E433>
 FLOAT x;
 FLOAT y;
 FLOAT z;
}

template Coords2d {
 <F6F23F44-7686-11cf-8F52-0040333594A3>
 FLOAT u;
 FLOAT v;
}

template Matrix4x4 {
 <F6F23F45-7686-11cf-8F52-0040333594A3>
 array FLOAT matrix[16];
}

template ColorRGBA {
 <35FF44E0-6C7C-11cf-8F52-0040333594A3>
 FLOAT red;
 FLOAT green;
 FLOAT blue;
 FLOAT alpha;
}

template ColorRGB {
 <D3E16E81-7835-11cf-8F52-0040333594A3>
 FLOAT red;
 FLOAT green;
 FLOAT blue;
}

template IndexedColor {
 <1630B820-7842-11cf-8F52-0040333594A3>
 DWORD index;
 ColorRGBA indexColor;
}

template Boolean {
 <4885AE61-78E8-11cf-8F52-0040333594A3>
 WORD truefalse;
}

template Boolean2d {
 <4885AE63-78E8-11cf-8F52-0040333594A3>
 Boolean u;
 Boolean v;
}

template MaterialWrap {
 <4885AE60-78E8-11cf-8F52-0040333594A3>
 Boolean u;
 Boolean v;
}

template TextureFilename {
 <A42790E1-7810-11cf-8F52-0040333594A3>
 STRING filename;
}

template Material {
 <3D82AB4D-62DA-11cf-AB39-0020AF71E433>
 ColorRGBA faceColor;
 FLOAT power;
 ColorRGB specularColor;
 ColorRGB emissiveColor;
 [...]
}

template MeshFace {
 <3D82AB5F-62DA-11cf-AB39-0020AF71E433>
 DWORD nFaceVertexIndices;
 array DWORD faceVertexIndices[nFaceVertexIndices];
}

template MeshFaceWraps {
 <4885AE62-78E8-11cf-8F52-0040333594A3>
 DWORD nFaceWrapValues;
 Boolean2d faceWrapValues;
}

template MeshTextureCoords {
 <F6F23F40-7686-11cf-8F52-0040333594A3>
 DWORD nTextureCoords;
 array Coords2d textureCoords[nTextureCoords];
}

template MeshMaterialList {
 <F6F23F42-7686-11cf-8F52-0040333594A3>
 DWORD nMaterials;
 DWORD nFaceIndexes;
 array DWORD faceIndexes[nFaceIndexes];
 [Material]
}

template MeshNormals {
 <F6F23F43-7686-11cf-8F52-0040333594A3>
 DWORD nNormals;
 array Vector normals[nNormals];
 DWORD nFaceNormals;
 array MeshFace faceNormals[nFaceNormals];
}

template MeshVertexColors {
 <1630B821-7842-11cf-8F52-0040333594A3>
 DWORD nVertexColors;
 array IndexedColor vertexColors[nVertexColors];
}

template Mesh {
 <3D82AB44-62DA-11cf-AB39-0020AF71E433>
 DWORD nVertices;
 array Vector vertices[nVertices];
 DWORD nFaces;
 array MeshFace faces[nFaces];
 [...]
}

Header{
1;
0;
1;
}

Mesh {
 13;
 17.60544;-18.37417;-27.69208;,
 11.68574;-18.37417;-16.21551;,
 11.71547;-18.37417;-4.00101;,
 35.00000;-18.37417;-35.00000;,
 17.60544;-18.37417;-35.00000;,
 35.00000;-18.37417;-4.00101;,
 17.60544;-18.37417;35.00000;,
 35.00000;-18.37417;35.00000;,
 17.60544;-18.37417;27.69208;,
 35.00000;-18.37417;27.69208;,
 35.00000;-18.37417;4.00101;,
 11.71547;-18.37417;4.00101;,
 11.68574;-18.37417;16.21551;;
 
 20;
 3;0,1,2;,
 3;0,3,4;,
 3;2,5,3;,
 4;2,5,3,0;,
 3;6,7,8;,
 3;7,9,8;,
 4;10,5,2,11;,
 3;8,9,12;,
 3;9,10,12;,
 3;10,11,12;,
 3;2,1,0;,
 3;4,3,0;,
 3;3,5,2;,
 4;0,3,5,2;,
 3;8,7,6;,
 3;8,9,7;,
 4;11,2,5,10;,
 3;12,9,8;,
 3;12,10,9;,
 3;12,11,10;;
 
 MeshMaterialList {
  1;
  20;
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0;;
  Material {
   0.810000;0.810000;0.810000;0.740000;;
   0.790000;
   0.880000;0.880000;0.880000;;
   0.200000;0.200000;0.200000;;
   TextureFilename {
    "Data\\Graphics\\�f��\\27953512_p11_master1200.jpg";
   }
  }
 }
 MeshNormals {
  2;
  0.000000;1.000000;0.000000;,
  0.000000;-1.000000;-0.000000;;
  20;
  3;0,0,0;,
  3;0,0,0;,
  3;0,0,0;,
  4;0,0,0,0;,
  3;0,0,0;,
  3;0,0,0;,
  4;0,0,0,0;,
  3;0,0,0;,
  3;0,0,0;,
  3;0,0,0;,
  3;1,1,1;,
  3;1,1,1;,
  3;1,1,1;,
  4;1,1,1,1;,
  3;1,1,1;,
  3;1,1,1;,
  4;1,1,1,1;,
  3;1,1,1;,
  3;1,1,1;,
  3;1,1,1;;
 }
 MeshTextureCoords {
  13;
  0.101181;3.020130;,
  -0.311452;1.953500;,
  -0.309380;0.818283;,
  1.313670;3.699330;,
  0.101181;3.699330;,
  1.313670;0.818283;,
  0.101181;-2.806470;,
  1.313670;-2.806470;,
  0.101181;-2.127270;,
  1.313670;-2.127270;,
  1.313670;0.074575;,
  -0.309380;0.074575;,
  -0.311452;-1.060640;;
 }
 MeshVertexColors {
  13;
  0;1.000000;1.000000;1.000000;1.000000;,
  1;1.000000;1.000000;1.000000;1.000000;,
  2;1.000000;1.000000;1.000000;1.000000;,
  3;1.000000;1.000000;1.000000;1.000000;,
  4;1.000000;1.000000;1.000000;1.000000;,
  5;1.000000;1.000000;1.000000;1.000000;,
  6;1.000000;1.000000;1.000000;1.000000;,
  7;1.000000;1.000000;1.000000;1.000000;,
  8;1.000000;1.000000;1.000000;1.000000;,
  9;1.000000;1.000000;1.000000;1.000000;,
  10;1.000000;1.000000;1.000000;1.000000;,
  11;1.000000;1.000000;1.000000;1.000000;,
  12;1.000000;1.000000;1.000000;1.000000;;
 }
}
