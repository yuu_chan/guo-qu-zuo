//==============================================================
// グローバル変数
//==============================================================


//--------------
//	通常2D描画用
//--------------

//	テクセルサイズ(1ピクセルのUV座標系でのサイズ)
float2 TexelSize;

//	入力テクスチャ
texture InputTex;


//------------
//	サンプラー
//------------
sampler InputSmp = sampler_state
{
	Texture = <InputTex>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	AddressU = Clamp;
	AddressV = Clamp;
};


//----------
//	3D描画用
//----------

//	ワールド行列
float4x4 mW;

//	ビュー行列
float4x4 mV;

//	射影行列
float4x4 mP;

//	視点の位置
float3 CamPos;



//========================================================
//	頂点シェーダーからピクセルシェーダに渡すデータ用構造体
//========================================================

struct VS_OUT
{
	float4 Pos		:	POSITION;	//	2D座標
	float2 MeshUV	:	TEXCORD0;	//	テクスチャUV

	float3 wPos		:	TEXCOORD1;	//	3D座標
	float3 wN		:	TEXCORD2;	//	法線
};


//======
//	頂点
//======

VS_OUT VC
	(
	float4 pos		: POSITION,		//	頂点座標
	float3 normal	: NORMAL,		//	法線
	float2 mesh_uv	: TEXCOORD0		//	テクスチャUV
	)
{
	//	ピクセルデータに渡すデータ用構造体
	VS_OUT Out = (VS_OUT)0;

	//	頂点座標
	Out.Pos = mul(pos, mW);

	//	3D空間での座標
	Out.wPos = Out.Pos.xyz;

	//	3Dカメラ座標系の座標
	Out.Pos = mul(Out.Pos, mW);

	//	2D空間での座標
	Out.Pos = mul(Out.Pos, mP);

	//	テクスチャ座標
	Out.MeshUV = mesh_uv;

	//	法線をワールドに変換する
	float3 w_normal = normalize(mul(normal, (float3x3)mW));



}


//==========
//	ピクセル
//==========
//　ぼかし
float4 XBokasiPS(float2 UV: TEXCOORD0) :COLOR0
{
	UV += TexelSize * 0.5f;
	//　周りの色の平均値
	float3 color = 0;
		color += tex2D(InputSmp, UV).rgb;


	//	21ピクセル
	/*for (int i = 1; i <= 10; i++){
		color += tex2D(InputSmp, UV + float2(-TexelSize.x*i, 0)).rgb;
		color += tex2D(InputSmp, UV + float2(TexelSize.x*i, 0)).rgb;
	}*/

	////	21ピクセル
	//color /= 21;


	//	11ピクセル
	for (int i = 1; i <= 5; i++){
		color += tex2D(InputSmp, UV + float2(-TexelSize.x*i, 0)).rgb;
		color += tex2D(InputSmp, UV + float2( TexelSize.x*i, 0)).rgb;
	}

	//	11ピクセル
	color *= 0.09090909;
	

	return float4(color, 1);
}


float4 YBokasiPS(float2 UV: TEXCOORD0) :COLOR0
{
	UV += TexelSize * 0.5f;
	//　周りの色の平均値
	float3 color = 0;
		color += tex2D(InputSmp, UV).rgb;
	for (int i = 1; i <= 5; i++){
		color += tex2D(InputSmp, UV + float2(0, -TexelSize.y*i)).rgb;
		color += tex2D(InputSmp, UV + float2(0,  TexelSize.y*i)).rgb;
	}

	color *= 0.09090909;



	//	21ピクセル
	/*for (int i = 1; i <= 10; i++){
	color += tex2D(InputSmp, UV + float2(-TexelSize.x*i, 0)).rgb;
	color += tex2D(InputSmp, UV + float2(TexelSize.x*i, 0)).rgb;
	}*/

	////	21ピクセル
	//color /= 21;

	return float4(color, 1);
}


//	通常描画PS
float4 NormalPS(float2 UV:TEXCOORD0) : COLOR0
{
	UV += TexelSize * 0.5;
	return tex2D(InputSmp, UV);
}



technique NormalTech
{
	pass P0
	{
		PixelShader = compile ps_3_0 NormalPS();
		ZEnable = false;				//	2D描画をする場合はこいつを消しておく Z判定OFF
		ZWriteEnable = false;				//	2D描画をする場合はこいつも消しておく Z書き込みOFF
	}
}

technique XBokasiTech
{
	pass P0
	{
		PixelShader = compile ps_3_0 XBokasiPS();
		ZEnable = false;				//	2D描画をする場合はこいつを消しておく Z判定OFF
		ZWriteEnable = false;				//	2D描画をする場合はこいつも消しておく Z書き込みOFF
	}
}
technique YBokasiTech
{
	pass P0
	{
		PixelShader = compile ps_3_0 YBokasiPS();
		ZEnable = false;				//	2D描画をする場合はこいつを消しておく Z判定OFF
		ZWriteEnable = false;				//	2D描画をする場合はこいつも消しておく Z書き込みOFF
	}
}

//
////------------------------
////	アルファ値を消す場合
////------------------------
//
////	グローバル
//float2 TexelSize;	//	テクセルサイズ(1ピクセルのUV座標系でのサイズ)
//texture InputTex;	//	入力テクスチャ
//
//sampler InputSmp = sampler_state
//{
//	Texture = <InputTex>;
//	MinFilter = LINEAR;
//	MagFilter = LINEAR;
//	AddressU = Clamp;
//	AddressV = Clamp;
//};
//
////　ぼかし
//float4 XBokasiPS(float2 UV: TEXCOORD0) :COLOR0
//{
//	UV += TexelSize * 0.5f;
//	//　周りの色の平均値
//	float4 color = 0;
//		color += tex2D(InputSmp, UV);
//
//
//	//	21ピクセル
//	/*for (int i = 1; i <= 10; i++){
//	color += tex2D(InputSmp, UV + float2(-TexelSize.x*i, 0)).rgb;
//	color += tex2D(InputSmp, UV + float2(TexelSize.x*i, 0)).rgb;
//	}*/
//
//	////	21ピクセル
//	//color /= 21;
//
//
//	//	11ピクセル
//	for (int i = 1; i <= 5; i++){
//		color += tex2D(InputSmp, UV + float2(-TexelSize.x*i, 0));
//		color += tex2D(InputSmp, UV + float2(TexelSize.x*i, 0));
//	}
//
//	//	11ピクセル
//	color *= 0.09090909;
//
//
//	return color;
//}
//
//
//float4 YBokasiPS(float2 UV: TEXCOORD0) :COLOR0
//{
//	UV += TexelSize * 0.5f;
//	//　周りの色の平均値
//	float4 color = 0;
//		color += tex2D(InputSmp, UV);
//	for (int i = 1; i <= 5; i++){
//		color += tex2D(InputSmp, UV + float2(0, -TexelSize.y*i));
//		color += tex2D(InputSmp, UV + float2(0, TexelSize.y*i));
//	}
//
//	color *= 0.09090909;
//
//
//
//	//	21ピクセル
//	/*for (int i = 1; i <= 10; i++){
//	color += tex2D(InputSmp, UV + float2(-TexelSize.x*i, 0)).rgb;
//	color += tex2D(InputSmp, UV + float2(TexelSize.x*i, 0)).rgb;
//	}*/
//
//	////	21ピクセル
//	//color /= 21;
//
//	return color;
//}
//
//
////	通常描画PS
//float4 NormalPS(float2 UV:TEXCOORD0) : COLOR0
//{
//	UV += TexelSize * 0.5;
//	return tex2D(InputSmp, UV);
//}
//
//
//
//technique NormalTech
//{
//	pass P0
//	{
//		PixelShader = compile ps_3_0 NormalPS();
//		ZEnable = false;				//	2D描画をする場合はこいつを消しておく Z判定OFF
//		ZWriteEnable = false;				//	2D描画をする場合はこいつも消しておく Z書き込みOFF
//	}
//}
//
//technique XBokasiTech
//{
//	pass P0
//	{
//		PixelShader = compile ps_3_0 XBokasiPS();
//		ZEnable = false;				//	2D描画をする場合はこいつを消しておく Z判定OFF
//		ZWriteEnable = false;				//	2D描画をする場合はこいつも消しておく Z書き込みOFF
//	}
//}
//technique YBokasiTech
//{
//	pass P0
//	{
//		PixelShader = compile ps_3_0 YBokasiPS();
//		ZEnable = false;				//	2D描画をする場合はこいつを消しておく Z判定OFF
//		ZWriteEnable = false;				//	2D描画をする場合はこいつも消しておく Z書き込みOFF
//	}
//}
