//==============================================================
// グローバル変数
//==============================================================

//	テクスチャサイズ
#define TEX_SIZE 256

//	ワールド行列
float4x4 mW;

//	ビュー行列
float4x4 mV;

//	射影行列
float4x4 mP;

//	視点の位置
float3 CamPos;

float4x4 World;
float4x4 WView;
float4x4 WVProj;

//	UVスクロール用
float3 ScrollUV;

//	テクセルサイズ(1ピクセルのUV座標系でのサイズ)
float2 TexelSize;

//	ライトのカラー
float3 LightColor;

//	ライトのワールドビュー座標
float3 LightWVPos;

//	ライトの方向
float3 LightDir;

float Power = 50;

//	環境光の強さ
float4 Ambient;

//	波の反射光の強さ
float WavePower;

//	バネの強さ
float Spring;

//	追加したい波の座標
float4 AddWavePos;

//	波の基本色
float4 WaveDiffuse;

//	波の反射光
float3 WaveSpecular;

//	追加する波の半径
float AddWaveRad;

//	追加する波の速度
float AddWaveVelocity;

// 反射色
float4	Specular = float4(1, 1, 1, 1);

//	自己発行色
float4 Emissive;

//	自己発行色の強さ
float  EmissivePow = 1.0f;


//------------
//	サンプラー
//------------

//	入力テクスチャ
texture InputTex;
sampler InputSmp = sampler_state
{
	Texture = <InputTex>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	AddressU = Clamp;
	AddressV = Clamp;
};

//	ハイトマップ
texture HeightTex;
sampler HeightSmp = sampler_state
{
	Texture = <HeightTex>;
	MipFilter = POINT;
	MinFilter = POINT;
	MagFilter = POINT;
};

//	法線マップ
texture NormalTex;
sampler NormalSmp = sampler_state
{
	Texture = <NormalTex>;
	// 浮動小数テクスチャの線形補間ができない環境への対応
	MipFilter = POINT;
	MinFilter = POINT;
	MagFilter = POINT;
};


//========================================================
//	頂点シェーダーからピクセルシェーダに渡すデータ用構造体
//========================================================

struct VS_OUT
{
	float4 Pos		: POSITION;

	float2 NormalUV	: TEXCOORD0;

	float3 WVPos	: TEXCOORD1;
	float3 wPos		: TEXCOORD2;
	float3 wN		: TEXCOORD3;
	float3 wViewPos : TEXCOORD4;
	float3 wBin		: TEXCOORD5;
	float3 wTan		: TEXCOORD6;

	// 2D座標
	float4 Pos2D	: TEXCOORD7;

	//	ビュー座標系で変換されたライトの方向
	float3 LightDir : TEXCOORD8;
};


VS_OUT MyWaveVS(
	float4	pos			: POSITION,
	float3	vNormal : NORAML,
	float3	binormal : BINORMAL,
	float3	tangent : TANGENT,
	float2	NormalUV : TEXCOORD0
	)
{
	VS_OUT Out = (VS_OUT)0;

	//	頂点座標とテクスチャ座標
	Out.Pos = mul(pos, WVProj);

	Out.NormalUV = NormalUV;

	Out.NormalUV.x = NormalUV.x + ScrollUV.x;
	Out.NormalUV.y = NormalUV.y + ScrollUV.y;

	Out.WVPos = mul(pos, WView);

	Out.wBin = normalize(mul(binormal, (float3x3)World));
	Out.wTan = normalize(mul(tangent, (float3x3)World));

	//	2D座標をTEXCOORDのセマンティクスに渡す
	Out.Pos2D = Out.Pos;

	Out.LightDir = mul(LightDir, mV);


	return Out;
}


struct PS_OUT
{
	float4 Color : COLOR0;

	float4 Bloom : COLOR1;
};


PS_OUT MyWavePS(VS_OUT In)
//float4 MyWavePS(VS_OUT In) : COLOR0
{
	/*------------------------------------------>

	ここまでが波を生成

	<------------------------------------------*/
	// 浮動小数テクスチャの線形補間ができない環境への対応
	float4
		t0 = tex2D(NormalSmp, In.NormalUV),
		t1 = tex2D(NormalSmp, In.NormalUV + float2(1.0f / TEX_SIZE, 0)),
		t2 = tex2D(NormalSmp, In.NormalUV + float2(0, 1.0f / TEX_SIZE)),
		t3 = tex2D(NormalSmp, In.NormalUV + float2(1.0f / TEX_SIZE, 1.0f / TEX_SIZE));

	float2 f = frac(In.NormalUV * TEX_SIZE);
		float4 t = lerp(lerp(t0, t1, f.x), lerp(t2, t3, f.x), f.y);


		/*--------------------------------------->

		ここからライトの処理

		<---------------------------------------*/

		float3 normal = normalize(cross(float3(0, t.g, 1), float3(1, t.r, 0)));

		//	ビュー座標系のライトの方向を正規化
		In.LightDir = normalize(In.LightDir);

	//	逆行列を生成
	float3x3 mTBN;

	mTBN[0] = normalize(In.wTan);
	mTBN[1] = normalize(In.wBin);
	mTBN[2] = normalize(In.wN);

	float3x3 mInvTBN = transpose(mTBN);

		float3 ViewCamPos = mul(float4(In.WVPos, 1), mTBN);

		//	カメラの向き - 頂点
		float3 lp = normalize(ViewCamPos - In.WVPos);

		//	法線
		float3 wv_normal = normalize(mul(normal, (float3x3)WView));


		float3 vRef = In.LightDir + 2.0f * dot(wv_normal, -In.LightDir) * wv_normal;

		float hl = dot(lp, normalize(reflect(In.WVPos, wv_normal)));

	float SpePow = pow(max(0, dot(lp, vRef)), Power);

	float4 color;
	color.rgb =
		Ambient +
		max(0, dot(wv_normal, lp)) * WaveDiffuse;

	//	最終的に算出されるライトの力
	float lPow = max(0, dot(wv_normal, lp));

	//	カラーを一度保存
	color = float4(color.rgb * LightColor + SpePow, WaveDiffuse.a);


	/*--------------------------------------->

	ここから鏡面反射の処理

	<---------------------------------------*/

	//	ビュー座標系に変換されたカメラの位置
	float3 vCam = mul(normalize(CamPos - In.wPos), mV);

	float3 w_nomal = normalize(In.wN);

	//================
	//	色を出力させる
	//================

	PS_OUT Out;
	Out.Color = color;

	//	スペキュラーを足して * スペキュラーを抑えたい値
	Out.Color.rgb += (pow(hl, WavePower) * 0.6f);

	//	ライトブルームを入れる
	float4 Spe = Specular * SpePow;

		Out.Bloom = Spe + (Emissive * EmissivePow) * tex2D(InputSmp, In.NormalUV);

	Out.Bloom.a = 0.5f;


	//--------------------
	//	全体のフォグを出す
	//--------------------
	float2 FogRange = float2(50, 200);
		float FogPow = saturate((FogRange.y - In.WVPos.z) / (FogRange.y - FogRange.x));

	Out.Color.rgb = lerp(float3(0.7, 0.8, 1), Out.Color.rgb, FogPow);

	return Out;
}


//======
//	頂点
//======

VS_OUT VC
	(
	float4 pos		: POSITION,		//	頂点座標
	float3 normal	: NORMAL,		//	法線
	float2 mesh_uv	: TEXCOORD0		//	テクスチャUV
	)
{
	//	ピクセルデータに渡すデータ用構造体
	VS_OUT Out = (VS_OUT)0;

	//	頂点座標
	Out.Pos = mul(pos, mW);

	//	3D空間での座標
	Out.wPos = Out.Pos.xyz;

	//	3Dカメラ座標系の座標
	Out.Pos = mul(Out.Pos, mW);

	//	2D空間での座標
	Out.Pos = mul(Out.Pos, mP);

	//	テクスチャ座標
	Out.NormalUV = mesh_uv;

	//	法線をワールドに変換する
	float3 w_normal = normalize(mul(normal, (float3x3)mW));



}


//==========
//	ピクセル
//==========
//　ぼかし
float4 XBokasiPS(float2 UV: TEXCOORD0) :COLOR0
{
	UV += TexelSize * 0.5f;
	//　周りの色の平均値
	float3 color = 0;
		color += tex2D(InputSmp, UV).rgb;

	
	//	11ピクセル
	for (int i = 1; i <= 5; i++){
		color += tex2D(InputSmp, UV + float2(-TexelSize.x*i, 0)).rgb;
		color += tex2D(InputSmp, UV + float2( TexelSize.x*i, 0)).rgb;
	}

	//	11ピクセル
	color *= 0.09090909;
	

	return float4(color, 1);
}


float4 YBokasiPS(float2 UV: TEXCOORD0) :COLOR0
{
	UV += TexelSize * 0.5f;
	//　周りの色の平均値
	float3 color = 0;
		color += tex2D(InputSmp, UV).rgb;
	for (int i = 1; i <= 5; i++){
		color += tex2D(InputSmp, UV + float2(0, -TexelSize.y*i)).rgb;
		color += tex2D(InputSmp, UV + float2(0,  TexelSize.y*i)).rgb;
	}

	color *= 0.09090909;


	return float4(color, 1);
}


//==============================================================
// シェーダ（法線マップ）
float4 NormalPS(
float2 HeightUV : TEXCOORD0
) : COLOR
{
	// 中心点に隣接する4点をサンプリング
	float4
	t1 = tex2D(HeightSmp, HeightUV + float2(-1.0f / TEX_SIZE, 0)),
	t2 = tex2D(HeightSmp, HeightUV + float2(1.0f / TEX_SIZE, 0)),
	t3 = tex2D(HeightSmp, HeightUV + float2(0, -1.0f / TEX_SIZE)),
	t4 = tex2D(HeightSmp, HeightUV + float2(0, 1.0f / TEX_SIZE));

	// 傾きを計算して出力
	return float4((t2.r - t1.r) * 0.5f, (t4.r - t3.r) * 0.5f, 0, 0);
}


//==============================================================
// シェーダ（高度マップ）
float4 HeightPS(
float2 HeightUV : TEXCOORD0
) : COLOR
{

	// 中心点と隣接する4点をサンプリング
	float4
	t0 = tex2D(HeightSmp, HeightUV),
	t1 = tex2D(HeightSmp, HeightUV + float2(-1.0f / TEX_SIZE, 0)),
	t2 = tex2D(HeightSmp, HeightUV + float2(1.0f / TEX_SIZE, 0)),
	t3 = tex2D(HeightSmp, HeightUV + float2(0, -1.0f / TEX_SIZE)),
	t4 = tex2D(HeightSmp, HeightUV + float2(0, 1.0f / TEX_SIZE));

	// 速度の計算
	float velocity = t0.g + Spring * (t1.r + t2.r + t3.r + t4.r - t0.r * 4);

	// 位置の計算
	float height = t0.r + velocity;

	// 波の追加
	if (distance(AddWavePos, HeightUV) <= AddWaveRad) {
		velocity += AddWaveVelocity;
	}

	// 新しい位置と速度の出力
	return float4(height, velocity, 0, 0);
}


////	通常描画PS
//float4 NormalPS(float2 UV:TEXCOORD0) : COLOR0
//{
//	UV += TexelSize * 0.5;
//	return tex2D(InputSmp, UV);
//}

technique Wave
{
	pass P0 {
		VertexShader = compile vs_3_0 MyWaveVS();
		PixelShader = compile ps_3_0 MyWavePS();
	}
}

technique Height 
{
	pass P0 {
		PixelShader = compile ps_2_0 HeightPS();
		ZEnable = FALSE;
		ZWriteEnable = FALSE;
		AlphaBlendEnable = FALSE;
	}
}

technique Normal 
{
	pass P0 {
		PixelShader = compile ps_2_0 NormalPS();
		ZEnable = FALSE;
		ZWriteEnable = FALSE;
		AlphaBlendEnable = FALSE;
	}
}
